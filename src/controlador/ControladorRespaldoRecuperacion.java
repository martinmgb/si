package controlador;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.swing.JFileChooser;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class ControladorRespaldoRecuperacion extends SelectorComposer<Component> {
	private static final long serialVersionUID = -986552569263113746L;

	@Wire
	private Button btnRespaldar;
	@Wire
	private Button btnSalir;
	@Wire
	private Textbox txtRuta;
	@Wire
	private Radio rbLocal;
	@Wire
	private Radio rbDispositivo;
	@Wire
	private Radio rbWindows;
	@Wire
	private Radio rbLinux;
	@Wire
	private Window wdwRespaldoInformacion;
	@Wire
	private Groupbox gbSistema;
	@Wire
	private Groupbox gbDispositivo;
	@Wire
	private Button btnRestaurar;

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy" + "_"+ "hhmmss");

	//ESTA ES LA RUTA QUE NO SABEMOS DONDE ESTA
	private static final String rutaPostgres = "/opt/postgreSQL/9.4";
	private static final String clavePostgres = "postgres";
	private static final String nombreBD = "pruebaBD";
	private static final String nombreFileRespaldo = "Teide";

	private String ruta;
	private String extension;

	/**
	 * Metodo que permite seleccionar en que sistema operativo se realizara el
	 * respaldo de la informacion
	 */
	@Listen("onClick = #btnRespaldar")
	public void respaldarInformacion() {
		if (rbLinux.isChecked()) {
			respaldarBD();
		} else if (rbWindows.isChecked()) {
			ejecutarComandos();
		}
	}

	/**
	 * Metodo que permite cerrar la pantalla actualizando los cambios realizados
	 */
	@Listen("onClick = #btnSalir")
	public void salir() {
		wdwRespaldoInformacion.onClose();
	}

	@Listen("onClick = #btnRestaurar")
	public void restaurar() {
		if (rbLinux.isChecked()) {
			restaurarBD("pg_restore");
		} else if (rbWindows.isChecked()) {
			restaurarBD(rutaPostgres+"\\bin\\pg_restore.exe");
		}
	}

	/***
	 * Metodo que respalda la informacion contenida en el Sistema
	 * 
	 */
	private void respaldarBD() {
		if (rbLocal.isChecked()) {
			if (txtRuta.getText().equals("")) {
				Messagebox.show("Debe indicar la ruta", "Error", Messagebox.OK, Messagebox.ERROR);
			} else {
				ruta = this.txtRuta.getText()+"/";
				Calendar cal = Calendar.getInstance(); // hoy
				String valor = sdf.format(cal.getTime());
				String nombre = nombreFileRespaldo + valor + ".backup";
				try {
					Runtime.getRuntime().exec("pg_dump -i -h localhost -p 5432 -U postgres -F c -v -f "+ ruta + nombre + " "+nombreBD);
					Messagebox.show("Respaldo realizado exitosamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
		//			cancelar();
					txtRuta.setValue("");
				} catch (IOException e) {
					Messagebox.show("No se pudo realizar el respaldo", "Error", Messagebox.OK, Messagebox.ERROR);
			//		cancelar();
					txtRuta.setValue("");
				}
			}
		}
	}

	/**
	 * Metodo que permite seleccionar la ruta del archivo
	 */
	@Listen("onClick = #btnSleccionar")
	public void seleccionar() {
		if (rbLocal.isChecked()) {
			seleccionarRuta();
		} else if (rbDispositivo.isChecked()) {
			seleccionarArchivo();
		}
	}

	/***
	 * Metodo que selecciona la ruta para la carpeta
	 */
	public void seleccionarRuta() {
		JFileChooser chooser = new JFileChooser();
		// Titulo que llevara la ventana
		chooser.setDialogTitle("Seleccione...");

		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		chooser.showOpenDialog(null);

		// Retornando el directorio destino directorio
		if (chooser.getSelectedFile() != null) {
			txtRuta.setValue(chooser.getSelectedFile().getPath());
		}
	}

	/***
	 * Metodo que permite respaldar la base de datos en windows
	 */
	public void ejecutarComandos() {
		if (txtRuta.getText().equals("")) {
			Messagebox.show("Debe indicar la ruta", "Error", Messagebox.OK,
					Messagebox.ERROR);
		} else {
			try {
				ruta = this.txtRuta.getText()+"\\";
				Calendar cal = Calendar.getInstance(); // hoy
				String valor = sdf.format(cal.getTime());
				String nombre = nombreFileRespaldo + valor + ".backup";

				List<String> lista = new ArrayList<String>();
				lista.add(rutaPostgres+"\\bin\\pg_dump.exe");
				lista.add("-i");
				lista.add("-h");
				lista.add("localhost");
				lista.add("-p");
				lista.add("5432");
				lista.add("-U");
				lista.add("postgres");
				lista.add("-F");
				lista.add("c");
				lista.add("-b");
				lista.add("-v");
				lista.add("-f");
				lista.add(ruta + nombre);
				lista.add(nombreBD);

				ProcessBuilder pb = new ProcessBuilder(lista);
				pb.environment().put("PGPASSWORD", (clavePostgres));
				pb.redirectErrorStream(true);
				pb.start();
				Messagebox.show("Respaldo en proceso", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
				//cancelar();
				txtRuta.setValue("");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Messagebox.show("No se pudo realizar el respaldo", "Error", Messagebox.OK, Messagebox.ERROR);
				//cancelar();
				txtRuta.setValue("");
			}
		}
	}

	/**
	 * Metodo que permite reiniciar los campos de la vista a su estado original
	 */
/*
	public void cancelar() {
		rbDispositivo.setChecked(false);
		rbLocal.setChecked(false);
		rbLinux.setChecked(false);
		rbWindows.setChecked(false);
		txtRuta.setValue("");
		gbDispositivo.setVisible(false);
		gbSistema.setVisible(false);
		btnRespaldar.setVisible(false);
		btnRestaurar.setVisible(false);
	}
*/

	/**
	 * Metodo que permite limpiar los campos de la vista, asi como tambien la
	 * variable global id
	 */
	@Listen("onClick = #btnCancelar")
	public void cancelarRespaldo() {		
	//	cancelar();
	}

	/***
	 * Metodo que permite seleccionar el archivo a restaurar
	 */
	public void seleccionarArchivo() {
		JFileChooser chooser = new JFileChooser();
		// Titulo que llevara la ventana
		chooser.setDialogTitle("Seleccione...");

		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.showOpenDialog(null);

		// Si seleccionamos algunn archivo retornaremos su ubicacion
		if (chooser.getSelectedFile() != null) {
			extension = chooser.getSelectedFile().getName();
			txtRuta.setValue(chooser.getSelectedFile().getPath());
		}
	}

	/***
	 * Metodo que permite restaurar la base de datos
	 */
	public void restaurarBD(String rutapg) {
		if ((txtRuta.getValue().equals(""))) {
			Messagebox.show("Debe seleccionar un archivo", "Error", Messagebox.OK, Messagebox.ERROR);

		} else if (!extension.contains("backup")) {
			Messagebox.show("Debe seleccionar un archivo de tipo .backup",
					"Error", Messagebox.OK, Messagebox.ERROR);
		} else {
			try {
				borrarEsquema("public");
				crearEsquema("public");
				String archivoBD = txtRuta.getValue();

				List<String> lista = new ArrayList<String>();
				lista.add(rutapg);
				lista.add("-i");
				lista.add("-h");
				lista.add("localhost");
				lista.add("-p");
				lista.add("5432");
				lista.add("-U");
				lista.add("postgres");
				lista.add("-d");
				lista.add(nombreBD);
				lista.add("-v");
				lista.add(archivoBD);

				ProcessBuilder pb = new ProcessBuilder(lista);
				pb.environment().put("PGPASSWORD", (clavePostgres));
				pb.redirectErrorStream(true);
				pb.start();
				Messagebox.show("Restauracion en proceso", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
		//		cancelar();
			} catch (Exception e) {
				Messagebox.show("No se pudo realizar la restauracion", "Error", Messagebox.OK, Messagebox.ERROR);
			//	cancelar();
			}
		}
	}

	/***
	 * Metodo que permite borrar los esquemas de la base de datos
	 */
	public void borrarEsquema(String nombreEsquema) {
		String driver = "org.postgresql.Driver";
		String connectString = "jdbc:postgresql://localhost:5432/"+nombreBD;
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(connectString, "postgres", clavePostgres);
			Statement stmt = con.createStatement();

			// Borrando el esquema

			stmt.executeUpdate("DROP SCHEMA " + nombreEsquema + " CASCADE");
			stmt.close();
			con.close();
		} catch (java.lang.ClassNotFoundException e) {
			System.err.println("ClassNotFoundException: " + e.getMessage());
		} catch (SQLException e) {
			System.err.println("SQLException: " + e.getMessage());
		}
	}

	/***
	 * Metodo que permite crear los esquemas de la base de datos
	 */
	public void crearEsquema(String nombreEsquema) {
		String driver = "org.postgresql.Driver";
		String connectString = "jdbc:postgresql://localhost:5432/"+nombreBD;
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(connectString, "postgres", clavePostgres);
			Statement stmt = con.createStatement();

			// Borrando el esquema

			stmt.executeUpdate("CREATE SCHEMA " + nombreEsquema);
			stmt.close();
			con.close();
		} catch (java.lang.ClassNotFoundException e) {
			System.err.println("ClassNotFoundException: " + e.getMessage());
		} catch (SQLException e) {
			System.err.println("SQLException: " + e.getMessage());
		}
	}
}
