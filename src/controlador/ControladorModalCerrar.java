package controlador;

import javax.swing.JOptionPane;


import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

public class ControladorModalCerrar extends SelectorComposer<Component> {
	private static final long serialVersionUID = 1L;

	@Wire
	Window modalDialog;
	@Wire
	Window modalDialog2;
	@Wire
	Window modalDialog3;
	@Wire
	Window modalDialog4;
	@Wire
	Window modalDialogRm;
	@Wire
	Window modalDialogams;
	@Wire
	Window closeBtn3;
	@Wire
	Window closeBtn4;
	@Wire
	Window ModalMotivo;
	
	@Listen("onClick = #closeBtn")
	public void showModal(Event e) {
		modalDialog.detach();
	}
	@Listen("onClick = #closeBtnMotivo")
	public void showModalModalMotivo(Event e) {
		ModalMotivo.detach();
	}
	
	@Listen("onClick = #closeBtnRegistrar")
	public void showModalRegistrarEvento(Event e) {
		modalDialog.detach();
	}
	@Listen("onClick = #closeBtnRm")
	public void showModalRm(Event e) {
		modalDialogRm.detach();
	}
	
	@Listen("onClick = #closeBtn2")
	public void showModal2(Event e) {
		modalDialog2.detach();
	}
	
	@Listen("onClick = #closeBtn3")
	public void showModal3(Event e) {
		closeBtn3.detach();
		modalDialog3.detach();
	}
	
	@Listen("onClick = #closeBtn4")
	public void showModal4(Event e) {
		closeBtn4.detach();
		modalDialog4.detach();
	}
	
	@Listen("onClick = #closeBtnArr")
	public void showModalArr(Event e) {
		modalDialog2.detach();
	}

	@Listen("onClick = #salirBtn")
	public void showModalSalir(Event e) {
		modalDialog.detach();
	}@Listen("onClick = #salirBtnRm")
	public void showModalSalirRm(Event e) {
		modalDialogRm.detach();
	}
	
	@Listen("onClick = #salirBtn2")
	public void showModalSalir2(Event e) {
		modalDialog2.detach();
	}
	
	@Listen("onClick = #salirBtn3")
	public void showModalSalir3(Event e) {
		modalDialog3.detach();
	}
	
	@Listen("onClick = #salirBtn4")
	public void showModalSalir4(Event e) {
		modalDialog4.detach();
	}

	@Listen("onClick = #closeBtneventos")
	public void showModaleventos(Event e) {
		modalDialog.detach();
	}

	@Listen("onClick = #closeBtn1")
	public void showModaleventos1(Event e) {
		modalDialog.detach();
	}
	@Listen("onClick = #closeUnidad")
	public void showModalUnidad(Event e) {
		modalDialog.detach();
	}
	@Listen("onClick = #closeArea")
	public void showModalArea(Event e) {
		modalDialog.detach();
	}
	@Listen("onClick = #closeAcceso")
	public void showModalAcceso(Event e) {
		modalDialog.detach();
	}
	@Listen("onClick = #closeBtnApelar")
	public void showModalApelar(Event e) {
		modalDialog.detach();
	}
	@Listen("onClick = #closeBtninsolventes")
	public void showModalinsolventes(Event e) {
		modalDialog.detach();
	}
	@Listen("onClick = #closeBtnams")
	public void showModalams(Event e) {
		modalDialogams.detach();
	}
	@Listen("onClick = #salirBtnams")
	public void showModalSalirams(Event e) {
		modalDialogams.detach();
	}
	
//	
//	@Listen("onClick = #rechazarArr")
//	public void showModalRechazo(Event e) {
//		Window window = (Window)Executions.createComponents(
//				"content/modalRechazo.zul", null,null);
//		window.doModal();
//	}
//	
//	@Listen("onClick = #aprobarArr")
//	public void showAprobarArr(Event e) {
//		JOptionPane.showMessageDialog(null, "Se Aprobo Con Exito");
//	}

	//	@Listen("onClick = #closeBtnEncuesta")
	//	public void showModalinsolventes(Event e) {
	//		modalDialog.detach();
	//	}

}