package controlador;

import modelo.entidad.Area;
import modelo.entidad.Cargo;
import modelo.entidad.Categoria;
import modelo.entidad.Comision;
import modelo.entidad.Evento;
import modelo.entidad.Evento_comision;
import modelo.entidad.Evento_indicador;
import modelo.entidad.Galeria;
import modelo.entidad.Grupo;
import modelo.entidad.Indicador;
import modelo.entidad.Motivo;
import modelo.entidad.MotivoDesvinculacion;
import modelo.entidad.Noticia;
import modelo.entidad.Parentesco;
import modelo.entidad.Persona;
import modelo.entidad.Preferencia;
import modelo.entidad.Recurso;
import modelo.entidad.TipoArea;
import modelo.entidad.TipoArrendamiento;
import modelo.entidad.TipoEvento;
import modelo.entidad.TipoIncidencia;
import modelo.entidad.TipoIndicador;
import modelo.entidad.TipoNoticia;
import modelo.entidad.TipoPersona;
import modelo.entidad.TipoPublico;
import modelo.entidad.TipoRechazo;
import modelo.entidad.TipoSancion;
import modelo.entidad.UnidadMedida;
import modelo.entidad.Usuario;
import modelo.entidad.Visita;
import modelo.entidad.Actividad;

import java.awt.event.InputEvent;
import java.util.HashMap;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;

import bsh.This;

public class ControladorModalAbrir extends SelectorComposer<Component> {
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView,isVer,estado  = false;

	//Nuevas

	@Listen("onClick = #recuperarContrasenna")
	public void showModalRecuperacion(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRecuperarContrasenna.zul", null, null);
		window.doModal();
	}

	@Listen("onClick = #registrarGrupo")
	public void showModalGrupo(Event e) {
		//Grupo grup = new Grupo();
		HashMap grupo = new HashMap();
		//grupo.put("objGrupo");
		this.isEdit = false;
		this.isCreate = true;
		grupo.put("edit", this.isEdit);
		grupo.put("create", this.isCreate);
		grupo.put("view", this.isView);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalGrupo.zul", null, grupo);
		window.doModal();
	}

	@Listen("onClick = #registrarEmpleado")
	public void showModalEmpleado(Event e) {
		Persona persona = new Persona();
		HashMap personaMap = new HashMap();
		personaMap .put("objeto",persona);
		this.isEdit = false;
		this.isCreate = true;
		this.isVer = false;
		personaMap.put("edit", this.isEdit);
		personaMap.put("create", this.isCreate);
		personaMap.put("ver", this.isVer);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRegistrarEmpleado.zul", null,personaMap);
		window.doModal();
	}

	@Listen("onClick = #registrarElDocumento")
	public void showModalRegistrarDocumento(Event e) {
		//		Grupo grup = new Grupo();
		//		HashMap grupo = new HashMap();
		//		grupo.put("objGrupo", grup);
		//		this.isEdit = false;
		//		this.isCreate = true;
		//		grupo.put("edit", this.isEdit);
		//		grupo.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRegistrarDocumento.zul", null, null);
		window.doModal();
	}

	@Listen("onClick = #registrarPreferencia")
	public void showModalPreferencia(Event e) {
		Preferencia pref = new Preferencia();
		HashMap prefe = new HashMap();
		prefe.put("objetopref", pref);
		this.isEdit = false;
		this.isCreate = true;
		prefe.put("edit", this.isEdit);
		prefe.put("create", this.isCreate);
		//		create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRegistrarPreferencia.zul", null, prefe);
		window.doModal();
	}

	@Listen("onClick = #registrarUsuario")
	public void showModalUsuario(Event e) {
		Usuario usuario = new Usuario();
		HashMap user = new HashMap();
		user.put("userObj", usuario);
		this.isEdit = false;
		this.isCreate = true;
		user.put("edit", this.isEdit);
		user.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalCrearUsuario.zul", null, user);
		window.doModal();
	}

	@Listen("onClick = #registrarTipoArea")
	public void showModalTipoArea(Event e) {
		TipoArea tipoA = new TipoArea();
		HashMap tipoArea = new HashMap();
		tipoArea.put("objTipoArea", tipoA);
		this.isEdit = false;
		this.isCreate = true;
		tipoArea.put("edit", this.isEdit);
		tipoArea.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalTipoArea.zul", null, tipoArea);
		window.doModal();
	}







	@Listen("onClick = #RegistrarNoticia")
	public void showModalRegistrarNoticia(Event e) {

		Noticia noticia1 = new Noticia();
		HashMap noticiaMap = new HashMap();
		noticiaMap .put("objeto",noticia1);
		this.isEdit = false;
		this.isCreate = true;
		this.isVer = false;
		this.estado=false;
		noticiaMap.put("edit", this.isEdit);
		noticiaMap.put("create", this.isCreate);
		noticiaMap.put("ver", this.isVer);
		noticiaMap.put("estado", this.estado);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRegistrarNoticia.zul", null,noticiaMap);
		window.doModal();
	}

	/*
			@Listen("onClick = #agregarImagen")
			public void showModalAgregarImagen(Event e) {
				Imagen img = new Imagen();
				HashMap a = new HashMap();
				a.put("objeto", img);
				this.isEdit = false;
				this.isCreate = true;
				a.put("edit", this.isEdit);
				a.put("create", this.isCreate);
				//create a window programmatically and use it as a modal dialog.
				Window window = (Window)Executions.createComponents(
						"content/modalImagenGaleria.zul", null, a);
				window.doModal();
			}*/

	@Listen("onClick = #registrarActividad")
	public void showModalActividad(Event e) {
		Actividad act = new Actividad();
		HashMap actividad = new HashMap();
		actividad.put("objActividad", act);
		this.isEdit = false;
		this.isCreate = true;
		actividad.put("edit", this.isEdit);
		actividad.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalActividad.zul", null, actividad);
		window.doModal();
	}

	@Listen("onClick = #registrarBeneM")
	public void showModalRegistarBeneM(Event e) {
		Persona pe = new Persona();
		HashMap a = new HashMap();
		a.put("persona", pe);
		this.isEdit = false;
		this.isCreate = true;
		a.put("edit", this.isEdit);
		a.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/Beneficiario2.zul", null, a);
		window.doModal();
	}

	@Listen("onClick = #registrarTipoIncidencia")
	public void showModalTipoIncidencia(Event e) {
		TipoIncidencia tipoI = new TipoIncidencia();
		HashMap tipoIncidencia = new HashMap();
		tipoIncidencia.put("objTipoIncidencia", tipoI);
		this.isEdit = false;
		this.isCreate = true;
		tipoIncidencia.put("edit", this.isEdit);
		tipoIncidencia.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalTipoIncidencia.zul", null, tipoIncidencia);
		window.doModal();
	}







	@Listen("onClick = #nuevoEventoPorDuracion")
	public void showModalEventoPorDuracion(Event e) {
		//Evento evento =new Evento();
		HashMap MapEvento= new HashMap();
		//MapEvento.put("objetoEvento", evento);
		this.isEdit = false;
		this.isCreate = true;
		this.isView =false;
		this.estado=true;
		MapEvento.put("edit", this.isEdit);
		MapEvento.put("create", this.isCreate);
		MapEvento.put("ver", this.isView);
		MapEvento.put("estado", this.estado);
		Window window = (Window)Executions.createComponents(
				"content/modalRegistrarEventoPorDuracion.zul", null, MapEvento);
		window.doModal();
	}
	@Listen("onClick = #registarComision")
	public void showModalRegistrarComisionAct(Event e) {
		HashMap MapComision= new HashMap();
		this.isEdit = false;
		this.isCreate = true;
		this.isView =false;
		this.estado=true;
		MapComision.put("edit", this.isEdit);
		MapComision.put("create", this.isCreate);
		MapComision.put("ver", this.isView);
		MapComision.put("estado", this.estado);
		Window window = (Window)Executions.createComponents(
				"content/modalComision.zul", null, MapComision);
		window.doModal();
	}

	@Listen("onClick = #registrarTipoRechazo")
	public void showModalTipoRechazo(Event e) {
		TipoRechazo tipoR = new TipoRechazo();
		HashMap tipoRechazo = new HashMap();
		tipoRechazo.put("objTipoRechazo", tipoR);
		this.isEdit = false;
		this.isCreate = true;
		tipoRechazo.put("edit", this.isEdit);
		tipoRechazo.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalTipoRechazo.zul", null, tipoRechazo);
		window.doModal();
	}

	@Listen("onClick = #registrarRecurso")
	public void showModalRecurso(Event e) {
		Recurso re = new Recurso();
		HashMap Recurso = new HashMap();
		Recurso.put("objRecurso", re);
		this.isEdit = false;
		this.isCreate = true;
		Recurso.put("edit", this.isEdit);
		Recurso.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRecurso.zul", null, Recurso);
		window.doModal();
	}

	@Listen("onClick = #registrarTipoPersona")
	public void showModalTipoPersona(Event e) {
		TipoPersona tipoP = new TipoPersona();
		HashMap tipoPersona = new HashMap();
		tipoPersona.put("objTipoPersona", tipoP);
		this.isEdit = false;
		this.isCreate = true;
		tipoPersona.put("edit", this.isEdit);
		tipoPersona.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalTipoPersona.zul", null, tipoPersona);
		window.doModal();
	}

	@Listen("onClick = #registrarTipoIndicador")
	public void showModalTipodicadorIndicador(Event e) {
		TipoIndicador tipoI = new TipoIndicador();
		HashMap tipoIndicador = new HashMap();
		tipoIndicador.put("objTipoIndicador", tipoI);
		this.isEdit = false;
		this.isCreate = true;
		tipoIndicador.put("edit", this.isEdit);
		tipoIndicador.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalTipoIndicador.zul", null, tipoIndicador);
		window.doModal();
	}



	@Listen("onClick = #registrarCargo")
	public void showModalCargo(Event e) {
		Cargo car = new Cargo();
		HashMap cargo = new HashMap();
		cargo.put("objCargo", car);
		this.isEdit = false;
		this.isCreate = true;
		cargo.put("edit", this.isEdit);
		cargo.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalCargo.zul", null, cargo);
		window.doModal();
	}


	@Listen("onClick = #registrarcargos")
	public void showModalcargo(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalTipoCargo.zul", null, null);
		window.doModal();
	}




	@Listen("onClick = #registrarTipoPublico")
	public void showModalTipoPublico(Event e) {
		TipoPublico tipoP = new TipoPublico();
		HashMap tipoPublico = new HashMap();
		tipoPublico.put("objTipoPublico", tipoP);
		this.isEdit = false;
		this.isCreate = true;
		tipoPublico.put("edit", this.isEdit);
		tipoPublico.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalTipoPublico.zul", null, tipoPublico);
		window.doModal();
	}
	@Listen("onClick = #registrarTipoNoticia")
	public void showModalTipoNoticia(Event e) {
		TipoNoticia tipoN = new TipoNoticia();
		HashMap tipoNoticia = new HashMap();
		tipoNoticia.put("objTipoNoticia", tipoN);
		this.isEdit = false;
		this.isCreate = true;
		tipoNoticia.put("edit", this.isEdit);
		tipoNoticia.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalTipoNoticia.zul", null, tipoNoticia);
		window.doModal();
	}

	@Listen("onClick = #registrarTipoEvento")
	public void showModalTipoEvento(Event e) {
		TipoEvento tipoE = new TipoEvento();
		HashMap tipoEvento = new HashMap();
		tipoEvento.put("objTipoEvento", tipoE);
		this.isEdit = false;
		this.isCreate = true;
		tipoEvento.put("edit", this.isEdit);
		tipoEvento.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalTipoEvento.zul", null, tipoEvento);
		window.doModal();
	}

	@Listen("onClick = #registrarIndicador")
	public void showModalIndicador(Event e) {
		Indicador ind = new Indicador();
		HashMap indicador = new HashMap();
		indicador.put("objIndicador", ind);
		this.isEdit = false;
		this.isCreate = true;
		indicador.put("edit", this.isEdit);
		indicador.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalIndicador.zul", null, indicador);
		window.doModal();
	}

	@Listen("onClick = #registrarTipoSancion")
	public void showModalTipoSancion(Event e) {
		TipoSancion tipoS = new TipoSancion();
		HashMap tipoSancion = new HashMap();
		tipoSancion.put("objTipoSancion", tipoS);
		this.isEdit = false;
		this.isCreate = true;
		tipoSancion.put("edit", this.isEdit);
		tipoSancion.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalTipoSancion.zul", null, tipoSancion);
		window.doModal();
	}




	@Listen("onClick = #registrarUnidadMedida")
	public void showModalUnidadMedida(Event e) {
		UnidadMedida uni = new UnidadMedida();
		HashMap unidadMedida = new HashMap();
		unidadMedida.put("objUnidadMedida", uni);
		this.isEdit = false;
		this.isCreate = true;
		unidadMedida.put("edit", this.isEdit);
		unidadMedida.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalUnidadMedida.zul", null, unidadMedida);
		window.doModal();
	}

	@Listen("onClick = #registrarParentesco")
	public void showModalParentesco(Event e) {
		Parentesco par = new Parentesco();
		HashMap parentesco = new HashMap();
		parentesco.put("objParentesco", par);
		this.isEdit = false;
		this.isCreate = true;
		parentesco.put("edit", this.isEdit);
		parentesco.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalParentesco.zul", null, parentesco);
		window.doModal();
	}

	@Listen("onClick = #registrarCategoria")
	public void showModalCategoria(Event e) {
		Categoria cat = new Categoria();
		HashMap categoria = new HashMap();
		categoria.put("objCategoria", cat);
		this.isEdit = false;
		this.isCreate = true;
		categoria.put("edit", this.isEdit);
		categoria.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalCategoria.zul", null, categoria);
		window.doModal();
	}

	@Listen("onClick = #registrarArea")
	public void showModalArea(Event e) {
		Area ar = new Area();
		HashMap area = new HashMap();
		area.put("objArea", ar);
		this.isEdit = false;
		this.isCreate = true;
		area.put("edit", this.isEdit);
		area.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalArea.zul", null, area);
		window.doModal();
	}


	@Listen("onClick = #registrarTipoArrendamiento")
	public void showModalTipoArrendamiento(Event e) {
		TipoArrendamiento tipoArr = new TipoArrendamiento();
		HashMap tipoArrendamiento = new HashMap();
		tipoArrendamiento.put("objTipoArren", tipoArr);
		this.isEdit = false;
		this.isCreate = true;
		tipoArrendamiento.put("edit", this.isEdit);
		tipoArrendamiento.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalTipoArrendamiento.zul", null, tipoArrendamiento);
		window.doModal();
	}

	@Listen("onClick = #registrarMotivo")
	public void showModalMotivo(Event e) {
		Motivo mot = new Motivo();
		HashMap motivo = new HashMap();
		motivo.put("objMotivo", mot);
		this.isEdit = false;
		this.isCreate = true;
		motivo.put("edit", this.isEdit);
		motivo.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalMotivo.zul", null, motivo);
		window.doModal();
	}
	@Listen("onClick = #registrarMotivoDes")
	public void showModalMotivoDesvinculacion(Event e) {
		MotivoDesvinculacion mot = new MotivoDesvinculacion();
		HashMap motivodes = new HashMap();
		motivodes.put("objMotivodes", mot);
		this.isEdit = false;
		this.isCreate = true;
		motivodes.put("edit", this.isEdit);
		motivodes.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalMotivoDesvinculacion.zul", null, motivodes);
		window.doModal();
	}
	@Listen("onClick = #orderBtn")
	public void showModal(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"modal_area.zul", null, null);
		window.doModal();

	}
	@Listen("onClick = #PagarsolicitudArren")
	public void showModalpagoarrem(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRegistroPagoLuis.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #registrarUnidad")
	public void showModalUnidad(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalUnidadMedida.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #nuevaArea")
	public void show(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/registroArea.zul", null, null);
		window.doModal();
	}

	@Listen("onClick = #registrartdifusiones")
	public void showModalnoticia(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalTipoDifusion.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #registrarpublico")
	public void showModalpublico(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalTipoPublico.zul", null, null);
		window.doModal();
	}

	@Listen("onClick = #btnNuevaNoticia")
	public void showModalNoticia(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalNoticia.zul", null, null);
		window.doModal();
	}

	@Listen("onClick = #btnSancionar")
	public void showModalSancionar(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalSancionar.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #personaBtn")
	public void showModalPersona(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalPersona.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #opinionesBtn")
	public void showModalOpinion(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalOpinion.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #registrarpagos")
	public void showModaregistrarpagos(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRegistroPago.zul", null, null);
		window.doModal();
	}

	//visualiza el Detalle del Pago del Cliente que selecciona en la lista de Aprovado
	@Listen("onClick = #visualizarpagos")
	public void visualizarpagos() {
		Executions.sendRedirect("registroPagoCliente.zul");
	}



	@Listen("onClick = 	#registarecurso")
	public void showModalrecurso(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRegistroRecurso.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = 	#RegistrarCliente")
	public void showModalregistrocliente(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRegistroCliente.zul", null, null);
		window.doModal();
	}


	@Listen("onClick = #registrarinsolventes")
	public void showModalinsolvente(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRegistroInsolvente.zul", null, null);
		window.doModal();
	}  

	@Listen("onClick = #registrarincidencia")
	public void showModalIncidencia(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalIncidencia.zul", null, null);
		window.doModal();
	} 

	@Listen("onClick = #registrarComision")
	public void showModalComision(Event e) {
		Comision com = new Comision();
		HashMap comision = new HashMap();
		comision.put("objetocom", com);
		this.isEdit = false;
		this.isCreate = true;
		comision.put("edit", this.isEdit);
		comision.put("create", this.isCreate);
		comision.put("ver",this.isView);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalComision.zul", null, comision);
		window.doModal();
	}


	@Listen("onClick = #registrarTipoParentesco")
	public void showModalTipoParentesco(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalTipoParentesco.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #BtnRegistrarIncidencias")
	public void showModalRegistrarIncidencias(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/registrarIncidenciaEvento.zul", null, null);
		window.doModal();
	}
	@Listen("onSelect = #listaVisitas")
	public void showVisitas(Event e) {
		Listbox list = new Listbox();
		list = (Listbox) e.getTarget();
		String id = list.getSelectedItem().getId();
		//alert(id);


		if(id.equals("listaVisitasBenAfil")){

			//create a window programmatically and use it as a modal dialog.
			Window window = (Window)Executions.createComponents(
					"content/listaVisitas.zul", null, null);
			window.doModal();

		}else if(id.equals("listaVisitasBenAfil2")){
			//create a window programmatically and use it as a modal dialog.
			Window window = (Window)Executions.createComponents(
					"content/listaVisitas.zul", null, null);
			window.doModal();

		}else if(id.equals("listaVisitasInvitados")){
			//create a window programmatically and use it as a modal dialog.
			Window window = (Window)Executions.createComponents(
					"content/modal_anniadirVisitaInvitados.zul", null, null);
			window.doModal();

		}        	
	}

	@Listen("onClick = #nuevaImagen")
	public void showImagen(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modal_anniadirImagen.zul", null, null);
		window.doModal();
	}

	@Listen("onClick = #visitaInvitados")
	public void showVisitaInvitados(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modal_anniadirVisitaInvitados.zul", null, null);
		window.doModal();
	}

	@Listen("onClick = #verificarEvento")
	public void showVerificarEvento(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalVerificarActividad.zul", null, null);
		window.doModal();
	}

/*
	@Listen("onClick = #registrarRecursoEvento")
	public void showModalRecursoEvento(Event e) {
		RecursoEvento ce = new RecursoEvento();
		HashMap a = new HashMap();
		a.put("recursoEvento", ce);
		this.isEdit = false;
		this.isCreate = true;
		a.put("edit", this.isEdit);
		a.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRecursoEvento.zul", null, a);
		window.doModal();
	}
	 */
	@Listen("onClick = #registrarIndicadorEvento")
	public void showModalIndicadorEvento(Event e) {
		Evento_indicador ce = new Evento_indicador();
		HashMap a = new HashMap();
		a.put("objeto", ce);
		this.isEdit = false;
		this.isCreate = true;
		a.put("edit", this.isEdit);
		a.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalIndicadorEvento.zul", null, a);
		window.doModal();
	}

	@Listen("onClick = #registrarVisita")
	public void showModalAcompanante(Event e) {
		Visita ce = new Visita();
		HashMap a = new HashMap();
		a.put("objeto", ce);
		this.isEdit = false;
		this.isCreate = true;
		a.put("edit", this.isEdit);
		a.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRegistroVisita.zul", null, a);
		window.doModal();
	}
	
	@Listen("onClick = #registrarGaleria")
	public void showModalGaleria(Event e) {
		Galeria ce = new Galeria();
		HashMap a = new HashMap();
		a.put("objGaleria", ce);
		this.isEdit = false;
		this.isCreate = true;
		a.put("edit", this.isEdit);
		a.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalGaleria.zul", null, a);
		window.doModal();
	}


}