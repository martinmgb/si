package controlador;

import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import modelo.entidad.Usuario;
import modelo.hibernate.dao.DaoUsuario;

public class ManejadorMail extends GenericForwardComposer<Window> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static String username;
	private static String password;
	private DaoUsuario dao = new DaoUsuario();
	private List<Usuario> todosUsuarios;
	private Usuario user;

	//wire components
	@Wire
	public Textbox correoUsuario;
	@Wire
	public Window modalDialog;

	public static void enviarEmail(String mensaje, String destinatario, String asunto) throws Exception{

		username = "hogarclbarquisimeto@gmail.com";
		password = "hclb1234";

		Properties propiedades = new Properties();
		propiedades.put("mail.smtp.auth", "true");
		propiedades.put("mail.smtp.starttls.enable", "true");
		propiedades.put("mail.smtp.host", "smtp.gmail.com");
		propiedades.put("mail.smtp.port", "587");

		Session sesion = Session.getInstance(propiedades, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication(){
				return new PasswordAuthentication(username, password);
			}
		});

		Message msg = new MimeMessage(sesion);
		msg.setFrom(new InternetAddress(username));
		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatario));
		msg.setSubject(asunto);
		msg.setText(mensaje);
		Transport.send(msg);
	}

	public void onClick$btnRecuperarContrasenna() throws Exception{
		todosUsuarios = dao.obtenerTodosUsuarios();
		boolean encontro = false;
		for (int i = 0; i < todosUsuarios.size(); i++) {
			if(correoUsuario.getValue().equals(todosUsuarios.get(i).getPersona().getCorreoPersona())){
				user = todosUsuarios.get(i);
				encontro = true;
				break;
			}
			else {
				encontro = false;
			}
		}
		if(encontro == true){
			System.out.println(user.getClaveUsuario()+user.getNombreUsuario());
			String mensaje, destinatario, asunto;
			mensaje = "Sr(a) " + user.getPersona().getNombrePersona() + " " + user.getPersona().getApellidoPersona()
					+ " este correo es para notificarle que acaba de mandar una solicitud para recuperar "
					+ "su clave: " + user.getClaveUsuario() + " y su usuario: " + user.getNombreUsuario()
					+ ", Si usted no hizo esta solicitud acerquece a nuestras oficinas, Att. el equipo Hogar Canario Larense";
			destinatario = user.getPersona().getCorreoPersona();
			asunto = "Recuperar Cuenta y Clave ";

			ManejadorMail.enviarEmail(mensaje, destinatario, asunto);
			Messagebox.show("Se ha realizado su petición con exito", "Information", Messagebox.OK, Messagebox.INFORMATION);
			modalDialog.detach();
		}
		else{
			Messagebox.show("No se han encontrado coincidencias", "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			modalDialog.detach();
		}
	}

}