package controlador;

import java.text.SimpleDateFormat;
import java.util.List;

import modelo.entidad.Evento;
import modelo.entidad.Preferencia_evento;
import modelo.entidad.Preferencia_persona;
import modelo.entidad.Usuario;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoPreferenciaPersona;
import modelo.hibernate.dao.DaoPreferencia_evento;
import modelo.hibernate.dao.DaoUsuario;

import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Label;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Span;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;


public class ControladorNotificacion extends GenericForwardComposer<Window> {

	private DaoUsuario dao = new DaoUsuario();
	private DaoPreferenciaPersona daopp = new DaoPreferenciaPersona();
	private DaoEvento daoe = new DaoEvento();
	private DaoPreferencia_evento daoep = new DaoPreferencia_evento();
	private Usuario user;
	
	@Wire
	private Popup msgpp;
	@Wire
	private  org.zkoss.zul.A anoti;
	private String nronot;
	
	
	public ControladorNotificacion() throws Exception {
		super();	
		
	}
	
	
	public String getNronot() throws Exception {
		Session miSession = Sessions.getCurrent();
		this.msgpp.getChildren().clear();
		this.user=dao.obtenerUsuario((Integer) miSession.getAttribute("idUsuario"));
		List<Preferencia_persona> preferencias= daopp.obtenerPorPersona(this.user.getPersona());
		List<Evento> eventos = daoe.obtenerEventoParaNotificar();
		boolean enc=false;
		Integer nro=0;
		for (int i = 0; i < eventos.size(); i++) {
			
			List<Preferencia_evento> subcategorias= daoep.obtenerPreferenciaPorEvento(eventos.get(i));
			for (int k = 0; k < subcategorias.size(); k++) {
				for (int j = 0; j < preferencias.size(); j++) {
					if(subcategorias.get(k).getPreferencia().getIdPreferencia()==preferencias.get(j).getPreferencia().getIdPreferencia()){
						enc=true;
						//break;
					}
					
				}
			}
			if(enc==true){
				nro++;
			}
		}
		nronot=nro.toString();
		return nronot;
	}


	public void setNronot(String nronot) {
		this.nronot = nronot;
	}


	public List<Evento> obtenerEventos(){
		try {
			return daoe.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void onClick$anoti() {
		Session miSession = Sessions.getCurrent();
		try {
			this.msgpp.getChildren().clear();
			this.user=dao.obtenerUsuario((Integer) miSession.getAttribute("idUsuario"));
			List<Preferencia_persona> preferencias= daopp.obtenerPorPersona(this.user.getPersona());
			List<Evento> eventos = daoe.obtenerEventoParaNotificar();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			org.zkoss.zul.A ref = new org.zkoss.zul.A("Notificaciones");
			ref.setSclass("header");
			ref.setIconSclass("z-icon-envelope-o");
			this.msgpp.appendChild(ref);
			Vlayout layout = new Vlayout();
			layout.setSpacing("0");
			org.zkoss.zul.A pie = new org.zkoss.zul.A("Ver todas las notificaciones");
			pie.setHref("#");
			Span span4 = new Span();
			span4.setClass("z-icon-arrow-right");
			pie.appendChild(span4);
			Integer nro=0;
			boolean enc=false;
			for (int i = 0; i < eventos.size(); i++) {
				
				List<Preferencia_evento> subcategorias= daoep.obtenerPreferenciaPorEvento(eventos.get(i));
				for (int k = 0; k < subcategorias.size(); k++) {
					for (int j = 0; j < preferencias.size(); j++) {
						if(subcategorias.get(k).getPreferencia().getIdPreferencia()==preferencias.get(j).getPreferencia().getIdPreferencia()){
							enc=true;
							//break;
						}
						
					}
//					if(enc){
//						break;
//					}
				}
				if(enc){
//					notificacion.getChildren().clear();
//					imagen.getChildren().clear();
//					span.getChildren().clear();
//					span1.getChildren().clear();
//					span2.getChildren().clear();
					org.zkoss.zul.A notificacion = new org.zkoss.zul.A("");
					notificacion.setHref("#");
					org.zkoss.zul.Image imagen = new org.zkoss.zul.Image();
					imagen.setTooltiptext("Evento");
					imagen.setSclass("msg-photo");
					imagen.setSrc("");
					Span span = new Span();
					span.setSclass("msg-body");
					Span span1 = new Span();
					span1.setSclass("msg-title");
					Label label = new Label();
					label.setSclass("blue");
					Span span2 = new Span();
					span2.setSclass("msg-time");
					Span span3 = new Span();
					span3.setClass("z-icon-clock-o");
					label.setValue("Próximo Evento: "+eventos.get(i).getNombreEvento()+" el día "+sdf.format(eventos.get(i).getFechaEvento()));
					span1.appendChild(label);
					span2.appendChild(span3);
					span.appendChild(span1);
					span.appendChild(span2);
					notificacion.appendChild(imagen);
					notificacion.appendChild(span);
					layout.appendChild(notificacion);
					enc=false;
					nro++;
				}
			}
			layout.appendChild(pie);
			this.msgpp.appendChild(layout);
			
			System.out.print("Cantidad de Eventos---------------------------------------------------------------------------------: "+eventos.size());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
}
