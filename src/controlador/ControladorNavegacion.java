package controlador;

import java.util.HashMap;


import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zkmax.zul.Navitem;
import org.zkoss.zul.Div;
import org.zkoss.zul.Window;

public class ControladorNavegacion extends GenericForwardComposer<Component> {

	private static final long serialVersionUID = 1L;

	private Div contenido;

	private Div contenedor;

	private String pagina;
	
	@Wire
	Window modalDialog;
	
	@Wire
	public Navitem TipoArea, Reportes;

	//DATOS BASICOS****************************************************************

	//MIEMBRO******++**********************************+**********++++++
	
	public void onClick$cerrarModalGrupo(){
		modalDialog.detach();
		pagina = "vista/crearGrupo.zul";
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	
	public void onClick$TipoPersona(){

		System.out.println("Entro al metodo");
		pagina = "vista/tipoPersona.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$TipoSancion(){
		pagina = "vista/tipoSancion.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$Cargo(){
		pagina = "vista/cargo.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$TipoIncidencia(){

		System.out.println("Entro al metodo");
		pagina = "vista/tipoIncidencia.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	public void onClick$Parentesco(){
		pagina = "vista/parentesco.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	//*******************************************************************
	//EVENTO************************************************************
	public void onClick$RegistrarTipoEvento(){


		pagina = "vista/tipoEvento.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$TipoPublico(){


		pagina = "vista/tipoPublico.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}


	public void onClick$Actividad(){
		pagina = "vista/actividad.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	//****************************************************************
	//ARRENDAMIENTO************************************************************
	public void onClick$TipoArea(){

		System.out.println("Entro al metodo");
		pagina = "vista/tipoArea.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
		TipoArea.setSelected(true);
		//TipoArea.setIconSclass("z-icon-cogs");
	}

	//****************************************************************
	//CLUB****************************************************************
	public void onClick$TipoRechazo(){
		pagina = "vista/tipoRechazo.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$TipoNoticia(){

		pagina = "vista/tipoNoticia.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$Recurso(){

		System.out.println("Entro al metodo");
		pagina = "vista/recurso.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$UnidadMedida(){
		pagina = "vista/unidadMedida.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$RegistrarTipoIndicador(){
		pagina = "vista/tipoIndicador.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	public void onClick$Categoria(){
		pagina = "vista/categoria.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	//****************************************************************
	//CONFIGURACION****************************************************************
	public void onClick$Areas(){
		pagina = "vista/area.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	public void onClick$Comisiones(){
		pagina = "vista/comisiones.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	public void onClick$Indicador(){
		pagina = "vista/indicador.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	public void onClick$Preferencia(){
		pagina = "vista/preferencia.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	//****************************************************************

	//GESTION DE MIEMBRO****************************************************************
	public void onClick$AtenderSolicitud(){
		pagina = "vista/atenderSolicitudmembresia.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$AsignarAccion(){
		pagina = "vista/asignarAccion.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$RegistrarBeneficiario(){
		pagina = "vista/registrarBeneficiario.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$RegistrarVisitas(){
		pagina = "vista/registrarVisita.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$RegistrarIncidenciaM(){
		pagina = "vista/registrarIncidenciaMiembro.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$Sancionar(){
		pagina = "vista/sancionar.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$ReactivarMiembro(){
		pagina = "vista/reactivarMiembro.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	public void onClick$CeseAccion(){
		pagina = "vista/ceseAccion.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	//****************************************************************
	//GESTION DE EVENTO****************************************************************
	public void onClick$RegistrarEvento(){
		pagina = "vista/registrarEvento.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$agregarComision(){
		pagina = "vista/agregarComisionesEvento.zul"; 
		contenido.getChildren().clear();
		
		HashMap a = new HashMap();
		a.put("comision", true);
		a.put("recurso", false);
		a.put("indicador", false);
		
		contenedor = (Div) Executions.createComponents(pagina, null, a);
		contenido.appendChild(contenedor);
	}
	
	public void onClick$asignarRecurso(){
		pagina = "vista/asignarRecursosEvento.zul"; 
		contenido.getChildren().clear();
		
		HashMap a = new HashMap();
		a.put("comision", true);
		a.put("recurso", false);
		a.put("indicador", false);
		
		contenedor = (Div) Executions.createComponents(pagina, null, a);
		contenido.appendChild(contenedor);
	}
	
	public void onClick$agregarIndicador(){
		pagina = "vista/agregarIndicadoresEvento.zul"; 
		contenido.getChildren().clear();
		
		HashMap a = new HashMap();
		a.put("comision", true);
		a.put("recurso", false);
		a.put("indicador", false);
		
		contenedor = (Div) Executions.createComponents(pagina, null, a);
		contenido.appendChild(contenedor);
	}
	public void onClick$EventosRegistrados(){
		pagina = "vista/registrarEvento.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}			

	public void onClick$RegistrarEjecucionEvento(){
		//pagina = "vista/atenderSolicitudEvento.zul";
		pagina = "vista/ejecucionEventos.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}	

	public void onClick$RegistrarAsistencia(){
		pagina = "vista/registrarAsistenciaEvento.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}			

	public void onClick$RegistrarIncidencia(){
		pagina = "vista/registrarIncidenciaEvento.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$RegistrarResultado(){
		pagina = "vista/resultadosEventos.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	//****************************************************************
	//GESTION DE ARRENDAMIENTO****************************************************************
	public void onClick$RegistrarSolicitudA(){
		HashMap a = new HashMap();
		a.put("objeto", true);
		pagina = "vista/solicitudMiembroArrendamiento.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, a);
		contenido.appendChild(contenedor);
	}
	public void onClick$AtenderSolicitudAr(){
		pagina = "vista/atenderSolicitudesArrendamiento.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$CancelarArrendamientoM(){
		pagina = "vista/arrendamientos.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$RegistrarincidenciaA(){
		pagina = "vista/registrarIncidenciaArrendamiento.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	//****************************************************************
	//ADMINISTRACION DEL SISTEMA****************************************************************

	//SEGURIDAD FUNCIONAL****************************************************************
	public void onClick$CrearUsuario(){
		pagina = "vista/crearusuario.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$CrearGrupoUsuario(){
		pagina = "vista/creargrupo.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	//****************************************************************
	//CONFIGURACION****************************************************************


	//****************************************************************


	public void onClick$AsignarFunciones(){
		pagina = "vista/asignarFunciones.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$AsignarGruposFuncionales(){
		pagina = "vista/asignarGrupos.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$EnviarNotificaciones(){
		pagina = "vista/enviarNotificacion.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$RegistrarNoticias(){
		pagina = "vista/registrarNoticias.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$ConfigurarGaleria(){
		pagina = "vista/configuracionGaleria.zul";  
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	public void onClick$RegistrarDocumentos(){
		pagina = "vista/documento.zul";  
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}


	//****************************************************************
	//USUARIO****************************************************************


	public void onClick$cambiarContrasenna(){
		pagina = "vista/cambiarContrasena.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$PerfilUsuario(){
		pagina = "vista/perfilUsuario.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	
	
	


	

	
	//****************************************************************
	//REPORTE****************************************************************
	public void onClick$VerSanciones(){
		pagina = "vista/verSanciones.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}
	public void onClick$ListaMiembro(){
		pagina = "vista/listaAcciones.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	public void onClick$RegistrarAsistenciaEvento(){
		pagina = "vista/registrarAsistenciaEvento.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	//****************************************************************



}
