package controlador;
import java.io.IOException;
import java.util.List;

import modelo.entidad.Evento;
import modelo.entidad.Persona;
import modelo.entidad.Preferencia_evento;
import modelo.entidad.Preferencia_persona;
import modelo.entidad.Usuario;
import modelo.hibernate.dao.DaoClub;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoUsuario;
import modelo.hibernate.dao.DaoPreferencia_evento;
import modelo.hibernate.dao.DaoPreferenciaPersona;





//import sun.awt.AWTAccessor.SystemColorAccessor;
import org.zkoss.bind.annotation.Command;
import org.zkoss.image.AImage;
import org.zkoss.lang.Strings;
import org.zkoss.zhtml.A;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Span;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.itextpdf.text.Image;



public class ControladorSesion extends GenericForwardComposer<Window> {

	private static final long serialVersionUID = 1L;
	private DaoUsuario dao = new DaoUsuario();
	private DaoPreferenciaPersona daopp = new DaoPreferenciaPersona();
	private DaoEvento daoe = new DaoEvento();
	private DaoClub daoc = new DaoClub();
	private DaoPreferencia_evento daoep = new DaoPreferencia_evento();
	private List<Usuario> todosUsuarios;
	private Usuario user;

	//wire components
	@Wire
	public Textbox usuario;
	@Wire
	public Textbox contrasena;
	@Wire
	public Textbox direccionp;
	@Wire
	public Textbox tlfmovil;
	@Wire
	public Textbox tlffijo;
	@Wire
	public Textbox correop;
	@Wire
	public Label msg;
	@Wire
	public Div box;
	@Wire
	public Label message;
	@Wire
	private Textbox clavenueva;
	@Wire
	private Textbox claveactual;
	@Wire
	private Textbox repetirclave;
	@Wire
	private Popup msgpp;

	private static AImage myImage;



	private Div contenido;

	private Div contenedor;

	private String pagina;


	public ControladorSesion() {
		super();
		//		byte[] img = null;
		//		try {
		//			img = daoc.obtenerClub(1).getLogoClub();
		//		} catch (Exception e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}
		//		if(img==null){
		//			myImage =null;
		//		}else{
		//			try {
		//				myImage=new AImage("",img);
		//			} catch (IOException e) {
		//				// TODO Auto-generated catch block
		//				e.printStackTrace();
		//			}
		//		}
		try {
			//todosUsuarios=dao.obtenerTodos();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}


	public void onClick$btnAceptar() {

		String user = usuario.getValue();
		String pwd = contrasena.getValue();	
		Session miSession = Sessions.getCurrent();
		boolean encontrado = false;

		if(Strings.isBlank(user) || Strings.isEmpty(pwd)){
			msg.setValue("¡Debe llenar los campos Nombre de Usuario y Contraseña!");
			box.setVisible(true);
		}
		else{
			try {
				todosUsuarios = dao.obtenerTodosUsuarios();
				System.out.println("ENTRO!!!!!!!!!!!");
				for (int i = 0; i < todosUsuarios.size(); i++) {
					if((todosUsuarios.get(i).getNombreUsuario().equals(user) &&
							todosUsuarios.get(i).getClaveUsuario().equals(pwd))){
						miSession.setAttribute("user", todosUsuarios.get(i).getNombreUsuario());
						Integer intObj = new Integer(todosUsuarios.get(i).getGrupo().getIdGrupo());
						Integer intIdPersona = new Integer(todosUsuarios.get(i).getIdUsuario());
						AImage imagen;
						byte[] image= todosUsuarios.get(i).getPersona().getImagenPersona();
						imagen=new AImage("",image);
						//						byte[] img= daoc.obtenerClub(1).getLogoClub();
						//						AImage logo;
						//						if(img==null){
						//							logo =null;
						//						}else{
						//							logo=new AImage("",img);
						//						}
						miSession.setAttribute("idUsuario", intIdPersona);
						miSession.setAttribute("idGrupo", intObj);
						miSession.setAttribute("imagen", imagen);
						//miSession.setAttribute("logo", logo);
						encontrado = true;
						break;
					}
					else{
						encontrado = false;
					}
				}
				if(encontrado == true){
					Executions.sendRedirect("index.zul");
					box.setVisible(false);
				}
				else{
					msg.setValue("¡Nombre de Usuario y/o Contraseña incorrecto!");
					box.setVisible(true);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void onClick$cerrarSesion() {
		Executions.sendRedirect("http://localhost:8080/CanarioWeb/portal/CaraWeb/index.html");
		Sessions.getCurrent().invalidate();
	}

	public void onClick$cambiarContrasena() {
		pagina = "vista/cambiarContrasena.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	public void onClick$perfil() {
		pagina = "vista/perfilUsuario.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	public void onClick$registrarSugerencia(){
		pagina = "vista/registrarSugerencia.zul";
		contenido.getChildren().clear();
		contenedor = (Div) Executions.createComponents(pagina, null, null);
		contenido.appendChild(contenedor);
	}

	public AImage getMyImage() {
		return myImage;
	}
	public void setMyImage(AImage myImage) {
		ControladorSesion.myImage = myImage;
	}
}