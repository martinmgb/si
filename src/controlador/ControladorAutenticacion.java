package controlador;

import java.util.Map;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Initiator;

public class ControladorAutenticacion implements Initiator {

	@Override
	public void doInit(Page page, Map<String, Object> args) {
		try{
			Session session = Sessions.getCurrent();
			String nombre = (String)session.getAttribute("user");
			if(nombre.equals(null)){
				Executions.sendRedirect("index.zul");
			}
		}catch(Exception e){
			Executions.sendRedirect("sesion.zul");
		}
		return;

	}
	// TODO Auto-generated constructor stub
}
