package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Comision;
import modelo.entidad.Evento;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class DaoComision {
	
	private Sesion sesionPostgres;

	public void agregarComision(Comision dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Comision obtenerComision(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Comision dato = null;        
		try{
			dato = (Comision) sesion.get(Comision.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public void eliminarComision(Comision dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  
			//return false;
			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
		//return true;
	}


	
	
	public void modificarComision(Comision dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}



	public List<Comision> obtenerTodos() throws Exception {            

		List<Comision> datos = new ArrayList<Comision>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Comision>) em.createCriteria(Comision.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	

}
