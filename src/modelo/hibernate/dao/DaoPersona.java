package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import modelo.entidad.Accion;
import modelo.entidad.Cargo;
import modelo.entidad.Evento;
import modelo.entidad.Parentesco;
import modelo.entidad.Persona;
import modelo.entidad.Sancion;
import modelo.entidad.TipoPersona;
import modelo.hibernate.config.Sesion;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;



public class DaoPersona {
	private Sesion sesionPostgres;

private DaoAccion accionDao = new DaoAccion();
	
public void agregarPersona(Persona dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}
	


	public Persona obtenerPersona(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Persona dato = null;        
		try{
			dato = (Persona) sesion.get(Persona.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public void eliminarPersona(Persona dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}

	public void modificarPersona(Persona dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<Persona> obtenerTodos() throws Exception {            

		List<Persona> datos = new ArrayList<Persona>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Persona>) em.createCriteria(Persona.class).addOrder( Property.forName("idPersona").asc() ).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	
// BUSCA LAS PERSONAS QUE NO ESTEN SANCIONADAS, EDO S.. y PERSONAS TIPO 
	public List<Persona> obtenerPersonaMenosSancion(String estado, int tipoPersona,int tipoPersona1,int tipoPersona2) throws Exception {            
		TipoPersona h = new TipoPersona();
		TipoPersona w = new TipoPersona();
		TipoPersona z = new TipoPersona();
		 z.setIdTipoPersona(tipoPersona2);
		 w.setIdTipoPersona(tipoPersona1);
		 h.setIdTipoPersona(tipoPersona);
		List<Persona> datos = new ArrayList<Persona>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			 Criteria criteria =em.createCriteria(Persona.class); 
			 criteria.add(Restrictions.ne("estadoPersona", estado)).add(Restrictions.in("tipoPersona", new TipoPersona[]{h,w,z}));
			//datos =  (List<Evento>) em.createCriteria(Evento.class);
			 datos=criteria.list();
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	 
	}
	
	
// BUSCA LAS PERSONAS QUE NO ESTEN SANCIONADAS,(EDO S).. y PERSONAS TIPO HONORARIO Y PROPIETARIO
		public List<Persona> obtenerPersonaMenosSancionPropiHonora(String estado, int tipoPersona,int tipoPersona1) throws Exception {            
			TipoPersona h = new TipoPersona();
			TipoPersona w = new TipoPersona();
			 
			 w.setIdTipoPersona(tipoPersona1);
			 h.setIdTipoPersona(tipoPersona);
			List<Persona> datos = new ArrayList<Persona>();  
			Session em = sesionPostgres.openSession();  	
			try {  	
				 Criteria criteria =em.createCriteria(Persona.class); 
				 criteria.add(Restrictions.ne("estadoPersona", estado)).add(Restrictions.in("tipoPersona", new TipoPersona[]{h,w}));
				//datos =  (List<Evento>) em.createCriteria(Evento.class);
				 datos=criteria.list();
			} catch (Exception e) {             

				throw new Exception(e.getMessage(),e.getCause());
			} finally {  
				em.close();  
			} 

			return datos; 
		 
		}
	
	
		public List<Persona> obtenerPersonaTipoMiembros(int tipoPersona,int tipoPersona1) throws Exception {            
			TipoPersona h = new TipoPersona();
			TipoPersona w = new TipoPersona();
			 
			 w.setIdTipoPersona(tipoPersona1);
			 h.setIdTipoPersona(tipoPersona);
			List<Persona> datos = new ArrayList<Persona>();  
			Session em = sesionPostgres.openSession();  	
			try {  	
				 Criteria criteria =em.createCriteria(Persona.class); 
				 criteria.add(Restrictions.in("tipoPersona", new TipoPersona[]{h,w}));
				//datos =  (List<Evento>) em.createCriteria(Evento.class);
				 datos=criteria.list();
			} catch (Exception e) {             

				throw new Exception(e.getMessage(),e.getCause());
			} finally {  
				em.close();  
			} 

			return datos; 
		 
		}
	
	// BUSCA LAS PERSONAS  ACTIVAS. y PERSONAS TIPO PROPIETARIO Y HONORARIO
		public List<Persona> obtenerActivasTipoM(String estado, int tipoPersona, int tipoPersona1,int tipoPersona2) throws Exception {            
			TipoPersona h = new TipoPersona();
			TipoPersona w = new TipoPersona();
			TipoPersona z = new TipoPersona();
			
			 h.setIdTipoPersona(tipoPersona);
			 w.setIdTipoPersona(tipoPersona1);
			 z.setIdTipoPersona(tipoPersona2);
			List<Persona> datos = new ArrayList<Persona>();  
			Session em = sesionPostgres.openSession();  	
			try {  	
				 Criteria criteria =em.createCriteria(Persona.class); 
				 criteria.add(Restrictions.eq("estadoPersona", estado)).add(Restrictions.in("tipoPersona", new TipoPersona[]{h,w,z}))  ;
				//datos =  (List<Evento>) em.createCriteria(Evento.class);
				 datos=criteria.list();
			} catch (Exception e) {             

				throw new Exception(e.getMessage(),e.getCause());
			} finally {  
				em.close();  
			} 

			return datos; 
		 
		}
		
		// BUSCA LAS PERSONAS QUE ACTIVAS. y PERSONAS TIPO 
			public List<Persona> obtenerActivasTipoM(String estado, int tipoPersona) throws Exception {            
				TipoPersona h = new TipoPersona();				
				 h.setIdTipoPersona(tipoPersona);
				List<Persona> datos = new ArrayList<Persona>();  
				Session em = sesionPostgres.openSession();  	
				try {  	
					 Criteria criteria =em.createCriteria(Persona.class); 
					 criteria.add(Restrictions.eq("estadoPersona", estado)).add(Restrictions.eq("tipoPersona", h)) ;
					//datos =  (List<Evento>) em.createCriteria(Evento.class);
					 datos=criteria.list();
				} catch (Exception e) {             

					throw new Exception(e.getMessage(),e.getCause());
				} finally {  
					em.close();  
				} 

				return datos; 
			 
			}
	
	
	
	
// BUSCA UNA PERSONA TIPO MIEMBRO que este activa
	public List<Persona> obtenerPersonaTipoMiembro(int tipoPersona ) throws Exception {            
		TipoPersona h = new TipoPersona();
		 h.setIdTipoPersona(tipoPersona);
		List<Persona> datos = new ArrayList<Persona>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Persona>) em.createCriteria(Persona.class).add(Restrictions.eq("tipoPersona", h)).list();             
 
		} catch (Exception e) {             
			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	
// BUSCA UNA PERSONA TIPO MIEMBRO
		public List<Persona> obtenerPersonaTipoMiembro(TipoPersona tipPersona ) throws Exception {  
			List<Persona> datos = new ArrayList<Persona>();  
			Session em = sesionPostgres.openSession();  	
			try {  	
				datos =  (List<Persona>) em.createCriteria(Persona.class).add(Restrictions.eq( "tipoPersona", tipPersona )).list();             
			} catch (Exception e) {             
				throw new Exception(e.getMessage(),e.getCause());
			} finally {  
				em.close();  
			} 

			return datos; 
		}
	
		
		// BUSCA BENEFICIARIOS POR PROPIETARIO
		public List<Persona> obtenerBeneficiariosPorPropietario(int idPropietario ) throws Exception {            
			List<Persona> datos = new ArrayList<Persona>();  
			Session em = sesionPostgres.openSession();  	
			try {  	
				datos =  (List<Persona>) em.createCriteria(Persona.class).add(Restrictions.eq("idPadrePersona", idPropietario)).list();             
	 
			} catch (Exception e) {             
				throw new Exception(e.getMessage(),e.getCause());
			} finally {  
				em.close();  
			} 

			return datos; 
		}
		
		// BUSCA CONYUGUE POR PROPIETARIO
		public Persona obtenerConyuguePorPropietario(int idPropietario, Parentesco p ) throws Exception {            
			List<Persona> datos = new ArrayList<Persona>();  
			Session em = sesionPostgres.openSession();  	
			try {  	
				datos =  (List<Persona>) em.createCriteria(Persona.class).add(Restrictions.eq("idPadrePersona", idPropietario))
						.add(Restrictions.eq("parentesco", p)).list();             
	 
			} catch (Exception e) {             
				throw new Exception(e.getMessage(),e.getCause());
			} finally {  
				em.close();  
			} 
			if(datos.size()>0){
				return datos.get(0);
			}
			else return null;
			
		}
		
		
		public List<Accion> getListaAcciones(Persona person) throws Exception {
			
			 Set<Accion> acciones = new HashSet<Accion>();
			
			 ArrayList<Accion> as = new ArrayList<Accion>(accionDao.obtenerTodos());
		    
			 for(Accion a : as){
				 if(a.getPersonai()==null){
					 System.out.println("ES NULLLL");
				 }
				 else if(a.getPersonai().getIdPersona() == person.getIdPersona()){
				  
					 acciones.add(a);
				 }
			 }

			return new ArrayList<Accion>(acciones);
		}
	
	
	
}