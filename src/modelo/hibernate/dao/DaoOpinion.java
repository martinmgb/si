package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;
import modelo.entidad.Opinion;
import modelo.hibernate.config.Sesion;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class DaoOpinion {
	private Sesion sesionPostgres;

	public void agregarOpinion(Opinion dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Opinion obtenerOpinion(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Opinion dato = null;        
		try{
			dato = (Opinion) sesion.get(Opinion.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public void eliminarOpinion(Opinion dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}

	public void modificarOpinion(Opinion dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<Opinion> obtenerOpinionPorEstado(String estado) throws Exception {            

		List<Opinion> datos = new ArrayList<Opinion>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Opinion>) em.createCriteria(Opinion.class).add(Restrictions.eq("estadoOpinion",estado )).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	public List<Opinion> obtenerTodos() throws Exception {            

		List<Opinion> datos = new ArrayList<Opinion>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Opinion>) em.createCriteria(Opinion.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
	
	public List<Opinion> obtenerOpinionesPorEstado(String estado) throws Exception {            

		List<Opinion> datos = new ArrayList<Opinion>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			 Criteria criteria =em.createCriteria(Opinion.class); 
			 criteria.add(Restrictions.ne("estadoOpinion", estado));
			 datos=criteria.list();
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	
	}	
}