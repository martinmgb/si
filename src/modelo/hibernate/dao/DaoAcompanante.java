package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Sancion;
import modelo.entidad.Acompanante;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class DaoAcompanante {
	private Sesion sesionPostgres;

	public void agregarAcompanante(Acompanante dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Acompanante obtenerAcompanante(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Acompanante dato = null;        
		try{
			dato = (Acompanante) sesion.get(Acompanante.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public void eliminarAcompanante(Acompanante dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}

	public void modificarAcompanante(Acompanante dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<Acompanante> obtenerTodos() throws Exception {       

		List<Acompanante> datos = new ArrayList<Acompanante>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Acompanante>) em.createCriteria(Acompanante.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
	
	// BUSCA UNA SANCION DEPENDE SU ESTADO
		public List<Acompanante> obtenerPorSancion(String edo) throws Exception {            

			List<Acompanante> datos = new ArrayList<Acompanante>();  
			Session em = sesionPostgres.openSession();  	
			try {  	
				datos =  (List<Acompanante>) em.createCriteria(Acompanante.class).add(Restrictions.eq( "estadoAcompanante", edo )).list();             
			} catch (Exception e) {             

				throw new Exception(e.getMessage(),e.getCause());
			} finally {  
				em.close();  
			} 

			return datos; 
		}
	
	
}
