package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Galeria;
import modelo.hibernate.config.Sesion;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DaoGaleria{
	
	private Sesion sesionPostgres;

	
public void agregarGaleria(Galeria dato) throws Exception{
		
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
}


public Galeria obtenerGaleria(int id) throws Exception{		 
		
	    @SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Galeria dato = null;        
		try{
			dato = (Galeria) sesion.get(Galeria.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
}

public void eliminarGaleria(Galeria dato) throws Exception{		 
	
	    @SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
}

public void modificarGaleria(Galeria dato) throws Exception{
		
	
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
}

public List<Galeria> obtenerTodos() throws Exception{            

		List<Galeria> datos = new ArrayList<Galeria>();  
		Session em = sesionPostgres.openSession();  	
		try{  	
			datos =  (List<Galeria>) em.createCriteria(Galeria.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
}