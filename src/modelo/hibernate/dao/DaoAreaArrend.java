package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import modelo.entidad.Area;
import modelo.entidad.Area_arrend;
import modelo.entidad.Evento;
import modelo.hibernate.config.Sesion;

public class DaoAreaArrend {

	private Sesion sesionPostgres;

	public List<Area_arrend> obtenerPorArea(Area area) throws Exception {            
	      
		   List<Area_arrend> datos = new ArrayList<Area_arrend>();  
		   Session em =sesionPostgres.openSession();    	
	        try {  	
		    datos =  (List<Area_arrend>) em.createCriteria(Area_arrend.class)
		    		.add(Restrictions.eq("area", area)).list();             
	        } catch (Exception e) {             
	       
	         throw new Exception(e.getMessage(),e.getCause());
	        } finally {  
	          em.close();  
	        } 
	       
	        return datos; 
	
	}	




	public void eliminarArea_arrend(Area_arrend dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}


}
