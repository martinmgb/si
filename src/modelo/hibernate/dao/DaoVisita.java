package modelo.hibernate.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import modelo.entidad.Persona;
import modelo.entidad.Sancion;
import modelo.entidad.Visita;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class DaoVisita {
	private Sesion sesionPostgres;

	public void agregarVisita(Visita dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Visita obtenerVisita(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Visita dato = null;        
		try{
			dato = (Visita) sesion.get(Visita.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public void eliminarVisita(Visita dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}

	public void modificarVisita(Visita dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<Visita> obtenerTodos() throws Exception {       

		List<Visita> datos = new ArrayList<Visita>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Visita>) em.createCriteria(Visita.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
	
	// BUSCA UNA SANCION DEPENDE SU ESTADO
		public List<Visita> obtenerPorSancion(String edo) throws Exception {            

			List<Visita> datos = new ArrayList<Visita>();  
			Session em = sesionPostgres.openSession();  	
			try {  	
				datos =  (List<Visita>) em.createCriteria(Visita.class).add(Restrictions.eq( "estadoVisita", edo )).list();             
			} catch (Exception e) {             

				throw new Exception(e.getMessage(),e.getCause());
			} finally {  
				em.close();  
			} 

			return datos; 
		}
		
	// visita por miembro por mes
		public List<Visita> obtenerVisitaxMiembroxMes( Persona persona) throws Exception {       
			int ano = 2009;
			Date fechaI= new Date(109,9,1);
			Date fechaF= new Date(109,9,31);
			List<Visita> datos = new ArrayList<Visita>();  
			Session em = sesionPostgres.openSession();  	
			try {  	
			 
				datos = (List<Visita>) em.createCriteria(Visita.class).add(Restrictions.eq("persona", persona )).add(Restrictions.gt("fechaVisita",fechaI )).add(Restrictions.lt("fechaVisita", fechaF )).list();
				
				
			} catch (Exception e) {             

				throw new Exception(e.getMessage(),e.getCause());
			} finally {  
				em.close();  
			} 

			return datos; 
		}
		
		
		
		
	
	
}
