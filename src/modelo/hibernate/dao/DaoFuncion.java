package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Evento;
import modelo.entidad.Funcion;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Property;

public class DaoFuncion {
	private Sesion sesionPostgres;

	public void agregarFuncion(Funcion dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Funcion obtenerFuncion(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Funcion dato = null;        
		try{
			dato = (Funcion) sesion.get(Funcion.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}


	public List<Funcion> obtenerTodos() throws Exception {            

		List<Funcion> datos = new ArrayList<Funcion>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Funcion>) em.createCriteria(Funcion.class).addOrder( Property.forName("idFuncion").asc() ).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
}