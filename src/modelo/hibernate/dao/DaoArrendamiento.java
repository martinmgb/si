package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Arrendamiento;
import modelo.entidad.Evento;
import modelo.hibernate.config.Sesion;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class DaoArrendamiento {
	
	private Sesion sesionPostgres;

	public void agregarArrendamiento(Arrendamiento dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Arrendamiento obtenerArrendamiento(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Arrendamiento dato = null;        
		try{
			dato = (Arrendamiento) sesion.get(Arrendamiento.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public void eliminarArrendamiento(Arrendamiento dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}

	public void modificarArrendamiento(Arrendamiento dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}



	public List<Arrendamiento> obtenerTodos() throws Exception {            

		List<Arrendamiento> datos = new ArrayList<Arrendamiento>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Arrendamiento>) em.createCriteria(Arrendamiento.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
	
	public List<Arrendamiento> obtenerSolicitudesArr() throws Exception {            

		List<Arrendamiento> datos = new ArrayList<Arrendamiento>();  
		Session em = sesionPostgres.openSession();  
		String estado = "P";
		try {  	
//			datos =  (List<Arrendamiento>) em.createCriteria(Arrendamiento.this.getEstadoArrend().equals(estado)).list();   
			datos = (List<Arrendamiento>) em.createCriteria(Arrendamiento.class, estado).list();
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
	
	public List<Arrendamiento> obtenerArrendamientosPorEstado(String estado) throws Exception {            

		List<Arrendamiento> datos = new ArrayList<Arrendamiento>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			 Criteria criteria =em.createCriteria(Arrendamiento.class); 
			 criteria.add(Restrictions.ne("estadoArrend", estado));
			 datos=criteria.list();
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	
	}
	
	public List<Arrendamiento> obtenerArrendamientosPorEstado1(String estado) throws Exception {            

		List<Arrendamiento> datos = new ArrayList<Arrendamiento>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Arrendamiento>) em.createCriteria(Arrendamiento.class).add(Restrictions.eq( "estadoArrend",estado )).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	
	public List<Arrendamiento> obtenerArrendamientoParaIncidencia() throws Exception {            

		List<Arrendamiento> datos = new ArrayList<Arrendamiento>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Arrendamiento>) em.createCriteria(Arrendamiento.class).add(Restrictions.or(Restrictions.eq("estadoArrend", "A" ), Restrictions.eq("estadoArrend", "F" ))).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	

}
