package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Accion;
import modelo.entidad.Evento;
import modelo.entidad.Evento_comision;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

public class DaoAccion {
	private Sesion sesionPostgres;

	public void agregarAccion(Accion dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Accion obtenerAccion(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Accion dato = null;        
		try{
			dato = (Accion) sesion.get(Accion.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}
  
	public Accion obtenerAccions(String nroAccion) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Accion dato = null;        
		try{
			dato = (Accion) sesion.get(Accion.class, nroAccion);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}
	
	public Accion obtenerPorNroAccion(String nroAccion) throws Exception {            

		List<Accion> datos = new ArrayList<Accion>(); 
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos = (List<Accion>) em.createCriteria(Accion.class).add(Restrictions.eq( "nroAccion", nroAccion )).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos.get(0); 
	}	
	
	
	public void eliminarAccion(Accion dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}

	public void modificarAccion(Accion dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<Accion> obtenerTodos() throws Exception {            

		List<Accion> datos = new ArrayList<Accion>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Accion>) em.createCriteria(Accion.class).addOrder(Property.forName("idAccion").asc()).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
	
	
	
//   sirve para buscar por filtros
//	
//	public List<Accion> obtenerAccionXPersona( int id) throws Exception {            
//	     
//		  List<Accion> datos = new ArrayList<Accion>();  
//		  Session em = sesionPostgres.openSession();    	
//	       try {  	
//		   datos =  (List<Accion>) em.createCriteria(Accion.class).add(Restrictions.eq("personaiId", id)).list();             
//	       } catch (Exception e) {             
//	      
//	        throw new Exception(e.getMessage(),e.getCause());
//	       } finally {  
//	         em.close();  
//	       } 
//	      
//	       return datos; 
//		}
	
	 
	
	
}