package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Indicador;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class DaoIndicador {
	private Sesion sesionPostgres;

	public void agregarIndicador(Indicador dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Indicador obtenerIndicador(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Indicador dato = null;        
		try{
			dato = (Indicador) sesion.get(Indicador.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public boolean eliminarIndicador(Indicador dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  
			return false;
			//throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
		return true;
	}

	public void modificarIndicador(Indicador dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<Indicador> obtenerTodos() throws Exception {            

		List<Indicador> datos = new ArrayList<Indicador>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Indicador>) em.createCriteria(Indicador.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
}