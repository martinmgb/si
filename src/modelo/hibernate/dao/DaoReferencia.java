package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Persona;
import modelo.entidad.Postulacion;
import modelo.entidad.Referencia;
import modelo.entidad.TipoPersona;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

public class DaoReferencia {
	private Sesion sesionPostgres;

	public void agregarReferencia(Referencia dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Referencia obtenerReferencia(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Referencia dato = null;        
		try{
			dato = (Referencia) sesion.get(Referencia.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public void eliminarReferencia(Referencia dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}

	public void modificarReferencia(Referencia dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}
	// BUSCA BENEFICIARIOS POR PROPIETARIO
	public List<Referencia> obtenerReferenciaPorPostulado(Postulacion postulacion ) throws Exception {            
	
		List<Referencia> datos = new ArrayList<Referencia>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =(List<Referencia>) em.createCriteria(Referencia.class).add(Restrictions.eq("postulacion",postulacion)).list();             
 
		} catch (Exception e) {             
			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}


		
		
		
		
	public List<Referencia> obtenerTodos() throws Exception {            

		List<Referencia> datos = new ArrayList<Referencia>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Referencia>) em.createCriteria(Referencia.class).addOrder( Property.forName("idReferencia").asc() ).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
}