package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import modelo.entidad.Area;
import modelo.entidad.Noticia;
import modelo.hibernate.config.Sesion;

public class DaoNoticia {
	private Sesion sesionPostgres;


	public void agregarNoticia(Noticia dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}
	
	public Noticia obtenerNoticia(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Noticia dato = null;        
		try{
			dato = (Noticia) sesion.get(Noticia.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public void eliminarNoticia(Noticia dato) throws Exception{		 
		//System.out.print("Hola");
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}
	
	public void modificarNoticia(Noticia dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}
	
	public List<Noticia> obtenerTodos() throws Exception {            

		List<Noticia> datos = new ArrayList<Noticia>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Noticia>) em.createCriteria(Noticia.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	
	public List<Noticia> obtenerNoticiasPorEstado(String estado) throws Exception {            

		List<Noticia> datos = new ArrayList<Noticia>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Noticia>) em.createCriteria(Noticia.class).add(Restrictions.eq( "estadoNoticia", estado )).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	
	public List<Noticia> obtenerNoticiasParaPlanificar() throws Exception {            

		List<Noticia> datos = new ArrayList<Noticia>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Noticia>) em.createCriteria(Noticia.class).add(Restrictions.or(Restrictions.eq("estadoNoticia", "R" ), Restrictions.eq("estadoNoticia", "P" ))).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	public List<Noticia> obtenerNoticiaPorEstado(String estado) throws Exception {            

		List<Noticia> datos = new ArrayList<Noticia>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			 Criteria criteria =em.createCriteria(Noticia.class); 
			 criteria.add(Restrictions.ne("estadoEvento", estado));
			//datos =  (List<Evento>) em.createCriteria(Evento.class);
			 datos=criteria.list();
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	

	}
}
