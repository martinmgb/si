package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Evento;
import modelo.entidad.Postulacion;
import modelo.entidad.Referencia;
import modelo.hibernate.config.Sesion;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class DaoPostulacion {
	private Sesion sesionPostgres;

	public void agregarPostulacion(Postulacion dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Postulacion obtenerPostulacion(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Postulacion dato = null;        
		try{
			dato = (Postulacion) sesion.get(Postulacion.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public void eliminarPostulacion(Postulacion dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}

	public void modificarPostulacion(Postulacion dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<Postulacion> obtenerTodos() throws Exception {            

		List<Postulacion> datos = new ArrayList<Postulacion>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Postulacion>) em.createCriteria(Postulacion.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
	
	
	
	
	// ME TRAE LA REFERENCIA DE ESA POSTULACION
	public List<Referencia> obtenerReferenciaPorPostulacion(Postulacion post) throws Exception {            

		List<Referencia> datos = new ArrayList<Referencia>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			 Criteria criteria =em.createCriteria(Referencia.class); 
			 criteria.add(Restrictions.eq("postulacion", post));
			//datos =  (List<Evento>) em.createCriteria(Evento.class);
			 datos=criteria.list();
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	
//    Criteria criteria = session.createCriteria(Student.class);  
//    criteria.add(Restrictions.ge("rollNumber", 3));  
//    List list = criteria.list(); 
	}
	
	// POSTULADOS SOLICITADA
	public List<Postulacion> obtenerPostuladoSolicitado(String estado) throws Exception {            

		List<Postulacion> datos = new ArrayList<Postulacion>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Postulacion>) em.createCriteria(Postulacion.class).add(Restrictions.eq( "estadoPostulacion", estado )).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	
	// POSTULADOS Pendiente
	public List<Postulacion> obtenerPostuladoPendiente(String estado) throws Exception {            

		List<Postulacion> datos = new ArrayList<Postulacion>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Postulacion>) em.createCriteria(Postulacion.class).add(Restrictions.eq( "estadoPostulacion", estado )).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	
	
	

}