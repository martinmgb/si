package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import modelo.entidad.Evento;
import modelo.entidad.TipoEvento;
import modelo.entidad.TipoPublico;
import modelo.hibernate.config.Sesion;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

public class DaoEvento {
	private Sesion sesionPostgres;

	public void agregarEvento(Evento dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Evento obtenerEvento(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Evento dato = null;        
		try{
			dato = (Evento) sesion.get(Evento.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public void eliminarEvento(Evento dato) throws Exception{		 
		System.out.print("Hola");
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}
	
	

	
	
	public void modificarEvento(Evento dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<Evento> obtenerTodos() throws Exception {            

		List<Evento> datos = new ArrayList<Evento>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Evento>) em.createCriteria(Evento.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	
	public List<Evento> obtenerEventosPorEstado(String estado) throws Exception {            

		List<Evento> datos = new ArrayList<Evento>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Evento>) em.createCriteria(Evento.class).add(Restrictions.eq( "estadoEvento", estado )).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	
	public List<Evento> obtenerEventosParaPlanificar() throws Exception {            

		List<Evento> datos = new ArrayList<Evento>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Evento>) em.createCriteria(Evento.class).add(Restrictions.or(Restrictions.eq("estadoEvento", "R" ), Restrictions.eq("estadoEvento", "P" ))).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	public List<Evento> obtenerEventoPorEstado() throws Exception {            

		List<Evento> datos = new ArrayList<Evento>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			 Criteria criteria =em.createCriteria(Evento.class); 
			 criteria.add(Restrictions.not(Restrictions.in("estadoEvento", new String[]{"C","P","F"})));
			//datos =  (List<Evento>) em.createCriteria(Evento.class);
			 datos=criteria.list();
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	
//    Criteria criteria = session.createCriteria(Student.class);  
//    criteria.add(Restrictions.ge("rollNumber", 3));  
//    List list = criteria.list(); 
	}
	
	public List<Evento> obtenerEventoParaCancelar() throws Exception {            

		List<Evento> datos = new ArrayList<Evento>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			 Criteria criteria =em.createCriteria(Evento.class);   
			 criteria.add(Restrictions.not(Restrictions.in("estadoEvento", new String[]{"C","F"})));  
			 datos=criteria.list();
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	
//    Criteria criteria = session.createCriteria(Student.class);  
//    criteria.add(Restrictions.ge("rollNumber", 3));  
//    List list = criteria.list(); 
	}
	
	public List<Evento> obtenerEventoParaNotificar() throws Exception {            

		List<Evento> datos = new ArrayList<Evento>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			 Criteria criteria =em.createCriteria(Evento.class);   
			 criteria.add(Restrictions.not(Restrictions.in("estadoEvento", new String[]{"C","F"})));
			 criteria.add(Restrictions.ge("fechaEvento", new Date()));
			 criteria.addOrder( Property.forName("fechaEvento").desc() );
			 
			 datos=criteria.list();
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	
	}
	
	public List<Evento> obtenerTodosEventoParaNotificar() throws Exception {            

		List<Evento> datos = new ArrayList<Evento>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			 Criteria criteria =em.createCriteria(Evento.class);   
			 criteria.add(Restrictions.not(Restrictions.in("estadoEvento", new String[]{"C","F"})));
			 criteria.add(Restrictions.ge("fechaEvento", new Date()));
			 criteria.addOrder( Property.forName("fechaEvento").asc() );
			 
			 datos=criteria.list();
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	
	}
	
	
	public List<Evento> obtenerPorTipoPublico(TipoPublico tipo) throws Exception {            

		List<Evento> datos = new ArrayList<Evento>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Evento>) em.createCriteria(Evento.class).add(Restrictions.eq( "tipoPublico", tipo )).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	

}