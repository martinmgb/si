package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Comision;
import modelo.entidad.Evento;
import modelo.entidad.Evento_comision;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class DaoEvento_comision {
	private Sesion sesionPostgres;

	public void agregarEvento_comision(Evento_comision dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Evento_comision obtenerEvento_comision(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Evento_comision dato = null;        
		try{
			dato = (Evento_comision) sesion.get(Evento_comision.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public void eliminarEvento_comision(Evento_comision dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}

	public void modificarEvento_comision(Evento_comision dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<Evento_comision> obtenerPorEvento(Evento evento) throws Exception {            

		List<Evento_comision> datos = new ArrayList<Evento_comision>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Evento_comision>) em.createCriteria(Evento_comision.class).add(Restrictions.eq( "evento", evento )).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	
	public List<Evento_comision> obtenerPorComision(Comision comision) throws Exception {            

		List<Evento_comision> datos = new ArrayList<Evento_comision>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Evento_comision>) em.createCriteria(Evento_comision.class).add(Restrictions.eq( "comision", comision )).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
}