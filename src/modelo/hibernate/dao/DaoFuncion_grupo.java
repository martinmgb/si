package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Area;
import modelo.entidad.Funcion;
import modelo.entidad.Funcion_grupo;
import modelo.entidad.Grupo;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Property;

public class DaoFuncion_grupo {
	private Sesion sesionPostgres;

	public void agregarFuncion_grupo(Funcion_grupo dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public void eliminarFuncion_grupo(Funcion_grupo dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}

	public List<Funcion_grupo> obtenerTodos() throws Exception {            

		List<Funcion_grupo> datos = new ArrayList<Funcion_grupo>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Funcion_grupo>) em.createCriteria(Funcion_grupo.class).addOrder( Property.forName("funcion").asc() ).list();            
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
}