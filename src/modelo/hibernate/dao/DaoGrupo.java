package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Grupo;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class DaoGrupo {
	private Sesion sesionPostgres;

	public void agregarGrupo(Grupo dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Grupo obtenerGrupo(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Grupo dato = null;        
		try{
			dato = (Grupo) sesion.get(Grupo.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public Grupo obtenerGrupo(String nombre) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Grupo dato = null;        
		try{
			dato = (Grupo) sesion.get(Grupo.class, nombre);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public void eliminarGrupo(Grupo dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}

	public void modificarGrupo(Grupo dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<Grupo> obtenerTodos() throws Exception {            

		List<Grupo> datos = new ArrayList<Grupo>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Grupo>) em.createCriteria(Grupo.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
}