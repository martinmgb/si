package modelo.hibernate.dao;

import java.util.List;

import modelo.entidad.Categoria;
import modelo.entidad.Evento;
import modelo.entidad.Preferencia;
import modelo.entidad.Preferencia_evento;
import modelo.hibernate.config.Sesion;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
public class DaoPreferencia_evento {
	private Sesion sesionPostgres;
	
	public void agregarPreferencia_evento(Preferencia_evento dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}
	
	public void eliminarPreferencia_evento(Preferencia_evento dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}
	
	
	
	public void eliminarEvento_Preferencia(Evento dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}
	
	
	public List<Preferencia_evento> obtenerTodos() throws Exception {            

		List<Preferencia_evento> datos = new ArrayList<Preferencia_evento>();
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Preferencia_evento>) em.createCriteria(Preferencia_evento.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	
	public List<Preferencia_evento> obtenerPreferenciaPorEvento(Evento evento) throws Exception {            

		List<Preferencia_evento> datos = new ArrayList<Preferencia_evento>();
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Preferencia_evento>) em.createCriteria(Preferencia_evento.class).add(Restrictions.eq( "evento", evento )).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
}
	
	
	

