package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.TipoNoticia;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class DaoTipoNoticia {
	private Sesion sesionPostgres;

	public void agregarTipoNoticia(TipoNoticia dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public TipoNoticia obtenerTipoNoticia(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		TipoNoticia dato = null;        
		try{
			dato = (TipoNoticia) sesion.get(TipoNoticia.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public boolean eliminarTipoNoticia(TipoNoticia dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  
			return false;
			//throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		} 
		return true;
	}

	public void modificarTipoNoticia(TipoNoticia dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<TipoNoticia> obtenerTodos() throws Exception {            

		List<TipoNoticia> datos = new ArrayList<TipoNoticia>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<TipoNoticia>) em.createCriteria(TipoNoticia.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
}