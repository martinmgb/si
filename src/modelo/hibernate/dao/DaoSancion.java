package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Area;
import modelo.entidad.Area_recurso;
import modelo.entidad.Evento;
import modelo.entidad.Sancion;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class DaoSancion {
	private Sesion sesionPostgres;

	public void agregarSancion(Sancion dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Sancion obtenerSancion(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Sancion dato = null;        
		try{
			dato = (Sancion) sesion.get(Sancion.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public void eliminarSancion(Sancion dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}

	public void modificarSancion(Sancion dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	



	public List<Sancion> obtenerTodos() throws Exception {       

		List<Sancion> datos = new ArrayList<Sancion>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Sancion>) em.createCriteria(Sancion.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
	
	// BUSCA UNA SANCION DEPENDE SU ESTADO
	public List<Sancion> obtenerPorSancion(String edo) throws Exception {            

		List<Sancion> datos = new ArrayList<Sancion>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Sancion>) em.createCriteria(Sancion.class).add(Restrictions.eq( "estadoSancion", edo )).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
	
	
	
	
	
	
}