  
package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Acompanante;
import modelo.entidad.Evento;
import modelo.entidad.Evento_indicador;
import modelo.entidad.Sancion;
import modelo.entidad.Visita;
import modelo.entidad.Visita_acomp;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class DaoVisita_Acomp {
	private Sesion sesionPostgres;

	public void agregarVisita_acomp(Visita_acomp dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Visita_acomp obtenerVisita(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Visita_acomp dato = null;        
		try{
			dato = (Visita_acomp) sesion.get(Visita_acomp.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public void eliminarVisita_acomp(Visita_acomp dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}

	public void modificarVisita_acomp(Visita_acomp dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<Visita_acomp> obtenerTodos() throws Exception {       

		List<Visita_acomp> datos = new ArrayList<Visita_acomp>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Visita_acomp>) em.createCriteria(Visita_acomp.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
	
	// BUSCA UNA SANCION DEPENDE SU ESTADO
		public List<Visita_acomp> obtenerPorSancion(String edo) throws Exception {            

			List<Visita_acomp> datos = new ArrayList<Visita_acomp>();  
			Session em = sesionPostgres.openSession();  	
			try {  	
				datos =  (List<Visita_acomp>) em.createCriteria(Visita_acomp.class).add(Restrictions.eq( "estadoVisita", edo )).list();             
			} catch (Exception e) {             

				throw new Exception(e.getMessage(),e.getCause());
			} finally {  
				em.close();  
			} 

			return datos; 
		}
		
		
		
		public List<Visita_acomp> obtenerPorVisita(Visita visita) throws Exception {            

			List<Visita_acomp> datos = new ArrayList<Visita_acomp>();  
			Session em = sesionPostgres.openSession();  	
			try {  	
				datos =  (List<Visita_acomp>) em.createCriteria(Visita_acomp.class).add(Restrictions.eq( "visita", visita )).list();             
			} catch (Exception e) {             

				throw new Exception(e.getMessage(),e.getCause());
			} finally {  
				em.close();  
			} 

			return datos; 
		}
	
		
		public List<Visita_acomp> obtenerAcompananteXVisitaXMiembro(List<Visita> visita, Acompanante acompanante) throws Exception {            

			List<Visita_acomp> datos = new ArrayList<Visita_acomp>();  
			Session em = sesionPostgres.openSession();
			try {  	
				datos =  (List<Visita_acomp>) em.createCriteria(Visita_acomp.class).add(Restrictions.in( "visita", visita )).add(Restrictions.eq( "acompanante", acompanante )).list();             
			} catch (Exception e) {
				throw new Exception(e.getMessage(),e.getCause());
			} finally {  
				em.close();  
			} 

			return datos; 
		}
		
		
		
		
	
}
