package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.TipoArrendamiento;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class DaoTipoArrendamiento {
	private Sesion sesionPostgres;

	public void agregarTipoArrendamiento(TipoArrendamiento dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public TipoArrendamiento obtenerTipoArrendamiento(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		TipoArrendamiento dato = null;        
		try{
			dato = (TipoArrendamiento) sesion.get(TipoArrendamiento.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public boolean eliminarTipoArrendamiento(TipoArrendamiento dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  
			return false;
			//throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
		return true;
	}

	public void modificarTipoArrendamiento(TipoArrendamiento dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<TipoArrendamiento> obtenerTodos() throws Exception {            

		List<TipoArrendamiento> datos = new ArrayList<TipoArrendamiento>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<TipoArrendamiento>) em.createCriteria(TipoArrendamiento.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
}