package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Categoria;
import modelo.entidad.Evento;
import modelo.entidad.Evento_comision;
import modelo.entidad.Preferencia;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class DaoPreferencia {
	
	private Sesion sesionPostgres;

	public void agregarPreferencia(Preferencia dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Preferencia obtenerPreferencia(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Preferencia dato = null;        
		try{
			dato = (Preferencia) sesion.get(Preferencia.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public boolean eliminarPreferencia(Preferencia dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  
			return false;
			//throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		} 
		return true;
	}

	public void modificarPreferencia(Preferencia dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<Preferencia> obtenerTodos() throws Exception {            

		List<Preferencia> datos = new ArrayList<Preferencia>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Preferencia>) em.createCriteria(Preferencia.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}		

	
	public List<Preferencia> obtenerPreferenciaPorCategoria(Categoria categoria) throws Exception {            

		List<Preferencia> datos = new ArrayList<Preferencia>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Preferencia>) em.createCriteria(Preferencia.class).add(Restrictions.eq( "categoria", categoria )).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
}


