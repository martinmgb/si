package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import modelo.entidad.TipoEvento;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class DaoTipoEvento {
	private Sesion sesionPostgres;

	public void agregarTipoEvento(TipoEvento dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public TipoEvento obtenerTipoEvento(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		TipoEvento dato = null;        
		try{
			dato = (TipoEvento) sesion.get(TipoEvento.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public boolean eliminarTipoEvento(TipoEvento dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  
			return false;
			//throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
		return true;
	}

	public void modificarTipoEvento(TipoEvento dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<TipoEvento> obtenerTodos() throws Exception {            

		List<TipoEvento> datos = new ArrayList<TipoEvento>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<TipoEvento>) em.createCriteria(TipoEvento.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
}