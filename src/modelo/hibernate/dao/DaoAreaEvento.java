package modelo.hibernate.dao;


import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Area;
import modelo.entidad.Area_evento;
import modelo.entidad.Preferencia_evento;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

public class DaoAreaEvento {
	private Sesion sesionPostgres;

	public List<Area_evento> obtenerPorArea(Area area) throws Exception {            
	      
		   List<Area_evento> datos = new ArrayList<Area_evento>();  
		   Session em =sesionPostgres.openSession();    	
	        try {  	
		    datos =  (List<Area_evento>) em.createCriteria(Area_evento.class)
		    		.add(Restrictions.eq("area", area)).list();             
	        } catch (Exception e) {             
	       
	         throw new Exception(e.getMessage(),e.getCause());
	        } finally {  
	          em.close();  
	        } 
	       
	        return datos; 
	
	}	
	
	
	public void eliminarArea_evento(Area_evento dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}


}
