package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;
import modelo.entidad.TipoIndicador;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class DaoTipoIndicador {
	private static Sesion sesionPostgres;

	public void agregarTipoIndicador(TipoIndicador dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public TipoIndicador obtenerTipoIndicador(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		TipoIndicador dato = null;        
		try{
			dato = (TipoIndicador) sesion.get(TipoIndicador.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public boolean eliminarTipoIndicador(TipoIndicador dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  
			return false;
			//throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		} 
		return true;
	}

	public void modificarTipoIndicador(TipoIndicador dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public List<TipoIndicador> obtenerTodos() throws Exception {            

		List<TipoIndicador> datos = new ArrayList<TipoIndicador>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<TipoIndicador>) em.createCriteria(TipoIndicador.class).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}	
}