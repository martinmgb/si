package modelo.hibernate.dao;


import java.util.ArrayList;
import java.util.List;

import modelo.entidad.Actividad;
import modelo.entidad.Act_comision;
import modelo.entidad.Comision;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

public class DaoAct_comision {
	private Sesion sesionPostgres;

	public List<Act_comision> obtenerPorActividad(Actividad actividad) throws Exception {            
	      
		   List<Act_comision> datos = new ArrayList<Act_comision>();  
		   Session em =sesionPostgres.openSession();    	
	        try {  	
		    datos =  (List<Act_comision>) em.createCriteria(Act_comision.class)
		    		.add(Restrictions.eq("actividad", actividad)).list();             
	        } catch (Exception e) {             
	       
	         throw new Exception(e.getMessage(),e.getCause());
	        } finally {  
	          em.close();  
	        } 
	       
	        return datos; 
	
	}	
	
	
	public void eliminarAct_comision(Act_comision dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  

			throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
	}
	
	public List<Act_comision> obtenerPorComision(Comision comision) throws Exception {            
	      
		   List<Act_comision> datos = new ArrayList<Act_comision>();  
		   Session em =sesionPostgres.openSession();    	
	        try {  	
		    datos =  (List<Act_comision>) em.createCriteria(Act_comision.class)
		    		.add(Restrictions.eq("comision", comision)).list();             
	        } catch (Exception e) {             
	       
	         throw new Exception(e.getMessage(),e.getCause());
	        } finally {  
	          em.close();  
	        } 
	       
	        return datos; 
	
	}


}
