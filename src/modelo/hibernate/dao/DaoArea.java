package modelo.hibernate.dao;

import java.util.ArrayList;
import java.util.List;
import modelo.entidad.Area;
import modelo.entidad.Funcion;
import modelo.entidad.TipoArea;
import modelo.hibernate.config.Sesion;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

public class DaoArea {
	private Sesion sesionPostgres;

	public void agregarArea(Area dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.save( dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}

	public Area obtenerArea(int id) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Area dato = null;        
		try{
			dato = (Area) sesion.get(Area.class, id);
		} catch (Exception e) {  
			e.printStackTrace();

			throw new Exception(e.getMessage(),e.getCause());
		}  finally {  
			sesion.close();  
		}  
		return dato;
	}

	public boolean eliminarArea(Area dato) throws Exception{		 
		@SuppressWarnings("static-access")
		Session sesion = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {  
			tx = sesion.beginTransaction();  
			sesion.delete(dato);  
			tx.commit();  

		} catch (Exception e) {  
			tx.rollback();  
			return false;
			//throw new Exception(e.getMessage(), e.getCause());
		} finally {  
			sesion.close();  
		}  
		return true;
	}

	public void modificarArea(Area dato) throws Exception{
		@SuppressWarnings("static-access")
		Session em = sesionPostgres.openSession();  
		Transaction tx = null;  
		try {    
			tx = em.beginTransaction();
			em.update(dato);   
			tx.commit();  
		} catch (Exception e) {  
			tx.rollback();            
			e.printStackTrace();
			throw e;
		} finally {  
			em.close();  
		} 
	}


	public List<Area> obtenerTodos() throws Exception {            

		List<Area> datos = new ArrayList<Area>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Area>) em.createCriteria(Area.class).addOrder( Property.forName("idArea").asc() ).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}

	public List<Area> obtenerTiposAreabyArea(TipoArea dato) throws Exception {            

		List<Area> datos = new ArrayList<Area>();  
		Session em = sesionPostgres.openSession();  	
		try {  	
			datos =  (List<Area>) em.createCriteria(Area.class).add(Restrictions.eq("tipoArea",dato)).list();             
		} catch (Exception e) {             

			throw new Exception(e.getMessage(),e.getCause());
		} finally {  
			em.close();  
		} 

		return datos; 
	}
}