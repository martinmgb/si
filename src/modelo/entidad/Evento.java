/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package  modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="evento",schema="public")
@PrimaryKeyJoinColumn(name="IdEvento")
public class Evento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Evento() {
	}
	
	@Column(name="idEvento", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.evento_IdEvento_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idEvento;
	
	@ManyToOne(targetEntity=TipoEvento.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="tipoEventoId", referencedColumnName="idTipoEvento", nullable=false) })	
	private TipoEvento tipoEvento;
	
	@Column(name="nombreEvento", nullable=false, length=30)	
	private String nombreEvento;
	
	@Column(name="fechaEvento", nullable=false)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaEvento;
	
	@ManyToOne(targetEntity=TipoPublico.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="tipoPublicoId", referencedColumnName="idTipoPubico", nullable=false) })	
	private TipoPublico tipoPublico;
	
	@Column(name="horaEvento", nullable=true)	
	private java.sql.Time horaEvento;
	
	@Column(name="descEvento", nullable=true, length=160)	
	private String descEvento;
	
	@Column(name="estadoEvento", nullable=false, length=1)	
	private String estadoEvento;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="evento", targetEntity=Evento_comision.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set evento_comision = new java.util.HashSet();
	
	@OneToMany(mappedBy="evento", targetEntity=Evento_indicador.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set evento_indicador = new java.util.HashSet();
	
	@OneToMany(mappedBy="evento", targetEntity=Area_evento.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.FALSE)	
	private java.util.Set area_evento = new java.util.HashSet();
	
	@OneToMany(mappedBy="evento", targetEntity=Preferencia_evento.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.FALSE)	
	private java.util.Set preferencia_evento = new java.util.HashSet();
	
	@OneToOne(mappedBy="evento", targetEntity=Cancelacion.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	private Cancelacion cancelacion;
	
	private void setIdEvento(int value) {
		this.idEvento = value;
	}
	
	public int getIdEvento() {
		return idEvento;
	}
	
	public int getORMID() {
		return getIdEvento();
	}
	
	public void setNombreEvento(String value) {
		this.nombreEvento = value;
	}
	
	public String getNombreEvento() {
		return nombreEvento;
	}
	
	public void setHoraEvento(java.sql.Time value) {
		this.horaEvento = value;
	}
	
	public java.sql.Time getHoraEvento() {
		return horaEvento;
	}
	
	public void setDescEvento(String value) {
		this.descEvento = value;
	}
	
	public String getDescEvento() {
		return descEvento;
	}
	
	public void setFechaEvento(java.util.Date value) {
		this.fechaEvento = value;
	}
	
	public java.util.Date getFechaEvento() {
		return fechaEvento;
	}
	
	public void setEstadoEvento(String value) {
		this.estadoEvento = value;
	}
	
	public String getEstadoEvento() {
		return estadoEvento;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setTipoPublico(TipoPublico value) {
		this.tipoPublico = value;
	}
	
	public TipoPublico getTipoPublico() {
		return tipoPublico;
	}
	
	public void setTipoEvento(TipoEvento value) {
		this.tipoEvento = value;
	}
	
	public TipoEvento getTipoEvento() {
		return tipoEvento;
	}
	
	public void setEvento_comision(java.util.Set value) {
		this.evento_comision = value;
	}
	
	public java.util.Set getEvento_comision() {
		return evento_comision;
	}
	
	
	public void setEvento_indicador(java.util.Set value) {
		this.evento_indicador = value;
	}
	
	public java.util.Set getEvento_indicador() {
		return evento_indicador;
	}
	
	
	public void setArea_evento(java.util.Set value) {
		this.area_evento = value;
	}
	
	public java.util.Set getArea_evento() {
		return area_evento;
	}
	
	
	public void setPreferencia_evento(java.util.Set value) {
		this.preferencia_evento = value;
	}
	
	public java.util.Set getPreferencia_evento() {
		return preferencia_evento;
	}
	
	
	public void setCancelacion(Cancelacion value) {
		this.cancelacion = value;
	}
	
	public Cancelacion getCancelacion() {
		return cancelacion;
	}
	
	public String toString() {
		return String.valueOf(getIdEvento());
	}
	
}
