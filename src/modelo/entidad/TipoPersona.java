/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="tipoPersona",schema="public")
@PrimaryKeyJoinColumn(name="IdTipoPersona")
public class TipoPersona implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TipoPersona() {
	}
	
	@Column(name="idTipoPersona", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.tipoPersona_IdTipoPersona_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idTipoPersona;
	
	@Column(name="nombreTipoPersona", nullable=false, length=30)	
	private String nombreTipoPersona;
	
	@Column(name="descTipoPersona", nullable=true, length=160)	
	private String descTipoPersona;
	
	@Column(name="estadoTipoPersona", nullable=false, length=4)	
	private String estadoTipoPersona;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="tipoPersona", targetEntity=Persona.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set persona = new java.util.HashSet();
	
	public void setIdTipoPersona(int value) {
		this.idTipoPersona = value;
	}
	
	public int getIdTipoPersona() {
		return idTipoPersona;
	}
	
	public int getORMID() {
		return getIdTipoPersona();
	}
	
	public void setNombreTipoPersona(String value) {
		this.nombreTipoPersona = value;
	}
	
	public String getNombreTipoPersona() {
		return nombreTipoPersona;
	}
	
	public void setDescTipoPersona(String value) {
		this.descTipoPersona = value;
	}
	
	public String getDescTipoPersona() {
		return descTipoPersona;
	}
	
	public void setEstadoTipoPersona(String value) {
		this.estadoTipoPersona = value;
	}
	
	public String getEstadoTipoPersona() {
		return estadoTipoPersona;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setPersona(java.util.Set value) {
		this.persona = value;
	}
	
	public java.util.Set getPersona() {
		return persona;
	}
	
	
	public String toString() {
		return String.valueOf(getIdTipoPersona());
	}
	
}
