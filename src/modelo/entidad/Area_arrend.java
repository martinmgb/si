/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="area_arrend",schema="public")
@PrimaryKeyJoinColumn(name="IdArea_Arrend")
public class Area_arrend implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Area_arrend() {
	}
	
	@Column(name="idArea_arrend", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.area_arrend_IdArea_Arrend_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idArea_arrend;
	
	@ManyToOne(targetEntity=Area.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="areaId", referencedColumnName="idArea", nullable=false) })	
	private Area area;
	
	@ManyToOne(targetEntity=Arrendamiento.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="arrendamientoId", referencedColumnName="idArrendamiento", nullable=false) })	
	private Arrendamiento arrendamiento;
	
	private void setIdArea_arrend(int value) {
		this.idArea_arrend = value;
	}
	
	public int getIdArea_arrend() {
		return idArea_arrend;
	}
	
	public int getORMID() {
		return getIdArea_arrend();
	}
	
	public void setArea(Area value) {
		this.area = value;
	}
	
	public Area getArea() {
		return area;
	}
	
	public void setArrendamiento(Arrendamiento value) {
		this.arrendamiento = value;
	}
	
	public Arrendamiento getArrendamiento() {
		return arrendamiento;
	}
	
	public String toString() {
		return String.valueOf(getIdArea_arrend());
	}
	
}
