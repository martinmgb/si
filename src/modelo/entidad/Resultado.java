/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="resultado",schema="public")
@PrimaryKeyJoinColumn(name="IdResultado")
public class Resultado implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Resultado() {
	}
	
	@Column(name="idResultado", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.resultado_IdResultado_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idResultado;
	
	@ManyToOne(targetEntity=Evento_indicador.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="evento_indicadorId", referencedColumnName="idEvento_ind", nullable=false) })	
	private Evento_indicador evento_indicador;
	
	@Column(name="valorRealResultado", nullable=false)	
	private double valorRealResultado;
	
	@Column(name="calculoResultado", nullable=false)	
	private double calculoResultado;
	
	@Column(name="descResultado", nullable=true, length=160)	
	private String descResultado;
	
	@Column(name="estadoResultado", nullable=false, length=1)	
	private String estadoResultado;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	private void setIdResultado(int value) {
		this.idResultado = value;
	}
	
	public int getIdResultado() {
		return idResultado;
	}
	
	public int getORMID() {
		return getIdResultado();
	}
	
	public void setValorRealResultado(double value) {
		this.valorRealResultado = value;
	}
	
	public double getValorRealResultado() {
		return valorRealResultado;
	}
	
	public void setCalculoResultado(double value) {
		this.calculoResultado = value;
	}
	
	public double getCalculoResultado() {
		return calculoResultado;
	}
	
	public void setDescResultado(String value) {
		this.descResultado = value;
	}
	
	public String getDescResultado() {
		return descResultado;
	}
	
	public void setEstadoResultado(String value) {
		this.estadoResultado = value;
	}
	
	public String getEstadoResultado() {
		return estadoResultado;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setEvento_indicador(Evento_indicador value) {
		this.evento_indicador = value;
	}
	
	public Evento_indicador getEvento_indicador() {
		return evento_indicador;
	}
	
	public String toString() {
		return String.valueOf(getIdResultado());
	}
	
}
