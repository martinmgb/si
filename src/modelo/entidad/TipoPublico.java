/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="tipoPublico",schema="public")
@PrimaryKeyJoinColumn(name="IdTipoPublico")
public class TipoPublico implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TipoPublico() {
	}
	
	@Column(name="idTipoPubico", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.tipoPublico_IdTipoPubico_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idTipoPubico;
	
	@Column(name="nombreTipoPublico", nullable=false, length=30)	
	private String nombreTipoPublico;
	
	@Column(name="descTipoPublico", nullable=true, length=160)	
	private String descTipoPublico;
	
	@Column(name="estadoTipoPublico", nullable=false, length=1)	
	private String estadoTipoPublico;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="tipoPublico", targetEntity=Evento.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set evento = new java.util.HashSet();
	
	private void setIdTipoPubico(int value) {
		this.idTipoPubico = value;
	}
	
	public int getIdTipoPubico() {
		return idTipoPubico;
	}
	
	public int getORMID() {
		return getIdTipoPubico();
	}
	
	public void setNombreTipoPublico(String value) {
		this.nombreTipoPublico = value;
	}
	
	public String getNombreTipoPublico() {
		return nombreTipoPublico;
	}
	
	public void setDescTipoPublico(String value) {
		this.descTipoPublico = value;
	}
	
	public String getDescTipoPublico() {
		return descTipoPublico;
	}
	
	public void setEstadoTipoPublico(String value) {
		this.estadoTipoPublico = value;
	}
	
	public String getEstadoTipoPublico() {
		return estadoTipoPublico;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setEvento(java.util.Set value) {
		this.evento = value;
	}
	
	public java.util.Set getEvento() {
		return evento;
	}
	
	
	public String toString() {
		return String.valueOf(getIdTipoPubico());
	}
	
}
