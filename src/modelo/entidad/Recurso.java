/**

 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="recurso",schema="public")
@PrimaryKeyJoinColumn(name="IdRecurso")
public class Recurso implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Recurso() {
	}
	
	@Column(name="idRecurso", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.recurso_IdRecurso_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idRecurso;
	
	@Column(name="nombreRecurso", nullable=false, length=30)	
	private String nombreRecurso;
	
	@Column(name="cantidadRecurso", nullable=false)	
	private long cantidadRecurso;
	
	@Column(name="descRecurso", nullable=true, length=160)	
	private String descRecurso;
	
	@Column(name="estadoRecurso", nullable=false, length=1)	
	private String estadoRecurso;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="recurso", targetEntity=Area_recurso.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set area_recurso = new java.util.HashSet();
	
	private void setIdRecurso(int value) {
		this.idRecurso = value;
	}
	
	public int getIdRecurso() {
		return idRecurso;
	}
	
	public int getORMID() {
		return getIdRecurso();
	}
	
	public void setNombreRecurso(String value) {
		this.nombreRecurso = value;
	}
	
	public String getNombreRecurso() {
		return nombreRecurso;
	}
	
	public void setCantidadRecurso(long value) {
		this.cantidadRecurso = value;
	}
	
	public long getCantidadRecurso() {
		return cantidadRecurso;
	}
	
	public void setDescRecurso(String value) {
		this.descRecurso = value;
	}
	
	public String getDescRecurso() {
		return descRecurso;
	}
	
	public void setEstadoRecurso(String value) {
		this.estadoRecurso = value;
	}
	
	public String getEstadoRecurso() {
		return estadoRecurso;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setArea_recurso(java.util.Set value) {
		this.area_recurso = value;
	}
	
	public java.util.Set getArea_recurso() {
		return area_recurso;
	}
	
	
	public String toString() {
		return String.valueOf(getIdRecurso());
	}
	
}
