/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="acompanante",schema="public")
@PrimaryKeyJoinColumn(name="IdAcompanante")
public class Acompanante implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Acompanante() {
	}
	
	@Column(name="idAcompanante", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.acompanante_IdAcompanante_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idAcompanante;
	
	@Column(name="cedAcompanante", nullable=false, length=10)	
	private String cedAcompanante;
	
	@Column(name="nombreAcompanante", nullable=false, length=30)	
	private String nombreAcompanante;
	
	@Column(name="apellidoAcompanante", nullable=false, length=30)	
	private String apellidoAcompanante;
	
	@Column(name="estadoAcompanante", nullable=false, length=1)	
	private String estadoAcompanante;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="acompanante", targetEntity=Visita_acomp.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set visita_acomp = new java.util.HashSet();
	
	private void setIdAcompanante(int value) {
		this.idAcompanante = value;
	}
	
	public int getIdAcompanante() {
		return idAcompanante;
	}
	
	public int getORMID() {
		return getIdAcompanante();
	}
	
	public void setCedAcompanante(String value) {
		this.cedAcompanante = value;
	}
	
	public String getCedAcompanante() {
		return cedAcompanante;
	}
	
	public void setNombreAcompanante(String value) {
		this.nombreAcompanante = value;
	}
	
	public String getNombreAcompanante() {
		return nombreAcompanante;
	}
	
	public void setApellidoAcompanante(String value) {
		this.apellidoAcompanante = value;
	}
	
	public String getApellidoAcompanante() {
		return apellidoAcompanante;
	}
	
	public void setEstadoAcompanante(String value) {
		this.estadoAcompanante = value;
	}
	
	public String getEstadoAcompanante() {
		return estadoAcompanante;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setVisita_acomp(java.util.Set value) {
		this.visita_acomp = value;
	}
	
	public java.util.Set getVisita_acomp() {
		return visita_acomp;
	}
	
	
	public String toString() {
		return String.valueOf(getIdAcompanante());
	}
	
}
