/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Type;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="club",schema="public")
@PrimaryKeyJoinColumn(name="IdClub")
public class Club implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Club() {
	}

	@Column(name="idClub", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.club_IdClub_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idClub;

	@Column(name="nombreClub", nullable=false, length=30)	
	private String nombreClub;

	@Column(name="rifClub", nullable=false, length=20)	
	private String rifClub;

	@Column(name="direccionClub", nullable=false, length=50)	
	private String direccionClub;

	@Column(name="tlfClub", nullable=false, length=20)	
	private String tlfClub;

	@Column(name="correoClub", nullable=false, length=40)	
	private String correoClub;

	@Column(name="misionClub", nullable=false, length=255)	
	private String misionClub;

	@Column(name="visionClub", nullable=false, length=255)	
	private String visionClub;

	@Column(name="logoClub", nullable=true, length=100)
	@Type(type="org.hibernate.type.BinaryType") 
	private byte[] logoClub;

	@Column(name="estadoClub", nullable=false, length=1)	
	private String estadoClub;

	@Column(name="limiteAcciones", nullable=true, length=10)	
	private int limiteAcciones;

	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;

	@OneToMany(mappedBy="club", targetEntity=Noticia.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set noticia = new java.util.HashSet();

	@OneToMany(mappedBy="club", targetEntity=Documento.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set documento = new java.util.HashSet();

	@OneToMany(mappedBy="club", targetEntity=Galeria.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set galeria = new java.util.HashSet();

	public void setIdClub(int value) {
		this.idClub = value;
	}

	public int getIdClub() {
		return idClub;
	}

	public int getORMID() {
		return getIdClub();
	}

	public void setRifClub(String value) {
		this.rifClub = value;
	}

	public String getRifClub() {
		return rifClub;
	}

	public void setNombreClub(String value) {
		this.nombreClub = value;
	}

	public String getNombreClub() {
		return nombreClub;
	}

	public void setDireccionClub(String value) {
		this.direccionClub = value;
	}

	public String getDireccionClub() {
		return direccionClub;
	}

	public void setTlfClub(String value) {
		this.tlfClub = value;
	}

	public String getTlfClub() {
		return tlfClub;
	}

	public void setMisionClub(String value) {
		this.misionClub = value;
	}

	public String getMisionClub() {
		return misionClub;
	}

	public void setCorreoClub(String value) {
		this.correoClub = value;
	}

	public String getCorreoClub() {
		return correoClub;
	}

	public void setVisionClub(String value) {
		this.visionClub = value;
	}

	public String getVisionClub() {
		return visionClub;
	}

	public void setLogoClub(byte[] value) {
		this.logoClub = value;
	}

	public byte[] getLogoClub() {
		return logoClub;
	}

	public void setEstadoClub(String value) {
		this.estadoClub = value;
	}

	public String getEstadoClub() {
		return estadoClub;
	}

	public void setLimiteAcciones(int value) {
		this.limiteAcciones = value;
	}

	public Integer getLimiteAcciones() {
		return (Integer)limiteAcciones;
	}

	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}

	public boolean getEditingStatus() {
		return editingStatus;
	}

	public void setNoticia(java.util.Set value) {
		this.noticia = value;
	}

	public java.util.Set getNoticia() {
		return noticia;
	}


	public void setDocumento(java.util.Set value) {
		this.documento = value;
	}

	public java.util.Set getDocumento() {
		return documento;
	}


	public void setGaleria(java.util.Set value) {
		this.galeria = value;
	}

	public java.util.Set getGaleria() {
		return galeria;
	}


	public String toString() {
		return String.valueOf(getIdClub());
	}

}
