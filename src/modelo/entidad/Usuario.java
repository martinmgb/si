/**

 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.beans.Transient;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="usuario", schema="public")
@PrimaryKeyJoinColumn(name="IdUsuario")
public class Usuario implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Usuario() {
	}
	
	@Column(name="idUsuario", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.usuario_IdUsuario_seq", allocationSize=1)	
	@GeneratedValue(generator="SeqId")	
	private int idUsuario;
	
	@OneToOne(targetEntity=Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaId", referencedColumnName="idPersona", nullable=false) })	
	private Persona persona;
	
	@Column(name="nombreUsuario", nullable=false, length=30)	
	private String nombreUsuario;
	
	@Column(name="claveUsuario", nullable=false, length=20)	
	private String claveUsuario;
	
	@Column(name="fechaUsuario", nullable=false)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaUsuario;
	
	@ManyToOne(targetEntity=Grupo.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="grupoId", referencedColumnName="idGrupo", nullable=false) })	
	private Grupo grupo;
	
	@Column(name="estadoUsuario", nullable=false, length=1)	
	private String estadoUsuario;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	
	@OneToMany(mappedBy="usuario", targetEntity=Opinion.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set opinion = new java.util.HashSet();
	
	private void setIdUsuario(int value) {
		this.idUsuario = value;
	}
	
	public int getIdUsuario() {
		return idUsuario;
	}
	
	public int getORMID() {
		return getIdUsuario();
	}
	
	public void setNombreUsuario(String value) {
		this.nombreUsuario = value;
	}
	
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	
	public void setClaveUsuario(String value) {
		this.claveUsuario = value;
	}
	
	public String getClaveUsuario() {
		return claveUsuario;
	}
	
	public void setFechaUsuario(java.util.Date value) {
		this.fechaUsuario = value;
	}
	
	public java.util.Date getFechaUsuario() {
		return fechaUsuario;
	}
	
	public void setEstadoUsuario(String value) {
		this.estadoUsuario = value;
	}
	
	public String getEstadoUsuario() {
		return estadoUsuario;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	@Transient
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setGrupo(Grupo value) {
		this.grupo = value;
	}
	
	public Grupo getGrupo() {
		return grupo;
	}
	
	public void setPersona(Persona value) {
		this.persona = value;
	}
	
	public Persona getPersona() {
		return persona;
	}
	
	
	
	public void setOpinion(java.util.Set value) {
		this.opinion = value;
	}
	
	public java.util.Set getOpinion() {
		return opinion;
	}

	
}
