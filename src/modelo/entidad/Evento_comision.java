/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="evento_comision",schema="public")
@PrimaryKeyJoinColumn(name="IdEvento_Comision")
public class Evento_comision implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Evento_comision() {
	}



	public Evento_comision(Evento evento, Comision comision, String responsable) {
		super();
		this.evento = evento;
		this.comision = comision;
		this.responsable = responsable;
	}



	@Column(name="idEvento_comision", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.evento_comision_IdEvento_Comision_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idEvento_comision;
	
	@ManyToOne(targetEntity=Evento.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="eventoId", referencedColumnName="idEvento", nullable=false) })	
	private Evento evento;
	
	@ManyToOne(targetEntity=Comision.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="comisionId", referencedColumnName="idComision", nullable=false) })	
	private Comision comision;
	
	@Column(name="responsable", nullable=false, length=40)	
	private String responsable;
	
	private void setIdEvento_comision(int value) {
		this.idEvento_comision = value;
	}
	
	public int getIdEvento_comision() {
		return idEvento_comision;
	}
	
	public int getORMID() {
		return getIdEvento_comision();
	}
	
	public void setResponsable(String value) {
		this.responsable = value;
	}
	
	public String getResponsable() {
		return responsable;
	}
	
	public void setEvento(Evento value) {
		this.evento = value;
	}
	
	public Evento getEvento() {
		return evento;
	}
	
	public void setComision(Comision value) {
		this.comision = value;
	}
	
	public Comision getComision() {
		return comision;
	}
	
	public String toString() {
		return String.valueOf(getIdEvento_comision());
	}
	
}
