/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="desvinculacion",schema="public")
@PrimaryKeyJoinColumn(name="IdDesvinculacion")
public class Desvinculacion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Desvinculacion() {
	}
	
	@Column(name="idDesvinculacion", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.desvinculacion_IdDesvinculacion_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")		
	private int idDesvinculacion;
	
	@ManyToOne(targetEntity=Accion.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="accionId", referencedColumnName="idAccion", nullable=true) })	
	private Accion accion;
	
	@ManyToOne(targetEntity=Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaId", referencedColumnName="idPersona", nullable=false) })	
	private Persona persona;
	
	@ManyToOne(targetEntity=MotivoDesvinculacion.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="motivoDesvincId", referencedColumnName="idMotivoDesvinc", nullable=false) })	
	private MotivoDesvinculacion motivoDesvinculacion;
	
	@Column(name="descDesvinc", nullable=true, length=160)	
	private String descDesvinc;
	
	@Column(name="fechaDesvinc", nullable=false)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaDesvinc;
	
	@Column(name="estadoDesvinc", nullable=false, length=1)	
	private String estadoDesvinc;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	private void setIdDesvinculacion(int value) {
		this.idDesvinculacion = value;
	}
	
	public int getIdDesvinculacion() {
		return idDesvinculacion;
	}
	
	public int getORMID() {
		return getIdDesvinculacion();
	}
	
	public void setDescDesvinc(String value) {
		this.descDesvinc = value;
	}
	
	public String getDescDesvinc() {
		return descDesvinc;
	}
	
	public void setFechaDesvinc(java.util.Date value) {
		this.fechaDesvinc = value;
	}
	
	public java.util.Date getFechaDesvinc() {
		return fechaDesvinc;
	}
	
	public void setEstadoDesvinc(String value) {
		this.estadoDesvinc = value;
	}
	
	public String getEstadoDesvinc() {
		return estadoDesvinc;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setAccion(Accion value) {
		this.accion = value;
	}
	
	public Accion getAccion() {
		return accion;
	}
	
	public void setPersona(Persona value) {
		this.persona = value;
	}
	
	public Persona getPersona() {
		return persona;
	}
	
	public void setMotivoDesvinculacion(MotivoDesvinculacion value) {
		this.motivoDesvinculacion= value;
	}
	
	public MotivoDesvinculacion getMotivoDesvinculacion() {
		return motivoDesvinculacion;
	}
	
	public String toString() {
		return String.valueOf(getIdDesvinculacion());
	}
	
}
