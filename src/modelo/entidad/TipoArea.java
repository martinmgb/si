/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="tipoArea",schema="public")
@PrimaryKeyJoinColumn(name="IdTipoArea")
public class TipoArea implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TipoArea() {
	}
	
	@Column(name="idTipoArea", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.tipoArea_IdTipoArea_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idTipoArea;
	
	@Column(name="nombreTipoArea", nullable=false, length=30)	
	private String nombreTipoArea;
	
	@Column(name="descTipoArea", nullable=true, length=160)	
	private String descTipoArea;
	
	@Column(name="estadoTipoArea", nullable=false, length=1)	
	private String estadoTipoArea;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="tipoArea", targetEntity=Area.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set area = new java.util.HashSet();
	
	private void setIdTipoArea(int value) {
		this.idTipoArea = value;
	}
	
	public int getIdTipoArea() {
		return idTipoArea;
	}
	
	public int getORMID() {
		return getIdTipoArea();
	}
	
	public void setNombreTipoArea(String value) {
		this.nombreTipoArea = value;
	}
	
	public String getNombreTipoArea() {
		return nombreTipoArea;
	}
	
	public void setDescTipoArea(String value) {
		this.descTipoArea = value;
	}
	
	public String getDescTipoArea() {
		return descTipoArea;
	}
	
	public void setEstadoTipoArea(String value) {
		this.estadoTipoArea = value;
	}
	
	public String getEstadoTipoArea() {
		return estadoTipoArea;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setArea(java.util.Set value) {
		this.area = value;
	}
	
	public java.util.Set getArea() {
		return area;
	}
	
	
	public String toString() {
		return String.valueOf(getIdTipoArea());
	}
	
}
