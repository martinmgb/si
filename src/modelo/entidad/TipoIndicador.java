/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="tipoIndicador",schema="public")
@PrimaryKeyJoinColumn(name="IdTipoIndicador")
public class TipoIndicador implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TipoIndicador() {
	}
	
	@Column(name="idTipoIndicador", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.tipoIndicador_IdTipoIndicador_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idTipoIndicador;
	
	@Column(name="nombreTipoInd", nullable=false, length=30)	
	private String nombreTipoInd;
	
	@Column(name="descTipoInd", nullable=true, length=160)	
	private String descTipoInd;
	
	@Column(name="estadoTipoInd", nullable=false, length=1)	
	private String estadoTipoInd;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="tipoIndicador", targetEntity=Indicador.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set indicador = new java.util.HashSet();
	
	private void setIdTipoIndicador(int value) {
		this.idTipoIndicador = value;
	}
	
	public int getIdTipoIndicador() {
		return idTipoIndicador;
	}
	
	public int getORMID() {
		return getIdTipoIndicador();
	}
	
	public void setNombreTipoInd(String value) {
		this.nombreTipoInd = value;
	}
	
	public String getNombreTipoInd() {
		return nombreTipoInd;
	}
	
	public void setDescTipoInd(String value) {
		this.descTipoInd = value;
	}
	
	public String getDescTipoInd() {
		return descTipoInd;
	}
	
	public void setEstadoTipoInd(String value) {
		this.estadoTipoInd = value;
	}
	
	public String getEstadoTipoInd() {
		return estadoTipoInd;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setIndicador(java.util.Set value) {
		this.indicador = value;
	}
	
	public java.util.Set getIndicador() {
		return indicador;
	}
	
	
	public String toString() {
		return String.valueOf(getIdTipoIndicador());
	}
	
}
