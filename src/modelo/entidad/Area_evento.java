/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="area_evento",schema="public")
@PrimaryKeyJoinColumn(name="IdArea_Evento")
public class Area_evento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Area_evento() {
	}
	
	@Column(name="idArea_evento", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.area_evento_IdArea_Evento_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idArea_evento;
	
	@ManyToOne(targetEntity=Area.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="areaId", referencedColumnName="idArea", nullable=false) })	
	private Area area;
	
	@ManyToOne(targetEntity=Evento.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="eventoId", referencedColumnName="idEvento", nullable=false) })	
	private Evento evento;
	
	private void setIdArea_evento(int value) {
		this.idArea_evento = value;
	}
	
	public int getIdArea_evento() {
		return idArea_evento;
	}
	
	public int getORMID() {
		return getIdArea_evento();
	}
	
	public void setArea(Area value) {
		this.area = value;
	}
	
	public Area getArea() {
		return area;
	}
	
	public void setEvento(Evento value) {
		this.evento = value;
	}
	
	public Evento getEvento() {
		return evento;
	}
	
	public String toString() {
		return String.valueOf(getIdArea_evento());
	}
	
}
