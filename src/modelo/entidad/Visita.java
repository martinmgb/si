/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="visita",schema="public")
@PrimaryKeyJoinColumn(name="IdVisita")
public class Visita implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Visita() {
	}
	
	@Column(name="idVisita", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.visita_IdVisita_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idVisita;
	
	@ManyToOne(targetEntity=Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="PersonaId", referencedColumnName="idPersona", nullable=false) })	
	private Persona persona;
	
	@Column(name="fechaVisita", nullable=false)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaVisita;
	
	@Column(name="horaVisita", nullable=false)	
	private java.sql.Time horaVisita;
	
	@Column(name="estadoVisita", nullable=true, length=1)	
	private String estadoVisita;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="visita", targetEntity=Visita_acomp.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set visita_acomp = new java.util.HashSet();
	
	private void setIdVisita(int value) {
		this.idVisita = value;
	}
	
	public int getIdVisita() {
		return idVisita;
	}
	
	public int getORMID() {
		return getIdVisita();
	}
	
	public void setFechaVisita(java.util.Date value) {
		this.fechaVisita = value;
	}
	
	public java.util.Date getFechaVisita() {
		return fechaVisita;
	}
	
	public void setHoraVisita(java.sql.Time value) {
		this.horaVisita = value;
	}
	
	public java.sql.Time getHoraVisita() {
		return horaVisita;
	}
	
	public void setEstadoVisita(String value) {
		this.estadoVisita = value;
	}
	
	public String getEstadoVisita() {
		return estadoVisita;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setPersona(Persona value) {
		this.persona = value;
	}
	
	public Persona getPersona() {
		return persona;
	}
	
	public void setVisita_acomp(java.util.Set value) {
		this.visita_acomp = value;
	}
	
	public java.util.Set getVisita_acomp() {
		return visita_acomp;
	}
	
	
	public String toString() {
		return String.valueOf(getIdVisita());
	}
	
}
