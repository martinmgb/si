/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="cargo",schema="public")
@PrimaryKeyJoinColumn(name="IdCargo")
public class Cargo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Cargo() {
	}
	
	@Column(name="idCargo", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.cargo_IdCargo_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idCargo;
	
	@Column(name="nombreCargo", nullable=false, length=30)	
	private String nombreCargo;
	
	@Column(name="descCargo", nullable=true, length=160)	
	private String descCargo;
	
	@Column(name="estadoCargo", nullable=false, length=1)	
	private String estadoCargo;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="cargo", targetEntity=Persona.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set persona = new java.util.HashSet();
	
	private void setIdCargo(int value) {
		this.idCargo = value;
	}
	
	public int getIdCargo() {
		return idCargo;
	}
	
	public int getORMID() {
		return getIdCargo();
	}
	
	public void setNombreCargo(String value) {
		this.nombreCargo = value;
	}
	
	public String getNombreCargo() {
		return nombreCargo;
	}
	
	public void setDescCargo(String value) {
		this.descCargo = value;
	}
	
	public String getDescCargo() {
		return descCargo;
	}
	
	public void setEstadoCargo(String value) {
		this.estadoCargo = value;
	}
	
	public String getEstadoCargo() {
		return estadoCargo;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setPersona(java.util.Set value) {
		this.persona = value;
	}
	
	public java.util.Set getPersona() {
		return persona;
	}
	
	
	public String toString() {
		return String.valueOf(getIdCargo());
	}
	
}
