/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="opinion",schema="public")
@PrimaryKeyJoinColumn(name="IdOpinion")
public class Opinion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Opinion() {
	}
	
	@Column(name="idOpinion", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.opinion_IdOpinion_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idOpinion;
	
	
	@Column(name="fechaOpinion", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaOpinion;

	@ManyToOne(targetEntity=Usuario.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="usuarioId", referencedColumnName="idUsuario", nullable=true) })	
	private Usuario usuario;
	
	
	
	@Column(name="asuntoOpinion", nullable=true, length=20)	
	private String asuntoOpinion;
	
	@Column(name="descOpinion", nullable=true, length=160)	
	private String descOpinion;
	
	@Column(name="respuestaOpinion", nullable=true, length=160)	
	private String respuestaOpinion;
	
	@Column(name="estadoOpinion", nullable=false, length=1)	
	private String estadoOpinion;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	private void setIdOpinion(int value) {
		this.idOpinion = value;
	}
	
	public int getIdOpinion() {
		return idOpinion;
	}
	
	public int getORMID() {
		return getIdOpinion();
	}
	
	public void setAsuntoOpinion(String value) {
		this.asuntoOpinion = value;
	}
	
	public String getAsuntoOpinion() {
		return asuntoOpinion;
	}
	
	public void setDescOpinion(String value) {
		this.descOpinion = value;
	}
	
	public String getDescOpinion() {
		return descOpinion;
	}
	public java.util.Date getFechaOpinion() {
		return fechaOpinion;
	}

	public void setFechaOpinion(java.util.Date fechaOpinion) {
		this.fechaOpinion = fechaOpinion;
	}
	
	public void setRespuestaOpinion(String value) {
		this.respuestaOpinion = value;
	}
	
	public String getRespuestaOpinion() {
		return respuestaOpinion;
	}
	
	public void setEstadoOpinion(String value) {
		this.estadoOpinion = value;
	}
	
	public String getEstadoOpinion() {
		return estadoOpinion;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	 
	
	public void setUsuario(Usuario value) {
		this.usuario = value;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public String toString() {
		return String.valueOf(getIdOpinion());
	}
	
}
