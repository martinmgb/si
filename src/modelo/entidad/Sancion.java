/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="sancion",schema="public")
@PrimaryKeyJoinColumn(name="IdSancion")
public class Sancion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Sancion() {
	}
	
	@Column(name="idSancion", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.sancion_IdSancion_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idSancion;
	
	@ManyToOne(targetEntity=Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaId", referencedColumnName="idPersona", nullable=false) })	
	private Persona persona;
	
	@ManyToOne(targetEntity=TipoSancion.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="tipoSancionId", referencedColumnName="idTipoSancion", nullable=true) })	
	private TipoSancion tipoSancion;
	
	@Column(name="fechaSancion", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaSancion;
	
	@Column(name="fechaReactivacion", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaReactivacion;
	
 
	
	@Column(name="descSancion", nullable=true, length=160)	
	private String descSancion;
	
	@Column(name="estadoSancion", nullable=false, length=1)	
	private String estadoSancion;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	private void setIdSancion(int value) {
		this.idSancion = value;
	}
	
	public int getIdSancion() {
		return idSancion;
	}
	
	public int getORMID() {
		return getIdSancion();
	}
	
	public void setFechaSancion(java.util.Date value) {
		this.fechaSancion = value;
	}
	
	public java.util.Date getFechaSancion() {
		return fechaSancion;
	}
	
	public void setDescSancion(String value) {
		this.descSancion = value;
	}
	
	public String getDescSancion() {
		return descSancion;
	}
	
	public void setEstadoSancion(String value) {
		this.estadoSancion = value;
	}
	
	public String getEstadoSancion() {
		return estadoSancion;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setPersona(Persona value) {
		this.persona = value;
	}
	
	public Persona getPersona() {
		return persona;
	}
	
	public void setTipoSancion(TipoSancion value) {
		this.tipoSancion = value;
	}
	
	public TipoSancion getTipoSancion() {
		return tipoSancion;
	}
	
	public String toString() {
		return String.valueOf(getIdSancion());
	}

	public java.util.Date getFechaReactivacion() {
		return fechaReactivacion;
	}

	public void setFechaReactivacion(java.util.Date fechaReactivacion) {
		this.fechaReactivacion = fechaReactivacion;
	}
 	
}
