/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="area_recurso",schema="public")
@PrimaryKeyJoinColumn(name="IdArea_Recurso")
public class Area_recurso implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Area_recurso() {
	}
	
	
	public Area_recurso(long cantRecurso, Area area, Recurso recurso) {
		super();
		this.cantRecurso = cantRecurso;
		this.area = area;
		this.recurso = recurso;
	}






	@Column(name="idArea_recurso", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.area_recurso_IdArea_Recurso_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idArea_recurso;
	
	@Column(name="cantRecurso", nullable=false)	
	private long cantRecurso;
	
	@ManyToOne(targetEntity=Area.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="areaId", referencedColumnName="idArea", nullable=false) })	
	private Area area;
	
	@ManyToOne(targetEntity=Recurso.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="recursoId", referencedColumnName="idRecurso", nullable=false) })	
	private Recurso recurso;
	
	private void setIdArea_recurso(int value) {
		this.idArea_recurso = value;
	}
	
	public int getIdArea_recurso() {
		return idArea_recurso;
	}
	
	public void setCantRecurso(long value) {
		this.cantRecurso = value;
	}
	
	public long getCantRecurso() {
		return cantRecurso;
	}
	
	public int getORMID() {
		return getIdArea_recurso();
	}
	
	public void setArea(Area value) {
		this.area = value;
	}
	
	public Area getArea() {
		return area;
	}
	
	public void setRecurso(Recurso value) {
		this.recurso = value;
	}
	
	public Recurso getRecurso() {
		return recurso;
	}
	
	public String toString() {
		return String.valueOf(getIdArea_recurso());
	}
	
}
