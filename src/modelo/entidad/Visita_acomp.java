/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="visita_acomp",schema="public")
@PrimaryKeyJoinColumn(name="IdVisitaAcomp")
public class Visita_acomp implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Visita_acomp() {
	}
	
	@Column(name="idVisita_acomp", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.visita_acomp_IdVisita_Acomp_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idVisita_acomp;
	
	@ManyToOne(targetEntity=Visita.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="visitaId", referencedColumnName="idVisita", nullable=true) })	
	private Visita visita;
	
	@ManyToOne(targetEntity=Acompanante.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="acompananteId", referencedColumnName="idAcompanante", nullable=true) })	
	private Acompanante acompanante;
	
	private void setIdVisita_acomp(int value) {
		this.idVisita_acomp = value;
	}
	
	public int getIdVisita_acomp() {
		return idVisita_acomp;
	}
	
	public int getORMID() {
		return getIdVisita_acomp();
	}
	
	public void setVisita(Visita value) {
		this.visita = value;
	}
	
	public Visita getVisita() {
		return visita;
	}
	
	public void setAcompanante(Acompanante value) {
		this.acompanante = value;
	}
	
	public Acompanante getAcompanante() {
		return acompanante;
	}
	
	public String toString() {
		return String.valueOf(getIdVisita_acomp());
	}
	
}
