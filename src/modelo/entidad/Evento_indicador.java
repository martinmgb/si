/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="evento_indicador", schema="public")
@PrimaryKeyJoinColumn(name="IdEvento_Indicador")
public class Evento_indicador implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Evento_indicador() {
	}
	public Evento_indicador(Evento evento, Indicador indicador,
			double valorEspEvento_ind) {
		super();
		this.evento = evento;
		this.indicador = indicador;
		this.valorEspEvento_ind = valorEspEvento_ind;
	}

	@Column(name="idEvento_ind", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId",sequenceName="public.evento_indicador_IdEvento_Ind_seq", allocationSize=1)	
	@GeneratedValue(generator="SeqId")
	private int idEvento_ind;

	@ManyToOne(targetEntity=Evento.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="eventoId", referencedColumnName="idEvento", nullable=false) })	
	private Evento evento;

	@ManyToOne(targetEntity=Indicador.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="indicadorId", referencedColumnName="idIndicador", nullable=false) })	
	private Indicador indicador;

	@Column(name="valorEspEvento_ind", nullable=false)	
	private double valorEspEvento_ind;

	@Column(name="resultado", nullable=true)	
	private Double resultado;

	private void setIdEvento_ind(int value) {
		this.idEvento_ind = value;
	}

	public int getIdEvento_ind() {
		return idEvento_ind;
	}

	public int getORMID() {
		return getIdEvento_ind();
	}

	public void setValorEspEvento_ind(double value) {
		this.valorEspEvento_ind = value;
	}

	public double getValorEspEvento_ind() {
		return valorEspEvento_ind;
	}

	public void setResultado(double value) {
		setResultado(new Double(value));
	}

	public void setResultado(Double value) {
		this.resultado = value;
	}

	public Double getResultado() {
		return resultado;
	}

	public void setEvento(Evento value) {
		this.evento = value;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setIndicador(Indicador value) {
		this.indicador = value;
	}

	public Indicador getIndicador() {
		return indicador;
	}

	public String toString() {
		return String.valueOf(getIdEvento_ind());
	}

}
