/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="tipoNoticia",schema="public")
@PrimaryKeyJoinColumn(name="IdTipoNoticia")
public class TipoNoticia implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TipoNoticia() {
	}
	
	@Column(name="idTipoNoticia", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.tipoNoticia_IdTipoNoticia_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idTipoNoticia;
	
	@Column(name="nombreTipoNoticia", nullable=false, length=30)	
	private String nombreTipoNoticia;
	
	@Column(name="descTipoNoticia", nullable=true, length=160)	
	private String descTipoNoticia;
	
	@Column(name="estadoTipoNoticia", nullable=false, length=1)	
	private String estadoTipoNoticia;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="tipoNoticia", targetEntity=Noticia.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set noticia = new java.util.HashSet();
	
	private void setIdTipoNoticia(int value) {
		this.idTipoNoticia = value;
	}
	
	public int getIdTipoNoticia() {
		return idTipoNoticia;
	}
	
	public int getORMID() {
		return getIdTipoNoticia();
	}
	
	public void setNombreTipoNoticia(String value) {
		this.nombreTipoNoticia = value;
	}
	
	public String getNombreTipoNoticia() {
		return nombreTipoNoticia;
	}
	
	public void setDescTipoNoticia(String value) {
		this.descTipoNoticia = value;
	}
	
	public String getDescTipoNoticia() {
		return descTipoNoticia;
	}
	
	public void setEstadoTipoNoticia(String value) {
		this.estadoTipoNoticia = value;
	}
	
	public String getEstadoTipoNoticia() {
		return estadoTipoNoticia;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setNoticia(java.util.Set value) {
		this.noticia = value;
	}
	
	public java.util.Set getNoticia() {
		return noticia;
	}
	
	
	public String toString() {
		return String.valueOf(getIdTipoNoticia());
	}
	
}
