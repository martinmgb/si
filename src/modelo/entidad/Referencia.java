/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="referencia",schema="public")
@PrimaryKeyJoinColumn(name="IdReferencia")
public class Referencia implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Referencia() {
	}
	
	@Column(name="idReferencia", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.referencia_IdReferencia_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idReferencia;
	
	@ManyToOne(targetEntity=Postulacion.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="postulacionId", referencedColumnName="idPostulacion", nullable=false) })	
	private Postulacion postulacion;
	
	@ManyToOne(targetEntity=Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaId", referencedColumnName="idPersona", nullable=false) })	
	private Persona persona;
	
	@Column(name="estadoReferencia", nullable=false, length=1)	
	private String estadoReferencia;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	private void setIdReferencia(int value) {
		this.idReferencia = value;
	}
	
	public int getIdReferencia() {
		return idReferencia;
	}
	
	public int getORMID() {
		return getIdReferencia();
	}
	
	public void setEstadoReferencia(String value) {
		this.estadoReferencia = value;
	}
	
	public String getEstadoReferencia() {
		return estadoReferencia;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setPostulacion(Postulacion value) {
		this.postulacion = value;
	}
	
	public Postulacion getPostulacion() {
		return postulacion;
	}
	
	public void setPersona(Persona value) {
		this.persona = value;
	}
	
	public Persona getPersona() {
		return persona;
	}
	
	public String toString() {
		return String.valueOf(getIdReferencia());
	}
	
}
