/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
//aqui colocas el nombre de la tabla y lo del id
@Table(name="area",schema="public")
@PrimaryKeyJoinColumn(name="IdArea")
public class Area implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Area() {
	}
	
	@Column(name="idArea", nullable=false, unique=true)	
	//aqui lo de la secuencia
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.area_IdArea_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idArea;
	
	@ManyToOne(targetEntity=TipoArea.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="tipoAreaId", referencedColumnName="idTipoArea", nullable=false) })	
	private TipoArea tipoArea;
	
	@Column(name="nombreArea", nullable=false, length=30)	
	private String nombreArea;
	
	@Column(name="dimensionArea", nullable=false, length=20)	
	private String dimensionArea;
	
	@Column(name="capacidadArea", nullable=false)	
	private long capacidadArea;
	
	@Column(name="descArea", nullable=true, length=160)	
	private String descArea;
	
	@Column(name="estadoArea", nullable=false, length=1)	
	private String estadoArea;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="area", targetEntity=Area_recurso.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set area_recurso = new java.util.HashSet();
	
	@OneToMany(mappedBy="area", targetEntity=Area_evento.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set area_evento = new java.util.HashSet();
	
	@OneToMany(mappedBy="area", targetEntity=Area_arrend.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set area_arrend = new java.util.HashSet();
	
	private void setIdArea(int value) {
		this.idArea = value;
	}
	
	public int getIdArea() {
		return idArea;
	}
	
	public int getORMID() {
		return getIdArea();
	}
	
	public void setNombreArea(String value) {
		this.nombreArea = value;
	}
	
	public String getNombreArea() {
		return nombreArea;
	}
	
	public void setDimensionArea(String value) {
		this.dimensionArea = value;
	}
	
	public String getDimensionArea() {
		return dimensionArea;
	}
	
	public void setCapacidadArea(long value) {
		this.capacidadArea = value;
	}
	
	public long getCapacidadArea() {
		return capacidadArea;
	}
	
	public void setDescArea(String value) {
		this.descArea = value;
	}
	
	public String getDescArea() {
		return descArea;
	}
	
	public void setEstadoArea(String value) {
		this.estadoArea = value;
	}
	
	public String getEstadoArea() {
		return estadoArea;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setTipoArea(TipoArea value) {
		this.tipoArea = value;
	}
	
	public TipoArea getTipoArea() {
		return tipoArea;
	}
	
	public void setArea_recurso(java.util.Set value) {
		this.area_recurso = value;
	}
	
	public java.util.Set getArea_recurso() {
		return area_recurso;
	}
	
	
	public void setArea_evento(java.util.Set value) {
		this.area_evento = value;
	}
	
	public java.util.Set getArea_evento() {
		return area_evento;
	}
	
	
	public void setArea_arrend(java.util.Set value) {
		this.area_arrend = value;
	}
	
	public java.util.Set getArea_arrend() {
		return area_arrend;
	}
	
	
	public String toString() {
		return String.valueOf(getIdArea());
	}
	
}
