/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package  modelo.entidad;
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="motivo",schema="public")
@PrimaryKeyJoinColumn(name="IdMotivo")
public class Motivo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Motivo() {
	}
	
	@Column(name="idMotivo", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.motivo_IdMotivo_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idMotivo;
	
	@Column(name="nombreMotivo", nullable=false, length=30)	
	private String nombreMotivo;
	
	@Column(name="descMotivo", nullable=true, length=160)	
	private String descMotivo;
	
	@Column(name="estadoMotivo", nullable=false, length=1)	
	private String estadoMotivo;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="motivo", targetEntity=Cancelacion.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set cancelacion = new java.util.HashSet();
	
	private void setIdMotivo(int value) {
		this.idMotivo = value;
	}
	
	public int getIdMotivo() {
		return idMotivo;
	}
	
	public int getORMID() {
		return getIdMotivo();
	}
	
	public void setNombreMotivo(String value) {
		this.nombreMotivo = value;
	}
	
	public String getNombreMotivo() {
		return nombreMotivo;
	}
	
	public void setDescMotivo(String value) {
		this.descMotivo = value;
	}
	
	public String getDescMotivo() {
		return descMotivo;
	}
	
	public void setEstadoMotivo(String value) {
		this.estadoMotivo = value;
	}
	
	public String getEstadoMotivo() {
		return estadoMotivo;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setCancelacion(java.util.Set value) {
		this.cancelacion = value;
	}
	
	public java.util.Set getCancelacion() {
		return cancelacion;
	}
	
	
	public String toString() {
		return String.valueOf(getIdMotivo());
	}
	
}
