/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="preferencia_persona",schema="public")
@PrimaryKeyJoinColumn(name="IdPreferencia_Persona")
public class Preferencia_persona implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Preferencia_persona() {
	}
	
	@Column(name="idPreferencia_per", nullable=false)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.preferencia_persona_IdPreferencia_Per_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")		
	private int idPreferencia_per;
	
	@ManyToOne(targetEntity=Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaId", referencedColumnName="idPersona", nullable=false) })	
	private Persona persona;
	
	
	
	@ManyToOne(targetEntity=Preferencia.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="preferenciaId", referencedColumnName="idPreferencia", nullable=false) })	
	private Preferencia preferencia;
	

	
	private void setIdPreferencia_per(int value) {
		this.idPreferencia_per = value;
	}
	
	public int getIdPreferencia_per() {
		return idPreferencia_per;
	}
	
	public int getORMID() {
		return getIdPreferencia_per();
	}
	
	public void setPreferencia(Preferencia value) {
		this.preferencia = value;
	}
	
	public Preferencia getPreferencia() {
		return preferencia;
	}
	
	public void setPersona(Persona value) {
		this.persona = value;
	}
	
	public Persona getPersona() {
		return persona;
	}
	
	public String toString() {
		return String.valueOf(getIdPreferencia_per());
	}
	
}
