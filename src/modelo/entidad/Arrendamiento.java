/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package  modelo.entidad;
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="arrendamiento",schema="public")
@PrimaryKeyJoinColumn(name="idArrendamiento")
public class Arrendamiento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Arrendamiento() {
	}
	
	@Column(name="idArrendamiento", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.arrendamiento_IdArrendamiento_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idArrendamiento;
	
	@ManyToOne(targetEntity=Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaId", referencedColumnName="idPersona", nullable=false) })	
	private Persona persona;
	
	@ManyToOne(targetEntity=TipoArrendamiento.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="tipoArrendamientoId", referencedColumnName="idTipoArrend", nullable=false) })	
	private TipoArrendamiento tipoArrendamiento;
	
	@Column(name="fechaEmisionArrend", nullable=false)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaEmisionArrend;
	
	@Column(name="fechaArrend", nullable=false)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaArrend;
	
	@Column(name="horaArrend", nullable=false)	
	private java.sql.Time horaArrend;
	
	@Column(name="descArrend", nullable=true, length=160)	
	private String descArrend;
	
	@Column(name="estadoArrend", nullable=false, length=1)	
	private String estadoArrend;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToOne(mappedBy="arrendamiento", targetEntity=Rechazo.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	private Rechazo rechazo;
	
	@OneToMany(mappedBy="arrendamiento", targetEntity=Incidencia.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set incidencia = new java.util.HashSet();
	
	@OneToMany(mappedBy="arrendamiento", targetEntity=Area_arrend.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.FALSE)	
	private java.util.Set area_arrend = new java.util.HashSet();
	
	@OneToOne(mappedBy="arrendamiento", targetEntity=Cancelacion.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	private Cancelacion cancelacion;
	
	private void setIdArrendamiento(int value) {
		this.idArrendamiento = value;
	}
	
	public int getIdArrendamiento() {
		return idArrendamiento;
	}
	
	public int getORMID() {
		return getIdArrendamiento();
	}
	
	public void setFechaEmisionArrend(java.util.Date value) {
		this.fechaEmisionArrend = value;
	}
	
	public java.util.Date getFechaEmisionArrend() {
		return fechaEmisionArrend;
	}
	
	public void setFechaArrend(java.util.Date value) {
		this.fechaArrend = value;
	}
	
	public java.util.Date getFechaArrend() {
		return fechaArrend;
	}
	
	public void setDescArrend(String value) {
		this.descArrend = value;
	}
	
	public String getDescArrend() {
		return descArrend;
	}
	
	public void setEstadoArrend(String value) {
		this.estadoArrend = value;
	}
	
	public String getEstadoArrend() {
		return estadoArrend;
	}
	
	public void setHoraArrend(java.sql.Time value) {
		this.horaArrend = value;
	}
	
	public java.sql.Time getHoraArrend() {
		return horaArrend;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setPersona(Persona value) {
		this.persona = value;
	}
	
	public Persona getPersona() {
		return persona;
	}
	
	public void setTipoArrendamiento(TipoArrendamiento value) {
		this.tipoArrendamiento = value;
	}
	
	public TipoArrendamiento getTipoArrendamiento() {
		return tipoArrendamiento;
	}
	
	public void setRechazo(Rechazo value) {
		this.rechazo = value;
	}
	
	public Rechazo getRechazo() {
		return rechazo;
	}
	
	public void setIncidencia(java.util.Set value) {
		this.incidencia = value;
	}
	
	public java.util.Set getIncidencia() {
		return incidencia;
	}
	
	
	public void setArea_arrend(java.util.Set value) {
		this.area_arrend = value;
	}
	
	public java.util.Set getArea_arrend() {
		return area_arrend;
	}
	
	
	public void setCancelacion(Cancelacion value) {
		this.cancelacion = value;
	}
	
	public Cancelacion getCancelacion() {
		return cancelacion;
	}
	
	public String toString() {
		return String.valueOf(getIdArrendamiento());
	}
	
}
