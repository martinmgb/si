/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="notificacion",schema="public")
@PrimaryKeyJoinColumn(name="IdNotificacion")
public class Notificacion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Notificacion() {
	}
	
	@Column(name="idNotificacion", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.notificacion_IdNotificacion_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idNotificacion;
	
	@ManyToOne(targetEntity=TipoNotificacion.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="tipoNotificacionId", referencedColumnName="idTipoNotif", nullable=false) })	
	private TipoNotificacion tipoNotificacion;
	
	@ManyToOne(targetEntity=Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaId", referencedColumnName="idPersona", nullable=false) })	
	private Persona persona;
	
	@Column(name="asuntoNotificacion", nullable=false, length=160)	
	private String asuntoNotificacion;
	
	@Column(name="estadoNotificacion", nullable=false, length=1)	
	private String estadoNotificacion;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	private void setIdNotificacion(int value) {
		this.idNotificacion = value;
	}
	
	public int getIdNotificacion() {
		return idNotificacion;
	}
	
	public int getORMID() {
		return getIdNotificacion();
	}
	
	public void setAsuntoNotificacion(String value) {
		this.asuntoNotificacion = value;
	}
	
	public String getAsuntoNotificacion() {
		return asuntoNotificacion;
	}
	
	public void setEstadoNotificacion(String value) {
		this.estadoNotificacion = value;
	}
	
	public String getEstadoNotificacion() {
		return estadoNotificacion;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setPersona(Persona value) {
		this.persona = value;
	}
	
	public Persona getPersona() {
		return persona;
	}
	
	public void setTipoNotificacion(TipoNotificacion value) {
		this.tipoNotificacion = value;
	}
	
	public TipoNotificacion getTipoNotificacion() {
		return tipoNotificacion;
	}
	
	public String toString() {
		return String.valueOf(getIdNotificacion());
	}
	
}
