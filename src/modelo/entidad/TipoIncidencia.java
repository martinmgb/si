/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="tipoIncidencia",schema="public")
@PrimaryKeyJoinColumn(name="IdTipoIncidencia")
public class TipoIncidencia implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TipoIncidencia() {
	}
	
	@Column(name="idTipoIncid", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.tipoIncidencia_IdTipoIncid_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idTipoIncid;
	
	@Column(name="nombreTipoIncid", nullable=false, length=30)	
	private String nombreTipoIncid;
	
	@Column(name="descTipoIncid", nullable=true, length=160)	
	private String descTipoIncid;
	
	@Column(name="estadoTipoIncid", nullable=false, length=1)	
	private String estadoTipoIncid;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="tipoIncidencia", targetEntity=Incidencia.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set incidencia = new java.util.HashSet();
	
	public void setIdTipoIncid(int value) {
		this.idTipoIncid = value;
	}
	
	public int getIdTipoIncid() {
		return idTipoIncid;
	}
	
	public int getORMID() {
		return getIdTipoIncid();
	}
	
	public void setNombreTipoIncid(String value) {
		this.nombreTipoIncid = value;
	}
	
	public String getNombreTipoIncid() {
		return nombreTipoIncid;
	}
	
	public void setDescTipoIncid(String value) {
		this.descTipoIncid = value;
	}
	
	public String getDescTipoIncid() {
		return descTipoIncid;
	}
	
	public void setEstadoTipoIncid(String value) {
		this.estadoTipoIncid = value;
	}
	
	public String getEstadoTipoIncid() {
		return estadoTipoIncid;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setIncidencia(java.util.Set value) {
		this.incidencia = value;
	}
	
	public java.util.Set getIncidencia() {
		return incidencia;
	}
	
	
	public String toString() {
		return String.valueOf(getIdTipoIncid());
	}
	
}
