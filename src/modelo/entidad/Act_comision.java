/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="act_comision",schema="public")
@PrimaryKeyJoinColumn(name="IdAct_Comision")
public class Act_comision implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Act_comision() {
	}
	
	@Column(name="idAct_comision", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.act_comision_IdAct_Comision_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idAct_comision;
	
	@ManyToOne(targetEntity=Comision.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="comisionId", referencedColumnName="idComision", nullable=false) })	
	private Comision comision;
	
	@ManyToOne(targetEntity=Actividad.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="actividadId", referencedColumnName="idActividad", nullable=false) })	
	private Actividad actividad;
	
	private void setIdAct_comision(int value) {
		this.idAct_comision = value;
	}
	
	public int getIdAct_comision() {
		return idAct_comision;
	}
	
	public int getORMID() {
		return getIdAct_comision();
	}
	
	public void setComision(Comision value) {
		this.comision = value;
	}
	
	public Comision getComision() {
		return comision;
	}
	
	public void setActividad(Actividad value) {
		this.actividad = value;
	}
	
	public Actividad getActividad() {
		return actividad;
	}
	
	public String toString() {
		return String.valueOf(getIdAct_comision());
	}
	
}
