/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="postulacion",schema="public")
@PrimaryKeyJoinColumn(name="IdPostulacion")
public class Postulacion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Postulacion() {
	}
	
	@Column(name="idPostulacion", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.postulacion_IdPostulacion_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idPostulacion;
	
	@ManyToOne(targetEntity=Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaId", referencedColumnName="idPersona", nullable=false) })	
	private Persona persona;
	
	@Column(name="fechaPostulacion", nullable=false)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaPostulacion;
	
	@Column(name="estadoPostulacion", nullable=false, length=1)	
	private String estadoPostulacion;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToOne(mappedBy="postulacion", targetEntity=Rechazo.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	private Rechazo rechazo;
	
	
	@OneToMany(mappedBy="postulacion", targetEntity=Referencia.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set referencia = new java.util.HashSet();
	
	
	public void setIdPostulacion(int value) {
		this.idPostulacion = value;
	}
	
	public int getIdPostulacion() {
		return idPostulacion;
	}
	
	public int getORMID() {
		return getIdPostulacion();
	}
	
	public void setFechaPostulacion(java.util.Date value) {
		this.fechaPostulacion = value;
	}
	
	public java.util.Date getFechaPostulacion() {
		return fechaPostulacion;
	}
	
	public void setEstadoPostulacion(String value) {
		this.estadoPostulacion = value;
	}
	
	public String getEstadoPostulacion() {
		return estadoPostulacion;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setPersona(Persona value) {
		this.persona = value;
	}
	
	public Persona getPersona() {
		return persona;
	}
	
	public void setRechazo(Rechazo value) {
		this.rechazo = value;
	}
	
	public Rechazo getRechazo() {
		return rechazo;
	}
	
	 
	public void setReferencia(java.util.Set value) {
		this.referencia = value;
	}
	
	public java.util.Set getReferencia() {
		return referencia;
	}
	
	
	public String toString() {
		return String.valueOf(getIdPostulacion());
	}
	
}
