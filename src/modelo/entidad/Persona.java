/**

 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Type;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="persona", schema="public")
@PrimaryKeyJoinColumn(name="IdPersona")
public class Persona implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Persona() {
	}

	@Column(name="idPersona",nullable=false, unique=true)	
	@Id
	@SequenceGenerator(name="SeqId", sequenceName="public.persona_IdPersona_seq", allocationSize=1)	
	@GeneratedValue(generator="SeqId")
	private int idPersona;


	@ManyToOne(targetEntity=TipoPersona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="tipoPersonaId", referencedColumnName="idTipoPersona", nullable=false) })	
	private TipoPersona tipoPersona;




	@Column(name="idPadrePersona", nullable=true)	
	private Integer idPadrePersona;


	@Column(name="cedPersona", nullable=false, length=10)	
	private String cedPersona;

	@Column(name="nombrePersona", nullable=false, length=30)	
	private String nombrePersona;

	@Column(name="apellidoPersona", nullable=false, length=30)	
	private String apellidoPersona;

	@Column(name="sexoPersona", nullable=true, length=1)	
	private String sexoPersona;

	@Column(name="fechaNacPersona", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaNacPersona;

	@Column(name="lugarNacPersona", nullable=true, length=20)	
	private String lugarNacPersona;

	@Column(name="estadoCivilPersona", nullable=true, length=10)	
	private String estadoCivilPersona;

	@Column(name="tlfCasaPersona", nullable=true, length=20)	
	private String tlfCasaPersona;

	@Column(name="tlfCelPersona", nullable=true, length=20)	
	private String tlfCelPersona;

	@Column(name="direcPersona", nullable=true, length=50)	
	private String direcPersona;

	@Column(name="correoPersona", nullable=true, length=40)	
	private String correoPersona;

	@Column(name="profesionPersona", nullable=true, length=30)	
	private String profesionPersona;

	@Column(name="cargoPersona", nullable=true, length=30)	
	private String cargoPersona;

	@Column(name="sueldoPersona", nullable=true)	
	private Float sueldoPersona;

	@Column(name="direcTrabPersona", nullable=true, length=50)	
	private String direcTrabPersona;

	@Column(name="tlfTrabPersona", nullable=true, length=20)	
	private String tlfTrabPersona;

	@Column(name="imagenPersona", nullable=true, length=100)
	@Type(type="org.hibernate.type.BinaryType") 
	private byte[] imagenPersona;

	@ManyToOne(targetEntity=Cargo.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="cargoId", referencedColumnName="idCargo") })	
	private Cargo cargo;

	@Column(name="estadoPersona", nullable=false, length=1)	
	private String estadoPersona;   

	@Column(name="empresaTrabPersona", nullable=true, length=30)	
	private String empresaTrabPersona;

	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;

	@OneToMany(mappedBy="persona", targetEntity=Visita.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set visita = new java.util.HashSet();

	@OneToMany(mappedBy="persona", targetEntity=Arrendamiento.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set arrendamiento = new java.util.HashSet();

	@OneToMany(mappedBy="persona", targetEntity=Incidencia.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set incidencia = new java.util.HashSet();

	@OneToMany(mappedBy="persona", targetEntity=Sancion.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set sancion = new java.util.HashSet();


	@OneToMany(mappedBy="persona", targetEntity=Postulacion.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set postulacion = new java.util.HashSet();

	@OneToMany(mappedBy="persona", targetEntity=Preferencia_persona.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.FALSE)	
	private java.util.Set preferencia_persona = new java.util.HashSet();


	@OneToMany(mappedBy="persona", targetEntity=Notificacion.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set notificacion = new java.util.HashSet();


	@OneToMany(mappedBy="personai", targetEntity=Accion.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.FALSE)	
	private java.util.Set accion = new java.util.HashSet();


	@OneToOne(mappedBy="persona", targetEntity=Usuario.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	private Usuario usuario;

	@ManyToOne(targetEntity=Parentesco.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="parentescoId", referencedColumnName="idParentesco", nullable=true) })	
	private Parentesco parentesco;

	@OneToMany(mappedBy="persona", targetEntity=Desvinculacion.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set desvinculacion = new java.util.HashSet();

	@OneToMany(mappedBy="persona", targetEntity=Referencia.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set referencia = new java.util.HashSet();


	private void setIdPersona(int value) {
		this.idPersona = value;
	}

	public int getIdPersona() {
		return idPersona;
	}

	public int getORMID() {
		return getIdPersona();
	}

	public void setCedPersona(String value) {
		this.cedPersona = value;
	}

	public String getCedPersona() {
		return cedPersona;
	}

	public void setNombrePersona(String value) {
		this.nombrePersona = value;
	}

	public String getNombrePersona() {
		return nombrePersona;
	}

	public void setApellidoPersona(String value) {
		this.apellidoPersona = value;
	}

	public String getApellidoPersona() {
		return apellidoPersona;
	}

	public void setSexoPersona(String value) {
		this.sexoPersona = value;
	}

	public String getSexoPersona() {
		return sexoPersona;
	}

	public void setFechaNacPersona(java.util.Date value) {
		this.fechaNacPersona = value;
	}

	public java.util.Date getFechaNacPersona() {
		return fechaNacPersona;
	}

	public void setLugarNacPersona(String value) {
		this.lugarNacPersona = value;
	}

	public String getLugarNacPersona() {
		return lugarNacPersona;
	}

	public void setDirecPersona(String value) {
		this.direcPersona = value;
	}

	public String getDirecPersona() {
		return direcPersona;
	}

	public void setTlfCasaPersona(String value) {
		this.tlfCasaPersona = value;
	}

	public String getTlfCasaPersona() {
		return tlfCasaPersona;
	}

	public void setTlfCelPersona(String value) {
		this.tlfCelPersona = value;
	}

	public String getTlfCelPersona() {
		return tlfCelPersona;
	}

	public void setEstadoCivilPersona(String value) {
		this.estadoCivilPersona = value;
	}

	public String getEstadoCivilPersona() {
		return estadoCivilPersona;
	}

	public void setCorreoPersona(String value) {
		this.correoPersona = value;
	}

	public String getCorreoPersona() {
		return correoPersona;
	}

	public void setProfesionPersona(String value) {
		this.profesionPersona = value;
	}

	public String getProfesionPersona() {
		return profesionPersona;
	}

	public void setCargoPersona(String value) {
		this.cargoPersona = value;
	}

	public String getCargoPersona() {
		return cargoPersona;
	}

	public void setDirecTrabPersona(String value) {
		this.direcTrabPersona = value;
	}

	public String getDirecTrabPersona() {
		return direcTrabPersona;
	}

	public void setTlfTrabPersona(String value) {
		this.tlfTrabPersona = value;
	}

	public String getTlfTrabPersona() {
		return tlfTrabPersona;
	}

	public void setSueldoPersona(float value) {
		setSueldoPersona(new Float(value));
	}

	public void setSueldoPersona(Float value) {
		this.sueldoPersona = value;
	}

	public Float getSueldoPersona() {
		return sueldoPersona;
	}

	public void setImagenPersona(byte[] value) {
		this.imagenPersona = value;
	}

	public byte[] getImagenPersona() {
		return imagenPersona;
	}

	public void setEstadoPersona(String value) {
		this.estadoPersona = value;
	}

	public String getEstadoPersona() {
		return estadoPersona;
	}

	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}

	public boolean getEditingStatus() {
		return editingStatus;
	}

	public void setTipoPersona(TipoPersona value) {
		this.tipoPersona = value;
	}

	public TipoPersona getTipoPersona() {
		return tipoPersona;
	}

	public void setIdPadrePersona(int value) {
		setIdPadrePersona(new Integer(value));
	}

	public void setIdPadrePersona(Integer value) {
		this.idPadrePersona = value;
	}

	public void setCargo(Cargo value) {
		this.cargo = value;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setVisita(java.util.Set value) {
		this.visita = value;
	}

	public java.util.Set getVisita() {
		return visita;
	}


	public void setArrendamiento(java.util.Set value) {
		this.arrendamiento = value;
	}

	public java.util.Set getArrendamiento() {
		return arrendamiento;
	}


	public void setIncidencia(java.util.Set value) {
		this.incidencia = value;
	}

	public java.util.Set getIncidencia() {
		return incidencia;
	}


	public void setSancion(java.util.Set value) {
		this.sancion = value;
	}

	public java.util.Set getSancion() {
		return sancion;
	}


	public void setPostulacion(java.util.Set value) {
		this.postulacion = value;
	}

	public java.util.Set getPostulacion() {
		return postulacion;
	}


	public void setPreferencia_persona(java.util.Set value) {
		this.preferencia_persona = value;
	}

	public java.util.Set getPreferencia_persona() {
		return preferencia_persona;
	}


	public void setNotificacion(java.util.Set value) {
		this.notificacion = value;
	}

	public java.util.Set getNotificacion() {
		return notificacion;
	}

	public void setParentesco(Parentesco value) {
		this.parentesco = value;
	}

	public Parentesco getParentesco() {
		return parentesco;
	}


	public void setUsuario(Usuario value) {
		this.usuario = value;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	public void setDesvinculacion(java.util.Set value) {
		this.desvinculacion = value;
	}

	public java.util.Set getDesvinculacion() {
		return desvinculacion;
	}

	public String toString() {
		return String.valueOf(getIdPersona());
	}
	public void setReferencia(java.util.Set value) {
		this.referencia = value;
	}

	public java.util.Set getReferencia() {
		return referencia;
	}

	public String getEmpresaTrabPersona() {
		return empresaTrabPersona;
	}

	public void setEmpresaTrabPersona(String empresaTrabPersona) {
		this.empresaTrabPersona = empresaTrabPersona;
	}

}
