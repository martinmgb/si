/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="preferencia_evento",schema="public")
@PrimaryKeyJoinColumn(name="IdPreferencia_Eve")
public class Preferencia_evento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Preferencia_evento() {
	}
	
	@Column(name="idPreferencia_eve", nullable=false)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.preferencia_evento_IdPreferencia_Eve_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idPreferencia_eve;
	
	@ManyToOne(targetEntity=Preferencia.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="preferenciaId", referencedColumnName="idPreferencia", nullable=false) })	
	private Preferencia preferencia;
	
	@ManyToOne(targetEntity=Evento.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="eventoId", referencedColumnName="idEvento", nullable=false) })	
	private Evento evento;
	
	private void setIdPreferencia_eve(int value) {
		this.idPreferencia_eve = value;
	}
	
	public int getIdPreferencia_eve() {
		return idPreferencia_eve;
	}
	
	public int getORMID() {
		return getIdPreferencia_eve();
	}
	
	public void setPreferencia(Preferencia value) {
		this.preferencia = value;
	}
	
	public Preferencia getPreferencia() {
		return preferencia;
	}
	
	public void setEvento(Evento value) {
		this.evento = value;
	}
	
	public Evento getEvento() {
		return evento;
	}
	
	public String toString() {
		return String.valueOf(getIdPreferencia_eve());
	}

}
