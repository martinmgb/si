/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="unidadMedida",schema="public")
@PrimaryKeyJoinColumn(name="IdMedida")
public class UnidadMedida implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnidadMedida() {
	}
	
	@Column(name="idUnidadMedida", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.unidadMedida_IdUnidadMedida_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")		
	private int idUnidadMedida;
	
	@Column(name="nombreUnidadMedida", nullable=false, length=30)	
	private String nombreUnidadMedida;
	
	@Column(name="descUnidadMedida", nullable=true, length=160)	
	private String descUnidadMedida;
	
	@Column(name="estadoUnidadMedida", nullable=false, length=1)	
	private String estadoUnidadMedida;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="unidadMedida", targetEntity=Indicador.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set indicador = new java.util.HashSet();
	
	private void setIdUnidadMedida(int value) {
		this.idUnidadMedida = value;
	}
	
	public int getIdUnidadMedida() {
		return idUnidadMedida;
	}
	
	public int getORMID() {
		return getIdUnidadMedida();
	}
	
	public void setNombreUnidadMedida(String value) {
		this.nombreUnidadMedida = value;
	}
	
	public String getNombreUnidadMedida() {
		return nombreUnidadMedida;
	}
	
	public void setDescUnidadMedida(String value) {
		this.descUnidadMedida = value;
	}
	
	public String getDescUnidadMedida() {
		return descUnidadMedida;
	}
	
	public void setEstadoUnidadMedida(String value) {
		this.estadoUnidadMedida = value;
	}
	
	public String getEstadoUnidadMedida() {
		return estadoUnidadMedida;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setIndicador(java.util.Set value) {
		this.indicador = value;
	}
	
	public java.util.Set getIndicador() {
		return indicador;
	}
	
	
	public String toString() {
		return String.valueOf(getIdUnidadMedida());
	}
	
}
