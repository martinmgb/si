/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="opcionPregunta",schema="public")
@PrimaryKeyJoinColumn(name="IdOpcionPregunta")
public class OpcionPregunta implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OpcionPregunta() {
	}
	
	@Column(name="idOpcionPregunta", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.opcionPregunta_IdOpcionPregunta_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idOpcionPregunta;
	
	@Column(name="nombreOpcion", nullable=false, length=30)	
	private String nombreOpcion;
	
	@Column(name="estadoOpcionPreg", nullable=false, length=1)	
	private String estadoOpcionPreg;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="opcionPregunta", targetEntity=RespuestaEncuesta.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set respuestaEncuesta = new java.util.HashSet();
	
	@OneToMany(mappedBy="opcionPregunta", targetEntity=Pregunta.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set pregunta = new java.util.HashSet();
	
	private void setIdOpcionPregunta(int value) {
		this.idOpcionPregunta = value;
	}
	
	public int getIdOpcionPregunta() {
		return idOpcionPregunta;
	}
	
	public int getORMID() {
		return getIdOpcionPregunta();
	}
	
	public void setNombreOpcion(String value) {
		this.nombreOpcion = value;
	}
	
	public String getNombreOpcion() {
		return nombreOpcion;
	}
	
	public void setEstadoOpcionPreg(String value) {
		this.estadoOpcionPreg = value;
	}
	
	public String getEstadoOpcionPreg() {
		return estadoOpcionPreg;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setRespuestaEncuesta(java.util.Set value) {
		this.respuestaEncuesta = value;
	}
	
	public java.util.Set getRespuestaEncuesta() {
		return respuestaEncuesta;
	}
	
	
	public void setPregunta(java.util.Set value) {
		this.pregunta = value;
	}
	
	public java.util.Set getPregunta() {
		return pregunta;
	}
	
	
	public String toString() {
		return String.valueOf(getIdOpcionPregunta());
	}
	
}
