/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="incidencia",schema="public")
@PrimaryKeyJoinColumn(name="IdIncidencia")
public class Incidencia implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Incidencia() {
	}
	
	@Column(name="idIncidencia", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.incidencia_IdIncidencia_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idIncidencia;
	
	@ManyToOne(targetEntity=Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaId", referencedColumnName="idPersona", nullable=true) })	
	private Persona persona;
	
	@ManyToOne(targetEntity=TipoIncidencia.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="tipoIncidenciaId", referencedColumnName="idTipoIncid", nullable=false) })	
	private TipoIncidencia tipoIncidencia;
	
	@Column(name="fechaIncidencia", nullable=false)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaIncidencia;
	
	@Column(name="horaIncidencia", nullable=false)	
	private java.sql.Time horaIncidencia;
	
	@Column(name="descIncidencia", nullable=true, length=160)	
	private String descIncidencia;
	
	@Column(name="estadoIncidencia", nullable=false, length=1)	
	private String estadoIncidencia;
	
	@ManyToOne(targetEntity=Arrendamiento.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="arrendamientoId", referencedColumnName="idArrendamiento", nullable=true) })	
	private Arrendamiento arrendamiento;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	private void setIdIncidencia(int value) {
		this.idIncidencia = value;
	}
	
	public int getIdIncidencia() {
		return idIncidencia;
	}
	
	public int getORMID() {
		return getIdIncidencia();
	}
	
	public void setFechaIncidencia(java.util.Date value) {
		this.fechaIncidencia = value;
	}
	
	public java.util.Date getFechaIncidencia() {
		return fechaIncidencia;
	}
	
	public void setHoraIncidencia(java.sql.Time value) {
		this.horaIncidencia = value;
	}
	
	public java.sql.Time getHoraIncidencia() {
		return horaIncidencia;
	}
	
	public void setDescIncidencia(String value) {
		this.descIncidencia = value;
	}
	
	public String getDescIncidencia() {
		return descIncidencia;
	}
	
	public void setEstadoIncidencia(String value) {
		this.estadoIncidencia = value;
	}
	
	public String getEstadoIncidencia() {
		return estadoIncidencia;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setTipoIncidencia(TipoIncidencia value) {
		this.tipoIncidencia = value;
	}
	
	public TipoIncidencia getTipoIncidencia() {
		return tipoIncidencia;
	}
	
	public void setArrendamiento(Arrendamiento value) {
		this.arrendamiento = value;
	}
	
	public Arrendamiento getArrendamiento() {
		return arrendamiento;
	}
	
	public void setPersona(Persona value) {
		this.persona = value;
	}
	
	public Persona getPersona() {
		return persona;
	}
	
	public String toString() {
		return String.valueOf(getIdIncidencia());
	}
	
}
