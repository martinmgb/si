/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="categoria",schema="public")
@PrimaryKeyJoinColumn(name="IdCategoria")
public class Categoria implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Categoria() {
	}
	
	@Column(name="idCategoria", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.categoria_IdCategoria_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idCategoria;
	
	@Column(name="nombreCategoria", nullable=false, length=30)	
	private String nombreCategoria;
	
	@Column(name="descCategoria", nullable=true, length=160)	
	private String descCategoria;
	
	@Column(name="estadoCategoria", nullable=false, length=1)	
	private String estadoCategoria;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="categoria", targetEntity=Pregunta.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set pregunta = new java.util.HashSet();
	
	@OneToMany(mappedBy="categoria", targetEntity=Preferencia.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set preferencia = new java.util.HashSet();
	
	public void setIdCategoria(int value) {
		this.idCategoria = value;
	}
	
	public int getIdCategoria() {
		return idCategoria;
	}
	
	public int getORMID() {
		return getIdCategoria();
	}
	
	public void setNombreCategoria(String value) {
		this.nombreCategoria = value;
	}
	
	public String getNombreCategoria() {
		return nombreCategoria;
	}
	
	public void setDescCategoria(String value) {
		this.descCategoria = value;
	}
	
	public String getDescCategoria() {
		return descCategoria;
	}
	
	public void setEstadoCategoria(String value) {
		this.estadoCategoria = value;
	}
	
	public String getEstadoCategoria() {
		return estadoCategoria;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setPregunta(java.util.Set value) {
		this.pregunta = value;
	}
	
	public java.util.Set getPregunta() {
		return pregunta;
	}
	
	
	public void setPreferencia(java.util.Set value) {
		this.preferencia = value;
	}
	
	public java.util.Set getPreferencia() {
		return preferencia;
	}
	
	
	public String toString() {
		return String.valueOf(getIdCategoria());
	}
	
}
