/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="comision",schema="public")
@PrimaryKeyJoinColumn(name="IdComision")
public class Comision implements Serializable {

	public Comision( String nombre, String desc, String estado) {
		super();
		
		this.nombreComision = nombre;
		this.descComision = desc;
		this.estadoComision = estado;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Comision() {
	}
	
	@Column(name="idComision", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.comision_IdComision_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idComision;
	
	@Column(name="nombreComision", nullable=false, length=30)	
	private String nombreComision;
	
	@Column(name="descComision", nullable=true, length=160)	
	private String descComision;
	
	@Column(name="estadoComision", nullable=false, length=1)	
	private String estadoComision;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="comision", targetEntity=Evento_comision.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set evento_comision = new java.util.HashSet();
	
	@OneToMany(mappedBy="comision", targetEntity=Act_comision.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.FALSE)	
	private java.util.Set act_comision = new java.util.HashSet();
	
	private void setIdComision(int value) {
		this.idComision = value;
	}
	
	public int getIdComision() {
		return idComision;
	}
	
	public int getORMID() {
		return getIdComision();
	}
	
	public void setNombreComision(String value) {
		this.nombreComision = value;
	}
	
	public String getNombreComision() {
		return nombreComision;
	}
	
	public void setDescComision(String value) {
		this.descComision = value;
	}
	
	public String getDescComision() {
		return descComision;
	}
	
	public void setEstadoComision(String value) {
		this.estadoComision = value;
	}
	
	public String getEstadoComision() {
		return estadoComision;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setEvento_comision(java.util.Set value) {
		this.evento_comision = value;
	}
	
	public java.util.Set getEvento_comision() {
		return evento_comision;
	}
	
	
	public void setAct_comision(java.util.Set value) {
		this.act_comision = value;
	}
	
	public java.util.Set getAct_comision() {
		return act_comision;
	}
	
	
	public String toString() {
		return String.valueOf(getIdComision());
	}
	
	
}
