/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.IOException;
import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Type;
import org.zkoss.image.AImage;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="galeria",schema="public")
@PrimaryKeyJoinColumn(name="IdGaleria")
public class Galeria implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Galeria() {
	}

	@Column(name="idGaleria", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.galeria_IdGaleria_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")		
	private int idGaleria;

	@Column(name="nombreGaleria", nullable=false, length=30)	
	private String nombreGaleria;

	@Column(name="imagenGaleria", nullable=true, length=100)
	@Type(type="org.hibernate.type.BinaryType") 
	private byte[] imagenGaleria;

	@ManyToOne(targetEntity=Club.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="clubId", referencedColumnName="idClub", nullable=false) })	
	private Club club;

	@Column(name="estadoGaleria", nullable=false, length=1)	
	private String estadoGaleria;

	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;

	private void setIdGaleria(int value) {
		this.idGaleria = value;
	}

	public int getIdGaleria() {
		return idGaleria;
	}

	public int getORMID() {
		return getIdGaleria();
	}

	public void setNombreGaleria(String value) {
		this.nombreGaleria = value;
	}

	public String getNombreGaleria() {
		return nombreGaleria;
	}

	public void setImagenGaleria(byte[] value) {
		this.imagenGaleria = value;
	}

	public byte[] getImagenGaleria() {
		return imagenGaleria;
	}
	
	public AImage getImagen() throws IOException {
		AImage i =  new AImage("",imagenGaleria);
		return i;
	}

	public void setEstadoGaleria(String value) {
		this.estadoGaleria = value;
	}

	public String getEstadoGaleria() {
		return estadoGaleria;
	}

	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}

	public boolean getEditingStatus() {
		return editingStatus;
	}

	public void setClub(Club value) {
		this.club = value;
	}

	public Club getClub() {
		return club;
	}

	public String toString() {
		return String.valueOf(getIdGaleria());
	}

}
