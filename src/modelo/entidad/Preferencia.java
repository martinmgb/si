/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="preferencia",schema="public")
@PrimaryKeyJoinColumn(name="IdPreferencia")
public class Preferencia implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Preferencia() {
	}
	
	@Column(name="idPreferencia", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.preferencia_IdPreferencia_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idPreferencia;
	
	@ManyToOne(targetEntity=Categoria.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="categoriaId", referencedColumnName="idCategoria", nullable=false) })	
	private Categoria categoria;
	
	@Column(name="nombrePreferencia", nullable=false, length=30)	
	private String nombrePreferencia;
	
	@Column(name="descPreferencia", nullable=true, length=160)	
	private String descPreferencia;
	
	@Column(name="estadoPreferencia", nullable=false, length=1)	
	private String estadoPreferencia;
	
	@OneToMany(mappedBy="preferencia", targetEntity=Preferencia_persona.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set preferencia_persona = new java.util.HashSet();
	
	@OneToMany(mappedBy="preferencia", targetEntity=Preferencia_evento.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set preferencia_evento = new java.util.HashSet();
	
	public void setIdPreferencia(int value) {
		this.idPreferencia = value;
	}
	
	public int getIdPreferencia() {
		return idPreferencia;
	}
	
	public int getORMID() {
		return getIdPreferencia();
	}
	
	public void setNombrePreferencia(String value) {
		this.nombrePreferencia = value;
	}
	
	public String getNombrePreferencia() {
		return nombrePreferencia;
	}
	
	public void setDescPreferencia(String value) {
		this.descPreferencia = value;
	}
	
	public String getDescPreferencia() {
		return descPreferencia;
	}
	
	public void setEstadoPreferencia(String value) {
		this.estadoPreferencia = value;
	}
	
	public String getEstadoPreferencia() {
		return estadoPreferencia;
	}
	
	public void setCategoria(Categoria value) {
		this.categoria = value;
	}
	
	public Categoria getCategoria() {
		return categoria;
	}
	
	public void setPreferencia_persona(java.util.Set value) {
		this.preferencia_persona = value;
	}
	
	public java.util.Set getPreferencia_persona() {
		return preferencia_persona;
	}
	
	


	public void setPreferencia_evento(java.util.Set value) {
		this.preferencia_evento = value;
	}
	
	public java.util.Set getPreferencia_evento() {
		return preferencia_evento;
	}
	
	
	//public String toString() {
		//return String.valueOf(getIdPreferencia());
//	}

	
	public String toString(){
		return nombrePreferencia ;
		}
}
