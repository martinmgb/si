/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="motivoDesvinculacion",schema="public")
@PrimaryKeyJoinColumn(name="IdMotivoDesvinc")
public class MotivoDesvinculacion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MotivoDesvinculacion() {
	}
	
	@Column(name="idMotivoDesvinc", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.motivoDesvinculacion_IdMotivoDesvinc_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")		
	private int idMotivoDesvinc;
	
	@Column(name="nombreMotivoDesvinc", nullable=false, length=30)	
	private String nombreMotivoDesvinc;
	
	@Column(name="descMotivoDesvinc", nullable=true, length=160)	
	private String descMotivoDesvinc;
	
	@Column(name="estadoMotivoDesvinc", nullable=false, length=1)	
	private String estadoMotivoDesvinc;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus;
	
	@OneToMany(mappedBy="motivoDesvinculacion", targetEntity=Desvinculacion.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set desvinculacion = new java.util.HashSet();
	
	private void setIdMotivoDesvinc(int value) {
		this.idMotivoDesvinc = value;
	}
	
	public int getIdMotivoDesvinc() {
		return idMotivoDesvinc;
	}
	
	public int getORMID() {
		return getIdMotivoDesvinc();
	}
	
	public void setNombreMotivoDesvinc(String value) {
		this.nombreMotivoDesvinc = value;
	}
	
	public String getNombreMotivoDesvinc() {
		return nombreMotivoDesvinc;
	}
	
	public void setDescMotivoDesvinc(String value) {
		this.descMotivoDesvinc = value;
	}
	
	public String getDescMotivoDesvinc() {
		return descMotivoDesvinc;
	}
	
	public void setEstadoMotivoDesvinc(String value) {
		this.estadoMotivoDesvinc = value;
	}
	
	public String getEstadoMotivoDesvinc() {
		return estadoMotivoDesvinc;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setDesvinculacion(java.util.Set value) {
		this.desvinculacion = value;
	}
	
	public java.util.Set getDesvinculacion() {
		return desvinculacion;
	}
	
	
	public String toString() {
		return String.valueOf(getIdMotivoDesvinc());
	}
	
}
