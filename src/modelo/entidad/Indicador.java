/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */

package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="indicador",schema="public")
@PrimaryKeyJoinColumn(name="IdIndicador")
public class Indicador implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Indicador() {
	}
	
	@Column(name="idIndicador", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.indicador_IdIndicador_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idIndicador;
	
	@ManyToOne(targetEntity=TipoIndicador.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="tipoIndicadorId", referencedColumnName="idTipoIndicador", nullable=false) })	
	private TipoIndicador tipoIndicador;
	
	@Column(name="nombreIndicador", nullable=false, length=30)	
	private String nombreIndicador;
	
	@ManyToOne(targetEntity=UnidadMedida.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="unidadMedidaId", referencedColumnName="idUnidadMedida", nullable=false) })	
	private UnidadMedida unidadMedida;
	
	@Column(name="descIndicador", nullable=true, length=160)	
	private String descIndicador;
	
	@Column(name="estadoIndicador", nullable=false, length=1)	
	private String estadoIndicador;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="indicador", targetEntity=Evento_indicador.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set evento_indicador = new java.util.HashSet();
	
	private void setIdIndicador(int value) {
		this.idIndicador = value;
	}
	
	public int getIdIndicador() {
		return idIndicador;
	}
	
	public int getORMID() {
		return getIdIndicador();
	}
	
	public void setNombreIndicador(String value) {
		this.nombreIndicador = value;
	}
	
	public String getNombreIndicador() {
		return nombreIndicador;
	}
	
	public void setDescIndicador(String value) {
		this.descIndicador = value;
	}
	
	public String getDescIndicador() {
		return descIndicador;
	}
	
	public void setEstadoIndicador(String value) {
		this.estadoIndicador = value;
	}
	
	public String getEstadoIndicador() {
		return estadoIndicador;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setUnidadMedida(UnidadMedida value) {
		this.unidadMedida = value;
	}
	
	public UnidadMedida getUnidadMedida() {
		return unidadMedida;
	}
	
	public void setTipoIndicador(TipoIndicador value) {
		this.tipoIndicador = value;
	}
	
	public TipoIndicador getTipoIndicador() {
		return tipoIndicador;
	}
	
	public void setEvento_indicador(java.util.Set value) {
		this.evento_indicador = value;
	}
	
	public java.util.Set getEvento_indicador() {
		return evento_indicador;
	}
	
	
	public String toString() {
		return String.valueOf(getIdIndicador());
	}
	
}
