/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="documento",schema="public")
@PrimaryKeyJoinColumn(name="IdDocumento")
public class Documento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Documento() {
	}
	
	@Column(name="idDocumento", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.documento_IdDocumento_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")		
	private int idDocumento;
	
	@Column(name="nombreDocumento", nullable=false, length=30)	
	private String nombreDocumento;
	
	@Column(name="archivoDocumento", nullable=false, length=100)	
	private String archivoDocumento;
	
	@Column(name="estadoDocumento", nullable=true, length=1)	
	private String estadoDocumento;
	
	@ManyToOne(targetEntity=Club.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="clubId", referencedColumnName="idClub", nullable=false) })	
	private Club club;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	private void setIdDocumento(int value) {
		this.idDocumento = value;
	}
	
	public int getIdDocumento() {
		return idDocumento;
	}
	
	public int getORMID() {
		return getIdDocumento();
	}
	
	public void setNombreDocumento(String value) {
		this.nombreDocumento = value;
	}
	
	public String getNombreDocumento() {
		return nombreDocumento;
	}
	
	public void setArchivoDocumento(String value) {
		this.archivoDocumento = value;
	}
	
	public String getArchivoDocumento() {
		return archivoDocumento;
	}
	
	public void setEstadoDocumento(String value) {
		this.estadoDocumento = value;
	}
	
	public String getEstadoDocumento() {
		return estadoDocumento;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setClub(Club value) {
		this.club = value;
	}
	
	public Club getClub() {
		return club;
	}
	
	public String toString() {
		return String.valueOf(getIdDocumento());
	}
	
}
