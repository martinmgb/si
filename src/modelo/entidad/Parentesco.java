/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="parentesco",schema="public")
@PrimaryKeyJoinColumn(name="IdParentesco")
public class Parentesco implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Parentesco() {
	}
	
	@Column(name="idParentesco", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.parentesco_IdParentesco_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idParentesco;
	
	@Column(name="nombreParentesco", nullable=false, length=30)	
	private String nombreParentesco;
	
	@Column(name="descParentesco", nullable=true, length=160)	
	private String descParentesco;
	
	@Column(name="estadoParentesco", nullable=false, length=1)	
	private String estadoParentesco;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	 
 
	
	private void setIdParentesco(int value) {
		this.idParentesco = value;
	}
	
	public int getIdParentesco() {
		return idParentesco;
	}
	
	public int getORMID() {
		return getIdParentesco();
	}
	
	public void setNombreParentesco(String value) {
		this.nombreParentesco = value;
	}
	
	public String getNombreParentesco() {
		return nombreParentesco;
	}
	
	public void setDescParentesco(String value) {
		this.descParentesco = value;
	}
	
	public String getDescParentesco() {
		return descParentesco;
	}
	
	public void setEstadoParentesco(String value) {
		this.estadoParentesco = value;
	}
	
	public String getEstadoParentesco() {
		return estadoParentesco;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	 
	public String toString() {
		return String.valueOf(getIdParentesco());
	}
	
}
