/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.beans.Transient;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="grupo", schema="public")
@PrimaryKeyJoinColumn(name="IdGrupo")
public class Grupo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Grupo() {
	}
	
	@Column(name="idGrupo", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.grupo_IdGrupo_seq", allocationSize=1)	
	@GeneratedValue(generator="SeqId")	
	private int idGrupo;
	
	@Column(name="nombreGrupo", nullable=false, length=30)	
	private String nombreGrupo;
	
	@Column(name="descGrupo", nullable=true, length=160)	
	private String descGrupo;
	
	@Column(name="estadoGrupo", nullable=false, length=1)	
	private String estadoGrupo;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="grupo", targetEntity=Funcion_grupo.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.FALSE)	
	private java.util.Set funcion_grupo = new java.util.HashSet();
	
	@OneToMany(mappedBy="grupo", targetEntity=Usuario.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set usuario = new java.util.HashSet();
	
	private void setIdGrupo(int value) {
		this.idGrupo = value;
	}
	
	public int getIdGrupo() {
		return idGrupo;
	}
	
	public int getORMID() {
		return getIdGrupo();
	}
	
	public void setNombreGrupo(String value) {
		this.nombreGrupo = value;
	}
	
	public String getNombreGrupo() {
		return nombreGrupo;
	}
	
	public void setDescGrupo(String value) {
		this.descGrupo = value;
	}
	
	public String getDescGrupo() {
		return descGrupo;
	}
	
	public void setEstadoGrupo(String value) {
		this.estadoGrupo = value;
	}
	
	public String getEstadoGrupo() {
		return estadoGrupo;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	@Transient
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setFuncion_grupo(java.util.Set value) {
		this.funcion_grupo = value;
	}
	
	public java.util.Set getFuncion_grupo() {
		return funcion_grupo;
	}
	
	
	public void setUsuario(java.util.Set value) {
		this.usuario = value;
	}
	
	public java.util.Set getUsuario() {
		return usuario;
	}
	
	
	public String toString() {
		return String.valueOf(getIdGrupo());
	}
	
}
