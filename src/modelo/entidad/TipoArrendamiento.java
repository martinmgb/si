/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="TipoArrendamiento",schema="public")
@PrimaryKeyJoinColumn(name="IdTipoArrendamiento")
public class TipoArrendamiento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TipoArrendamiento() {
	}
	
	@Column(name="idTipoArrend", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.TipoArrendamiento_IdTipoArrend_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idTipoArrend;
	
	@Column(name="nombreArrend", nullable=false, length=30)	
	private String nombreArrend;
	
	@Column(name="descArrend", nullable=true, length=160)	
	private String descArrend;
	
	@Column(name="estadoTipoArrend", nullable=false, length=1)	
	private String estadoTipoArrend;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="tipoArrendamiento", targetEntity=Arrendamiento.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set arrendamiento = new java.util.HashSet();
	
	public void setIdTipoArrend(int value) {
		this.idTipoArrend = value;
	}
	
	public int getIdTipoArrend() {
		return idTipoArrend;
	}
	
	public int getORMID() {
		return getIdTipoArrend();
	}
	
	public void setNombreArrend(String value) {
		this.nombreArrend = value;
	}
	
	public String getNombreArrend() {
		return nombreArrend;
	}
	
	public void setDescArrend(String value) {
		this.descArrend = value;
	}
	
	public String getDescArrend() {
		return descArrend;
	}
	
	public void setEstadoTipoArrend(String value) {
		this.estadoTipoArrend = value;
	}
	
	public String getEstadoTipoArrend() {
		return estadoTipoArrend;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setArrendamiento(java.util.Set value) {
		this.arrendamiento = value;
	}
	
	public java.util.Set getArrendamiento() {
		return arrendamiento;
	}
	
	
	public String toString() {
		return String.valueOf(getIdTipoArrend());
	}
	
}
