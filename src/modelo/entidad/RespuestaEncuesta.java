/**

 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="respuestaEncuesta",schema="public")
@PrimaryKeyJoinColumn(name="IdRespuestaEncuesta")
public class RespuestaEncuesta implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RespuestaEncuesta() {
	}
	
	@Column(name="idRespuestaEnc", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.respuestaEncuesta_IdRespuestaEnc_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idRespuestaEnc;
	
	@ManyToOne(targetEntity=Usuario.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="usuarioId", referencedColumnName="idUsuario", nullable=false) })	
	private Usuario usuario;
	
	@ManyToOne(targetEntity=OpcionPregunta.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="opcionPreguntaId", referencedColumnName="idOpcionPregunta", nullable=false) })	
	private OpcionPregunta opcionPregunta;
	
	@Column(name="fechaRespuestaEnc", nullable=false)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaRespuestaEnc;
	
	@OneToOne(targetEntity=Pregunta.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="preguntaId", referencedColumnName="idPregunta", nullable=false) })	
	private Pregunta pregunta;
	
	@Column(name="estadoRespuestaEnc", nullable=true, length=1)	
	private String estadoRespuestaEnc;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	private void setIdRespuestaEnc(int value) {
		this.idRespuestaEnc = value;
	}
	
	public int getIdRespuestaEnc() {
		return idRespuestaEnc;
	}
	
	public int getORMID() {
		return getIdRespuestaEnc();
	}
	
	public void setFechaRespuestaEnc(java.util.Date value) {
		this.fechaRespuestaEnc = value;
	}
	
	public java.util.Date getFechaRespuestaEnc() {
		return fechaRespuestaEnc;
	}
	
	public void setEstadoRespuestaEnc(String value) {
		this.estadoRespuestaEnc = value;
	}
	
	public String getEstadoRespuestaEnc() {
		return estadoRespuestaEnc;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setUsuario(Usuario value) {
		this.usuario = value;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setOpcionPregunta(OpcionPregunta value) {
		this.opcionPregunta = value;
	}
	
	public OpcionPregunta getOpcionPregunta() {
		return opcionPregunta;
	}
	
	public void setPregunta(Pregunta value) {
		this.pregunta = value;
	}
	
	public Pregunta getPregunta() {
		return pregunta;
	}
	
	public String toString() {
		return String.valueOf(getIdRespuestaEnc());
	}
	
}
