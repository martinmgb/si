/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="accion",schema="public")
@PrimaryKeyJoinColumn(name="IdAccion")
public class Accion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Accion() {
	}
	
	@Column(name="idAccion", nullable=true, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.accion_IdAccion_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idAccion;
	
	@ManyToOne(targetEntity=Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaiId", referencedColumnName="idPersona", nullable=true) })	
	private Persona personai;

	@OneToMany(mappedBy="accion", targetEntity=Desvinculacion.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set desvinculacion = new java.util.HashSet();
	
	@Column(name="fechaAccion", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaAccion;
	
	@Column(name="estadoAccion", nullable=false, length=10)	
	private String estadoAccion;
	
	@Column(name="nroAccion", nullable=false, length=7)	
	private String nroAccion;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	private void setIdAccion(int value) {
		this.idAccion = value;
	}
	
	public int getIdAccion() {
		return idAccion;
	}
	
	public int getORMID() {
		return getIdAccion();
	}
	
	public void setFechaAccion(java.util.Date value) {
		this.fechaAccion = value;
	}
	
	public java.util.Date getFechaAccion() {
		return fechaAccion;
	}
	
	public void setEstadoAccion(String value) {
		this.estadoAccion = value;
	}
	
	public String getEstadoAccion() {
		return estadoAccion;
	}
	
	
	
	public String getNroAccion() {
		return nroAccion;
	}

	public void setNroAccion(String nroAccion) {
		this.nroAccion = nroAccion;
	}

	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setPersonai(Persona value) {
		this.personai = value;
	}
	
	public Persona getPersonai() {
		return personai;
	}
	public void setDesvinculacion(java.util.Set value) {
		this.desvinculacion = value;
	}
	
	public java.util.Set getDesvinculacion() {
		return desvinculacion;
	}
	
	public String toString() {
		return String.valueOf(getIdAccion());
	}
	
}
