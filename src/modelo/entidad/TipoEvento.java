/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="tipoEvento",schema="public")
@PrimaryKeyJoinColumn(name="IdTipoEvento")
public class TipoEvento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TipoEvento() {
	}
	
	@Column(name="idTipoEvento", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.tipoEvento_IdTipoEvento_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idTipoEvento;
	
	@Column(name="nombreTipoEvento", nullable=false, length=30)	
	private String nombreTipoEvento;
	
	@Column(name="descTipoEvento", nullable=true, length=160)	
	private String descTipoEvento;
	
	@Column(name="estadoTipoEvento", nullable=false, length=1)	
	private String estadoTipoEvento;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="tipoEvento", targetEntity=Evento.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set evento = new java.util.HashSet();
	
	private void setIdTipoEvento(int value) {
		this.idTipoEvento = value;
	}
	
	public int getIdTipoEvento() {
		return idTipoEvento;
	}
	
	public int getORMID() {
		return getIdTipoEvento();
	}
	
	public void setNombreTipoEvento(String value) {
		this.nombreTipoEvento = value;
	}
	
	public String getNombreTipoEvento() {
		return nombreTipoEvento;
	}
	
	public void setDescTipoEvento(String value) {
		this.descTipoEvento = value;
	}
	
	public String getDescTipoEvento() {
		return descTipoEvento;
	}
	
	public void setEstadoTipoEvento(String value) {
		this.estadoTipoEvento = value;
	}
	
	public String getEstadoTipoEvento() {
		return estadoTipoEvento;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setEvento(java.util.Set value) {
		this.evento = value;
	}
	
	public java.util.Set getEvento() {
		return evento;
	}
	
	
	public String toString() {
		return String.valueOf(getIdTipoEvento());
	}
	
}
