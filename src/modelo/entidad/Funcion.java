/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.beans.Transient;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="funcion", schema="public")
@PrimaryKeyJoinColumn(name="IdFuncion")
public class Funcion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Funcion() {
	}
	
	@Column(name="idFuncion", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.funcion_IdFuncion_seq", allocationSize=1)	
	@GeneratedValue(generator="SeqId")		
	private int idFuncion;
	
	@Column(name="idPadreFuncion", nullable=false, length=10)	
	private int idPadreFuncion;
	
	@Column(name="nombreFuncion", nullable=false, length=30)	
	private String nombreFuncion;
	
	@Column(name="iconUriFuncion", nullable=false, length=100)	
	private String iconUriFuncion;
	
	@Column(name="paginaFuncion", nullable=false, length=100)	
	private String paginaFuncion;
	
	@Column(name="extraFuncion", nullable=false, length=100)	
	private String extraFuncion;
	
	@Column(name="estadoFuncion", nullable=false, length=1)	
	private String estadoFuncion;
	
	
	@OneToMany(mappedBy="funcion", targetEntity=Funcion_grupo.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set funcion_grupo = new java.util.HashSet();
	
	private void setIdFuncion(int value) {
		this.idFuncion = value;
	}
	
	public int getIdFuncion() {
		return idFuncion;
	}
	
	public int getORMID() {
		return getIdFuncion();
	}
	
	public void setIdPadreFuncion(int value) {
		this.idPadreFuncion = value;
	}
	
	public int getIdPadreFuncion() {
		return idPadreFuncion;
	}
	
	public void setNombreFuncion(String value) {
		this.nombreFuncion = value;
	}
	
	public String getNombreFuncion() {
		return nombreFuncion;
	}
	
	public void setIconUriFuncion(String value) {
		this.iconUriFuncion = value;
	}
	
	public String getIconUriFuncion() {
		return iconUriFuncion;
	}
	
	public void setPaginaFuncion(String value) {
		this.paginaFuncion = value;
	}
	
	public String getPaginaFuncion() {
		return paginaFuncion;
	}
	
	public void setExtraFuncion(String value) {
		this.extraFuncion = value;
	}
	
	public String getExtraFuncion() {
		return extraFuncion;
	}
	
	public void setEstadoFuncion(String value) {
		this.estadoFuncion = value;
	}
	
	public String getEstadoFuncion() {
		return estadoFuncion;
	}
	
	public void setFuncion_grupo(java.util.Set value) {
		this.funcion_grupo = value;
	}
	
	public java.util.Set getFuncion_grupo() {
		return funcion_grupo;
	}
	
	
	public String toString() {
		return String.valueOf(getIdFuncion());
	}
	
}
