/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="rechazo",schema="public")
@PrimaryKeyJoinColumn(name="IdRechazo")
public class Rechazo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Rechazo() {
	}
	
	@Column(name="idRechazo", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.rechazo_IdRechazo_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idRechazo;
	
	@ManyToOne(targetEntity=TipoRechazo.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="tipoRechazoId", referencedColumnName="idTipoRechazo", nullable=false) })	
	private TipoRechazo tipoRechazo;
	
	@Column(name="descRechazo", nullable=true, length=160)	
	private String descRechazo;
	
	@OneToOne(targetEntity=Postulacion.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="postulacionId", referencedColumnName="idPostulacion") })	
	private Postulacion postulacion;
	
	@OneToOne(targetEntity=Arrendamiento.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="arrendamientoid", referencedColumnName="idArrendamiento") })	
	private Arrendamiento arrendamiento;
	
	@Column(name="estadoRechazo", nullable=false, length=1)	
	private String estadoRechazo;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	private void setIdRechazo(int value) {
		this.idRechazo = value;
	}
	
	public int getIdRechazo() {
		return idRechazo;
	}
	
	public int getORMID() {
		return getIdRechazo();
	}
	
	public void setDescRechazo(String value) {
		this.descRechazo = value;
	}
	
	public String getDescRechazo() {
		return descRechazo;
	}
	
	public void setEstadoRechazo(String value) {
		this.estadoRechazo = value;
	}
	
	public String getEstadoRechazo() {
		return estadoRechazo;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setTipoRechazo(TipoRechazo value) {
		this.tipoRechazo = value;
	}
	
	public TipoRechazo getTipoRechazo() {
		return tipoRechazo;
	}
	
	public void setPostulacion(Postulacion value) {
		this.postulacion = value;
	}
	
	public Postulacion getPostulacion() {
		return postulacion;
	}
	
	public void setArrendamiento(Arrendamiento value) {
		this.arrendamiento = value;
	}
	
	public Arrendamiento getArrendamiento() {
		return arrendamiento;
	}
	
	public String toString() {
		return String.valueOf(getIdRechazo());
	}
	
}
