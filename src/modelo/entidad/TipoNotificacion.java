/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="tipoNotificacion",schema="public")
@PrimaryKeyJoinColumn(name="IdTipoNotificacion")
public class TipoNotificacion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TipoNotificacion() {
	}
	
	@Column(name="idTipoNotif", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.tipoNotificacion_IdTipoNotif_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idTipoNotif;
	
	@Column(name="nombreTipoNotif", nullable=false, length=30)	
	private String nombreTipoNotif;
	
	@Column(name="descTipoNotif", nullable=true, length=160)	
	private String descTipoNotif;
	
	@Column(name="estadoTipoNotif", nullable=false, length=1)	
	private String estadoTipoNotif;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="tipoNotificacion", targetEntity=Notificacion.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set notificacion = new java.util.HashSet();
	
	private void setIdTipoNotif(int value) {
		this.idTipoNotif = value;
	}
	
	public int getIdTipoNotif() {
		return idTipoNotif;
	}
	
	public int getORMID() {
		return getIdTipoNotif();
	}
	
	public void setNombreTipoNotif(String value) {
		this.nombreTipoNotif = value;
	}
	
	public String getNombreTipoNotif() {
		return nombreTipoNotif;
	}
	
	public void setDescTipoNotif(String value) {
		this.descTipoNotif = value;
	}
	
	public String getDescTipoNotif() {
		return descTipoNotif;
	}
	
	public void setEstadoTipoNotif(String value) {
		this.estadoTipoNotif = value;
	}
	
	public String getEstadoTipoNotif() {
		return estadoTipoNotif;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setNotificacion(java.util.Set value) {
		this.notificacion = value;
	}
	
	public java.util.Set getNotificacion() {
		return notificacion;
	}
	
	
	public String toString() {
		return String.valueOf(getIdTipoNotif());
	}
	
}
