/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="actividad",schema="public")
@PrimaryKeyJoinColumn(name="IdActividad")
public class Actividad implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Actividad() {
	}
	
	@Column(name="idActividad", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.actividad_IdActividad_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idActividad;
	
	@Column(name="nombreActividad", nullable=false, length=30)	
	private String nombreActividad;
	
	@Column(name="descActividad", nullable=true, length=160)	
	private String descActividad;
	
	@Column(name="estadoActividad", nullable=false, length=1)	
	private String estadoActividad;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="actividad", targetEntity=Act_comision.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set act_comision = new java.util.HashSet();
	
	private void setIdActividad(int value) {
		this.idActividad = value;
	}
	
	public int getIdActividad() {
		return idActividad;
	}
	
	public int getORMID() {
		return getIdActividad();
	}
	
	public void setNombreActividad(String value) {
		this.nombreActividad = value;
	}
	
	public String getNombreActividad() {
		return nombreActividad;
	}
	
	public void setDescActividad(String value) {
		this.descActividad = value;
	}
	
	public String getDescActividad() {
		return descActividad;
	}
	
	public void setEstadoActividad(String value) {
		this.estadoActividad = value;
	}
	
	public String getEstadoActividad() {
		return estadoActividad;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setAct_comision(java.util.Set value) {
		this.act_comision = value;
	}
	
	public java.util.Set getAct_comision() {
		return act_comision;
	}
	
	
	public String toString() {
		return String.valueOf(getIdActividad());
	}
	
}
