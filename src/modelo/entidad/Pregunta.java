/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="pregunta",schema="public")
@PrimaryKeyJoinColumn(name="IdPregunta")
public class Pregunta implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Pregunta() {
	}
	
	@Column(name="idPregunta", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.pregunta_IdPregunta_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idPregunta;
	
	@Column(name="nombrePregunta", nullable=false, length=30)	
	private String nombrePregunta;
	
	@ManyToOne(targetEntity=Categoria.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="categoriaId", referencedColumnName="idCategoria", nullable=false) })	
	private Categoria categoria;
	
	@ManyToOne(targetEntity=OpcionPregunta.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="opcionPreguntaId", referencedColumnName="idOpcionPregunta", nullable=false) })	
	private OpcionPregunta opcionPregunta;
	
	@ManyToOne(targetEntity=Encuesta.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="encuestaId", referencedColumnName="idEncuesta", nullable=false) })	
	private Encuesta encuesta;
	
	@Column(name="estadoPregunta", nullable=false, length=1)	
	private String estadoPregunta;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToOne(mappedBy="pregunta", targetEntity=RespuestaEncuesta.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	private RespuestaEncuesta respuestaEncuesta;
	
	private void setIdPregunta(int value) {
		this.idPregunta = value;
	}
	
	public int getIdPregunta() {
		return idPregunta;
	}
	
	public int getORMID() {
		return getIdPregunta();
	}
	
	public void setNombrePregunta(String value) {
		this.nombrePregunta = value;
	}
	
	public String getNombrePregunta() {
		return nombrePregunta;
	}
	
	public void setEstadoPregunta(String value) {
		this.estadoPregunta = value;
	}
	
	public String getEstadoPregunta() {
		return estadoPregunta;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setCategoria(Categoria value) {
		this.categoria = value;
	}
	
	public Categoria getCategoria() {
		return categoria;
	}
	
	public void setOpcionPregunta(OpcionPregunta value) {
		this.opcionPregunta = value;
	}
	
	public OpcionPregunta getOpcionPregunta() {
		return opcionPregunta;
	}
	
	public void setEncuesta(Encuesta value) {
		this.encuesta = value;
	}
	
	public Encuesta getEncuesta() {
		return encuesta;
	}
	
	public void setRespuestaEncuesta(RespuestaEncuesta value) {
		this.respuestaEncuesta = value;
	}
	
	public RespuestaEncuesta getRespuestaEncuesta() {
		return respuestaEncuesta;
	}
	
	public String toString() {
		return String.valueOf(getIdPregunta());
	}
	
}
