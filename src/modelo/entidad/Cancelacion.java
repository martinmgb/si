/**

 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package  modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="cancelacion",schema="public")
@PrimaryKeyJoinColumn(name="IdCancelacion")
public class Cancelacion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Cancelacion() {
	}
	
	@Column(name="idCancelacion", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.cancelacion_IdCancelacion_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idCancelacion;
	
	@ManyToOne(targetEntity=Motivo.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="motivoId", referencedColumnName="idMotivo", nullable=false) })	
	private Motivo motivo;
	
	@OneToOne(targetEntity=Evento.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="eventoId", referencedColumnName="idEvento") })	
	private Evento evento;
	
	@OneToOne(targetEntity=Arrendamiento.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="arrendamientoId", referencedColumnName="idArrendamiento") })	
	private Arrendamiento arrendamiento;
	
	@Column(name="fechaCancelacion", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaCancelacion;
	
	
	@Column(name="estadoCancelacion", nullable=false, length=1)	
	private String estadoCancelacion;
	
	@Column(name="descCancelacion", nullable=true, length=160)	
	private String descCancelacion;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	private void setIdCancelacion(int value) {
		this.idCancelacion = value;
	}
	
	public int getIdCancelacion() {
		return idCancelacion;
	}
	
	public int getORMID() {
		return getIdCancelacion();
	}
	
	public void setFechaCancelacion(java.util.Date value) {
		this.fechaCancelacion = value;
	}
	
	public java.util.Date getFechaCancelacion() {
		return fechaCancelacion;
	}
	
	public void setEstadoCancelacion(String value) {
		this.estadoCancelacion = value;
	}
	
	public String getEstadoCancelacion() {
		return estadoCancelacion;
	}
	
	
	public void setDescCancelacion(String value) {
		this.descCancelacion = value;
	}
	
	public String getDescCancelacion() {
		return descCancelacion;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setEvento(Evento value) {
		this.evento = value;
	}
	
	public Evento getEvento() {
		return evento;
	}
	
	public void setArrendamiento(Arrendamiento value) {
		this.arrendamiento = value;
	}
	
	public Arrendamiento getArrendamiento() {
		return arrendamiento;
	}
	
	public void setMotivo(Motivo value) {
		this.motivo = value;
	}
	
	public Motivo getMotivo() {
		return motivo;
	}
	
	public String toString() {
		return String.valueOf(getIdCancelacion());
	}
	
}
