/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="tipoRechazo",schema="public")
@PrimaryKeyJoinColumn(name="IdTipoRechazo")
public class TipoRechazo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TipoRechazo() {
	}
	
	@Column(name="idTipoRechazo", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.tipoRechazo_IdTipoRechazo_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idTipoRechazo;
	
	@Column(name="nombreTipoRechazo", nullable=false, length=30)	
	private String nombreTipoRechazo;
	
	@Column(name="descTipoRechazo", nullable=true, length=160)	
	private String descTipoRechazo;
	
	@Column(name="estadoTipoRechazo", nullable=false, length=1)	
	private String estadoTipoRechazo;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="tipoRechazo", targetEntity=Rechazo.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set rechazo = new java.util.HashSet();
	
	private void setIdTipoRechazo(int value) {
		this.idTipoRechazo = value;
	}
	
	public int getIdTipoRechazo() {
		return idTipoRechazo;
	}
	
	public int getORMID() {
		return getIdTipoRechazo();
	}
	
	public void setNombreTipoRechazo(String value) {
		this.nombreTipoRechazo = value;
	}
	
	public String getNombreTipoRechazo() {
		return nombreTipoRechazo;
	}
	
	public void setDescTipoRechazo(String value) {
		this.descTipoRechazo = value;
	}
	
	public String getDescTipoRechazo() {
		return descTipoRechazo;
	}
	
	public void setEstadoTipoRechazo(String value) {
		this.estadoTipoRechazo = value;
	}
	
	public String getEstadoTipoRechazo() {
		return estadoTipoRechazo;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setRechazo(java.util.Set value) {
		this.rechazo = value;
	}
	
	public java.util.Set getRechazo() {
		return rechazo;
	}
	
	
	public String toString() {
		return String.valueOf(getIdTipoRechazo());
	}
	
}
