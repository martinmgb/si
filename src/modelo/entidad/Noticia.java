/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Type;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="noticia",schema="public")
@PrimaryKeyJoinColumn(name="IdNoticia")
public class Noticia implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Noticia() {
	}
	
	@Column(name="idNoticia", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.noticia_idnoticia_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")		
	private int idNoticia;
	
	@ManyToOne(targetEntity=TipoNoticia.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="tipoNoticiaId", referencedColumnName="idTipoNoticia", nullable=false) })	
	private TipoNoticia tipoNoticia;
	
	@Column(name="titulonoticia", nullable=false, length=30)	
	private String tituloNoticia;
	
	@Column(name="descnoticia", nullable=true, length=160)	
	private String descNoticia;
	
	@Column(name="imagennoticia", nullable=true, length=100)
	@Type(type="org.hibernate.type.BinaryType") 
	private byte[] imagenNoticia;
	
	@Column(name="difusionnoticia", nullable=false, length=30)	
	private String difusionNoticia;
	
	@Column(name="fechaininoticia", nullable=false)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaIniNoticia;
	
	@Column(name="fechaexpnoticia", nullable=false)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaExpNoticia;
	
	@Column(name="estadonoticia", nullable=false, length=1)	
	private String estadoNoticia;
	
	@ManyToOne(targetEntity=Club.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="clubid", referencedColumnName="idClub", nullable=false) })	
	private Club club;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	private void setIdNoticia(int value) {
		this.idNoticia = value;
	}
	
	public int getIdNoticia() {
		return idNoticia;
	}
	
	public int getORMID() {
		return getIdNoticia();
	}
	
	public void setTituloNoticia(String value) {
		this.tituloNoticia = value;
	}
	
	public String getTituloNoticia() {
		return tituloNoticia;
	}
	
	public void setDescNoticia(String value) {
		this.descNoticia = value;
	}
	
	public String getDescNoticia() {
		return descNoticia;
	}
	
	public void setImagenNoticia(byte[] value) {
		this.imagenNoticia = value;
	}
	
	public byte[] getImagenNoticia() {
		return imagenNoticia;
	}
	
	public void setDifusionNoticia(String value) {
		this.difusionNoticia = value;
	}
	
	public String getDifusionNoticia() {
		return difusionNoticia;
	}
	
	public void setFechaIniNoticia(java.util.Date value) {
		this.fechaIniNoticia = value;
	}
	
	public java.util.Date getFechaIniNoticia() {
		return fechaIniNoticia;
	}
	
	public void setFechaExpNoticia(java.util.Date value) {
		this.fechaExpNoticia = value;
	}
	
	public java.util.Date getFechaExpNoticia() {
		return fechaExpNoticia;
	}
	
	public void setEstadoNoticia(String value) {
		this.estadoNoticia = value;
	}
	
	public String getEstadoNoticia() {
		return estadoNoticia;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setTipoNoticia(TipoNoticia value) {
		this.tipoNoticia = value;
	}
	
	public TipoNoticia getTipoNoticia() {
		return tipoNoticia;
	}
	
	public void setClub(Club value) {
		this.club = value;
	}
	
	public Club getClub() {
		return club;
	}
	
	public String toString() {
		return String.valueOf(getIdNoticia());
	}
	
}
