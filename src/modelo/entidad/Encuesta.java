/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="encuesta",schema="public")
@PrimaryKeyJoinColumn(name="IdEncuesta")
public class Encuesta implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Encuesta() {
	}
	
	@Column(name="idEncuesta", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.encuesta_IdEncuesta_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")
	private int idEncuesta;
	
	@Column(name="nombreEncuesta", nullable=false, length=30)	
	private String nombreEncuesta;
	
	@Column(name="fechaEncuesta", nullable=false)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fechaEncuesta;
	
	@Column(name="descEncuesta", nullable=true, length=160)	
	private String descEncuesta;
	
	@Column(name="estadoEncuesta", nullable=false, length=1)	
	private String estadoEncuesta;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="encuesta", targetEntity=Pregunta.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set pregunta = new java.util.HashSet();
	
	private void setIdEncuesta(int value) {
		this.idEncuesta = value;
	}
	
	public int getIdEncuesta() {
		return idEncuesta;
	}
	
	public int getORMID() {
		return getIdEncuesta();
	}
	
	public void setNombreEncuesta(String value) {
		this.nombreEncuesta = value;
	}
	
	public String getNombreEncuesta() {
		return nombreEncuesta;
	}
	
	public void setFechaEncuesta(java.util.Date value) {
		this.fechaEncuesta = value;
	}
	
	public java.util.Date getFechaEncuesta() {
		return fechaEncuesta;
	}
	
	public void setDescEncuesta(String value) {
		this.descEncuesta = value;
	}
	
	public String getDescEncuesta() {
		return descEncuesta;
	}
	
	public void setEstadoEncuesta(String value) {
		this.estadoEncuesta = value;
	}
	
	public String getEstadoEncuesta() {
		return estadoEncuesta;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setPregunta(java.util.Set value) {
		this.pregunta = value;
	}
	
	public java.util.Set getPregunta() {
		return pregunta;
	}
	
	
	public String toString() {
		return String.valueOf(getIdEncuesta());
	}
	
}
