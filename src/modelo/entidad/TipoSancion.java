/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;

import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="tipoSancion",schema="public")
@PrimaryKeyJoinColumn(name="IdTipoSancion")
public class TipoSancion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TipoSancion() {
	}
	
	@Column(name="idTipoSancion", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.tipoSancion_IdTipoSancion_seq", allocationSize=1)
	@GeneratedValue(generator="SeqId")	
	private int idTipoSancion;
	
	@Column(name="nombreTipoSancion", nullable=false, length=30)	
	private String nombreTipoSancion;
	
	@Column(name="descTipoSancion", nullable=true, length=160)	
	private String descTipoSancion;
	
	@Column(name="estadoTipoSancion", nullable=false, length=1)	
	private String estadoTipoSancion;
	
	@Column(name="editingStatus", nullable=false)	
	private boolean editingStatus = false;
	
	@OneToMany(mappedBy="tipoSancion", targetEntity=Sancion.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set sancion = new java.util.HashSet();
	
	public void setIdTipoSancion(int value) {
		this.idTipoSancion = value;
	}
	
	public int getIdTipoSancion() {
		return idTipoSancion;
	}
	
	public int getORMID() {
		return getIdTipoSancion();
	}
	
	public void setNombreTipoSancion(String value) {
		this.nombreTipoSancion = value;
	}
	
	public String getNombreTipoSancion() {
		return nombreTipoSancion;
	}
	
	public void setDescTipoSancion(String value) {
		this.descTipoSancion = value;
	}
	
	public String getDescTipoSancion() {
		return descTipoSancion;
	}
	
	public void setEstadoTipoSancion(String value) {
		this.estadoTipoSancion = value;
	}
	
	public String getEstadoTipoSancion() {
		return estadoTipoSancion;
	}
	
	public void setEditingStatus(boolean value) {
		this.editingStatus = value;
	}
	
	public boolean getEditingStatus() {
		return editingStatus;
	}
	
	public void setSancion(java.util.Set value) {
		this.sancion = value;
	}
	
	public java.util.Set getSancion() {
		return sancion;
	}
	
	
	public String toString() {
		return String.valueOf(getIdTipoSancion());
	}
	
}
