/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package modelo.entidad;
import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="funcion_grupo", schema="public")
@PrimaryKeyJoinColumn(name="IdFuncion_Grupo")
public class Funcion_grupo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Funcion_grupo() {
	}

	@Column(name="idFuncion_grupo", nullable=false, unique=true)	
	@Id	
	@SequenceGenerator(name="SeqId", sequenceName="public.funcion_grupo_IdFuncion_Grupo_seq", allocationSize=1)	
	@GeneratedValue(generator="SeqId")		
	private int idFuncion_grupo;

	@ManyToOne(targetEntity=Funcion.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="funcionId", referencedColumnName="idFuncion", nullable=false) })	
	private Funcion funcion;

	@ManyToOne(targetEntity=Grupo.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="grupoId", referencedColumnName="idGrupo", nullable=false) })	
	private Grupo grupo;

	private void setIdFuncion_grupo(int value) {
		this.idFuncion_grupo = value;
	}

	public int getIdFuncion_grupo() {
		return idFuncion_grupo;
	}

	public int getORMID() {
		return getIdFuncion_grupo();
	}

	public void setFuncion(Funcion value) {
		this.funcion = value;
	}

	public Funcion getFuncion() {
		return funcion;
	}

	public void setGrupo(Grupo value) {
		this.grupo = value;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public String toString() {
		return String.valueOf(getIdFuncion_grupo());
	}

}
