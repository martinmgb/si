package vistaModelo;

public class AreaFilter {

	private String id="",tipoArea="",nombre="", descripcion="";

	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id==null?"":id.trim();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion==null?"":descripcion.trim();
	}

	public String getTipoArea() {
		return tipoArea;
	}

	public void setTipoArea(String tipoArea) {
		this.tipoArea = tipoArea ==null?"":tipoArea.trim();
	}

	
}