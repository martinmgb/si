package vistaModelo;

public class ComisionEventoFilter {
	private String nombre="";
	private String responsable="";

	public String getNombre() {
		return nombre;
	}
	
	public String getResponsable() {
		return responsable;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}
	
	public void setResponsable(String responsable) {
		this.responsable = responsable==null?"":responsable.trim();
	}
	
}
