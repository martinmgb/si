package vistaModelo;

public class PreferenciaFilter {

	private String id="",categoria="",nombre="", descripcion="";

	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id==null?"":id.trim();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion==null?"":descripcion.trim();
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria ==null?"":categoria.trim();
	}

	
}