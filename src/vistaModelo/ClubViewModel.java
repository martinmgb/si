package vistaModelo;

import java.util.List;
import java.awt.Image;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.zkoss.bind.BindContext;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Map;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;
 
import modelo.entidad.Cancelacion;
 
//POSTULACION
import modelo.entidad.Club;
import modelo.hibernate.dao.DaoClub;
import modelo.util.Util;

import java.util.Set;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.ListModelList;
//import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import bsh.This;


public class ClubViewModel extends GenericForwardComposer<Window> { 
	
	
	private Club club= new Club();
	private DaoClub daoclub = new DaoClub();
	private static AImage myImage;
	 
	
	@Wire
	private int id;
	@Wire
	private Textbox nombreClub;
	@Wire
	private Textbox rifClub;
	@Wire
	private Textbox direccionClub;
	@Wire
	private Textbox tlfClub;
	@Wire
	private Textbox misionClub;
	@Wire
	private Textbox visionClub;
	@Wire
	private Textbox correoClub;
	@Wire
	private Textbox limiteAcciones;
	
	



@Init
public void init() throws Exception {
      club = daoclub.obtenerClub(1);
      AImage imagen = new AImage("",club.getLogoClub());
	  setMyImage(imagen);
 }
						 

public Club getClub() {
	return club;
   }

   public void setClub(Club club) {
	this.club = club;
   }

	
	@Command
	public void Actualizar() throws Exception{
		if (!CamposVacio()){
			//System.out.print("Aqui"+user.getNombreUsuario());
			club.setLogoClub(myImage.getByteData());
			daoclub.modificarClub(club);
			//daoPersona.modificarPersona(people);
			Messagebox.show("Los Datos se actualizaron Correctamente!", 
					"Advertencia", Messagebox.OK, Messagebox.NONE);
		}
      
		else{
			Messagebox.show("Debe LLenar Todos Los Campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}

	} 
	

	public boolean CamposVacio(){
		if(club.getNombreClub() != null && !club.getNombreClub().equalsIgnoreCase("") && 
				this.club.getRifClub() != null && !this.club.getRifClub().equalsIgnoreCase("") &&
				this.club.getDireccionClub() != null && !this.club.getDireccionClub().equalsIgnoreCase("") &&
				this.club.getTlfClub()!= null && !this.club.getTlfClub().equalsIgnoreCase("") &&
				this.club.getMisionClub()!= null && !this.club.getMisionClub().equalsIgnoreCase("") &&
				this.club.getCorreoClub()!= null && !this.club.getCorreoClub().equalsIgnoreCase("") &&
				this.club.getLimiteAcciones()!=null
               )
			return false;
		return true;
	}

	@Command   
	@NotifyChange({"club"})
	public void  Limpiar(){
		this.club = new Club();
		
	}
//	public void onClick$btnLimpiar() {
//		nombreClub.setText("");
//		rifClub.setText("");
//		direccionClub.setText("");
//		tlfClub.setText("");
//		
//		misionClub.setText("");
//		visionClub.setText("");
//		//logoClub.setText("");
//		correoClub.setText("");
//		this.limiteAcciones.setText("");
//		nombreClub.setFocus(true);		
//	}
//	
 
	
	@Command("upload")
	@NotifyChange("myImage")
	public void onImageUpload(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx){
		UploadEvent upEvent = null;
		//alert(ctx.getCommandName());
		   Object objUploadEvent = ctx.getTriggerEvent();
		   if (objUploadEvent != null && (objUploadEvent instanceof UploadEvent)) {
		     upEvent = (UploadEvent) objUploadEvent;
		    }
		    if (upEvent != null) {
		     Media media = upEvent.getMedia();
		     int lengthofImage = media.getByteData().length;
		     if (media instanceof AImage) {
		      if (lengthofImage > 500 * 1024) {
		    	  Messagebox.show("Seleccione una imagen de tamaño menor que 500Kb", "Error", Messagebox.OK, Messagebox.ERROR);
		        return;
		      }
		      else{
		        setMyImage((AImage) media);//Initialize the bind object to show image in zul page and Notify it also
		        System.out.println(media.getContentType());
		        System.out.println(media.getName());
		        System.out.println(getMyImage().getName());
		      }
		     }
		      else {
		    	  Messagebox.show("Seleccione un archivo de imagen", "Error", Messagebox.OK, Messagebox.ERROR);
		     }
		 } else {
			 System.out.println("Upload Event Is not Coming");
		     }
	}
	
	public AImage getMyImage() {
		return myImage;
	}
	
	public void setMyImage(AImage myImage) {
		this.myImage = myImage;
	}
	
}	


 
