package vistaModelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import modelo.entidad.Area;
import modelo.entidad.Area_arrend;
import modelo.entidad.Area_evento;
import modelo.entidad.Arrendamiento;
import modelo.entidad.Categoria;
import modelo.entidad.Evento;
import modelo.entidad.Preferencia_evento;
import modelo.entidad.Rechazo;
import modelo.entidad.TipoRechazo;
import modelo.hibernate.dao.DaoAreaArrend;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoAreaEvento;
import modelo.hibernate.dao.DaoArrendamiento;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoPreferencia;
import modelo.hibernate.dao.DaoRechazo;
import modelo.hibernate.dao.DaoTipoRechazo;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Window;

import controlador.ManejadorMail;

import javax.swing.JOptionPane;
public class EvaluarSolicitudArrendamientoViewModel extends GenericForwardComposer<Window> {
	private  static Set<Area_arrend> listaAreaArrendamiento;
	private static final String footerMessage = "Total de %d Areas ";
	private DaoArrendamiento  daoArr;
	List<TipoRechazo> listaTipoRechazo;
	private DaoTipoRechazo daoTipoRechazo = new DaoTipoRechazo();
	private Rechazo rechazo;
	private DaoRechazo daoRechazo;
	private String rechazolimpiar;




	@Init
	public void init(@ExecutionArgParam("objeto") Arrendamiento arrendamiento) throws Exception {

		if (arrendamiento== null) {
			this.listaAreaArrendamiento= new HashSet<Area_arrend>();

		}
		else {
			this.listaAreaArrendamiento = arrendamiento.getArea_arrend();

		}
		this.daoArr = new DaoArrendamiento ();
		this.rechazo = new Rechazo();
		this.daoRechazo = new DaoRechazo();
		this.rechazolimpiar ="";



	}

	public List<TipoRechazo> getTiposRechazo() {
		try {
			this.listaTipoRechazo=this.daoTipoRechazo.obtenerTodos();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return listaTipoRechazo;
	}


	public Set<Area_arrend> getListaAreaArrendamiento() {
		return listaAreaArrendamiento;
	}


	public Rechazo getRechazo() {
		return rechazo;
	}

	public void setRechazo(Rechazo rechazo) {
		this.rechazo = rechazo;
	}

	public void setListaAreaArrendamiento(Set<Area_arrend> listaAreaArrendamiento) {
		this.listaAreaArrendamiento = listaAreaArrendamiento;
	}

	public String getFooter() {
		return String.format(footerMessage, this.listaAreaArrendamiento.size());
	}


	public String getRechazolimpiar() {
		return rechazolimpiar;
	}

	public void setRechazolimpiar(String rechazolimpiar) {
		this.rechazolimpiar = rechazolimpiar;
	}

	@Command
	public void AprobarSolicitud(@BindingParam("SolicitudR") Arrendamiento arr,@BindingParam("win") Window  win) throws Exception {
		//Arrendamiento arr = new Arrendamiento();
		//arr =daoArr.obtenerArrendamiento(objeto.getIdArrendamiento());
		arr.setEstadoArrend("A");
		this.daoArr.modificarArrendamiento(arr);
		String mensaje, destinatario, asunto;
		mensaje = "Sr(a) " + arr.getPersona().getNombrePersona() + " " + arr.getPersona().getApellidoPersona()
				+ " El presente correo es para informarle que su solicitud de arrendamiento ha sido aprobada."
				+ ", Ud. podrá hacer uso de nuestras instalaciones el día"+" "+ arr.getFechaArrend()+"."
				+ "Esperamos que disfrute ud. y toda su familia y recuerde que Teide es tu Socio Ideal";
		destinatario = arr.getPersona().getCorreoPersona();
		asunto = "Su solicitud de arrendamiento ha sido aprobada";
		ManejadorMail.enviarEmail(mensaje, destinatario, asunto);
		Messagebox.show("Solicitud aprobada!", "Información", Messagebox.OK, Messagebox.INFORMATION);
		win.detach();
		BindUtils.postGlobalCommand(null, null, "RefrescarTablaSolicitudesArrendamientos", null);

	}


	@Command	
	public void RechazarSolicitud (@BindingParam("Solicitud") Arrendamiento arre,@BindingParam("win") Window  win) throws Exception{
		Messagebox.show("¿Esta seguro de Cancelar El Arrendamiento?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					HashMap objetoArr = new HashMap();
					objetoArr.put("arrendamiento", arre);
					Window window = (Window)Executions.createComponents(
							"content/modalRechazoArrendamiento.zul", null,objetoArr);
					window.doModal();
					win.detach();
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
				}

			}



		}
				);
	}



	@Command	
	public void registrarRechazoSolicitud(@BindingParam("Solicitud") Arrendamiento arre,@BindingParam("win") Window  win) throws Exception {

		rechazo.setEstadoRechazo("A");
		rechazo.setEditingStatus(false);
		arre.setEstadoArrend("R");
		rechazo.setArrendamiento(arre);
		daoArr.modificarArrendamiento(arre);
		daoRechazo.agregarRechazo(rechazo);
		String mensaje, destinatario, asunto;
		mensaje = "Sr(a) " + arre.getPersona().getNombrePersona() + " " + arre.getPersona().getApellidoPersona()
				+ " El presente correo es para informarle que su solicitud de arrendamiento no pudó ser aprobada"
				+ ", debido a "+ rechazo.getTipoRechazo().getNombreTipoRechazo()+ ", ofrecemos disculpas por las molestías ocasionadas"
				+ "y recuerde que Teide es tu Socio Ideal";
		destinatario = arre.getPersona().getCorreoPersona();
		asunto = "Rechazo de solicitud de arrendamiento";
		ManejadorMail.enviarEmail(mensaje, destinatario, asunto);
		JOptionPane.showMessageDialog(null, "Solicitud Rechazada");
		BindUtils.postGlobalCommand(null, null, "RefrescarTablaSolicitudesArrendamientos", null);
		win.detach();

	}

	@Command
	@NotifyChange({"rechazo","rechazolimpiar"})
	public void Limpiar(){
		this.rechazo = new Rechazo();
		this.rechazolimpiar = "";

	}
}
