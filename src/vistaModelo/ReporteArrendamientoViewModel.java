package vistaModelo;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modelo.entidad.TipoArrendamiento;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoTipoArrendamiento;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;

import vistaModelo.ReporteMiembroViewModel.Estado;
import modelo.entidad.Area;


public class ReporteArrendamientoViewModel extends GenericForwardComposer {
	
	public class Estado{
		private String idEstado;
		private String nombreEstado;
		public Estado() {
			super();
			// TODO Auto-generated constructor stub
		}
		public String getIdEstado() {
			return idEstado;
		}
		public void setIdEstado(String idEstado) {
			this.idEstado = idEstado;
		}
		public String getNombreEstado() {
			return nombreEstado;
		}
		public void setNombreEstado(String nombreEstado) {
			this.nombreEstado = nombreEstado;
		}
		
		
		
		
	}


	private List<TipoArrendamiento> listaTipoArre;

	private ListModelList<Area> listaArea;
	private List<Estado> estados;

	private TipoArrendamiento tipoArrendamiento;

	String patch = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");

	private Area area;
	
	private DaoTipoArrendamiento daoTipoArrendamiento;
	
	private DaoArea daoArea;

	private Connection conexion;
	@Wire
	private Combobox  comboTipoArrenda;

	private Date fechaDesde;

	private Date fechaHasta;

	private AMedia archivoJasper;

		
	private String rutaPdf;
	
	String pathPdfProyecto;
	
	private String pathProyecto;
	
	String patchZulProyecto;
	
	String patchNoEstructurado;
	
	String rutaZul;
	private Estado estado = new Estado();
	
	Map<String,Object> parameterMap = new HashMap<String,Object>();

	private String rutabase;


	@Init
	public void init() {
		setFechaDesde(new Date());
		setFechaHasta(new Date());
		listaTipoArre = new ArrayList<TipoArrendamiento>();
		listaArea = new ListModelList<Area>();
		area = new Area();
		tipoArrendamiento = new TipoArrendamiento();
		daoArea = new DaoArea();
		daoTipoArrendamiento = new DaoTipoArrendamiento();
		try {
			tipoArrendamiento.setNombreArrend("Todos");
			tipoArrendamiento.setIdTipoArrend(0);
			listaTipoArre.add(tipoArrendamiento);
			listaTipoArre.addAll(daoTipoArrendamiento.obtenerTodos());
			
			area.setNombreArea("Todos");
//			area.setIdArea(0);
			listaArea.add(area);
			listaArea.addAll(daoArea.obtenerTodos());
			
			estados = new ArrayList<Estado>();
			estado.setIdEstado("Todos");
			estado.setNombreEstado("Todos");
			estados.add(estado);
			Estado d = new Estado();
			d.setIdEstado("R");
			d.setNombreEstado("Registrado");
			estados.add(d);
			Estado o = new Estado();
			o.setIdEstado("C");
			o.setNombreEstado("Cancelado");
			estados.add(o);
			Estado m = new Estado();
			m.setIdEstado("F");
			m.setNombreEstado("Finalizado");
			estados.add(m);
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

//////////////////////////////////////////////////////////IMPRIMIR//////////////////////////////////////////////////////////////////
File file= new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathProyecto= file.getParentFile().getParentFile().getPath();
///////////////////////////////////////////////////////////////////////////////
int i,f;
f=this.pathProyecto.length();
i=f-7;
String z=this.pathProyecto.substring(i, f);
this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
if(z.compareTo("classes")==0){
this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().getPath();
this.rutabase=file.getParentFile().getParentFile().getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
}
System.out.print("Z: "+z+" F: "+"classes");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
String ruta = this.pathProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator") + "arrendamiento09.jrxml";
System.out.print("RUTA: "+ruta);
File filePdf = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathPdfProyecto = this.pathProyecto;
rutaPdf = this.pathPdfProyecto + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator") + "arrendamientos.pdf";

File fileZul = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.patchZulProyecto = this.pathProyecto;
rutaZul = this.patchZulProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"content"+ System.getProperty("file.separator") + "modalreporteEstructurado.zul";

//Sustitucion de caracteres codificados (Para servidores con Windows)
this.pathProyecto= ruta.replaceAll("%20", " ");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
		this.patchNoEstructurado = file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator") +"noEstructurados"+  System.getProperty("file.separator") + "arrendamientoNoEstructurados.jrxml";

		

	}
	
	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public ListModelList<Area> getListaArea() {
		return listaArea;
	}
	
	public void setListaArea(ListModelList<Area> listaArea) {
		this.listaArea = listaArea;
	}
	
	public TipoArrendamiento getTipoArrendamiento() {
		return tipoArrendamiento;
	}
	
	public void setTipoArrendamiento(TipoArrendamiento tipoArrendamiento) {
		this.tipoArrendamiento = tipoArrendamiento;
	}
	
	public Area getArea() {
		return area;
	}
	
	public void setArea(Area area) {
		this.area = area;
	}
	
	public List<TipoArrendamiento> getListaTipoArre() {
		return listaTipoArre;
	}
	public void setListaTipoArre(List<TipoArrendamiento> listaTipoArre) {
		this.listaTipoArre = listaTipoArre;
	}
	
	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public String getPathProyecto() {
		return pathProyecto;
	}

	public Map<String, Object> getParameterMap() {
		return parameterMap;
	}


	public void setParameterMap(Map<String, Object> parameterMap) {
		this.parameterMap = parameterMap;
	}

	public void setPathProyecto(String pathProyecto) {
		this.pathProyecto = pathProyecto;
	}

	public void generarPdf() throws SQLException, ParseException, JRException  {
		generarReporte();
	}

	public void generarReporte() throws SQLException, ParseException, JRException{

		String desde = new SimpleDateFormat("YYYY-MM-dd").format(this.getFechaDesde());
		String hasta = new SimpleDateFormat("YYYY-MM-dd").format(this.getFechaHasta());

		SimpleDateFormat formatoFecha = new SimpleDateFormat("YYYY-MM-dd") ;
		SimpleDateFormat formatoFechaH=  new SimpleDateFormat("YYYY-MM-dd") ;
		Date fechaD = formatoFecha.parse(desde);
		Date fechaH = formatoFechaH.parse(hasta);

		try {
			Class.forName("org.postgresql.Driver");
			// para la conexion con la BD
			conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
		} catch (ClassNotFoundException ex) {

			ex.printStackTrace();
		}

System.out.print("fecha desde :" + this.getTipoArrendamiento().getNombreArrend());
System.out.print("fecha hasta :" + this.getArea().getNombreArea());
System.out.print("fecha hasta :" + this.getEstado().getIdEstado());

//		Map<String,Object> parameterMap = new HashMap<String,Object>();
		parameterMap.put("fd", this.getFechaDesde());
		parameterMap.put("fh", this.getFechaHasta());
		parameterMap.put("tarrend", this.getTipoArrendamiento().getNombreArrend());
		parameterMap.put("area", this.getArea().getNombreArea());
		parameterMap.put("estado", this.getEstado().getIdEstado());
		parameterMap.put("dirbase", this.rutabase);
		archivoJasper = null;
		JasperReport jasperReport = JasperCompileManager.compileReport(this.pathProyecto);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);

		if (jasperPrint.getPages().isEmpty()) {
			Messagebox.show("No se han encontrado datos para generar el reporte", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}else{
			archivoJasper = new AMedia(rutaPdf, "pdf" ,"application/pdf", JasperExportManager.exportReportToPdf(jasperPrint));
		}

	}


	@Command
	public void abrirModal(@ContextParam(ContextType.VIEW) Component view) throws SQLException, ParseException, JRException {
		parameterMap.put("ruta", patch);		
		Map<String, Object> args = new HashMap<String, Object>();
		generarPdf();
		if (archivoJasper != null) {
		args.put("reporte", archivoJasper);
		final Window component = (Window) Executions.getCurrent().createComponents("content/modalReporteEstructuradoArrend.zul", view, args);
		component.setAction("show: slideDown;hide: slideUp");
		component.doModal();
		}
	}


	@Command
	public void generarFormato() throws SQLException, FileNotFoundException, JRException{

		generarFormatoReporte();
	}

	public void generarFormatoReporte() throws SQLException, FileNotFoundException, JRException {
		try {
			//System.out.print("entre");
			Class.forName("org.postgresql.Driver");
			// para la conexion con la bd
			conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}

		parameterMap.put("fd", this.getFechaDesde());
		parameterMap.put("fh", this.getFechaHasta());
		parameterMap.put("tarrend", this.getTipoArrendamiento().getNombreArrend());
		parameterMap.put("area", this.getArea().getNombreArea());
		parameterMap.put("estado", this.getEstado().getIdEstado());

		JasperReport jasperReport = JasperCompileManager.compileReport(this.patchNoEstructurado);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);
		if (jasperPrint.getPages().isEmpty()) {
			Messagebox.show("No se han encontrado datos para generar el reporte", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}else{
		JRExporter exporter = new JRTextExporter();
		exporter.setParameter(JRTextExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, System.getProperty("user.home") + "/reportes/arrendamientos.txt");
		exporter.setParameter(JRTextExporterParameter.PAGE_WIDTH,130);
		exporter.setParameter(JRTextExporterParameter.PAGE_HEIGHT,130);
		exporter.exportReport();

		FileInputStream input = new FileInputStream(System.getProperty("user.home") + "/reportes/arrendamientos.txt");
		Filedownload.save(input, "txt", "eventos.txt");	    
		}
	}




}
