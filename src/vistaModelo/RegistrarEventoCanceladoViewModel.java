package vistaModelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import modelo.entidad.Area;
import modelo.entidad.Area_evento;
import modelo.entidad.Cancelacion;
import modelo.entidad.Categoria;
import modelo.entidad.Evento;
import modelo.entidad.Motivo;
import modelo.entidad.Persona;
import modelo.entidad.Preferencia_evento;
import modelo.hibernate.dao.DaoAreaArrend;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoAreaEvento;
import modelo.hibernate.dao.DaoCancelacion;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoMotivo;
import modelo.hibernate.dao.DaoPreferencia;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;


public class RegistrarEventoCanceladoViewModel extends GenericForwardComposer<Window> {

	private Evento evento;
	private static  Set<Preferencia_evento> listaPreferenciaEvento;
	private  static  Set<Area_evento> listaAreaEvento;
	private static final String footerMessage5 = "Total de %d Preferencia(s)";
	private static final String footerMessage6 = "Total de %d Areas(s)";
	private Cancelacion cancelacion;
	private List<Motivo> ListaMotivos;
	private DaoMotivo daomotivo;
	private DaoCancelacion daocancelacion;
	private DaoEvento daoevento;
	private String motivolimpiar;


	@Init
	public void init(@ExecutionArgParam("objetoEvento") Evento evento) throws Exception {

		if (evento == null) {
			this.evento = new Evento();
			//this.listaPreferenciaEvento=new HashSet<Preferencia_evento>();
			//this.listaAreaEvento = new HashSet<Area_evento>();


		} else {
			this.evento = evento;
			listaPreferenciaEvento = evento.getPreferencia_evento();
			this.listaAreaEvento = evento.getArea_evento();

		}
		this.daomotivo = new DaoMotivo();
		this.ListaMotivos = this.daomotivo.obtenerTodos();
		this.cancelacion = new Cancelacion();
		this.daocancelacion= new DaoCancelacion();
		this.daoevento = new DaoEvento();


	}


	public Set<Preferencia_evento> getListaPreferenciaEvento() {
		return listaPreferenciaEvento;
	}

	public void setListaPreferenciaEvento(
			Set<Preferencia_evento> listaPreferenciaEvento) {
		this.listaPreferenciaEvento = listaPreferenciaEvento;
	}

	public Set<Area_evento> getListaAreaEvento() {
		return listaAreaEvento;
	}

	public void setListaAreaEvento(Set<Area_evento> listaAreaEvento) {
		this.listaAreaEvento = listaAreaEvento;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public String getFooter5() {
		return String.format(footerMessage5, this.listaPreferenciaEvento.size());
	}

	public String getFooter6() {
		return String.format(footerMessage6, this.listaAreaEvento.size());
	}


	public Cancelacion getCancelacion() {
		return cancelacion;
	}


	public void setCancelacion(Cancelacion cancelacion) {
		this.cancelacion = cancelacion;
	}

	public List<Motivo> getListaMotivos() {
		return ListaMotivos;
	}


	public void setListaMotivos(List<Motivo> listaMotivos) {
		ListaMotivos = listaMotivos;
	}

	public String getMotivolimpiar() {
		return motivolimpiar;
	}


	public void setMotivolimpiar(String motivolimpiar) {
		this.motivolimpiar = motivolimpiar;
	}


	@Command   
	public void  ModalCancelarEvento(@BindingParam("Evento") Evento evento,@BindingParam("win") Window win) throws Exception {
		Messagebox.show("¿Esta seguro de Cancelar El Evento?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					HashMap<String, Evento> objetoMandarModal = new HashMap<String, Evento>();
					objetoMandarModal.put("objetoEvento", evento);
					Window window = (Window)Executions.createComponents(
							"content/modalCancelarEvento.zul", null, objetoMandarModal);
					window.doModal();
					win.detach();
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
				}

			}



		}
				);
	}
	@Command   
	public void  Cerrar(@BindingParam("win") Window win) throws Exception {
		win.detach();
	}


	@Command   
	public void  RegistrarCancelacion(@BindingParam("Evento") Evento evento1,@BindingParam("win") Window win) throws Exception {

		if (!CamposVacio()){
			Date fecha = new Date();
			String estado="R";
			cancelacion.setEditingStatus(false);
			cancelacion.setEstadoCancelacion(estado);
			cancelacion.setFechaCancelacion(fecha);
			this.cancelacion.setEvento(evento1);
			this.daocancelacion.agregarCancelacion(cancelacion);
			String estado1="C";
			evento1.setEstadoEvento(estado1);
			this.daoevento.modificarEvento(evento1);
			Messagebox.show("Se Cancelo el Evento Correctamente!", 
					"Advertencia", Messagebox.OK, Messagebox.INFORMATION);
			win.detach();
			BindUtils.postGlobalCommand(null, null, "RefrescarTablaEventos", null);


		}else
		{
			Messagebox.show("Debe LLenar Todos Los Campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}

	} 
	
	@Command   
	@NotifyChange({"cancelacion","motivolimpiar"})
	public void  LimpiarCancelacion(){
		this.cancelacion = new Cancelacion();
		this.motivolimpiar = "";
		
	}
	
	@Command   
	public void  CerrarModal(@BindingParam("win") Window win) throws Exception {
		win.detach();
	}

	public boolean CamposVacio(){
		if(this.cancelacion.getDescCancelacion() != null && !this.cancelacion.getDescCancelacion().equalsIgnoreCase(""))
			return false;
		return true;
	}
}
