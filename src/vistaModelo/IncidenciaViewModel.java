package vistaModelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timebox;
import org.zkoss.zul.Window;

import modelo.entidad.Area;
import modelo.entidad.Area_evento;
import modelo.entidad.Arrendamiento;
import modelo.entidad.Evento;
import modelo.entidad.Incidencia;
import modelo.entidad.Persona;
import modelo.entidad.Postulacion;
import modelo.entidad.Preferencia_evento;
import modelo.entidad.Rechazo;
import modelo.entidad.Sancion;
import modelo.entidad.TipoIndicador;
import modelo.entidad.TipoPersona;
import modelo.hibernate.dao.DaoAreaArrend;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoAreaEvento;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoIncidencia;
import modelo.hibernate.dao.DaoPersona;
import modelo.hibernate.dao.DaoPreferencia;
//TIPO INCIDENCIA
import modelo.entidad.TipoIncidencia;
import modelo.hibernate.dao.DaoTipoIncidencia;
 

 
 
public class IncidenciaViewModel extends GenericForwardComposer<Window>{
 
	 
    private static final String footerMessage = "Total de %d Incidencia(s) ";
    private IncidenciaFilter incidenciaFilter = new IncidenciaFilter();
    private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;
	 private DaoIncidencia dao = new DaoIncidencia();
	 private DaoPersona daope = new DaoPersona();
	 List<Incidencia> todosTipos;
	  private Persona persona; 
	  private Incidencia incidencia;
	  
	  
	  
	  
	 
	 public Incidencia getIncidencia() {
		return incidencia;
	}

	public void setIncidencia(Incidencia incidencia) {
		this.incidencia = incidencia;
	}

	@Init
		public void init(@ExecutionArgParam("objetoIncidencia") Persona person) {

			if (person == null) {

			 System.err.println("entroXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
 } else {
				this.persona = person;
		 } 
			 System.err.println("salio");
		}
    

//TIPO INCIDENCIA
	 List<TipoIncidencia> todosTiposIncidencia;
	 private DaoTipoIncidencia daoTipoIncidencia = new DaoTipoIncidencia();
    
    @Wire
	private Textbox nombreI, cedulaI, descripcionI;
	@Wire
	private Combobox comboTipoIncidencia;
	@Wire
	private Datebox fechaI; 
	@Wire
	private Timebox horaI;
	
    
    
	@Wire
	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public IncidenciaViewModel() {
		super();
		try {
			todosTipos = dao.obtenerTodos();
			this.incidencia= new Incidencia();
			  this.persona = new Persona();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public List<Incidencia> getObtenerTipos() {
		return todosTipos;
	}
	// para cargar los combos
	public List<TipoIncidencia> getAllTipoincidencia() {
	     try {
			return daoTipoIncidencia.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	  }
	 
	public IncidenciaFilter getIncidenciaFilter() {
		return incidenciaFilter;
	}

	public String getFooter() {
		return String.format(footerMessage, todosTipos.size());
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeFilter() {
		todosTipos = getFilterTipos(incidenciaFilter);
	}


	public List<Incidencia> getFilterTipos(IncidenciaFilter tipoFilter) {

		List<Incidencia> someTipos = new ArrayList<Incidencia>();
		String nombre = tipoFilter.getNombre().toLowerCase();
		String ced = tipoFilter.getCedula().toLowerCase();
		List<Incidencia> tiposAll = null;
		try {
			tiposAll = dao.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Incidencia> i = tiposAll.iterator(); i.hasNext();) {
			Incidencia tmp = i.next();
			if (tmp.getPersona().getNombrePersona().toLowerCase().contains(nombre)  ) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
	
	 @Command
	   	public void changeEditable(@BindingParam("AbrirModalIncidenciaMiembro") Incidencia incidencias) {
	   		HashMap registrarIncidencia = new HashMap();
	   		registrarIncidencia.put("objetoIncidencia", incidencias);
	   		Window window = (Window)Executions.createComponents(
	   				"content/modalRegistroIncidenciaMiembro.zul", null, registrarIncidencia);
	   		window.doModal();
	   	}
	
// REGISTRA LO QUE ESTA EN  MODAL DEL INCIDENCIA, REGISTRAR INCIDENCIA MIEMBRO
	@Command
	public void  RegistrarIncidencia(@BindingParam("Persona") Persona persona,@BindingParam("win") Window win) throws Exception {
	//	if (!CamposVacio()){
	//	Persona persona = (Persona) arg.get("objetoIncidencia");
	    String estado="A";
	    
	    incidencia.setEditingStatus(false);
		incidencia.setEstadoIncidencia(estado);
		
		
		incidencia.setPersona(persona);
		persona.setEstadoPersona("I");
		
		this.dao.agregarIncidencia(incidencia);
		daope.modificarPersona(persona);      // MODIFICO EL ESTADO DE PERSONA
		pagina = "vista/registrarEvento.zul";
		Messagebox.show("Se ha registrado Correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
		;	 
		win.detach();
	  
//	 }else
//		{
//			Messagebox.show("Debe llenar todos los Campos!", 
//					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
//		}
//		
		
			}  

	// VALIDA LOS CAMPOS VACIOS
		public boolean CamposVacio(){
			if(incidencia.getDescIncidencia()!=null  && incidencia.getDescIncidencia().equalsIgnoreCase("")
			&& incidencia.getTipoIncidencia() !=null &&
			incidencia.getHoraIncidencia()!=null) 
				return false;
			return true;
		}

		//Metodo creado por Michael para Limpiar los Campos Lyn del Boton Limpiar
		@Command
		@NotifyChange({"incidencia" })
		public void LimpiarIncidencia(){

			this.incidencia= new Incidencia();
		 
		}

	
	
	
	

//PARA FORMULARIO SANCIOANAR MIEMBRO
		@Command
		public void msjSancionar(){
			 JOptionPane.showMessageDialog(null, "Sancionado con EXITO");
			 System.out.println("weeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
		 }
	 
	
//	public void onClick$btnEditar() {
//
//		System.out.println(id.getValue());
//		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
//			box.setVisible(true);
//			message.setValue("Debe llenar todos los campos");
//		}
//		else {
//			try {
//				Postulacion objeto = dao.obtenerPostulacion(id.getValue());
//				objeto.setNombreParentesco(nombre.getValue());
//				objeto.setDescParentesco(descripcion.getValue());
//				dao.modificarostulacion(objeto);
//				modalDialog.detach();
//				pagina = "vista/parentesco.zul";
//				contenido.getChildren().clear();
//				contenedor = (Div) Executions.createComponents(pagina, null, null);
//				contenido.appendChild(contenedor);
//			}
//			catch (Exception x) {
//				// TODO Auto-generated catch block
//				x.printStackTrace();
//			}
//		}
//	}

//	@Command
//	public void changeEditable(@BindingParam("editarPostulacion") Postulacion tip) {
//		HashMap postulacion = new HashMap();
//		postulacion.put("objPostulacion", tip);
//		this.isEdit = true;
//		this.isCreate = false;
//		postulacion.put("edit", this.isEdit);
//		postulacion.put("create", this.isCreate);
//		Window window = (Window) Executions.createComponents(
//				"content/modalParentesco.zul", null, postulacion);
//		window.doModal();
//	}

	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeEditableStatus(@BindingParam("incidencia") Incidencia tipo) {
		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						todosTipos.remove(tipo);
						dao.eliminarIncidencia(tipo);


					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(tipo);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}
		}
				);

	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void confirm(@BindingParam("incidencia") Incidencia tipo) {
		changeEditableStatus(tipo);
		try {
			todosTipos.remove(tipo);
			dao.eliminarIncidencia(tipo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(tipo);
	}

	public void refreshRowTemplate(Incidencia tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}
	
 
 
    
    @Command
   	public void verCampos(@BindingParam("objetoIncidencia") Incidencia incidenci) {
   		HashMap incidenciaa = new HashMap();
   		incidenciaa.put("objetoIncidencia", incidenci);
   		Window window = (Window)Executions.createComponents(
   				"content/modalRegistroIncidenciaArrendamiento.zul", null, incidenciaa);
   		window.doModal();
   	}
    
	@Command
	public void AbrirModalRechazo() {
//		Window window = (Window) Executions.createComponents(
//				"content/modalRechazo.zul", null, null);
//		window.doModal();
		JOptionPane.showMessageDialog(null, "Registro Eliminado");
	}
	
	public void onClick$aprobarArr() {
		JOptionPane.showMessageDialog(null, "Se Aprobo Con Exito");

	}
      
}