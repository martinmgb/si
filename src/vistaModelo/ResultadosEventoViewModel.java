package vistaModelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import modelo.entidad.Comision;
import modelo.entidad.Evento;
import modelo.entidad.Evento_comision;
import modelo.entidad.Evento_indicador;
import modelo.entidad.Indicador;
import modelo.entidad.TipoArea;
import modelo.hibernate.dao.DaoComision;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoEvento_comision;
import modelo.hibernate.dao.DaoEvento_indicador;
import modelo.hibernate.dao.DaoIndicador;
import modelo.hibernate.dao.DaoResultado;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class ResultadosEventoViewModel extends GenericForwardComposer<Window> {

	private static  String footerMessage = "En Total %d Indicadores Asignados";
	private IndicadorEventoFilter filtroIndicadores = new IndicadorEventoFilter();
	private static List<Evento_indicador> indicadores;
	List<Indicador> i;
	private Evento evento;

	DaoEvento_indicador dao= new DaoEvento_indicador();
	DaoEvento daoe= new DaoEvento();
	DaoIndicador daoi = new DaoIndicador();
	DaoResultado daor = new DaoResultado();

	private boolean isEdit,isCreate = false;

	@Wire
	Window modalDialog;
	@Wire
	Grid grid;

	@Init
	public void init(@ExecutionArgParam("evento") Evento evento) throws Exception{
		this.evento=evento;
		System.out.print("Evento--- "+evento);
		this.indicadores=dao.obtenerPorEvento(evento);
		System.out.print("Indicadores--- "+this.indicadores);
	}

	public ResultadosEventoViewModel() {
		super();
		try {

			i = daoi.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Indicador> getAllIndicadores() throws Exception {
		try {
			return daoi.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}	
	}


	public  IndicadorEventoFilter  getfiltroIndicadores() {
		return filtroIndicadores;
	}

	public List<Evento_indicador> getIndicadorEventoModel() throws Exception {
		return this.indicadores;
	}

	public String getFooter() {
		return String.format(footerMessage, this.indicadores.size());
	}

	@Command
	@NotifyChange({"indicadorEventoModel", "footer"})
	public void changeFilter() {
		this.indicadores = getFilterEvento_indicador(filtroIndicadores);
	}


	@Command
	public void guardarMomentaneamente(@BindingParam("valor") Doublebox valor, @BindingParam("indicador") Evento_indicador indicador) {
		for (int i = 0; i < this.indicadores.size(); i++) {
			if(this.indicadores.get(i).getIdEvento_ind()==indicador.getIdEvento_ind()){
				this.indicadores.get(i).setResultado(valor.getValue());
				System.out.print("Resultado: "+this.indicadores.get(i).getResultado());
				//				try {
				//					dao.modificarEvento_indicador(this.indicadores.get(i));
				//				} catch (Exception e) {
				//					// TODO Auto-generated catch block
				//					e.printStackTrace();
				//				}
			}
		}
	}

	public void onClick$guardar() {
		//Messagebox.show("En Construcción", "Información", Messagebox.OK, Messagebox.INFORMATION);
		System.out.print("Resultado: "+this.indicadores);
		for (int a = 0; a < this.indicadores.size(); a++) {
			try {
				dao.modificarEvento_indicador(this.indicadores.get(a));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Messagebox.show("¡Se ha registrado correctamente!", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
	}

	public void onClick$finalizarResultados() {
		List<Evento_indicador> inds=this.indicadores;
		Messagebox.show("¿Esta seguro de finalizar el registro de resultados? Una vez finalizado no podrá editar los resultados", "Confirmación", Messagebox.OK | Messagebox.IGNORE  | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
			public void onEvent(Event evt) throws InterruptedException {
				if (evt.getName().equals("onOK")) {
					System.out.print("Resultado: "+inds);
					for (int a = 0; a < inds.size(); a++) {
						try {
							dao.modificarEvento_indicador(inds.get(a));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					Evento e = indicadores.get(0).getEvento();
					e.setEstadoEvento("F");
					try {
						daoe.modificarEvento(e);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					BindUtils.postGlobalCommand(null, null, "RefrescarTablaResultados", null);
					Messagebox.show("Se ha finalizado el registro de resultados correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
					modalDialog.detach();
				}
				else if (evt.getName().equals("onCancel")) {
					Messagebox.show("¡Operación Cancelada!", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
				}
			}
		});
	}


	private void refreshRowTemplate() {

		//Metodo Para Refrescar la Tabla cuando elimina
		BindUtils.postNotifyChange(null, null,  ResultadosEventoViewModel.this, "comisionEventoModel");
		//BindUtils.postNotifyChange(null, null,  ComisionEventoViewModel.this, "footer");
	}
	public void onClick$btnEditar() {

		//Messagebox.show("En Construcción", "Información", Messagebox.OK, Messagebox.INFORMATION);

		/*if(this.comboIndicadores.getSelectedItem().getValue().equals("") || valorEsperado.getValue().equals("")) {
			Messagebox.show("Debe llenar todos los campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else {
			try {
				Evento_indicador indicador = (Evento_indicador) arg.get("objeto");
				indicador.setIndicador(this.comboIndicadores.getSelectedItem().getValue());
				indicador.setValorEspEvento_ind(valorEsperado.getValue());
				dao.modificarEvento_indicador(indicador);
				modalDialog4.detach();
			}
			catch (Exception x) {
				// TODO Auto-generated catch block
				x.printStackTrace();
			}
		}*/
	}

	@Command
	@NotifyChange({"indicadores", "footer"})
	public void eliminarIndicadorEvento(@BindingParam("indicadorEliminar") Evento_indicador indicador) {

		/*Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						if(daor.obtenerPorIndicadorEvento(indicador).size()==0){
							indicadores.remove(indicador);
							dao.eliminarEvento_indicador(indicador);
							if(indicadores.size()==0){
								Evento evento = (Evento) arg.get("evento");
								evento.setEstadoEvento("C");
							}
						}
						else{
							Messagebox.show("No se puede eliminar, este indicador ya tiene resultado asignado.", "Error", Messagebox.OK, Messagebox.ERROR);
						}
					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(indicador);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}

			private void refreshRowTemplate(Evento_indicador indicador) {

				//Metodo Para Refrescar la Tabla cuando elimina
				BindUtils.postNotifyChange(null, null,  IndicadorEventoViewModel.this, "indicadores");
				BindUtils.postNotifyChange(null, null,  IndicadorEventoViewModel.this, "footer");
			}
		}
				);*/

	}


	public List<Evento_indicador> getFilterEvento_indicador(IndicadorEventoFilter tipoFilter) {

		List<Evento_indicador> someTipos = new ArrayList<Evento_indicador>();
		String nombre = tipoFilter.getNombre().toLowerCase();
		//String des = tipoFilter.getTipo().toLowerCase();
		List<Evento_indicador> tiposAll = null;
		try {
			//tiposAll = dao.obtenerPorEvento(evento);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Evento_indicador> i = tiposAll.iterator(); i.hasNext();) {
			Evento_indicador tmp = i.next();
			if (tmp.getIndicador().getNombreIndicador().toLowerCase().contains(nombre)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}


}
