package vistaModelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import modelo.entidad.Area;
import modelo.entidad.Cargo;
import modelo.entidad.Persona;
import modelo.entidad.TipoPersona;
import modelo.hibernate.dao.DaoPersona;
import modelo.hibernate.dao.DaoCargo;
import modelo.hibernate.dao.DaoTipoPersona;

public class EmpleadoViewModel extends GenericForwardComposer<Window> {

	private static final String footerMessage = "Total de %d Empleados ";
	private PersonaFilter empleadoFilter = new PersonaFilter();
	private DaoPersona dao = new DaoPersona();
	private DaoTipoPersona daoTipoPersona = new DaoTipoPersona();
	private DaoCargo daoCargo = new DaoCargo();
	private List<Persona> todosTipos;
	private TipoPersona tipoempleado;
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit, isCreate, isView = false;

	@Wire
	private Intbox id;
	@Wire
	private Textbox cedula;
	@Wire
	private Textbox nombre;
	@Wire
	private Textbox apellido;
	@Wire
	private Datebox fechaNac;
	@Wire
	private Textbox direccion;
	@Wire
	private Textbox correoEmpleado;
	@Wire
	private Textbox telefono;
	@Wire
	private Combobox comboSexo;
	@Wire
	private Combobox comboCargo;
	@Wire
	Window modalDialog;
	@Wire
	public Label message;
	@Wire
	public Div box;

	private Div contenido;

	private Div contenedor;

	private String pagina;

	private String cedulaBus;

	private Cargo cargoselect;
	private Combobox comboCivil;
	private String selecti;
	private boolean encontrado;
	private boolean emplea;

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public String getCedulaBus() {
		return cedulaBus;
	}

	public void setCedulaBus(String cedulaBus) {
		this.cedulaBus = cedulaBus;
	}

	public EmpleadoViewModel() {
		super();
		cargoselect = new Cargo();
		try {
			tipoempleado = daoTipoPersona.obtenerTipo("Empleado");
			todosTipos = dao.obtenerPersonaTipoMiembro(tipoempleado);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Cargo getCargoselect() {
		return cargoselect;
	}

	public void setCargoselect(Cargo cargoselect) {
		this.cargoselect = cargoselect;
	}

	public String getSelecti() {
		return selecti;
	}

	public void setSelecti(String selecti) {
		this.selecti = selecti;
	}

	public List<Cargo> getAllCargo() {
		try {
			return daoCargo.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public List<Persona> getObtenerTipos() {
		return todosTipos;
	}

	public PersonaFilter getEmpleadoFilter() {
		return empleadoFilter;
	}

	public String getFooter() {
		return String.format(footerMessage, todosTipos.size());
	}

	@Command
	@NotifyChange({ "obtenerTipos", "footer" })
	public void changeFilter() {
		todosTipos = getFilterTipos(empleadoFilter);
	}

	public List<Persona> getFilterTipos(PersonaFilter empleadoFilter) {

		List<Persona> someTipos = new ArrayList<Persona>();
		String cedula = empleadoFilter.getCedula().toLowerCase();
		String cargo = empleadoFilter.getCargo().toLowerCase();
		List<Persona> tiposAll = null;
		try {
			tiposAll = dao.obtenerPersonaTipoMiembro(tipoempleado);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (Iterator<Persona> i = tiposAll.iterator(); i.hasNext();) {
			Persona tmp = i.next();
			// if (tmp.getNombreArea().toLowerCase().contains(nombre) &&
			// tmp.getDescArea().toLowerCase().contains(des)&&
			// tmp.getTipoArea().getNombreTipoArea().toLowerCase().contains(tipoA))
			// {
			if (tmp.getCedPersona().toLowerCase().contains(cedula)
					&& tmp.getCargo().getNombreCargo().toLowerCase()
					.contains(cargo)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}

	public void onClick$btnRegistrar() {

		if  ((nombre.getValue().equals(""))|| (apellido.getValue().equals(""))||(fechaNac.getValue().equals(""))||
				(comboSexo.getSelectedItem().getValue().equals(""))||(direccion.getValue().equals(""))||(correoEmpleado.getValue().equals(""))||(comboCivil.getSelectedItem().getValue().equals(""))||(telefono.getValue().equals(""))||(comboCargo.getSelectedItem().getValue().equals("")))  {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		} else {
			try {
				Persona per = new Persona();
				per.setApellidoPersona(apellido.getValue());
				per.setCargo((Cargo) comboCargo.getSelectedItem().getValue());
				per.setCedPersona(cedula.getValue());
				per.setDirecPersona(direccion.getValue());
				per.setCorreoPersona(correoEmpleado.getValue());
				per.setEstadoCivilPersona(comboCivil.getSelectedItem().getValue());
				per.setFechaNacPersona(fechaNac.getValue());
				per.setNombrePersona(nombre.getValue());
				per.setSexoPersona(comboSexo.getSelectedItem().getValue());
				per.setTipoPersona(tipoempleado);
				per.setTlfCelPersona(telefono.getValue());
				per.setEstadoPersona("A");
				dao.agregarPersona(per);
				Messagebox.show("Se ha Registrado Correctamente", "Information", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/registrarEmpleado.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void onClick$btnLimpiar() {
		nombre.setText("");
		apellido.setText("");
		cedula.setText("");
		direccion.setText("");
		fechaNac.setText("");
		telefono.setText("");
		comboCargo.setText("");
		comboCivil.setText("");
		comboSexo.setText("");
		correoEmpleado.setText("");


	}




	


	public void onClick$btnBuscar() throws Exception {
		encontrado=false;
		//if (!"".equalsIgnoreCase(cedula.getValue())) {
		List<Persona> pers = dao.obtenerTodos();
		for (int i = 0; i < pers.size(); i++) {
			if (this.cedula.getText().equals(pers.get(i).getCedPersona())) {
				Messagebox.show("Ya se encuentra Registrado !", "Advertencia",
						Messagebox.OK, Messagebox.EXCLAMATION);
				this.nombre.setFocus(true);
				this.nombre.setDisabled(true);
				this.apellido.setDisabled(true);
				this.fechaNac.setDisabled(true);
				this.comboSexo.setDisabled(true);
				this.direccion.setDisabled(true);
				this.correoEmpleado.setDisabled(true);
				this.comboCivil.setDisabled(true);
				this.telefono.setDisabled(true);
				this.comboCargo.setDisabled(true);
				encontrado = true;
			}
		}
		if (encontrado==false){
			System.out.print("Aqui entro1");
			
			Messagebox.show("La cedula no Existe!", "Advertencia",
					Messagebox.OK, Messagebox.EXCLAMATION);
			this.nombre.setDisabled(false);
			this.apellido.setDisabled(false);
			this.fechaNac.setDisabled(false);
			this.comboSexo.setDisabled(false);
			this.direccion.setDisabled(false);
			this.correoEmpleado.setDisabled(false);
			this.comboCivil.setDisabled(false);
			this.telefono.setDisabled(false);
			this.comboCargo.setDisabled(false);
		}

	}

	public void onClick$btnEditar() {

		System.out.println(id.getValue());
		if ((nombre.getValue().equals(""))|| (apellido.getValue().equals(""))||(fechaNac.getValue().equals(""))||
				(comboSexo.getSelectedItem().getValue().equals(""))||(direccion.getValue().equals(""))||(correoEmpleado.getValue().equals(""))||(comboCivil.getSelectedItem().getValue().equals(""))||(telefono.getValue().equals(""))||(comboCargo.getSelectedItem().getValue().equals(""))) {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		} else {
			try { 
			
				
				Persona objeto = dao.obtenerPersona(id.getValue());
	
				objeto.setNombrePersona(nombre.getValue());
				objeto.setApellidoPersona(apellido.getValue());
				objeto.setFechaNacPersona(fechaNac.getValue());
				objeto.setSexoPersona(comboSexo.getSelectedItem().getValue());
				objeto.setDirecPersona(direccion.getValue());
				objeto.setCorreoPersona(correoEmpleado.getValue());
				objeto.setEstadoCivilPersona(comboCivil.getSelectedItem().getValue());
				objeto.setTlfCelPersona(telefono.getValue());
				objeto.setCargo(comboCargo.getSelectedItem().getValue());
			
				dao.modificarPersona(objeto);
				Messagebox.show("Se ha Modificado Correctamente", "Information", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/registrarEmpleado.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			} catch (Exception x) {
				// TODO Auto-generated catch block
				x.printStackTrace();
			}
		}
	}

	@Command
	public void changeEditable(@BindingParam("editarEmpleado") Persona empleado) {
		HashMap emple = new HashMap();
		emple.put("objEmpleado", empleado);
		this.isEdit = true;
		this.isCreate = false;
		emple.put("edit", this.isEdit);
		emple.put("create", this.isCreate);
		Window window = (Window) Executions.createComponents(
				"content/modalRegistrarEmpleado.zul", null, emple);
		window.doModal();
	}

	@NotifyChange({ "obtenerTipos", "displayEdit" })
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	@Command
	@NotifyChange({ "obtenerTipos", "footer" })
	public void changeEditableStatus(@BindingParam("empleado") Persona empleado) {
		Messagebox.show("¿Esta seguro de eliminar el registro?",
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
			public void onEvent(Event e) {
				if (Messagebox.ON_OK.equals(e.getName())) {
					// OK is clicked
					try {
						todosTipos.remove(empleado);
						dao.eliminarPersona(empleado);
						 Messagebox.show("¡Se ha eliminado correctamente!", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(empleado);
				} else if (Messagebox.ON_CANCEL.equals(e.getName())) {
					// Cancel is clicked
				}
			}

			private void refreshRowTemplate(Persona empleado) {
				// Metodo Para Refrescar la Tabla cuando elimina
				BindUtils.postNotifyChange(null, null,
						EmpleadoViewModel.this, "obtenerTipos");
				BindUtils.postNotifyChange(null, null,
						EmpleadoViewModel.this, "footer");
			}
		});

	}

	@Command
	@NotifyChange({ "obtenerTipos", "footer" })
	public void confirm(@BindingParam("empleado") Persona empleado) {
		changeEditableStatus(empleado);
		try {
			todosTipos.remove(empleado);
			dao.eliminarPersona(empleado);
			Messagebox.show("Se ha Eliminado Correctamente", "Information",
					Messagebox.OK, Messagebox.INFORMATION);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(empleado);
	}




	public void refreshRowTemplate(Persona empleado) {
		/*
		 * This code is special and notifies ZK that the bean's value has
		 * changed as it is used in the template mechanism. This stops the
		 * entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, empleado, "editingStatus");
	}

	@Command
	@NotifyChange({ "empleado", "" })
	public void BuscarCedula() throws Exception {
		List<Persona> pers = dao.obtenerTodos();
		// System.err.println(personas.size());
		for (int i = 0; i < pers.size(); i++) {
			System.out.print(cedula.getValue().trim().toString());
			if (this.cedula.getValue().equals(pers.get(i).getCedPersona())) {
				apellido.setValue(pers.get(i).getApellidoPersona());
				comboCargo.getSelectedItem().setValue(pers.get(i).getCargo());
				direccion.setValue(pers.get(i).getDirecPersona());
				correoEmpleado.setValue(pers.get(i).getCorreoPersona());
				comboCivil.getSelectedItem().setValue(
						pers.get(i).getEstadoCivilPersona());
				fechaNac.setValue(pers.get(i).getFechaNacPersona());
				nombre.setValue(pers.get(i).getNombrePersona());
				comboSexo.getSelectedItem().setValue(
						pers.get(i).getSexoPersona());
				telefono.setValue(pers.get(i).getTlfCelPersona());
				encontrado = true;
				break;
			}
		}

		if (encontrado == false) {
			Messagebox.show("La cedula no Existe!", "Advertencia",
					Messagebox.OK, Messagebox.EXCLAMATION);

		}
	}

	public TipoPersona getTipoempleado() {
		return tipoempleado;
	}

	public void setTipoempleado(TipoPersona tipoempleado) {
		this.tipoempleado = tipoempleado;
	}
}