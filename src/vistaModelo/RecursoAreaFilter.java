package vistaModelo;

public class RecursoAreaFilter {
	private String nombre="";
	private String cantidad="";

	public String getNombre() {
		return nombre;
	}
	
	public String getCantidad() {
		return cantidad;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}
	
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad==null?"":cantidad.trim();
	}
	
}
