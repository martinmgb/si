package vistaModelo;

public class TipoPublicoFilter {
	private String codigo="";
	private String nombre="";
	private String descripcion="";


	public String getCodigo() {
		return codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo==null?"":codigo.trim();
	}

}
