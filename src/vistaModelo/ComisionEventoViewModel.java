package vistaModelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import modelo.entidad.Comision;
import modelo.entidad.Evento;
import modelo.entidad.Evento_comision;
import modelo.hibernate.dao.DaoComision;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoEvento_comision;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class ComisionEventoViewModel extends GenericForwardComposer<Window> {

	private static  String footerMessage = "En Total %d Comisiones Asignadas";
	private ComisionEventoFilter filtroComisiones = new ComisionEventoFilter();
	List<Evento_comision> comisiones= new ArrayList<Evento_comision>();
	List<Comision> c;
	private Evento evento;

	DaoEvento_comision dao= new DaoEvento_comision();
	DaoEvento daoe= new DaoEvento();
	DaoComision daoc = new DaoComision();
	private boolean isEdit,isCreate = false;

	@Wire
	private Combobox comboComisiones;
	@Wire
	private Textbox responsable;
	@Wire
	Window modalDialog3;

	@Init
	public void init(@ExecutionArgParam("evento") Evento evento) throws Exception{
		this.evento=evento;
		comisiones=dao.obtenerPorEvento(evento);
	}

	public ComisionEventoViewModel() {
		super();
		try {

			c = daoc.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Comision> getAllComisiones() throws Exception {
		try {
			List<Comision> arr= new ArrayList<Comision>();
			List<Evento_comision> ce= comisiones;
			c = daoc.obtenerTodos();
			boolean enc;
			for (int i = 0; i < c.size(); i++) {
				enc=false;
				for (int j = 0; j < ce.size(); j++) {
					if(c.get(i).getIdComision()==ce.get(j).getComision().getIdComision()){
						enc=true;
					}
				}
				if(!enc){
					arr.add(c.get(i));
				}
			}
			return arr;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}	
	}

	public List<Comision> getAllComisiones2() throws Exception {
		return daoc.obtenerTodos();	
	}

	public  ComisionEventoFilter  getfiltroComisiones() {
		return filtroComisiones;
	}

	public List<Evento_comision> getComisionEventoModel() {
		return comisiones;
	}

	public String getFooter() {
		return String.format(footerMessage, comisiones.size());
	}

	@Command
	@NotifyChange({"comisionEventoModel", "footer"})
	public void changeFilter() {
		comisiones = getFilterEvento_comision(filtroComisiones);
	}

	@NotifyChange({"*"})
	public void onClick$btnRegistrarComisionEvento() {

		//Messagebox.show("En Construcción", "Información", Messagebox.OK, Messagebox.INFORMATION);

		if(comboComisiones.getValue()=="" || responsable.getText()=="") {
			Messagebox.show("Debe llenar todos los campos", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else {
			try {
				Evento e = (Evento) arg.get("evento");
				Evento_comision comision = new Evento_comision(e, comboComisiones.getSelectedItem().getValue(),responsable.getText());
				System.out.print("Comision -> "+ e);
				comisiones.add(comision);
				dao.agregarEvento_comision(comision);
				BindUtils.postGlobalCommand(null, null, "RefrescarTablaComisiones", null);
				modalDialog3.detach();
				Messagebox.show("Se ha Registrado Correctamente", "Information", Messagebox.OK, Messagebox.INFORMATION);

//				pagina = "content/gridComisionesEvento.zul";
//				System.out.print("TabPanel"+p1);
//				p1.getChildren().clear();
//				contenedor = (Panel) Executions.createComponents(pagina, null, null);
//				p1.appendChild(contenedor);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void onClick$btnEditarComisionEvento() {

		//Messagebox.show("En Construcción", "Información", Messagebox.OK, Messagebox.INFORMATION);

		if((this.comboComisiones.getSelectedItem().getValue().equals("")) || (responsable.getText().equals(""))) {
			Messagebox.show("Debe llenar todos los campos", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else {
			try {
				Evento_comision comision = (Evento_comision) arg.get("objeto");
				comision.setComision(this.comboComisiones.getSelectedItem().getValue());
				comision.setResponsable(responsable.getText());
				dao.modificarEvento_comision(comision);
				BindUtils.postGlobalCommand(null, null, "RefrescarTablaComisiones", null);
				modalDialog3.detach();
	            Messagebox.show("¡El registro se ha editado correctamente!", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
			}
			catch (Exception x) {
				// TODO Auto-generated catch block
				x.printStackTrace();
			}
		}
	}


	@Command
	@NotifyChange({"comisiones", "footer"})
	public void eliminarComisionEvento(@BindingParam("comisionEliminar") Evento_comision comision) {

		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Interrogativo", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						comisiones.remove(comision);
						dao.eliminarEvento_comision(comision);
			            Messagebox.show("Se ha eliminado correctamente", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(comision);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}

			
		}
				);

	}

	public void onClick$btnLimpiar() {
		comboComisiones.setValue("");
		responsable.setText("");
		//nombre.setFocus(true);
	}

	@Command
	public void changeEditable(@BindingParam("comision")  Evento_comision ce, @BindingParam("evento")  Evento e) {
		HashMap comisionEvento = new HashMap();
		comisionEvento.put("objeto", ce);
		comisionEvento.put("evento", e);
		this.isEdit = true;
		this.isCreate = false;
		comisionEvento.put("edit", this.isEdit);
		comisionEvento.put("create", this.isCreate);
		Window window = (Window) Executions.createComponents(
				"content/modalComisionEvento.zul", null, comisionEvento);
		window.doModal();
	}

	@Command
	public void showModalComisionEvento() {
		Evento_comision ce = new Evento_comision();
		Evento e = new Evento();
		e=this.evento;
		HashMap a = new HashMap();
		a.put("evento", e);
		this.isEdit = false;
		this.isCreate = true;
		a.put("edit", this.isEdit);
		a.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalComisionEvento.zul", null, a);
		window.doModal();
	}


	public List<Evento_comision> getFilterEvento_comision(ComisionEventoFilter tipoFilter) {

		List<Evento_comision> someTipos = new ArrayList<Evento_comision>();
		String nombre = tipoFilter.getNombre().toLowerCase();
		//String des = tipoFilter.getTipo().toLowerCase();
		List<Evento_comision> tiposAll = null;
		try {
			//tiposAll = dao.obtenerPorEvento(evento);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Evento_comision> i = tiposAll.iterator(); i.hasNext();) {
			Evento_comision tmp = i.next();
			if (tmp.getComision().getNombreComision().toLowerCase().contains(nombre)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
	
	
	private void refreshRowTemplate(Evento_comision evento) {
		//Metodo Para Refrescar la Tabla cuando elimina
		BindUtils.postNotifyChange(null, null,  ComisionEventoViewModel.this, "comisionEventoModel");
		BindUtils.postNotifyChange(null, null,  ComisionEventoViewModel.this, "footer");
	}
	
	@GlobalCommand
	@NotifyChange({"comisionEventoModel", "footer"})
	public void RefrescarTablaComisiones() throws Exception {
		comisiones=dao.obtenerPorEvento(this.evento);
	}


}
