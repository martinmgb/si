package vistaModelo;

public class RecursoFilter {

	private String id="", nombre="", descripcion="";
	private int cantidad ;

	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id==null?"":id.trim();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String tipo) {
		this.nombre = nombre==null?"":nombre.trim();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion==null?"":descripcion.trim();
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	
}