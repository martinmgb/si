package vistaModelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Map;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import controlador.ManejadorMail;
import modelo.entidad.Opinion;
import modelo.hibernate.dao.DaoOpinion;



public class OpinionViewModel extends GenericForwardComposer<Window> { 

	private static final String footerMessage = "Total de %d Opiniones ";
	private OpinionFilter opinionFilter;
	private DaoOpinion dao = new DaoOpinion();
	private List<Opinion> listaOpinion;
	private List<Opinion> todosTipos;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;
	private Opinion opinion;


	public Opinion getOpinion() {
		return opinion;
	}



	public void setOpinion(Opinion opinion) {
		this.opinion = opinion;
	}


	@Init
	public void init() throws Exception {
		listaOpinion=this.dao.obtenerOpinionPorEstado("E");
		this.opinionFilter =new OpinionFilter();
		this.opinion = new Opinion();


	}



	public List<Opinion> getListaOpinion() {
		return listaOpinion;
	}


	@Command
	public void RegistrarSugerencia(@BindingParam("objeto") Opinion opinion1,@BindingParam("win") Window win ) throws Exception{
		opinion1.setRespuestaOpinion(opinion.getRespuestaOpinion());
		String estado="R";
		opinion1.setEstadoOpinion(estado);
		this.dao.modificarOpinion(opinion1);
		win.detach();
		Messagebox.show("La Repuesta se Registro Con Exito!", 
				"Advertencia", Messagebox.OK, Messagebox.NONE);
		BindUtils.postGlobalCommand(null, null, "RefrescarTablaOpinion", null);
		String mensaje, destinatario, asunto;
		mensaje = "Sr(a) " + opinion1.getUsuario().getPersona().getNombrePersona() + " " + opinion1.getUsuario().getPersona().getApellidoPersona()
				+ " El presente correo es para informarle que su Sugerencia ha sido Respondida."
				+ ", Es la siguiente"+" "+ opinion1.getRespuestaOpinion()+"."
				+ " y recuerde que Teide es tu Socio Ideal";
		destinatario = opinion1.getUsuario().getPersona().getCorreoPersona();
		asunto = "Su Sugerencia ha sido Respondida";
		ManejadorMail.enviarEmail(mensaje, destinatario, asunto);

	}
	@Command
	@NotifyChange({"opinion"})
	public void Limpiar(){
		this.opinion = new Opinion();
	}




	public void setListaOpinion(List<Opinion> listaOpinion) {
		this.listaOpinion = listaOpinion;
	}

	public OpinionFilter getOpinionFilter() {
		return opinionFilter;
	}

	public void setOpinionFilter(OpinionFilter opinionFilter) {
		this.opinionFilter = opinionFilter;
	}
	public String getFooter() {
		return String.format(footerMessage, this.listaOpinion.size());
	}

	@Command
	@NotifyChange({"listaOpinion", "footer"})
	public void changeFilter() {
		listaOpinion = getFilterTipos(opinionFilter);
	}


	public List<Opinion> getFilterTipos(OpinionFilter tipoFilter) {

		List<Opinion> someTipos = new ArrayList<Opinion>();
		String ced = tipoFilter.getCedula().toLowerCase();
		List<Opinion> tiposAll = null;
		try {
			listaOpinion=this.dao.obtenerOpinionPorEstado("E");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Opinion> i = tiposAll.iterator(); i.hasNext();) {
			Opinion tmp = i.next();
			if (tmp.getUsuario().getPersona().getCedPersona().contains(ced)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}











	//METODO

	@Command
	public void changeEditable(@BindingParam("atenderOpiniones") Opinion atenderOpi) {
		HashMap atenderOpiniones = new HashMap();
		atenderOpiniones.put("objOpinion", atenderOpi);
		Window window = (Window)Executions.createComponents(
				"content/modalOpinion.zul", null, atenderOpiniones);
		window.doModal();
	}

	@GlobalCommand
	@NotifyChange({"listaOpinion", "footer"})
	public void RefrescarTablaOpinion() throws Exception {
		listaOpinion=this.dao.obtenerOpinionPorEstado("E");
	}


}