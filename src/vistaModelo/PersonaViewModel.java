package vistaModelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import modelo.entidad.Accion;
import modelo.entidad.Evento;
import modelo.entidad.Incidencia;
import modelo.entidad.Parentesco;
import modelo.entidad.Persona;
import modelo.entidad.Postulacion;
import modelo.entidad.TipoPersona;
import modelo.entidad.Visita;
import modelo.hibernate.dao.DaoVisita;
import modelo.hibernate.dao.DaoAccion;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoParentesco;
import modelo.hibernate.dao.DaoPersona;


public class PersonaViewModel extends GenericForwardComposer<Window> { 

	private static final String footerMessage = "Total de %d Personas ";
	private static final String footerMessage1 = "Total de %d Miembro(s) Activo(s)";
	private static final String footerMessage2 = "Total de %d Miembros(s) Activo(s)";
 
	private static final String footerMessageNroAcciones = "Total de %d Accion(es) "; 
	private static final String footerMessageActivasMiembro = "Total de %d Miembrodasdasdsa(s) Activo(s)"; // PARA EL FILTRO DE VISITAS
	private PersonaFilter personaFilter = new PersonaFilter();
	private DaoPersona dao = new DaoPersona();
	private List<Persona> todosTipos, menosSancion, todosMiembros,activaTipoM, propietarios;
 
	
	private static final  String footerMessage3 = "Total de %d Miembro(s) Activo(s)";
 
	
 
	private List<Accion> listaAcciones;
	private DaoAccion accionDao = new DaoAccion();
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;
	
	private TipoPersona tipPersona; 
	
	
	
//Accion
	private Accion accion = new Accion();
	
//Incidencia
	private Incidencia incidencia = new Incidencia();
	
	private Visita visita = new Visita();



 
	@Wire
	Window modalDialog;
	@Wire
	public Label message;
	@Wire
	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;
	private Persona persona = new Persona();
	
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}
 
	HashMap cesarPersona = new HashMap();


public List<Persona> getPropietariosActivosModel() throws Exception {
	return propietarios;
}



 
	public PersonaViewModel() {
		super();
		try {
			todosTipos = dao.obtenerTodos(); //	TODAS LAS PERSONAS
			menosSancion = dao.obtenerPersonaMenosSancion("S",1,2,5) ; // TODOS ESTADOS MENOS SANCION, y trae tipo(propietario, honorario,beneficiario)
			activaTipoM = dao.obtenerActivasTipoM("A",1,2,5);   // PERSONAS  ACTIVAS y  trae tipo(propietario, honorario,beneficiario)
			propietarios=dao.obtenerPersonaMenosSancionPropiHonora("S",1,2); // TODOS ESTADOS MENOS SANCION, y  trae tipo(propietario, honorario)
			
			todosMiembros = dao.obtenerPersonaTipoMiembros(1,2);// TODAS LOS MIEMBROS, honorario y propietario
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
 
	 

	public List<Accion> getListaAcciones() {
		return listaAcciones;
	}

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public Accion getAccion() {
		return accion;
	}
 
	public List<Persona> getObtenerTipos() {
		return todosTipos;
	}
	 
	public List<Persona> getActivaTipoM() {
		return activaTipoM;
	}
	
	public List<Persona> getMenosSancion() {
		return menosSancion;
	}
	

	public List<Persona> getTodosMiembros() {
		return todosMiembros;
	}
 

	public PersonaFilter getPersonaFilter() {
		return personaFilter;
	}

	public String getFooter() {
		return String.format(footerMessage, todosTipos.size());
	}
	public String getFooter1() {
		return String.format(footerMessage1, menosSancion.size());
	}
	public String getFooter2() {
		return String.format(footerMessage2, todosMiembros.size()); // PARA FILTRO BENEFICIARIO Y CESE ACCION
	}
	public String getFooter3() {
		return String.format(footerMessage3, propietarios.size());
	}
	public String getFooterAccionesMiembro() {
		return String.format(footerMessageNroAcciones, listaAcciones.size());
	}
	public String getFooterVisitas() {
		return String.format(footerMessageActivasMiembro, activaTipoM.size()); // PARA EL FILTRO DE VISITAS
	}

// METODO PARA FILTRAR-----------------------------------------------------------------
	 @Command
	@NotifyChange({"propietariosActivosModel", "footer3"})
	public void changeFilterPro() {
		propietarios = getFilterPropietarios(personaFilter);
	}
  
	@Command
	@NotifyChange({"todosMiembros", "footer2"})
	public void changeFiltersTodosMiembros() {
		todosMiembros = getFilterTodosMiembro(personaFilter);
	}
	// PARA EL FILTRO DE VISITAS
	@Command
	@NotifyChange({"activaTipoM", "footerMessageActivasMiembro"})
	public void changeFilterVisita() {
		activaTipoM = getFilterVisita(personaFilter);
	}
	@Command
	@NotifyChange({"menosSancion", "footer1"})
	public void changeFiltersMenoSancion() {
		menosSancion =  getFilterMenosSancion(personaFilter);
	}
	
// PARA FILTAR COMO NO LO USO TODAVIA
	public List<Persona> getFilterMenosSancion(PersonaFilter tipoFilter) {

		List<Persona> someTipos = new ArrayList<Persona>();
		String ced = tipoFilter.getCedula().toLowerCase();
		List<Persona> tiposAll = null;
		try {
			tiposAll =dao.obtenerPersonaMenosSancion("S",1,2,5);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Persona> i = tiposAll.iterator(); i.hasNext();) {
			Persona tmp = i.next();
			if (tmp.getCedPersona().toLowerCase().contains(ced)  ) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
	
	public List<Persona> getFilterTodosMiembro(PersonaFilter tipoFilter) {

		List<Persona> someTipos = new ArrayList<Persona>();
		String ced = tipoFilter.getCedula().toLowerCase();
		List<Persona> tiposAll = null;
		try {
			tiposAll = dao.obtenerPersonaTipoMiembros(1,2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Persona> i = tiposAll.iterator(); i.hasNext();) {
			Persona tmp = i.next();
			if (tmp.getCedPersona().toLowerCase().contains(ced)  ) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
 
	
	public List<Persona> getFilterPropietarios(PersonaFilter filtroBeneficiarios2) {

		List<Persona> someTipos = new ArrayList<Persona>();
		String cedula = filtroBeneficiarios2.getCedula().toLowerCase(); 
		List<Persona> tiposAll = null;
		try {
			tiposAll = dao.obtenerPersonaMenosSancionPropiHonora("S",1,2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Persona> i = tiposAll.iterator(); i.hasNext();) {
			Persona tmp = i.next();
			if (tmp.getCedPersona().toLowerCase().contains(cedula)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
	
	public List<Persona> getFilterVisita(PersonaFilter tipoFilter) {

		List<Persona> someTipos = new ArrayList<Persona>();
		String ced = tipoFilter.getCedula().toLowerCase();
		List<Persona> tiposAll = null;
		try {
			tiposAll = dao.obtenerActivasTipoM("A",1,2,5);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Persona> i = tiposAll.iterator(); i.hasNext();) {
			Persona tmp = i.next();
			if (tmp.getCedPersona().toLowerCase().contains(ced)  ) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
	
	
	 
	
	
//PARA REGISTRARA	
//	public void onClick$btnRegistrar() {
//
//		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
//			box.setVisible(true);
//			message.setValue("Debe llenar todos los campos");
//		}
//		else {
//			try {
//				Parentesco parentesco= new Parentesco(nombre.getText(),
//						descripcion.getText(),"A");
//				dao.agregarParentesco(parentesco);
//				modalDialog.detach();
//				pagina = "vista/parentesco.zul";
//				contenido.getChildren().clear();
//				contenedor = (Div) Executions.createComponents(pagina, null, null);
//				contenido.appendChild(contenedor);
//			}
//			catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}

	
//PARA LIMPIAR
//	public void onClick$btnLimpiar() {
//		nombre.setText("");
//		descripcion.setText("");
//		nombre.setFocus(true);
//	}
	
	//PARA EDITAR
//	public void onClick$btnEditar() {
//
//		System.out.println(id.getValue());
//		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
//			box.setVisible(true);
//			message.setValue("Debe llenar todos los campos");
//		}
//		else {
//			try {
//				Parentesco objeto = dao.obtenerParentesco(id.getValue());
//				objeto.setNombreParentesco(nombre.getValue());
//				objeto.setDescParentesco(descripcion.getValue());
//				dao.modificarParentesco(objeto);
//				modalDialog.detach();
//				pagina = "vista/parentesco.zul";
//				contenido.getChildren().clear();
//				contenedor = (Div) Executions.createComponents(pagina, null, null);
//				contenido.appendChild(contenedor);
//			}
//			catch (Exception x) {
//				// TODO Auto-generated catch block
//				x.printStackTrace();
//			}
//		}
//	}

//	@Command
//	public void changeEditable(@BindingParam("editarParentesco") Parentesco tip) {
//		HashMap parentesco = new HashMap();
//		parentesco.put("objParentesco", tip);
//		this.isEdit = true;
//		this.isCreate = false;
//		parentesco.put("edit", this.isEdit);
//		parentesco.put("create", this.isCreate);
//		Window window = (Window) Executions.createComponents(
//				"content/modalParentesco.zul", null, parentesco);
//		window.doModal();
//	}
 
	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeEditableStatus(@BindingParam("persona") Persona tipo) {
		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						todosTipos.remove(tipo);
						dao.eliminarPersona(tipo);


					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(tipo);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}
		}
				);

	}

 

	public void refreshRowTemplate(Persona tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}       
	
	
// ABRE MODAL DE REGISTRAR INCIDENCIA	
	  @Command
	   	public void changeEditableAbrirIncidenciaMiembro(@BindingParam("AbrirModalIncidenciaMiembro") Persona incidencias) {
	   		HashMap registrarIncidencia = new HashMap();
	   		registrarIncidencia.put("objetoIncidencia", incidencias);
	   		Window window = (Window)Executions.createComponents(
	   				"content/modalRegistroIncidenciaMiembro.zul", null, registrarIncidencia);
	   		window.doModal();
	   	}

 

 

	
// PARA VISITAS
	
	  @Command({"visitaMoDDDdal"}) @GlobalCommand("visitaModal")
		    public void abrirModalRegistrarAcompanante(Event e) {
			Window window = (Window)Executions.createComponents(
					"content/modalVisita.zul", null, null);
			window.doModal();
		       
		    }
	  
// PARA ABRIR VISITAS
	  @Command
	 	public void abrirSi(@BindingParam("abrirSi") Persona visitas) {
	 			HashMap visit = new HashMap();
	    		 visit.put("objVisita", visitas);
	    		 Window window = (Window)Executions.createComponents(
	    		 "content/modalVisitaSi.zul", null, visit);
	    		 window.doModal();
	    		}  	
	  
	  @Command
	 	public void abrirNo(@BindingParam("abrirNo") Persona visitas) {
	 			HashMap visit = new HashMap();
	    		 visit.put("objVisita", visitas);
	    		 Window window = (Window)Executions.createComponents(
	    		 "content/modalVisitaNo.zul", null, visit);
	    		 window.doModal();
	    		}    
	  
//ABRE MODAL CESE ACCION
	  @Command
	  public void changeEditable(@BindingParam("ceseAcciones") Persona cese) {
	  //HashMap cesarPersona = new HashMap();
	  System.out.println("Persona"+ cese.getApellidoPersona());
	  setPersona(cese);
	  	cesarPersona.put("ObjPersona",cese);
	  	System.err.println("2");
	  Window window = (Window)Executions.createComponents(
	  		"content/modalCeseAccion.zul", null, cesarPersona);
	  window.doModal();
	  }
	  
	  
 
// ABRE MODAL DESVINCULAR ACCION
	   @Command
	  public void AbrirModalRechazoCeseAccion(@BindingParam("rechazoCese") Persona rechazarCe, @BindingParam("accion") Accion acc){
	  	System.out.println("entro a modaaaaaaal");
	  	HashMap motivoCese = new HashMap();
	  	motivoCese.put("ObjDesvinculacion", rechazarCe);
	  	motivoCese.put("accion", acc);
	  	Window window = (Window)Executions.createComponents(
	  			"content/modalDesvincularAccion.zul", null, motivoCese);
	  	window.doModal();
	  //	win.detach();
	  	}
	   
	   @GlobalCommand
		@NotifyChange({"todosMiembros", "footer2"})
		public void RefrescarTablaCeseAccion() throws Exception {
		  //	alert(this.visita.getFechaVisita().toString());
		   todosMiembros = dao.obtenerPersonaTipoMiembro(1);
		 }
	   
	   @GlobalCommand
		@NotifyChange({"listaAcciones" })
		public void RefrescarTablaTraeAccion() throws Exception {
		  //	alert(this.visita.getFechaVisita().toString());
		   listaAcciones = getListaAcciones(persona);
		 }
	 
	   
	   
	   
	   
	   
 
	  @Command
		public void showModalBeneficiarios(@BindingParam("propietario")  Persona propietario) {
			HashMap a = new HashMap();
			a.put("propietario", propietario);
			Window window = (Window) Executions.createComponents(
					"content/modalBeneficiarios.zul", null, a);
			window.doModal();
		}
 
	  
	  
 // OBTENGO TODAS LAS ACCIONES DE UNA PERSONA
		public List<Accion> getListaAcciones(Persona person) throws Exception {
			
			 Set<Accion> acciones = new HashSet<Accion>();
			
			 ArrayList<Accion> as = new ArrayList<Accion>(accionDao.obtenerTodos());
		    
			 for(Accion a : as){
				 if(a.getPersonai()==null){
					 System.out.println("ES NULLLL");
				 }
				 else if(a.getPersonai().getIdPersona() == person.getIdPersona()){
				  
					 acciones.add(a);
				 }
			 }

			return new ArrayList<Accion>(acciones);
		}
		
	

	    
}
