package vistaModelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Window;

import modelo.entidad.Opinion;
import modelo.entidad.Usuario;
import modelo.hibernate.dao.DaoOpinion;
import modelo.hibernate.dao.DaoUsuario;


public class RegistrarOpinionViewModel {
	private Opinion opinion;
	private DaoOpinion daoopinion;
	private DaoUsuario dao;



	@Init
	public void init() {
		this.opinion = new Opinion();
		this.daoopinion = new DaoOpinion();
		this.dao = new DaoUsuario();


	}


	public Opinion getOpinion() {
		return opinion;
	}


	public void setOpinion(Opinion opinion) {
		this.opinion = opinion;
	}


	@Command
	@NotifyChange({"opinion"})
	public void RegistrarSugerencia() throws Exception{	
		if (!CamposVacio()){
			Date fecha = new Date();
			String estado="E";
			Session miSession = Sessions.getCurrent();
			Integer id = (Integer) miSession.getAttribute("idUsuario");
			Usuario usuario = dao.obtenerUsuario(id);
			opinion.setEditingStatus(false);
			opinion.setEstadoOpinion(estado);
			opinion.setUsuario(usuario);
			opinion.setFechaOpinion(fecha);
			this.daoopinion.agregarOpinion(opinion);
			Messagebox.show("Se ha registradro exitosamente", 
					"Información", Messagebox.OK, Messagebox.INFORMATION);
			this.opinion = new Opinion();

		}else
		{
			Messagebox.show("Debe LLenar Todos Los Campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}
	}
	@Command
	@NotifyChange({"opinion"})
	public void Limpiar() throws Exception{	
		this.opinion = new Opinion();	
	}


	public boolean CamposVacio(){
		if(opinion.getAsuntoOpinion() != null && !opinion.getAsuntoOpinion().equalsIgnoreCase("") && 
				opinion.getDescOpinion() != null && !opinion.getDescOpinion().equalsIgnoreCase(""))
			return false;
		return true;
	}
}
