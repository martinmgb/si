package vistaModelo;

public class IndicadorFilter {

	private String id="",nombre="",tipoIndicador="",unidadMedida="", descripcion="";

	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id==null?"":id.trim();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion==null?"":descripcion.trim();
	}

	public String getTipoIndicador() {
		return tipoIndicador;
	}

	public void setTipoIndicador(String tipoIndicador) {
		this.tipoIndicador = tipoIndicador ==null?"":tipoIndicador.trim();
	}

	public String getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida ==null?"":unidadMedida.trim();
	}
}