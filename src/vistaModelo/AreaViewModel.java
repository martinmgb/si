package vistaModelo;	
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

//import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;












import modelo.entidad.Act_comision;
import modelo.entidad.Actividad;
import modelo.entidad.Area;
import modelo.entidad.Area_recurso;
import modelo.entidad.TipoArea;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoAreaArrend;
import modelo.hibernate.dao.DaoAreaEvento;
import modelo.hibernate.dao.DaoArea_recurso;
import modelo.hibernate.dao.DaoTipoArea;


public class AreaViewModel extends GenericForwardComposer<Window> { 

	private static final String footerMessage = "Total de %d Área(s) ";
	private AreaFilter areaFilter = new AreaFilter();
	private DaoArea dao = new DaoArea();
	private DaoTipoArea daoTipoArea = new DaoTipoArea();
	private DaoArea_recurso daoar = new DaoArea_recurso();
	private DaoAreaEvento daoae = new DaoAreaEvento();
	private DaoAreaArrend daoaa = new DaoAreaArrend();
	private List<Area> todosTipos;
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;
	
	///////////////////////////////////////////////Para Boton Imprimir///////////////////////////////////////////////////////////
	private AMedia archivoJasper;
	private String rutaPdf;
	String pathPdfProyecto;
	private String pathProyecto;
	String patchZulProyecto;
	String rutaZul;
	Map<String,Object> parameterMap = new HashMap<String,Object>();
	private String rutabase;
	private Connection conexion;
	String patch = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");
	
	///////////////////////////////////////////////////////////////////////////////////////////////
	
///////////////////////////////////////////////Para Boton Imprimir///////////////////////////////////////////////////////////
private AMedia archivoJasper2;
private String rutaPdf2;
String pathPdfProyecto2;
private String pathProyecto2;
String patchZulProyecto2;
String rutaZul2;
Map<String,Object> parameterMap2 = new HashMap<String,Object>();
private String rutabase2;
private Connection conexion2;
String patch2 = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");

///////////////////////////////////////////////////////////////////////////////////////////////

	@Wire
	private Intbox id;
	@Wire
	private Textbox nombre;
	@Wire
	private Textbox descripcion;
	@Wire
	private Combobox  comboTipoArea;
	@Wire
	private Intbox capacidad;
	@Wire
	private Textbox dimension;
	@Wire
	Window modalDialog;
	@Wire
	public Label message;
	@Wire
	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;
	
	private TipoArea tipoAreaselect;
	
	

	private String selecti;

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public AreaViewModel() {
		super();
		tipoAreaselect = new TipoArea();
		try {
			todosTipos = dao.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//////////////////////////////////////////////////////////IMPRIMIR//////////////////////////////////////////////////////////////////
File file= new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathProyecto= file.getParentFile().getParentFile().getPath();
///////////////////////////////////////////////////////////////////////////////
int i,f;
f=this.pathProyecto.length();
i=f-7;
String z=this.pathProyecto.substring(i, f);
this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
if(z.compareTo("classes")==0){
this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().getPath();
this.rutabase=file.getParentFile().getParentFile().getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
}
System.out.print("Z: "+z+" F: "+"classes");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
String ruta = this.pathProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "area.jrxml";
System.out.print("RUTA: "+ruta);
File filePdf = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathPdfProyecto = this.pathProyecto;
rutaPdf = this.pathPdfProyecto + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "area.pdf";

File fileZul = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.patchZulProyecto = this.pathProyecto;
rutaZul = this.patchZulProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"content"+ System.getProperty("file.separator") + "modalreporteEstructurado.zul";

//Sustitucion de caracteres codificados (Para servidores con Windows)
this.pathProyecto= ruta.replaceAll("%20", " ");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

//////////////////////////////////////////////////////////IMPRIMIR//////////////////////////////////////////////////////////////////
File file2= new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathProyecto2= file.getParentFile().getParentFile().getPath();
///////////////////////////////////////////////////////////////////////////////
int i2,f2;
f2=this.pathProyecto2.length();
i2=f2-7;
String z2=this.pathProyecto2.substring(i2, f2);
this.rutabase2=file2.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
if(z2.compareTo("classes")==0){
this.pathProyecto2= file2.getParentFile().getParentFile().getParentFile().getParentFile().getPath();
this.rutabase2=file2.getParentFile().getParentFile().getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
}
System.out.print("Z: "+z2+" F: "+"classes");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
String ruta2 = this.pathProyecto2 + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "areas.jrxml";
System.out.print("RUTA: "+ruta2);
File filePdf2 = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathPdfProyecto2 = this.pathProyecto2;
rutaPdf2 = this.pathPdfProyecto2 + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "areas.pdf";

File fileZul2 = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.patchZulProyecto2 = this.pathProyecto2;
rutaZul2 = this.patchZulProyecto2 + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"content"+ System.getProperty("file.separator") + "modalreporteEstructurado.zul";

//Sustitucion de caracteres codificados (Para servidores con Windows)
this.pathProyecto2= ruta2.replaceAll("%20", " ");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	}
	}
	public TipoArea getTipoAreaselect() {
		return tipoAreaselect;
	}

	public void setTipoAreaselect(TipoArea tipoAreaselect) {
		this.tipoAreaselect = tipoAreaselect;
	}

	public String getSelecti() {
		return selecti;
	}

	public void setSelecti(String selecti) {
		this.selecti = selecti;
	}

	
	public List<TipoArea> getAllTipoArea() {
	     try {
			return daoTipoArea.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	  }

	
	public List<Area> getObtenerTipos() {
		return todosTipos;
	}

	public AreaFilter getAreaFilter() {
		return areaFilter;
	}

	public String getFooter() {
		return String.format(footerMessage, todosTipos.size());
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeFilter() {
		todosTipos = getFilterTipos(areaFilter);
	}



	public List<Area> getFilterTipos(AreaFilter tipoFilter) {

		List<Area> someTipos = new ArrayList<Area>();
		String nombre = tipoFilter.getNombre().toLowerCase();
		String des = tipoFilter.getDescripcion().toLowerCase();
		String tipoA= tipoFilter.getTipoArea().toLowerCase();
		List<Area> tiposAll = null;
		try {
			tiposAll = dao.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Area> i = tiposAll.iterator(); i.hasNext();) {
			Area tmp = i.next();
			if (tmp.getNombreArea().toLowerCase().contains(nombre) && tmp.getDescArea().toLowerCase().contains(des)&& tmp.getTipoArea().getNombreTipoArea().toLowerCase().contains(tipoA)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
	public void onClick$btnRegistrar() {

		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))|| (comboTipoArea.getValue().equals(""))|| (dimension.getValue().equals(""))|| (capacidad.getValue()==0)) {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		}
		else {
			try {
				tipoAreaselect = comboTipoArea.getSelectedItem().getValue();
				Area area= new Area();
				area.setTipoArea(tipoAreaselect);
				area.setNombreArea(nombre.getValue());
				area.setDimensionArea(dimension.getValue());
				area.setCapacidadArea(capacidad.getValue());
				area.setDescArea(descripcion.getValue());
				area.setEstadoArea("A");
				dao.agregarArea(area);
				Messagebox.show("Se ha registrado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/area.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void onClick$btnLimpiar() {
		nombre.setText("");
		descripcion.setText("");
		comboTipoArea.setText("");
		capacidad.setText("");
		dimension.setText("");
		comboTipoArea.setFocus(true);
	}
	public void onClick$btnEditar() {

		System.out.println(id.getValue());
		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))|| (comboTipoArea.getValue().equals(""))|| (dimension.getValue().equals(""))|| (capacidad.getValue()==0)) {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		}
		else {
			try {
				Area objeto = dao.obtenerArea(id.getValue());
				objeto.setNombreArea(nombre.getValue());
				objeto.setDescArea(descripcion.getValue());
				objeto.setTipoArea(comboTipoArea.getSelectedItem().getValue());
				objeto.setCapacidadArea(capacidad.getValue());
				objeto.setDimensionArea(dimension.getValue());
				dao.modificarArea(objeto);
				Messagebox.show("Se ha modificado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/area.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
			catch (Exception x) {
				// TODO Auto-generated catch block
				x.printStackTrace();
			}
		}
	}

	@Command
	public void changeEditable(@BindingParam("editarArea") Area tip) {
		HashMap area = new HashMap();
		area.put("objArea", tip);
		this.isEdit = true;
		this.isCreate = false;
		area.put("edit", this.isEdit);
		area.put("create", this.isCreate);
		Window window = (Window) Executions.createComponents(
				"content/modalArea.zul", null, area);
		window.doModal();
	}
	
	@Command
	public void showModalConfiguracion(@BindingParam("area") Area tip) {
		HashMap area = new HashMap();
		area.put("area", tip);
		Window window = (Window) Executions.createComponents(
				"content/modalConfiguracionArea.zul", null, area);
		window.doModal();
	}

	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeEditableStatus(@BindingParam("area") Area tipo) {
		try {
			int nro=daoae.obtenerPorArea(tipo).size();
			int nro2=daoaa.obtenerPorArea(tipo).size();
			if(nro>0 && nro2>0){
				Messagebox.show("No se puede eliminar el registro", "Información", Messagebox.OK, Messagebox.INFORMATION);
			}else{
		Messagebox.show("¿Esta seguro de eliminar el registro? Se eliminaran los recursos asignados", 
				"Interrogativo", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						try {
							List<Area_recurso> arr = daoar.obtenerPorArea(tipo);
							for (int i = 0; i < arr.size(); i++) {
								daoar.eliminarArea_recurso(arr.get(i));
							}
							dao.eliminarArea(tipo);
							todosTipos.remove(tipo);
							Messagebox.show("Se ha eliminado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
							
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(tipo);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}
			private void refreshRowTemplate(Area area) {
				//Metodo Para Refrescar la Tabla cuando elimina
				BindUtils.postNotifyChange(null, null, AreaViewModel.this, "obtenerTipos");
				BindUtils.postNotifyChange(null, null, AreaViewModel.this, "footer");
			}
		}
				);
			}
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void confirm(@BindingParam("area") Area tipo) {
		changeEditableStatus(tipo);
		try {
			todosTipos.remove(tipo);
			dao.eliminarArea(tipo);
			Messagebox.show("Se ha eliminado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(tipo);
	}

	public void refreshRowTemplate(Area tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}
	
///////////////////////////////////////////////METODOS PARA BOTON IMPRIMIR///////////////////////////////////////////////////////
	
public void generarPdf() throws SQLException, ParseException, JRException  {
generarReporte();
}

public void generarReporte() throws SQLException, ParseException, JRException{

try {
Class.forName("org.postgresql.Driver");
// para la conexion con la BD
conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
} catch (ClassNotFoundException ex) {

ex.printStackTrace();
}

parameterMap.put("dirbase", this.rutabase);

JasperReport jasperReport = JasperCompileManager.compileReport(this.pathProyecto);
JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);


archivoJasper = new AMedia(rutaPdf, "pdf" ,"application/pdf", JasperExportManager.exportReportToPdf(jasperPrint));

}


@Command
public void abrirModal(@ContextParam(ContextType.VIEW) Component view) throws SQLException, ParseException, JRException {
parameterMap.put("ruta", patch);		
Map<String, Object> args = new HashMap<String, Object>();
generarPdf();

args.put("reporte", archivoJasper);
final Window component = (Window) Executions.getCurrent().createComponents("content/modalImprimir.zul", view, args);
component.setAction("show: slideDown;hide: slideUp");
component.doModal();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////METODOS PARA BOTON IMPRIMIR///////////////////////////////////////////////////////

public void generarPdf2() throws SQLException, ParseException, JRException  {
generarReporte2();
}

public void generarReporte2() throws SQLException, ParseException, JRException{

try {
Class.forName("org.postgresql.Driver");
//para la conexion con la BD
conexion2 = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
} catch (ClassNotFoundException ex) {

ex.printStackTrace();
}

parameterMap2.put("dirbase", this.rutabase2);

JasperReport jasperReport2 = JasperCompileManager.compileReport(this.pathProyecto2);
JasperPrint jasperPrint2 = JasperFillManager.fillReport(jasperReport2, parameterMap2, conexion2);


archivoJasper2 = new AMedia(rutaPdf2, "pdf" ,"application/pdf", JasperExportManager.exportReportToPdf(jasperPrint2));

}


@Command
public void abrirModal2(@ContextParam(ContextType.VIEW) Component view) throws SQLException, ParseException, JRException {
parameterMap2.put("ruta", patch2);		
Map<String, Object> args = new HashMap<String, Object>();
generarPdf2();

args.put("reporte", archivoJasper2);
final Window component = (Window) Executions.getCurrent().createComponents("content/modalImprimir.zul", view, args);
component.setAction("show: slideDown;hide: slideUp");
component.doModal();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}