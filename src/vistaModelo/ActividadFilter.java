package vistaModelo;

public class ActividadFilter {

	private String id="",nombre="", descripcion="",codigo="";

	public String getCodigo() {
		return codigo;
	}


	public String getID() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion==null?"":descripcion.trim();
	}
}