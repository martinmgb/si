package vistaModelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import modelo.entidad.Comision;
import modelo.entidad.Evento;
import modelo.entidad.Evento_comision;
import modelo.entidad.Evento_indicador;
import modelo.entidad.Indicador;
import modelo.entidad.TipoArea;
import modelo.hibernate.dao.DaoComision;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoEvento_comision;
import modelo.hibernate.dao.DaoEvento_indicador;
import modelo.hibernate.dao.DaoIndicador;
import modelo.hibernate.dao.DaoResultado;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class IndicadorEventoViewModel extends GenericForwardComposer<Window> {

	private static  String footerMessage = "En Total %d Indicadores Asignados";
	private IndicadorEventoFilter filtroIndicadores = new IndicadorEventoFilter();
	List<Evento_indicador> indicadores;
	List<Indicador> ind;
	private Evento evento;

	DaoEvento_indicador dao= new DaoEvento_indicador();
	DaoEvento daoe= new DaoEvento();
	DaoIndicador daoi = new DaoIndicador();
	DaoResultado daor = new DaoResultado();

	private boolean isEdit,isCreate = false;

	@Wire
	private Combobox comboIndicadores;
	@Wire
	private Doublebox valorEsperado;
	@Wire
	Window modalDialog4;
	@Wire
	Window modalDialog;

	@Init
	public void init(@ExecutionArgParam("evento") Evento evento) throws Exception{
		this.evento=evento;
		indicadores=dao.obtenerPorEvento(evento);
	}

	public IndicadorEventoViewModel() {
		super();
		try {

			ind = daoi.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Indicador> getAllIndicadores() throws Exception {
		try {
			List<Indicador> arr= new ArrayList<Indicador>();
			List<Evento_indicador> ie= indicadores;
			ind = daoi.obtenerTodos();
			boolean enc;
			for (int i = 0; i < ind.size(); i++) {
				enc=false;
				for (int j = 0; j < ie.size(); j++) {
					if(ind.get(i).getIdIndicador()==ie.get(j).getIndicador().getIdIndicador()){
						enc=true;
					}
				}
				if(!enc){
					arr.add(ind.get(i));
				}
			}
			return arr;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}	
	}

	public List<Indicador> getAllIndicadores2() throws Exception {
		return daoi.obtenerTodos();	
	}

	public  IndicadorEventoFilter  getfiltroIndicadores() {
		return filtroIndicadores;
	}

	public List<Evento_indicador> getIndicadorEventoModel() throws Exception {
		return indicadores;
	}

	public String getFooter() {
		return String.format(footerMessage, indicadores.size());
	}

	@Command
	@NotifyChange({"indicadorEventoModel", "footer"})
	public void changeFilter() {
		indicadores = getFilterEvento_indicador(filtroIndicadores);
	}


	public void onClick$btnRegistrarIndicadorEvento() {

		//Messagebox.show("En Construcción", "Información", Messagebox.OK, Messagebox.INFORMATION);

		if(comboIndicadores.getValue()=="" || valorEsperado.getText()=="") {
			Messagebox.show("Debe llenar todos los campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else {
			try {
				Evento e = (Evento) arg.get("evento");
				Evento_indicador indicador = new Evento_indicador(e, comboIndicadores.getSelectedItem().getValue(),valorEsperado.getValue());
				dao.agregarEvento_indicador(indicador);
				//e.setEstadoEvento("P");
				daoe.modificarEvento(e);
				BindUtils.postGlobalCommand(null, null, "RefrescarTablaIndicadores", null);
				//BindUtils.postGlobalCommand(null, null, "RefrescarTablaPlanificar", null);
				modalDialog4.detach();
				Messagebox.show("Se ha Registrado Correctamente", "Information", Messagebox.OK, Messagebox.INFORMATION);
				refreshRowTemplate();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	private void refreshRowTemplate() {

		//Metodo Para Refrescar la Tabla cuando elimina
		BindUtils.postNotifyChange(null, null,  IndicadorEventoViewModel.this, "comisionEventoModel");
		//BindUtils.postNotifyChange(null, null,  ComisionEventoViewModel.this, "footer");
	}
	public void onClick$btnEditarIndicadorEvento() {

		//Messagebox.show("En Construcción", "Información", Messagebox.OK, Messagebox.INFORMATION);

		if(this.comboIndicadores.getSelectedItem().getValue().equals("") || valorEsperado.getValue().equals("")) {
			Messagebox.show("Debe llenar todos los campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else {
			try {
				Evento_indicador indicador = (Evento_indicador) arg.get("objeto");
				indicador.setIndicador(this.comboIndicadores.getSelectedItem().getValue());
				indicador.setValorEspEvento_ind(valorEsperado.getValue());
				dao.modificarEvento_indicador(indicador);
				BindUtils.postGlobalCommand(null, null, "RefrescarTablaIndicadores", null);
				modalDialog4.detach();
				Messagebox.show("¡El registro se ha editado correctamente!", "Información", Messagebox.OK, Messagebox.EXCLAMATION);

			}
			catch (Exception x) {
				// TODO Auto-generated catch block
				x.printStackTrace();
			}
		}
	}

	public void onClick$finalizarPlanificacion() {
		Evento e = (Evento) arg.get("evento");
		Messagebox.show("¿Esta seguro de finalizar la planificación? Una vez finalizada no podrá editar la planificación", "Confirmación", Messagebox.OK | Messagebox.IGNORE  | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
			public void onEvent(Event evt) throws InterruptedException {
				if (evt.getName().equals("onOK")) {
					try {
						e.setEstadoEvento("P");
						daoe.modificarEvento(e);
						BindUtils.postGlobalCommand(null, null, "RefrescarTablaPlanificar", null);
						modalDialog.detach();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					//this.detach();
					//alert("¡Registro de Resultados Finalizado!");
					Messagebox.show("Planificación Finalizado", "Información", Messagebox.OK, Messagebox.INFORMATION);
				} else if (evt.getName().equals("onCancel")) {
					Messagebox.show("¡Operación Cancelada!", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
				}
			}
		});


	}

	@Command
	@NotifyChange({"indicadores", "footer"})
	public void eliminarIndicadorEvento(@BindingParam("indicadorEliminar") Evento_indicador indicador) {
		Evento evento = this.evento;
		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					indicadores.remove(indicador);
					try {
						dao.eliminarEvento_indicador(indicador);
						Messagebox.show("¡Se ha eliminado correctamente!", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					//					try {
					//						if(indicador.getResultado()==null){
					//							indicadores.remove(indicador);
					//							dao.eliminarEvento_indicador(indicador);
					//						}
					//						else{
					//							Messagebox.show("No se puede eliminar, este indicador ya tiene resultado asignado.", "Error", Messagebox.OK, Messagebox.ERROR);
					//						}
					//					} catch (Exception x) {
					//						// TODO Auto-generated catch block
					//						x.printStackTrace();
					//					}
					refreshRowTemplate(indicador);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}

			private void refreshRowTemplate(Evento_indicador indicador) {

				//Metodo Para Refrescar la Tabla cuando elimina
				BindUtils.postNotifyChange(null, null,  IndicadorEventoViewModel.this, "indicadorEventoModel");
				BindUtils.postNotifyChange(null, null,  IndicadorEventoViewModel.this, "footer");
			}
		}
				);

	}

	public void onClick$btnLimpiar() {
		comboIndicadores.setValue("");
		valorEsperado.setText("");
	}

	@Command
	public void changeEditable(@BindingParam("indicador")  Evento_indicador ce) {
		HashMap indicadorEvento = new HashMap();
		indicadorEvento.put("objeto", ce);
		this.isEdit = true;
		this.isCreate = false;
		indicadorEvento.put("edit", this.isEdit);
		indicadorEvento.put("create", this.isCreate);
		Window window = (Window) Executions.createComponents(
				"content/modalIndicadorEvento.zul", null, indicadorEvento);
		window.doModal();
	}

	@Command
	public void showModalIndicadorEvento() {
		Evento_indicador ie = new Evento_indicador();
		Evento e = new Evento();
		e=this.evento;
		HashMap a = new HashMap();
		a.put("evento", e);
		this.isEdit = false;
		this.isCreate = true;
		a.put("edit", this.isEdit);
		a.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalIndicadorEvento.zul", null, a);
		window.doModal();
	}


	public List<Evento_indicador> getFilterEvento_indicador(IndicadorEventoFilter tipoFilter) {

		List<Evento_indicador> someTipos = new ArrayList<Evento_indicador>();
		String nombre = tipoFilter.getNombre().toLowerCase();
		//String des = tipoFilter.getTipo().toLowerCase();
		List<Evento_indicador> tiposAll = null;
		try {
			//tiposAll = dao.obtenerPorEvento(evento);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Evento_indicador> i = tiposAll.iterator(); i.hasNext();) {
			Evento_indicador tmp = i.next();
			if (tmp.getIndicador().getNombreIndicador().toLowerCase().contains(nombre)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}

	@GlobalCommand
	@NotifyChange({"indicadorEventoModel", "footer"})
	public void RefrescarTablaIndicadores() throws Exception {
		indicadores=dao.obtenerPorEvento(this.evento);
	}


}
