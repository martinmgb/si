package vistaModelo;

public class ResultadosEventoFilter {
	private String indicador="";
	private String valorEsperado="";
	private String valorReal="";

	public String getIndicador() {
		return indicador;
	}
	
	public String getValorEsperado() {
		return valorEsperado;
	}
	
	public String getValorReal() {
		return valorReal;
	}
	
	public void setIndicador(String indicador) {
		this.indicador = indicador==null?"":indicador.trim();
	}
	
	public void setValorEsperado(String valorEsperado) {
		this.valorEsperado = valorEsperado==null?"":valorEsperado.trim();
	}
	
	public void setValorReal(String valorReal) {
		this.valorReal = valorReal==null?"":valorReal.trim();
	}
	
}
