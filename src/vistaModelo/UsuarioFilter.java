package vistaModelo;

public class UsuarioFilter {

	private String nombreUsuario="", fechaUsuario="", estadoUsuario="";

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario==null?"":nombreUsuario.trim();
	}

	public String getFechaUsuario() {
		return fechaUsuario;
	}

	public void setFechaUsuario(String fechaUsuario) {
		this.fechaUsuario = fechaUsuario==null?"":fechaUsuario.trim();
	}

	public String getEstadoUsuario() {
		return estadoUsuario;
	}

	public void setEstadoUsuario(String estadoUsuario) {
		this.estadoUsuario = estadoUsuario==null?"":estadoUsuario.trim();
	}
}