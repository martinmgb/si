package vistaModelo;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import modelo.entidad.Area;
import modelo.entidad.Area_recurso;
import modelo.entidad.Comision;
import modelo.entidad.Evento;
import modelo.entidad.Evento_comision;
import modelo.entidad.Recurso;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoArea_recurso;
import modelo.hibernate.dao.DaoComision;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoEvento_comision;
import modelo.hibernate.dao.DaoRecurso;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class RecursoAreaViewModel extends GenericForwardComposer<Window> {

	private static  String footerMessage = "En Total %d Recursos Asignados";
	private RecursoAreaFilter filtroRecursos = new RecursoAreaFilter();
	List<Area_recurso> recursos;
	List<Recurso> r;
	private Area area;

	DaoArea_recurso dao= new DaoArea_recurso();
	DaoArea daoa= new DaoArea();
	DaoRecurso daor = new DaoRecurso();

	private boolean isEdit,isCreate = false;
	
	///////////////////////////////////////////////Para Boton Imprimir///////////////////////////////////////////////////////////
	private AMedia archivoJasper;
	private String rutaPdf;
	String pathPdfProyecto;
	private String pathProyecto;
	String patchZulProyecto;
	String rutaZul;
	Map<String,Object> parameterMap = new HashMap<String,Object>();
	private String rutabase;
	private Connection conexion;
	String patch = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");
	
	///////////////////////////////////////////////////////////////////////////////////////////////

	@Wire
	private Combobox comboRecursos;
	@Wire
	private Intbox cantidad;
	@Wire
	Window modalDialog4;

	@Init
	public void init(@ExecutionArgParam("area") Area area) throws Exception{
		this.area=area;
		recursos=dao.obtenerPorArea(area);
	}

	public RecursoAreaViewModel() {
		super();
		try {

			r = daor.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//////////////////////////////////////////////////////////IMPRIMIR//////////////////////////////////////////////////////////////////
File file= new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathProyecto= file.getParentFile().getParentFile().getPath();
String ruta = this.pathProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "areas.jrxml";

File filePdf = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathPdfProyecto = filePdf.getParentFile().getParentFile().getPath();
rutaPdf = this.pathPdfProyecto + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "areas.pdf";

File fileZul = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.patchZulProyecto = fileZul.getParentFile().getParentFile().getPath();
rutaZul = this.patchZulProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"content"+ System.getProperty("file.separator") + "modalreporteEstructurado.zul";

//Sustitucion de caracteres codificados (Para servidores con Windows)
this.pathProyecto= ruta.replaceAll("%20", " ");
this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}

	public List<Recurso> getAllRecursos() throws Exception {
		try {
			return daor.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}	
	}

	public  RecursoAreaFilter  getfiltroRecursos() {
		return filtroRecursos;
	}

	public List<Area_recurso> getRecursoAreaModel() throws Exception {
		return recursos;
	}

	public String getFooter() {
		return String.format(footerMessage, recursos.size());
	}

	@Command
	@NotifyChange({"recursoAreaModel", "footer"})
	public void changeFilter() {
		recursos = getFilterArea_recurso(filtroRecursos);
	}

	@NotifyChange({"*"})
	public void onClick$btnRegistrarRecursoArea() {

		//Messagebox.show("En Construcción", "Información", Messagebox.OK, Messagebox.INFORMATION);

		if((this.comboRecursos.getSelectedItem().getValue().equals("")) || (cantidad.getValue().equals(""))) {
			Messagebox.show("Debe llenar todos los campos", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else {
			try {
				Area a = (Area) arg.get("area");
				Area_recurso recurso = new Area_recurso(cantidad.getValue(),a, comboRecursos.getSelectedItem().getValue());	
				dao.agregarArea_recurso(recurso);
				BindUtils.postGlobalCommand(null, null, "RefrescarTablaRecursos", null);
				modalDialog4.detach();
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	private void refreshRowTemplate(Area_recurso area) {

		//Metodo Para Refrescar la Tabla cuando elimina
		BindUtils.postNotifyChange(null, null,  RecursoAreaViewModel.this, "recursoAreaModel");
		BindUtils.postNotifyChange(null, null,  RecursoAreaViewModel.this, "footer");
	}

	public void onClick$btnEditarRecursoArea() {

		//Messagebox.show("En Construcción", "Información", Messagebox.OK, Messagebox.INFORMATION);

		if((this.comboRecursos.getSelectedItem().getValue().equals("")) || (cantidad.getValue().equals(""))) {
			Messagebox.show("Debe llenar todos los campos", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else {
			try {
				Area_recurso recurso = (Area_recurso) arg.get("objeto");
				recurso.setRecurso(this.comboRecursos.getSelectedItem().getValue());
				recurso.setCantRecurso(cantidad.getValue());
				dao.modificarArea_recurso(recurso);
				BindUtils.postGlobalCommand(null, null, "RefrescarTablaRecursos", null);
				modalDialog4.detach();
			}
			catch (Exception x) {
				// TODO Auto-generated catch block
				x.printStackTrace();
			}
		}
	}


	@Command
	@NotifyChange({"recursos", "footer"})
	public void eliminarArea_recurso(@BindingParam("recursoEliminar") Area_recurso recurso) {

		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Interrogativo", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						recursos.remove(recurso);
						dao.eliminarArea_recurso(recurso);
					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(recurso);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}

			/*private void refreshRowTemplate(Evento_comision evento) {

				//Metodo Para Refrescar la Tabla cuando elimina
				BindUtils.postNotifyChange(null, null,  ComisionEventoViewModel.this, "comisionEventoModel");
				BindUtils.postNotifyChange(null, null,  ComisionEventoViewModel.this, "footer");
			}*/
		}
				);

	}

	public void onClick$btnLimpiar() {
		comboRecursos.setValue("");
		cantidad.setText("");
		//nombre.setFocus(true);
	}

	@Command
	public void changeEditable(@BindingParam("recurso")  Area_recurso ra) {
		HashMap recursoArea = new HashMap();
		recursoArea.put("objeto", ra);
		recursoArea.put("area", this.area);
		this.isEdit = true;
		this.isCreate = false;
		recursoArea.put("edit", this.isEdit);
		recursoArea.put("create", this.isCreate);
		Window window = (Window) Executions.createComponents(
				"content/modalRecursoArea.zul", null, recursoArea);
		window.doModal();
	}

	@Command
	public void showModalRecursoArea() {
		Area_recurso ra = new Area_recurso();
		Area area = new Area();
		area=this.area;
		HashMap a = new HashMap();
		a.put("area", area);
		this.isEdit = false;
		this.isCreate = true;
		a.put("edit", this.isEdit);
		a.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRecursoArea.zul", null, a);
		window.doModal();
	}


	public List<Area_recurso> getFilterArea_recurso(RecursoAreaFilter tipoFilter) {

		List<Area_recurso> someTipos = new ArrayList<Area_recurso>();
		String nombre = tipoFilter.getNombre().toLowerCase();
		//String des = tipoFilter.getTipo().toLowerCase();
		List<Area_recurso> tiposAll = null;
		try {
			//tiposAll = dao.obtenerPorEvento(evento);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Area_recurso> i = tiposAll.iterator(); i.hasNext();) {
			Area_recurso tmp = i.next();
			if (tmp.getRecurso().getNombreRecurso().toLowerCase().contains(nombre)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
	
	@GlobalCommand
	@NotifyChange({"recursoAreaModel", "footer"})
	public void RefrescarTablaRecursos() throws Exception {
		recursos=dao.obtenerPorArea(this.area);
	}
	
	
///////////////////////////////////////////////METODOS PARA BOTON IMPRIMIR///////////////////////////////////////////////////////
	
public void generarPdf() throws SQLException, ParseException, JRException  {
generarReporte();
}

public void generarReporte() throws SQLException, ParseException, JRException{

try {
Class.forName("org.postgresql.Driver");
// para la conexion con la BD
conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
} catch (ClassNotFoundException ex) {

ex.printStackTrace();
}

parameterMap.put("dirbase", this.rutabase);

JasperReport jasperReport = JasperCompileManager.compileReport(this.pathProyecto);
JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);


archivoJasper = new AMedia(rutaPdf, "pdf" ,"application/pdf", JasperExportManager.exportReportToPdf(jasperPrint));

}


@Command
public void abrirModala(@ContextParam(ContextType.VIEW) Component view) throws SQLException, ParseException, JRException {
parameterMap.put("ruta", patch);		
Map<String, Object> args = new HashMap<String, Object>();
generarPdf();

args.put("reporte", archivoJasper);
final Window component = (Window) Executions.getCurrent().createComponents("content/modalImprimir.zul", view, args);
component.setAction("show: slideDown;hide: slideUp");
component.doModal();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


}
