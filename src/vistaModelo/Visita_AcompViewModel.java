package vistaModelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

 
//PERSONA
import modelo.entidad.Visita_acomp;
import modelo.hibernate.dao.DaoVisita_Acomp;
import modelo.entidad.Accion;
import modelo.entidad.Evento;
import modelo.entidad.Parentesco;
import modelo.entidad.Persona;
import modelo.entidad.Postulacion;
import modelo.entidad.Visita;
 
public class Visita_AcompViewModel extends GenericForwardComposer<Window> {
   	private static final String footerMessage = "Total de %d Miembros ";
    
    	private DaoVisita_Acomp dao = new DaoVisita_Acomp();
//    	private List<Accion> todosTipos;
    	private List<Visita_acomp> todosTipos, acompanteVisita; // Para obtner las personas q se le asignaran acciones
    	private boolean displayEdit = true;
    	private static final long serialVersionUID = 1L;
    	private boolean isEdit,isCreate,isView = false;
//    	private Visita_acompFilter visitaFilter = new Visita_acompFilter();
    	private Visita visita;
    
    	@Wire
    	Window nroAccion;
    	
    	@Wire
    	Window fechaIngreso;
    	@Wire
    	Window modalDialog;
    	@Wire
    	public Label message;
    	@Wire
    	public Div box;
    	private Div contenido;

    	private Div contenedor;

    	private String pagina;

    	public boolean isDisplayEdit() {
    		return displayEdit;
    	}

    	
    	
    	@Init
    	public void init(@ExecutionArgParam("objVisita") Visita visita) throws Exception{
    		this.visita=visita;
    		acompanteVisita=dao.obtenerPorVisita(visita);
    	}
    	


    	public List<Visita_acomp> getObtenerTipos() {
    		return todosTipos;
    	}
    	
    	 
    	public List<Visita_acomp> getAcompanteVisita() {
			return acompanteVisita;
		}

 	public void setAcompanteVisita(List<Visita_acomp> acompanteVisita) {
			this.acompanteVisita = acompanteVisita;
		}



		public String getFooter() {
    		return String.format(footerMessage, todosTipos.size());
    	}
    	

//    	@Command
//    	@NotifyChange({"obtenerTipos", "footer"})
//    	public void changeFilter() {
//    		todosTipos = getFilterTiposVisita_Acomp(visita_acompFilter);
//    	}


//    	public List<Visita_acomp> getFilterVisita_acomp(Visita_acompFilter tipoFilter) {
//
//    		List<Visita_acomp> someTipos = new ArrayList<Visita_acomp>();
//    		String ced = tipoFilter.getCedula().toLowerCase();
//    		 List<Visita_acomp> tiposAll = null;
//    		try {
//    			tiposAll = dao.obtenerTodos();
//    		} catch (Exception e) {
//    			// TODO Auto-generated catch block
//    			e.printStackTrace();
//    		} 
//
//    		for (Iterator<Visita_acomp> i = tiposAll.iterator(); i.hasNext();) {
//    			Visita_acomp tmp = i.next();
//    			if (tmp.getPersona().getCedPersona().toLowerCase().contains(ced)  ) {
//    				someTipos.add(tmp);
//    			}
//    		}
//    		return someTipos;
//    	}
    	
    	
    	
//    	public void onClick$btnRegistrar() {
//
//    		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
//    			box.setVisible(true);
//    			message.setValue("Debe llenar todos los campos");
//    		}
//    		else {
//    			try {
//    				Accion accion= new Accion(nombre.getText(),
//    						descripcion.getText(),"A");
//    				dao.agregarAccion(accion);
//    				modalDialog.detach();
//    				pagina = "vista/parentesco.zul";
//    				contenido.getChildren().clear();
//    				contenedor = (Div) Executions.createComponents(pagina, null, null);
//    				contenido.appendChild(contenedor);
//    			}
//    			catch (Exception e) {
//    				// TODO Auto-generated catch block
//    				e.printStackTrace();
//    			}
//    		}
//    	}

//    	public void onClick$btnLimpiar() {
//    		nroAccion.setText("");
//    		fechaIngreso.setText("");
//    		nombre.setFocus(true);
//    	}
//    	public void onClick$btnEditar() {
//
//    		System.out.println(id.getValue());
//    		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
//    			box.setVisible(true);
//    			message.setValue("Debe llenar todos los campos");
//    		}
//    		else {
//    			try {
//    				Accion objeto = dao.obtenerAccion(id.getValue());
//    				objeto.setNombreAccion(nombre.getValue());
//    				objeto.setDescAccion(descripcion.getValue());
//    				dao.modificarAccion(objeto);
//    				modalDialog.detach();
//    				pagina = "vista/parentesco.zul";
//    				contenido.getChildren().clear();
//    				contenedor = (Div) Executions.createComponents(pagina, null, null);
//    				contenido.appendChild(contenedor);
//    			}
//    			catch (Exception x) {
//    				// TODO Auto-generated catch block
//    				x.printStackTrace();
//    			}
//    		}
//    	}

//    	@Command
//    	public void changeEditable(@BindingParam("editarAccion") Accion tip) {
//    		HashMap accion = new HashMap();
//    		accion.put("objAccion", tip);
//    		this.isEdit = true;
//    		this.isCreate = false;
//    		accion.put("edit", this.isEdit);
//    		accion.put("create", this.isCreate);
//    		Window window = (Window) Executions.createComponents(
//    				"content/modalParentesco.zul", null, accion);
//    		window.doModal();
//    	}

    	@NotifyChange({"obtenerTipos", "displayEdit"})
    	public void setDisplayEdit(boolean displayEdit) {
    		this.displayEdit = displayEdit;
    	}

    	@Command
    	@NotifyChange({"obtenerTipos", "footer"})
    	public void changeEditableStatus(@BindingParam("visita") Visita_acomp  tipo) {
    		Messagebox.show("¿Esta seguro de eliminar el registro?", 
    				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
    				Messagebox.QUESTION,
    				new org.zkoss.zk.ui.event.EventListener(){
    			public void onEvent(Event e){
    				if(Messagebox.ON_OK.equals(e.getName())){
    					//OK is clicked
    					try {
    						todosTipos.remove(tipo);
    						dao.eliminarVisita_acomp(tipo);


    					} catch (Exception x) {
    						// TODO Auto-generated catch block
    						x.printStackTrace();
    					}
    					refreshRowTemplate(tipo);
    				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
    					//Cancel is clicked
    				}
    			}
    		}
    				);

    	}
    	

    	@Command
    	@NotifyChange({"obtenerTipos", "footer"})
    	public void confirm(@BindingParam("visita") Visita_acomp tipo) {
    		changeEditableStatus(tipo);
    		try {
    			todosTipos.remove(tipo);
    			dao.eliminarVisita_acomp(tipo);
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		refreshRowTemplate(tipo);
    	}

    	public void refreshRowTemplate(Visita_acomp tipo) {
    		/*
    		 * This code is special and notifies ZK that the bean's value
    		 * has changed as it is used in the template mechanism.
    		 * This stops the entire Grid's data from being refreshed
    		 */
    		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
    	}
   // PARA ABRIR MODAL ASIGNAR ACCION
//			  	  @Command
//			  		public void changeEditableAsignarAccion(@BindingParam("AbrirModalAsignarAccion") Accion acciones) {
//			  			HashMap registrarAccion = new HashMap();
//			  			System.out.println("wwwwwwwwwwwwwwwwwwe:" + acciones.getPersonai().getCedPersona()  )  ;
//			  			registrarAccion.put("objAccion", acciones);
//			  			
//			  			Window window = (Window)Executions.createComponents(
//			  					"content/modalAsignarAccion.zul", null, registrarAccion);
//			  			window.doModal();
//			  		} 
//   	  }


  	 
 // ABRE LA MODAL PARA AGREGAR ACOMPAÑANTE
    	 @Command
    	public void registarAcompanante(@BindingParam("ObjVisita") Visita visitas) {
    	 	HashMap registrarAcompa = new HashMap();
    		registrarAcompa.put("objVisita", visitas);
    		Window window = (Window)Executions.createComponents(
    				"content/modalAcompanante.zul", null, registrarAcompa);
    		window.doModal();
    	}
    	 
	@GlobalCommand
	@NotifyChange({"acompanteVisita", "footer"})
	public void RefrescarTablaAcompanantes() throws Exception {
	  //	alert(this.visita.getFechaVisita().toString());
	 	acompanteVisita=dao.obtenerPorVisita(this.visita);
	 }
 
}
