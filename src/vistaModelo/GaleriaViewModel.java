package vistaModelo;	
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import modelo.entidad.Area;
import modelo.entidad.Club;
import modelo.entidad.Galeria;
import modelo.entidad.Noticia;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoGaleria;

import org.zkoss.bind.BindContext;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.image.Image;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;


public class GaleriaViewModel extends GenericForwardComposer<Window>{ 

	private static final String footerMessage = "Total de %d Imágenes en Galeria ";
	private GaleriaFilter galeriaFilter = new GaleriaFilter();
	private DaoGaleria dao = new DaoGaleria();
	private DaoArea daoa = new DaoArea();
	private List<Galeria> todosTipos;
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit;
	private boolean isCreate;
	private boolean isVer;
	private boolean estado;

	@Wire
	private Intbox id;
	@Wire
	private Combobox nombreG;
	@Wire
	private Textbox imagenG;
	@Wire
	Window modalDialog;
	@Wire
	public Label message;
	@Wire
	public Div box;

	private Div contenido;

	private Div contenedor;

	private String pagina;

	private static AImage myImage;

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public GaleriaViewModel(){

		super();
		try{
			todosTipos = dao.obtenerTodos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}




	public List<Galeria> getTodosTipos() {
		return todosTipos;
	}
	
	public List<Area> getAreas() throws Exception {
		return daoa.obtenerTodos();
	}

	public void setTodosTipos(List<Galeria> todosTipos) {
		this.todosTipos = todosTipos;
	}

	public GaleriaFilter getGaleriaFilter() {
		return galeriaFilter;
	}

	public String getFooter(){
		return String.format(footerMessage, todosTipos.size());
	}

	@Command
	@NotifyChange({"todosTipos", "footer"})
	public void changeFilter() {
		todosTipos = getFilterTipos(galeriaFilter);
	}


	public List<Galeria> getFilterTipos(GaleriaFilter tipoFilter){

		List<Galeria> someTipos = new ArrayList<Galeria>();
		String nombreG = tipoFilter.getNombreG().toLowerCase();
		List<Galeria> tiposAll = null;
		try {
			tiposAll = dao.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Galeria> i = tiposAll.iterator(); i.hasNext();){
			Galeria tmp = i.next();
			if (tmp.getNombreGaleria().toLowerCase().contains(nombreG)){
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}

	@NotifyChange("myImage")
	public void onClick$btnLimpiar() {
		this.nombreG.setText("");
		this.myImage =null;
	}

	public void onClick$btnRegistrar() {

		if((nombreG.getValue().equals("")) || (this.myImage == null)){
			Messagebox.show("Debe LLenar Todos Los Campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}
		else {
			try {
				Galeria galeria= new Galeria();
				galeria.setNombreGaleria(nombreG.getValue());
				Club club= new Club();
				club.setIdClub(1);
				galeria.setImagenGaleria(this.myImage.getByteData());
				galeria.setEstadoGaleria("A");
				galeria.setClub(club);
				dao.agregarGaleria(galeria);
				Messagebox.show("Imagen Cargada con Exito", "Information", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/galeria.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}




	@Command
	public void limpiarImagen(){
		this.myImage = null;
		Galeria galeria = new Galeria();
		HashMap galeriaMap = new HashMap();
		galeriaMap .put("objeto",galeria);
		this.isEdit = false;
		this.isCreate = true;
		this.isVer = false;
		this.estado=false;
		galeriaMap.put("edit", this.isEdit);
		galeriaMap.put("create", this.isCreate);
		galeriaMap.put("ver", this.isVer);
		galeriaMap.put("estado", this.estado);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalGaleria.zul", null,galeriaMap);
		window.doModal();
	}

	@Command
	public void registrarImagenGaleria(){
		//this.myImage = null;
		Galeria ce = new Galeria();
		HashMap a = new HashMap();
		a.put("objGaleria", ce);
		this.isEdit = false;
		this.isCreate = true;
		a.put("edit", this.isEdit);
		a.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalGaleria.zul", null, a);
		window.doModal();
	}


	public void onClick$btnEditar() {

		if((nombreG.getValue().equals("")) || (this.myImage == null)){
			box.setVisible(true);
			Messagebox.show("Debe LLenar Todos Los Campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}
		else {
			try {



				Galeria objeto = dao.obtenerGaleria(id.getValue());
				objeto.setNombreGaleria(nombreG.getValue()); 
				objeto.setImagenGaleria(this.myImage.getByteData()); 
				dao.modificarGaleria(objeto);
				Messagebox.show("Se ha Modificado Correctamente",
						"Information", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/galeria.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
			catch (Exception x) {
				// TODO Auto-generated catch block
				x.printStackTrace();
			}
		}
	}





	@Command
	public void changeEditable(@BindingParam("editarGaleria") Galeria tip){

		HashMap  galeria= new HashMap();
		try {
			AImage nueva = new AImage("nombre", tip.getImagenGaleria());
			this.setMyImage(nueva);
		} catch (IOException e) {
			System.out.print("ERROR EN LA IMAGEN" + tip.getIdGaleria());
		}
		galeria.put("objeto", tip);
		this.isEdit = true;
		this.isCreate = false;
		this.isVer = false;
		this.estado = false;
		galeria.put("edit", this.isEdit);
		galeria.put("create", this.isCreate);
		galeria.put("estado", this.estado);
		galeria.put("ver", this.isVer);

		Window window = (Window) Executions.createComponents(
				"content/modalGaleria.zul", null, galeria);
		window.doModal();
	}




	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}


	@Command
	@NotifyChange({ "todosTipos", "footer" })
	public void changeEditableStatus(@BindingParam("galeria") Galeria tipo) {
		Messagebox.show("¿Esta seguro de eliminar el registro?",
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
			public void onEvent(Event e) {
				if (Messagebox.ON_OK.equals(e.getName())) {
					// OK is clicked
					try {
						todosTipos.remove(tipo);
						dao.eliminarGaleria(tipo);
					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(tipo);
				} else if (Messagebox.ON_CANCEL.equals(e.getName())) {
					// Cancel is clicked
				}
			}

			private void refreshRowTemplate(Galeria tipo) {
				// Metodo Para Refrescar la Tabla cuando elimina
				BindUtils.postNotifyChange(null, null,
						GaleriaViewModel.this, "todosTipos");
				BindUtils.postNotifyChange(null, null,
						GaleriaViewModel.this, "footer");
			}
		});

	}



	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void confirm(@BindingParam("galeria") Galeria tipo){
		changeEditableStatus(tipo);
		try {
			todosTipos.remove(tipo);
			dao.eliminarGaleria(tipo);
		} catch (Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(tipo);
	}

	public void refreshRowTemplate(Galeria tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}

	@Command("upload")
	@NotifyChange("myImage")
	public void onImageUpload(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx){
		UploadEvent upEvent = null;
		Object objUploadEvent = ctx.getTriggerEvent();
		if (objUploadEvent != null && (objUploadEvent instanceof UploadEvent)) {
			upEvent = (UploadEvent) objUploadEvent;
		}
		if (upEvent != null) {
			Media media = upEvent.getMedia();
			int lengthofImage = media.getByteData().length;
			if (media instanceof Image) {
				if (lengthofImage > 500 * 1024) {
					Messagebox.show("Seleccione una imagen de tamaño menor que 500Kb", "Error", Messagebox.OK, Messagebox.ERROR);
					return;
				}
				else{
					setMyImage((AImage) media);//Initialize the bind object to show image in zul page and Notify it also
					System.out.println(media.getContentType());
					System.out.println(media.getName());
					System.out.println(getMyImage().getName());
				}
			}
			else {
				Messagebox.show("Seleccione un archivo de imagen", "Error", Messagebox.OK, Messagebox.ERROR);
			}
		} else {
			System.out.println("Upload Event Is not Coming");
		}
	}

	public AImage getMyImage() {
		return myImage;
	}

	public void setMyImage(AImage myImage) {
		this.myImage = myImage;
	}



}
