package vistaModelo;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import modelo.entidad.Area_arrend;
import modelo.entidad.Arrendamiento;
import modelo.entidad.Cancelacion;
import modelo.entidad.Evento;
import modelo.entidad.Motivo;
import modelo.entidad.Rechazo;
import modelo.hibernate.dao.DaoArrendamiento;
import modelo.hibernate.dao.DaoCancelacion;
import modelo.hibernate.dao.DaoMotivo;
import modelo.hibernate.dao.DaoRechazo;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;

public class CancelarArrendamientoViewModel  extends GenericForwardComposer<Window> {
	private   Set<Area_arrend> listaAreaArrendamiento;
	private static final String footerMessage = "Total de %d Areas ";
	List<Motivo> listaMotivos;
	private DaoMotivo daoMotivo = new DaoMotivo();
	private Cancelacion cancelacion;
	private DaoArrendamiento daoArr;
	private DaoCancelacion daoCancelacion;
	private String cancelacionlimpiar;



	@Init
	public void init(@ExecutionArgParam("objetoArr") Arrendamiento arrendamiento) throws Exception {

		if (arrendamiento== null) {
			//this.listaAreaArrendamiento= new HashSet<Area_arrend>();

		}
		else {
			this.listaAreaArrendamiento = arrendamiento.getArea_arrend();


		}
		this.daoArr = new DaoArrendamiento();
		this.daoCancelacion = new DaoCancelacion();
		this.cancelacion = new Cancelacion();
		this.cancelacionlimpiar = "";
	}

	public Set<Area_arrend> getListaAreaArrendamiento() {
		return listaAreaArrendamiento;
	}
	public void setListaAreaArrendamiento(Set<Area_arrend> listaAreaArrendamiento) {
		this.listaAreaArrendamiento = listaAreaArrendamiento;
	}
	public String getFooter() {
		return String.format(footerMessage, this.listaAreaArrendamiento.size());
	}

	public List<Motivo> getMotivos() {
		try {
			this.listaMotivos=this.daoMotivo.obtenerTodos();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return listaMotivos;
	}


	public Cancelacion getCancelacion() {
		return cancelacion;
	}

	public void setCancelacion(Cancelacion cancelacion) {
		this.cancelacion = cancelacion;
	}


	public String getCancelacionlimpiar() {
		return cancelacionlimpiar;
	}

	public void setCancelacionlimpiar(String cancelacionlimpiar) {
		this.cancelacionlimpiar = cancelacionlimpiar;
	}
	@Command
	public void cancelarArrendamiento(@BindingParam("objeto") Arrendamiento arr,@BindingParam("win") Window  win) {
		Messagebox.show("¿Esta seguro de Cancelar El Arrendamiento?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					HashMap objetoArr = new HashMap();
					objetoArr.put("objetoArr", arr);
					Window window = (Window)Executions.createComponents(
							"content/modalRechazoArrendamientoCancelacion.zul", null, objetoArr);
					window.doModal();
					win.detach();
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
				}

			}



		}
				);
	}





	@Command
	public void RegistrarCancelacionArrendamiento(@BindingParam("Objeto") Arrendamiento arre,@BindingParam("win") Window  win) throws Exception {

		if(!CamposVacios()){

			Date fecha = new Date();
			arre.setEstadoArrend("C");
			cancelacion.setEstadoCancelacion("A");
			cancelacion.setEditingStatus(false);
			cancelacion.setArrendamiento(arre);
			cancelacion.setFechaCancelacion(fecha);
			daoArr.modificarArrendamiento(arre);
			daoCancelacion.agregarCancelacion(cancelacion);
			JOptionPane.showMessageDialog(null, "Arrendamiento cancelado");
			win.detach();
			BindUtils.postGlobalCommand(null, null, "RefrescarTablaArrendamientosACancelar", null);
		}
		else
		{
			Messagebox.show("Debe LLenar Todos Los Campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}
	}


	@Command	
	@NotifyChange({"cancelacion","cancelacionlimpiar"})
	public void Limpiar(){
		this.cancelacion = new Cancelacion();
		this.cancelacionlimpiar = "";
	}

	public boolean CamposVacios(){

		if(cancelacion.getDescCancelacion() != null && cancelacion.getMotivo() != null)
			return false;
		return true;

	}
}
