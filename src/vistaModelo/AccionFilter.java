package vistaModelo;

public class AccionFilter {

	private String idAccion ="",cedula="",nombre="", apellido="" ;

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula==null?"":cedula.trim();
	}
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String Apellido) {
		this.apellido = apellido==null?"":apellido.trim();
	}

	public String getIdAccion() {
		return idAccion;
	}

	public void setIdAccion(String idAccion) {
		this.idAccion = idAccion;
	}
	
	
	
}