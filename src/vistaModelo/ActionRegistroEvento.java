package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.ListModelList;

public class ActionRegistroEvento extends SelectorComposer<Component> {
	
	@Wire Listbox GridTareas;
	@Command
	@Listen("onClick = #btnAsignar")
	public void mostrarGrid(Event e){
		GridTareas.setVisible(true);
	    }
	}