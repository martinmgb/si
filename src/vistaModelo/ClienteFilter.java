package vistaModelo;

public class ClienteFilter {
	private String cedula="";
	private String nombre="";
	private String fechanacimiento="";
	private String direccion="";
	private String telefonof="";
	private String telefonom="";
	private String correo="";
	private String twitter="";
	private String facebook="";
	
	
	public String getCedula() {
		return cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public String getFechanacimiento() {
		return fechanacimiento;
	}

	public String getDireccion() {
		return direccion;
	}

	public String getTelefonof() {
		return telefonof;
	}

	public String getTelefonom() {
		return telefonom;
	}

	public String getCorreo() {
		return correo;
	}

	public String getTwitter() {
		return twitter;
	}
	public String getFacebook() {
		return facebook;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula==null?"":cedula.trim();
	}

	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}
	
	public void setFechaNacimiento(String fechanacimiento) {
		this.fechanacimiento = fechanacimiento==null?"":fechanacimiento.trim();

	}
	

}
