package vistaModelo;

public class MiembroFilter {

	private String accion="";
	private String cedula="";
	private String nombre="";
	private String apellido="";
	
	
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion==null?"":accion.trim();;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula==null?"":cedula.trim();;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido==null?"":apellido.trim();;
	}
}
