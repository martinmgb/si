package vistaModelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Map;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import modelo.entidad.Accion;
import modelo.entidad.Area;
import modelo.entidad.Area_evento;
import modelo.entidad.Arrendamiento;
import modelo.entidad.Evento;
import modelo.entidad.Parentesco;
import modelo.entidad.Persona;
//POSTULACION
import modelo.entidad.Postulacion;
import modelo.entidad.Preferencia_evento;
import modelo.entidad.Referencia;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoAreaArrend;
import modelo.hibernate.dao.DaoAreaEvento;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoParentesco;
import modelo.hibernate.dao.DaoPersona;
import modelo.hibernate.dao.DaoPostulacion;
import modelo.hibernate.dao.DaoPreferencia;
//RECHAZO
import modelo.entidad.Rechazo;
import modelo.entidad.TipoArea;
import vistaModelo.PostulacionFilter;


public class PostulacionViewModel extends GenericForwardComposer<Window> { 

	
	private static final String footerMessage = "Total de %d Postulados ";
	private static final String footerMessageSP = "Total de %d Postulado(s) Pendiente(s) ";
	private static final String footerMessageSS = "Total de %d Postulacion(s) Solicitada(s) ";

	private static final String footerMessageReferencia = "Total de %d  Referencia()";
	private PostulacionFilter postulacionFilter = new PostulacionFilter();
	private DaoPostulacion dao = new DaoPostulacion();
	private DaoPersona daop = new DaoPersona();
	private DaoParentesco daopa = new DaoParentesco();
	private List<Postulacion> todosTipos;
	private List<Referencia> referenciaPost;
 	
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;
	//Accion
	private Postulacion postulacion;
		private Accion accion = new Accion();
	//RECHAZO
		private Rechazo rechazo = new Rechazo();
		
	//PARA LOS ESTADOS
	List<Postulacion> todosPostulados, solicitudesPendiente, solicitudesSolicitadas, todosRechazados ;
//	List<Arrendamiento> todosArr, solArr, toincidenciaArr, tocancelArr;

	// todosARR= todosPostulados,  todas las postulaciones solicitadas ( solicitadas)
	// solArr = solicitudesPendiente , todas las postulaciones pendientes ( proceso)
	// toincidenciaArr = solicitudesSolicitadas, para incidencias, osea los aprovados
	//tocancelArr = todosRechazados , son los q son para cancelar ( rechazar)
	


	@Wire
	private Intbox idams;
	@Wire
	private Textbox nombre;
	@Wire
	private Textbox descripcion;
	@Wire
	Window modalDialog2;
	@Wire
	public Label message;
	@Wire
	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;

	public boolean isDisplayEdit() {
		return displayEdit;
	}
 
	
	
	
//	public PostulacionViewModel() {
//		super();
//		try {
//			todosTipos = dao.obtenerTodos();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

	   
	@Init
	public void init(@ExecutionArgParam("objetoEvento") Evento evento) throws Exception {

//		System.out.print("AquiLALALALALAA");
//		//todosTipos = dao.obtenerTodos();
//		String estadoSolicitado = "S";
//		String estadoPendiente = "P";
//		 
//		solicitudesPendiente = new ArrayList<Postulacion>();
//		solicitudesSolicitadas = new ArrayList<Postulacion>();
//		todosRechazados = new ArrayList<Postulacion>();
//		todosPostulados = dao.obtenerTodos();
		
		this.solicitudesSolicitadas = dao.obtenerPostuladoSolicitado("S");

		this.solicitudesPendiente = dao.obtenerPostuladoPendiente("P");
	

	}
	
	
	
	 
//	
	 
	
 
	 
	 	public List<Referencia> getReferenciaPost() throws Exception {

		return referenciaPost;
	}

	 	public List<Postulacion> getSolicitudesPendiente() {
			return solicitudesPendiente;
		}




		public void setSolicitudesPendiente(List<Postulacion> solicitudesPendiente) {
			this.solicitudesPendiente = solicitudesPendiente;
		}




		public List<Postulacion> getSolicitudesSolicitadas() {
			return solicitudesSolicitadas;
		}

		public List<Postulacion> obtenerSolicitudesSolicitadas() {
			return solicitudesSolicitadas;
		}




		public void setSolicitudesSolicitadas(List<Postulacion> solicitudesSolicitadas) {
			this.solicitudesSolicitadas = solicitudesSolicitadas;
		}




public void setReferenciaPost(List<Referencia> referenciaPost) {
		this.referenciaPost = referenciaPost;
	}
 public List<Postulacion> getObtenerTipos() {
		return todosPostulados;
	}
	 public PostulacionFilter getPostulacionFilter() {
		return postulacionFilter;
	}

	public String getFooter() {
		return String.format(footerMessage, todosPostulados.size()); // para todas las solicitudes
	} 
	
	public String getFooterSP() {
		return String.format(footerMessageSP, solicitudesPendiente.size()); // para solicitudes pendiente
	}
	public String getFooterSS() {
		return String.format(footerMessageSS, solicitudesSolicitadas.size()); // para solicitudes solicitada
	}

// METODO PARA FILTRAR-----------------------------------------------------------------
	@Command
	@NotifyChange({"solicitudesSolicitadas", "footerMessageSS"})
	public void changeFilterSoli() {
		solicitudesSolicitadas = getFilterTiposSolicitada(postulacionFilter);
	}
	@Command
	@NotifyChange({"solicitudesPendiente", "footerMessageSS"})
	public void changeFilterPendi() {
		solicitudesPendiente = getFilterTiposPendiente(postulacionFilter);
	}
	 
	 


	public List<Postulacion> getFilterTiposSolicitada(PostulacionFilter tipoFilter) {

		List<Postulacion> someTipos = new ArrayList<Postulacion>();
		String ced = tipoFilter.getCedula().toLowerCase();
		List<Postulacion> tiposAll = null;
		try {
			tiposAll = dao.obtenerPostuladoSolicitado("S");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Postulacion> i = tiposAll.iterator(); i.hasNext();) {
			Postulacion tmp = i.next();
			if (tmp.getPersona().getCedPersona().toLowerCase().contains(ced)  ) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
	
	public List<Postulacion> getFilterTiposPendiente(PostulacionFilter tipoFilter) {

		List<Postulacion> someTipos = new ArrayList<Postulacion>();
		String ced = tipoFilter.getCedula().toLowerCase();
		List<Postulacion> tiposAll = null;
		try {
			tiposAll = dao.obtenerPostuladoPendiente("P");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Postulacion> i = tiposAll.iterator(); i.hasNext();) {
			Postulacion tmp = i.next();
			if (tmp.getPersona().getCedPersona().toLowerCase().contains(ced)  ) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
	
// METODO PARA FILTRAR-------------------------------FIN----------------------------------
	
 

	public void onClick$btnLimpiar() {
		nombre.setText("");
		descripcion.setText("");
		nombre.setFocus(true);
	}
 
 

	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeEditableStatus(@BindingParam("postulacion") Postulacion tipo) {
		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						todosTipos.remove(tipo);
						dao.eliminarPostulacion(tipo);


					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(tipo);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}
		}
				);

	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void confirm(@BindingParam("postulacion") Postulacion tipo) {
		changeEditableStatus(tipo);
		try {
			todosTipos.remove(tipo);
			dao.eliminarPostulacion(tipo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(tipo);
	}

	public void refreshRowTemplate(Postulacion tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}
	
	
//ABRE MODAL SOLICITUD
	@Command
	public void changeEditable(@BindingParam("atendersolicitudes") Postulacion atenderSo) throws Exception {
		HashMap atenderSolicitud = new HashMap();
		Parentesco parentesco= daopa.obtenerParentesco(1);
		Persona conyuge=daop.obtenerConyuguePorPropietario(atenderSo.getPersona().getIdPersona(), parentesco);
		if(conyuge==null){
			conyuge=new Persona();
		}
		atenderSolicitud.put("objeto", atenderSo);
		atenderSolicitud.put("ObjConyuge", conyuge);
		Window window = (Window)Executions.createComponents(
				"content/modalAprobarPostulacion.zul", null, atenderSolicitud);
		window.doModal();
	}

 	
	
// PARA ABRIR MODAL ASIGNAR ACCION
		  @Command
			public void changeEditableAsignarAccion(@BindingParam("AbrirModalAsignarAccion") Postulacion acciones) {
				HashMap registrarAccion = new HashMap(); 
				registrarAccion.put("objPostulacion", acciones);
				
				Window window = (Window)Executions.createComponents(
						"content/modalAsignarAccion.zul", null, registrarAccion);
				window.doModal();
			} 
 
 

 
 
	
 
	 
	 
	
	
}