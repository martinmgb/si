
package vistaModelo;

public class IncidenciaFilter { 

	private String codigo="", numAccion="", cedula ="", nombre="", apellido="", fecha="", tipo="", codigocaso,  descripcion="";

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo==null?"":codigo.trim();
	}
	
	public String getNumAccion() {
		return numAccion;
	}

	public void setNumAccion(String numAccion) {
		this.numAccion = numAccion==null?"":numAccion.trim();
	}
	
	
	
	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula==null?"":cedula.trim();
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido==null?"":apellido.trim();
	}
	
	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha==null?"":fecha.trim();
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo==null?"":tipo.trim();
	}

	public String getCodigocaso() {
		return codigocaso;
	}

	public void setCodigocaso(String codigocaso) {
		this.codigocaso = codigocaso==null?"":codigocaso.trim();
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion==null?"":descripcion.trim();
	}
}
