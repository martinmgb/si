package vistaModelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Map;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

//POSTULACION
import modelo.entidad.Postulacion;
import modelo.hibernate.dao.DaoPostulacion;


import modelo.entidad.TipoArea;
import vistaModelo.PostulacionFilter;


public class PerfilViewModel extends GenericForwardComposer<Window> { 

	private static final String footerMessage = "Total de %d Postulados ";
	private DaoPostulacion dao = new DaoPostulacion();
	private List<Postulacion> todosTipos;
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;

	@Wire
	private Textbox nombre;
	@Wire
	private Textbox rif;
	@Wire
	private Textbox direccion;
	@Wire
	private Textbox telefono;
	@Wire
	private Textbox mision;
	@Wire
	private Textbox vision;
	@Wire
	private Textbox logo;
	@Wire
	private Textbox correo;
	@Wire
	Window modalDialog;
	@Wire
	public Label message;
	@Wire
	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public PerfilViewModel() {
		super();
		try {
			todosTipos = dao.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public List<Postulacion> getObtenerTipos() {
		return todosTipos;
	}


	public String getFooter() {
		return String.format(footerMessage, todosTipos.size());
	}



	public void onClick$btnLimpiar() {
		nombre.setText("");
		nombre.setFocus(true);
	}



	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeEditableStatus(@BindingParam("postulacion") Postulacion tipo) {
		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						todosTipos.remove(tipo);
						dao.eliminarPostulacion(tipo);


					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(tipo);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}
		}
				);

	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void confirm(@BindingParam("postulacion") Postulacion tipo) {
		changeEditableStatus(tipo);
		try {
			todosTipos.remove(tipo);
			dao.eliminarPostulacion(tipo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(tipo);
	}

	public void refreshRowTemplate(Postulacion tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}
	
	

	@Command
	public void changeEditable(@BindingParam("atendersolicitudes") Postulacion atenderSo) {
		HashMap atenderSolicitud = new HashMap();
		atenderSolicitud.put("objeto", atenderSo);
		Window window = (Window)Executions.createComponents(
				"content/modalAprobarPostulacion.zul", null, atenderSolicitud);
		window.doModal();
	}
	
	@Command
	public void AbrirModalRechazo(){
		Window window = (Window)Executions.createComponents(
				"content/modalRechazo.zul", null,null);
		window.doModal();
		}
 
	@Command
	public void AprobarSolicitudMembresia(){
		 JOptionPane.showMessageDialog(null, "Se Aprobo Con Exito");

		}
 
	
	
}