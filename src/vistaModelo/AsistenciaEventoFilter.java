package vistaModelo;

public class AsistenciaEventoFilter {

	private String cedula="", nombre="", miembroAsociado="";

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula==null?"":cedula.trim();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}

	public String getmiembroAsociado() {
		return miembroAsociado;
	}

	public void setmiembroAsociado(String miembroAsociado) {
		this.miembroAsociado = miembroAsociado==null?"":miembroAsociado.trim();
	}
}
