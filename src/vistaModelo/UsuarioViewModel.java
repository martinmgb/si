package vistaModelo;	
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import controlador.ManejadorMail;

import org.zkoss.bind.BindContext;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.image.Image;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import modelo.entidad.Grupo;
import modelo.entidad.Persona;
import modelo.entidad.TipoArea;
import modelo.entidad.Usuario;
import modelo.hibernate.dao.DaoGrupo;
import modelo.hibernate.dao.DaoPersona;
import modelo.hibernate.dao.DaoTipoPersona;
import modelo.hibernate.dao.DaoUsuario;


public class UsuarioViewModel extends GenericForwardComposer<Window> { 

	private static final String footerMessage = "Total de %d Usuario(s)";
	private UsuarioFilter usuarioFilter = new UsuarioFilter();
	private DaoUsuario dao = new DaoUsuario();
	private List<Usuario> todosUsuarios;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate = false;
	java.util.Date fechaActual = new Date();
	private boolean displayEdit = true;
	private Persona personaUsuario = new Persona();
	private DaoGrupo daoGrupo = new DaoGrupo();
	private DaoPersona daoPersona = new DaoPersona();
	private Usuario usuario = new Usuario();
	private Persona objPersona;
	private List<Persona> personas;
	private List<Usuario> usuarios;


	@Wire
	private Intbox id;
	@Wire
	private Textbox cedula;
	@Wire
	private Label nombUsuario;
	@Wire
	private Label appUsuario;
	@Wire
	private Label correoUsuario;
	@Wire
	private Combobox comboGrupo;
	@Wire
	private Window modalDialog;
	@Wire
	public Label message;
	@Wire
	public Div box;
	@Wire
	private Textbox clave;
	@Wire
	private Textbox repetirclave;

	private Grupo grupoSelect;
	private String selectItem;
	private Div contenido;
	private Div contenedor;
	private String pagina;
	private static AImage myImage;


	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public UsuarioViewModel() {
		super();
		grupoSelect = new Grupo();
		try {
			todosUsuarios = dao.obtenerTodosUsuarios();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Grupo getGrupoSelect() {
		return grupoSelect;
	}

	public void setGrupoSelect(Grupo grupoSelect) {
		this.grupoSelect = grupoSelect;
	}

	public String getSelecItem() {
		return selectItem;
	}

	public void setSelecItem(String selectItem) {
		this.selectItem = selectItem;
	}

	public List<Grupo> getTodosGrupos() {
		try {
			return daoGrupo.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public List<Usuario> getObtenerUsuarios() {
		return todosUsuarios;
	}

	public UsuarioFilter getUsuarioFilter() {
		return usuarioFilter;
	}

	public String getFooter() {
		return String.format(footerMessage, todosUsuarios.size());
	}

	@Command
	@NotifyChange({"obtenerUsuarios", "footer"})
	public void changeFilter() {
		todosUsuarios = getFilterUsuarios(usuarioFilter);
	}

	public List<Usuario> getFilterUsuarios(UsuarioFilter usuarioFilter) {

		List<Usuario> someUsuarios = new ArrayList<Usuario>();
		String nombUsuario = usuarioFilter.getNombreUsuario().toLowerCase();
		List<Usuario> usuariosAll = null;
		try {
			usuariosAll = dao.obtenerTodosUsuarios();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Usuario> i = usuariosAll.iterator(); i.hasNext();) {
			Usuario tmp = i.next();
			if (tmp.getNombreUsuario().toLowerCase().contains(nombUsuario)) {
				someUsuarios.add(tmp);
			}
		}
		return someUsuarios;
	}


	public void onClick$btnBuscar() {
		try {
			personas = daoPersona.obtenerTodos();
			usuarios = dao.obtenerTodosUsuarios();
			System.err.println(personas.size());
			boolean encontrado = false;
			boolean existente = false;
			for (int i = 0; i < usuarios.size(); i++) {
				if(usuarios.get(i).getPersona().getCedPersona().equals(cedula.getValue())){
					existente = true;
					break;
				}
				else{
					existente = false;
				}
			}

			if(existente == false){
				for (int j = 0; j < personas.size(); j++) {
					if(cedula.getValue().equals(personas.get(j).getCedPersona())){
						if(personas.get(j).getTipoPersona().getEstadoTipoPersona().equals("M") || personas.get(j).getTipoPersona().getEstadoTipoPersona().equals("A") ){
							personaUsuario = personas.get(j);
							encontrado = true;
							break;
						}

						else{
							encontrado = false;	
						}
					}
				}
			}
			else{
				box.setVisible(true);
				message.setValue("El usuario ya existe!");
			}

			if(encontrado == true && existente == false){
				nombUsuario.setValue(personaUsuario.getNombrePersona());
				appUsuario.setValue(personaUsuario.getApellidoPersona());
				correoUsuario.setValue(personaUsuario.getCorreoPersona());
				box.setVisible(false);
			}
			else if(encontrado == false && existente == false){
				nombUsuario.setValue("");
				appUsuario.setValue("");
				correoUsuario.setValue("");
				grupoSelect.setDescGrupo("Seleccione");
				message.setValue("Cedula Invalida!");
				box.setVisible(true);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onClick$btnRegistrar() {

		try {
			grupoSelect = comboGrupo.getSelectedItem().getValue();
			//List<Grupo> grupo;
			List<Persona> personas;
			int contador = usuarios.size();
			//grupo = daoGrupo.obtenerTodos();
			personas = daoPersona.obtenerTodos();

			for (int i = 0; i < personas.size(); i++) {
				if(cedula.getValue().equals(personas.get(i).getCedPersona())){
					objPersona = personas.get(i);
				}
			}

			if(nombUsuario.getValue().equals("") || appUsuario.getValue().equals("") || 
					correoUsuario.getValue().equals("") || grupoSelect.equals("")){
				Messagebox.show("¡Debe llenar todos los campos!", "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			}
			else{
				usuario = new Usuario();
				usuario.setPersona(objPersona);
				usuario.setClaveUsuario(getCadenaAlfanumAleatoria(6));
				usuario.setNombreUsuario(nombUsuario.getValue()+(contador+1)+"HCL");
				usuario.setFechaUsuario(fechaActual);
				usuario.setGrupo(grupoSelect);
				usuario.setEstadoUsuario("A");
				usuario.getPersona().setImagenPersona(this.myImage.getByteData());
				dao.agregarUsuario(usuario);


				String mensaje, destinatario, asunto;
				mensaje = "Sr(a) " + objPersona.getNombrePersona() + " " + objPersona.getApellidoPersona()
				+ ", el presente correo es para informarle que usted ha sido(a) aceptado(a) como usuario del Club Hogar Canario Larense "
				+ ", al ingresar a nuestro sistema Teide podrá efectuar el cambio de su clave, además de consultas, realizar transacciones y obtener informacion relevante"
				+ ", esperamos que disfrute de sus bondades y recuerde que Teide es tu Socio Ideal."
				+ " Su Usuario: " + usuario.getNombreUsuario() + " y su Clave: " + usuario.getClaveUsuario();
				destinatario = usuario.getPersona().getCorreoPersona();
				asunto = "Bienvenido(a) al Club Hogar Canario Larense ";


				ManejadorMail.enviarEmail(mensaje, destinatario, asunto);
				Messagebox.show("Se ha realizado el registro con exito", "Information", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/crearUsuario.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onClick$btnLimpiar() {
		cedula.setValue("");
		nombUsuario.setValue("");
		appUsuario.setValue("");
		correoUsuario.setValue("");
		grupoSelect.setDescGrupo("Seleccione");
		box.setVisible(false);
		cedula.setFocus(true);
	}

	public void onClick$btnEditar() {
		grupoSelect = comboGrupo.getSelectedItem().getValue();
		//System.out.println(id.getValue());
		if(nombUsuario.getValue().equals("")) {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		}
		else {
			try {
				Usuario objeto = dao.obtenerUsuario(id.getValue());
				objeto.setNombreUsuario(nombUsuario.getValue());
				objeto.setGrupo(grupoSelect);
				dao.modificarUsuario(objeto);
				modalDialog.detach();
				pagina = "vista/crearUsuario.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
			catch (Exception x) {
				// TODO Auto-generated catch block
				x.printStackTrace();
			}
		}
	}

	@NotifyChange({"obtenerUsuarios", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	@Command
	public void changeEditable(@BindingParam("editarUsuario") Usuario user) {
		HashMap usuario = new HashMap();
		usuario.put("userObj", user);
		this.isEdit = true;
		this.isCreate = false;
		usuario.put("edit", this.isEdit);
		usuario.put("create", this.isCreate);
		Window window = (Window) Executions.createComponents(
				"content/modalCrearUsuario.zul", null, usuario);
		window.doModal();
	}

	@Command
	@NotifyChange({"obtenerUsuarios", "footer"})
	public void changeEditableStatus(@BindingParam("usuario") Usuario usuario) {
		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						todosUsuarios.remove(usuario);
						dao.eliminarUsuario(usuario);
					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(usuario);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}
			private void refreshRowTemplate(Usuario usuario) {
				//Metodo Para Refrescar la Tabla cuando elimina
				BindUtils.postNotifyChange(null, null, UsuarioViewModel.this, "obtenerUsuarios");
				BindUtils.postNotifyChange(null, null, UsuarioViewModel.this, "footer");
			}
		}
				);
	}

	@Command
	@NotifyChange({"obtenerUsuarios", "footer"})
	public void confirm(@BindingParam("usuario") Usuario usuario) {
		changeEditableStatus(usuario);
		try {
			todosUsuarios.remove(usuario);
			dao.eliminarUsuario(usuario);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(usuario);
	}

	public void refreshRowTemplate(Usuario usuario) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, usuario, "editingStatus");
	}

	public String getCadenaAlfanumAleatoria (int longitud){
		String cadenaAleatoria = "";
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int i = 0;
		while ( i < longitud){
			char c = (char)r.nextInt(255);
			if ( (c >= '0' && c <='9') || (c >='A' && c <='Z') ){
				cadenaAleatoria += c;
				i ++;
			}
		}
		return cadenaAleatoria;
	}

	@Command("upload")
	@NotifyChange("myImage")
	public void onImageUpload(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx){
		UploadEvent upEvent = null;
		Object objUploadEvent = ctx.getTriggerEvent();
		if (objUploadEvent != null && (objUploadEvent instanceof UploadEvent)) {
			upEvent = (UploadEvent) objUploadEvent;
		}
		if (upEvent != null) {
			Media media = upEvent.getMedia();
			int lengthofImage = media.getByteData().length;
			if (media instanceof Image) {
				if (lengthofImage > 500 * 1024) {
					Messagebox.show("Seleccione una imagen de tamaño menor que 500Kb", "Error", Messagebox.OK, Messagebox.ERROR);
					return;
				}
				else{
					setMyImage((AImage) media);//Initialize the bind object to show image in zul page and Notify it also
					System.out.println(media.getContentType());
					System.out.println(media.getName());
					System.out.println(getMyImage().getName());
				}
			}
			else {
				Messagebox.show("Seleccione un archivo de imagen", "Error", Messagebox.OK, Messagebox.ERROR);
			}
		} else {
			System.out.println("Upload Event Is not Coming");
		}
	}

	public AImage getMyImage() {
		return myImage;
	}

	public void setMyImage(AImage myImage) {
		this.myImage = myImage;
	}
}