package vistaModelo;	
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import modelo.entidad.Motivo;
import modelo.hibernate.dao.DaoMotivo;


public class MotivoViewModel extends GenericForwardComposer<Window> { 

	private static final String footerMessage = "Total de %d Motivo(s) ";
	private MotivoFilter motivoFilter = new MotivoFilter();
	private DaoMotivo dao = new DaoMotivo();
	private List<Motivo> todosTipos;
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;
	
	///////////////////////////////////////////////Para Boton Imprimir///////////////////////////////////////////////////////////
	private AMedia archivoJasper;
	private String rutaPdf;
	String pathPdfProyecto;
	private String pathProyecto;
	String patchZulProyecto;
	String rutaZul;
	Map<String,Object> parameterMap = new HashMap<String,Object>();
	private String rutabase;
	private Connection conexion;
	String patch = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Wire
	private Intbox id;
	@Wire
	private Textbox nombre;
	@Wire
	private Textbox descripcion;
	@Wire
	Window modalDialog;
	@Wire
	public Label message;
	@Wire
	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public MotivoViewModel() {
		super();
		try {
			todosTipos = dao.obtenerTodos();

			//////////////////////////////////////////////////////////IMPRIMIR//////////////////////////////////////////////////////////////////
			File file= new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			this.pathProyecto= file.getParentFile().getParentFile().getPath();
			///////////////////////////////////////////////////////////////////////////////
			int i,f;
			f=this.pathProyecto.length();
			i=f-7;
			String z=this.pathProyecto.substring(i, f);
			this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
			if(z.compareTo("classes")==0){
				this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().getPath();
				this.rutabase=file.getParentFile().getParentFile().getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
			}
			System.out.print("Z: "+z+" F: "+"classes");
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////
			String ruta = this.pathProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "motivorechazo.jrxml";
			System.out.print("RUTA: "+ruta);
			File filePdf = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			this.pathPdfProyecto = this.pathProyecto;
			rutaPdf = this.pathPdfProyecto + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "motivorechazo.pdf";

			File fileZul = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			this.patchZulProyecto = this.pathProyecto;
			rutaZul = this.patchZulProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"content"+ System.getProperty("file.separator") + "modalreporteEstructurado.zul";

			//Sustitucion de caracteres codificados (Para servidores con Windows)
			this.pathProyecto= ruta.replaceAll("%20", " ");
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public List<Motivo> getObtenerTipos() {
		return todosTipos;
	}

	public MotivoFilter getMotivoFilter() {
		return motivoFilter;
	}

	public String getFooter() {
		return String.format(footerMessage, todosTipos.size());
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeFilter() {
		todosTipos = getFilterTipos(motivoFilter);
	}


	public List<Motivo> getFilterTipos(MotivoFilter tipoFilter) {

		List<Motivo> someTipos = new ArrayList<Motivo>();
		String nombre = tipoFilter.getNombre().toLowerCase();
		String des = tipoFilter.getDescripcion().toLowerCase();
		List<Motivo> tiposAll = null;
		try {
			tiposAll = dao.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Motivo> i = tiposAll.iterator(); i.hasNext();) {
			Motivo tmp = i.next();
			if (tmp.getNombreMotivo().toLowerCase().contains(nombre) && tmp.getDescMotivo().toLowerCase().contains(des)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
	public void onClick$btnRegistrar() {

		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		}
		else {
			try {
				Motivo motivo= new Motivo();
				motivo.setNombreMotivo(nombre.getText());
				motivo.setDescMotivo(descripcion.getText());
				motivo.setEstadoMotivo("A");
				dao.agregarMotivo(motivo);
				Messagebox.show("Se ha registrado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/motivo.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void onClick$btnLimpiar() {
		nombre.setText("");
		descripcion.setText("");
		nombre.setFocus(true);
	}
	public void onClick$btnEditar() {

		System.out.println(id.getValue());
		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		}
		else {
			try {
				Motivo objeto = dao.obtenerMotivo(id.getValue());
				objeto.setNombreMotivo(nombre.getValue());
				objeto.setDescMotivo(descripcion.getValue());
				dao.modificarMotivo(objeto);
				Messagebox.show("Se ha modificado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/motivo.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
			catch (Exception x) {
				// TODO Auto-generated catch block
				x.printStackTrace();
			}
		}
	}

	@Command
	public void changeEditable(@BindingParam("editarMotivo") Motivo tip) {
		HashMap motivo = new HashMap();
		motivo.put("objMotivo", tip);
		this.isEdit = true;
		this.isCreate = false;
		motivo.put("edit", this.isEdit);
		motivo.put("create", this.isCreate);
		Window window = (Window) Executions.createComponents(
				"content/modalMotivo.zul", null, motivo);
		window.doModal();
	}

	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeEditableStatus(@BindingParam("motivo") Motivo tipo) {
		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Interrogativo", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {

						if(dao.eliminarMotivo(tipo)){
							todosTipos.remove(tipo);
							Messagebox.show("Se ha eliminado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);

						}else{
							Messagebox.show("No se puede eliminar el registro", "Información", Messagebox.OK, Messagebox.INFORMATION);

						}
					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(tipo);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}
			private void refreshRowTemplate(Motivo motivo) {
				//Metodo Para Refrescar la Tabla cuando elimina
				BindUtils.postNotifyChange(null, null, MotivoViewModel.this, "obtenerTipos");
				BindUtils.postNotifyChange(null, null, MotivoViewModel.this, "footer");
			}
		}
				);

	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void confirm(@BindingParam("motivo") Motivo tipo) {
		changeEditableStatus(tipo);
		try {
			todosTipos.remove(tipo);
			dao.eliminarMotivo(tipo);
			Messagebox.show("Se ha eliminado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(tipo);
	}

	public void refreshRowTemplate(Motivo tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}
	///////////////////////////////////////////////METODOS PARA BOTON IMPRIMIR///////////////////////////////////////////////////////

	public void generarPdf() throws SQLException, ParseException, JRException  {
		generarReporte();
	}

	public void generarReporte() throws SQLException, ParseException, JRException{

		try {
			Class.forName("org.postgresql.Driver");
			// para la conexion con la BD
			conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
		} catch (ClassNotFoundException ex) {

			ex.printStackTrace();
		}

		parameterMap.put("dirbase", this.rutabase);

		JasperReport jasperReport = JasperCompileManager.compileReport(this.pathProyecto);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);


		archivoJasper = new AMedia(rutaPdf, "pdf" ,"application/pdf", JasperExportManager.exportReportToPdf(jasperPrint));

	}


	@Command
	public void abrirModal(@ContextParam(ContextType.VIEW) Component view) throws SQLException, ParseException, JRException {
		parameterMap.put("ruta", patch);		
		Map<String, Object> args = new HashMap<String, Object>();
		generarPdf();

		args.put("reporte", archivoJasper);
		final Window component = (Window) Executions.getCurrent().createComponents("content/modalImprimir.zul", view, args);
		component.setAction("show: slideDown;hide: slideUp");
		component.doModal();
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}