package vistaModelo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import modelo.entidad.Area;
import modelo.entidad.Categoria;
import modelo.entidad.Evento;
import modelo.entidad.Evento_comision;
import modelo.entidad.Preferencia;
import modelo.entidad.Preferencia_evento;
import modelo.entidad.TipoEvento;
import modelo.entidad.TipoPersona;
import modelo.entidad.TipoPublico;
import modelo.hibernate.dao.DaoAreaArrend;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoCategoria;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoPreferencia;
import modelo.hibernate.dao.DaoPreferencia_evento;
import modelo.hibernate.dao.DaoTipoEvento;
import modelo.hibernate.dao.DaoTipoPublico;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class EventoViewModel extends GenericForwardComposer<Window>{
	private static final String footerMessage = "Total de %d Solicitudes(s) ";
	private static final String footerMessage2 = "Total de %d Evento(s) Registrados";
	private static final String footerMessage3 = "Total de %d Evento(s) Planificados";
	private  EventoFilter filterEvento;
	private DaoEvento dao ;
	private List<Evento>  eventos;
	private DaoPreferencia_evento daopreferencia_evento = new DaoPreferencia_evento();
	private DaoAreaArrend daoareaarrend = new DaoAreaArrend();
	private List<Evento>  eventosPlanificar;

	private List<Evento>  eventosResultados;
	private boolean isEdit,isCreate,isView,isVer,estado  = false;

	@Init
	public void init() throws Exception {

		this.eventos= new ArrayList<Evento>();
		eventosPlanificar =new ArrayList<Evento>();
		filterEvento = new  EventoFilter();
		this.dao= new DaoEvento();
		//eventos = dao.obtenerTodos();
		eventosPlanificar=dao.obtenerEventosPorEstado("R");//dao.obtenerEventosParaPlanificar();
		eventosResultados=dao.obtenerEventosPorEstado("P");
		eventos=this.dao.obtenerEventoPorEstado();


	}


	public List<Evento> getFilterEventos(EventoFilter tipoFilter) {

		List<Evento> someTipos = new ArrayList<Evento>();
		String tipo = tipoFilter.getTipo().toLowerCase();
		String nombre = tipoFilter.getNombre().toLowerCase();
		List<Evento> tiposAll = null;
		try {
			//Aqui trae todos los R
			tiposAll =this.dao.obtenerEventoPorEstado();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Evento> i = tiposAll.iterator(); i.hasNext();) {
			Evento tmp = i.next();
			if (tmp.getTipoEvento().getNombreTipoEvento().toLowerCase().contains(tipo) && tmp.getNombreEvento().toLowerCase().contains(nombre)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
	
	
	
	public List<Evento> getFilterEventosP(EventoFilter tipoFilter) {
		

		List<Evento> someTipos = new ArrayList<Evento>();
		String tipo = tipoFilter.getTipo().toLowerCase();
		String nombre = tipoFilter.getNombre().toLowerCase();
		List<Evento> tiposAll = null;
		try {
			tiposAll= this.dao.obtenerEventosPorEstado("R");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Evento> i = tiposAll.iterator(); i.hasNext();) {
			Evento tmp = i.next();
			if (tmp.getTipoEvento().getNombreTipoEvento().toLowerCase().contains(tipo) && tmp.getNombreEvento().toLowerCase().contains(nombre)) {
				someTipos.add(tmp);
				System.out.print("lalalal");
			}
		}
		return someTipos;
		
	}
	
public List<Evento> getFilterEventosR(EventoFilter tipoFilter) {
		
      List<Evento> someTipos = new ArrayList<Evento>();
		String tipo = tipoFilter.getTipo().toLowerCase();
		String nombre = tipoFilter.getNombre().toLowerCase();
		List<Evento> tiposAll = null;
		try {
			tiposAll=dao.obtenerEventosPorEstado("P");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Evento> i = tiposAll.iterator(); i.hasNext();) {
			Evento tmp = i.next();
			if (tmp.getTipoEvento().getNombreTipoEvento().toLowerCase().contains(tipo) && tmp.getNombreEvento().toLowerCase().contains(nombre)) {
				someTipos.add(tmp);
				
			}
		}
		return someTipos;
		
	}


public List<Evento> getEventosResultados() {
	return eventosResultados;
}


public void setEventosResultados(List<Evento> eventosResultados) {
	this.eventosResultados = eventosResultados;
}

	public EventoFilter getFilterEvento() {
		return filterEvento;
	}


	public void setFilterEvento(EventoFilter filterEvento) {
		this.filterEvento = filterEvento;
	}

	@Command
	@NotifyChange({"eventos", "footer"})
	public void changeFilterEventos() throws Exception {
		if(this.filterEvento!=null){
		eventos =getFilterEventos(filterEvento);	
		}
	

	}
	@Command
	@NotifyChange({"eventosPlanificar", "footer2"})
	public void changeFilter1(){
		if(this.filterEvento!=null){
		this.eventosPlanificar= this.getFilterEventosP(filterEvento);
		
	}
		}
	
	@Command
	@NotifyChange({"eventosResultados", "footer3"})
	public void changeFilterEjecutados(){
		this.eventosResultados=getFilterEventosR(filterEvento);
	}
	//@Command
	//@NotifyChange({"eventosPlanificar", "footer2"})
	//public void changeFilter1() throws Exception {
		//if(this.filterEvento!=null){
			//System.out.print("EPALE");
			//eventosPlanificar=getFilterEventosM(filterEvento);
	//	}
	

	//}





	public ListModel<Evento> getTipoModel() {
		return new ListModelList<Evento>(eventos);
	}

	public String getFooter() {
		return String.format(footerMessage2, eventos.size());
	}

	public String getFooter2() {
		return String.format(footerMessage2, eventosPlanificar.size());
	}
	public String getFooter3() {
		return String.format(footerMessage3, eventosResultados.size());
	}




	@Command
	public void verCamposDuracion(@BindingParam("objetoEvento")Evento objeto){
		HashMap<String, Evento> objetoMandarModal = new HashMap<String, Evento>();
		objetoMandarModal.put("ObjetoVerEvento", objeto);
		Window window = (Window)Executions.createComponents(
				"content/modalVerEventoPorDuracion.zul", null, objetoMandarModal);
		window.doModal();
	}
	@Command
	public void AbrirModalRechazo(){
		Window window = (Window)Executions.createComponents(
				"content/modalRechazo.zul", null,null);
		window.doModal();
	}
	@Command
	public void AprobarSolicitudEvento(){
		JOptionPane.showMessageDialog(null, "Se Aprobo Con Exito");

	}



	public List<Evento> getEventos() {
		return eventos;
	}
	public List<Evento> getEventosPlanificar() {
		return eventosPlanificar;
	}


	public void setEventosPlanificar(List<Evento> eventosPlanificar) {
		this.eventosPlanificar = eventosPlanificar;
	}


	public void setEventos(List<Evento> eventos) {
		this.eventos = eventos;
	}

	public List<Evento> getObtenerEventos() {
		return eventos;
	}
	
	public List<Evento> getObtenerEventosRegistrados() {
		return eventosPlanificar;
	}
	
	
	
	public List<Evento> getObtenerEventosPlanificar() {
		try {
			return dao.obtenerEventosParaPlanificar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Evento> getObtenerEventosFinalizados() {
		try {
			return dao.obtenerEventosPorEstado("F");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Command
	@NotifyChange({"eventos", "footer"})
	public void editarEvento(@BindingParam("editarEvento") Evento evento) {
		HashMap<String, Object> MapEvento = new HashMap<String, Object>();
		MapEvento.put("objetoEvento", evento);
		this.isEdit = true;
		this.isCreate = false;
		this.isView =false;
		this.estado=true;
		MapEvento.put("edit", this.isEdit);
		MapEvento.put("create", this.isCreate);
		MapEvento.put("ver", this.isCreate);
		MapEvento.put("estado", this.estado);
		Window window = (Window) Executions.createComponents(
				"content/modalRegistrarEventoPorDuracion.zul", null, MapEvento);
		window.doModal();
	}


	
	
	
	@Command
	public void verEvento(@BindingParam("verEvento") Evento evento) {
		HashMap<String, Serializable> MapEvento = new HashMap<String, Serializable>();
		MapEvento.put("objetoEvento", evento);
		this.isEdit = false;
		this.isCreate = false;
		this.isView =true;
		this.estado=true;
		MapEvento.put("edit", this.isEdit);
		MapEvento.put("create", this.isCreate);
		MapEvento.put("ver", this.isView);
		MapEvento.put("estado", this.estado);

		Window window = (Window) Executions.createComponents(
				"content/modalVerEventoPorDuracion.zul", null, MapEvento);
		window.doModal();
	}

//	@Command
//	@NotifyChange({"eventos", "footer"})
//	public void eliminarEvento(@BindingParam("EliminarEvento") Evento evento) {
//
//		Messagebox.show("¿Esta seguro de eliminar el registro?", 
//				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
//				Messagebox.QUESTION,
//				new org.zkoss.zk.ui.event.EventListener(){
//			public void onEvent(Event e){
//				if(Messagebox.ON_OK.equals(e.getName())){
//					//OK is clicked
//					try {
//						eventos.remove(evento);
//						daopreferencia_evento.eliminarEvento_Preferencia(evento);
//						daoareaarrend.eliminarEvento_Area(evento);
//						dao.eliminarEvento(evento);
//						
//					} catch (Exception x) {
//						// TODO Auto-generated catch block
//						x.printStackTrace();
//					}
//					refreshRowTemplate(evento);
//				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
//					//Cancel is clicked
//				}
//			}
//
//			private void refreshRowTemplate(Evento evento) {
//
//				//Metodo Para Refrescar la Tabla cuando elimina
//				BindUtils.postNotifyChange(null, null,  EventoViewModel.this, "eventos");
//				BindUtils.postNotifyChange(null, null,  EventoViewModel.this, "footer");
//			}
//		}
//				);
//
//	}



	@Command
	public void changeEditable(@BindingParam("evento") Evento eve) {
		HashMap<String, Evento> evento = new HashMap<String, Evento>();
		evento.put("evento", eve);
		Window window = (Window)Executions.createComponents(
				"content/modalConfiguracionEventos.zul", null, evento);
		//Executions.createComponents("content/gridComisionesEvento.zul", null, evento);
		window.doModal();
	}

	@Command
	public void changeEditableEjecucion(@BindingParam("eventos") Evento eve) {
		HashMap<String, Evento> evento = new HashMap<String, Evento>();
		evento.put("objeto", eve);
		Window window = (Window)Executions.createComponents(
				"content/modalEjecucionEvento.zul", null, evento);
		//Executions.createComponents("content/gridComisionesEvento.zul", null, evento);
		window.doModal();
	}



	@Command
	public void changeEditableResultados(@BindingParam("evento") Evento eve) {
		HashMap<String, Evento> evento = new HashMap<String, Evento>();
		evento.put("evento", eve);
		Window window = (Window)Executions.createComponents(
				"content/modalResultadosEvento.zul", null, evento);
		window.doModal();
	}

	@Command
	public void changeEditableComisiones(@BindingParam("eventos") Evento eve) {
		HashMap<String, Evento> evento = new HashMap<String, Evento>();
		evento.put("comision", eve);
		Window window = (Window)Executions.createComponents(
				"content/modalConfigComision.zul", null, evento);
		window.doModal();
	}

	@Command
	public void changeEditableRecursos(@BindingParam("eventos") Evento eve) {
		HashMap<String, Evento> evento = new HashMap<String, Evento>();
		evento.put("recurso", eve);
		Window window = (Window)Executions.createComponents(
				"content/modalConfigRecurso.zul", null, evento);
		window.doModal();
	}

	@Command
	public void changeEditableIndicadores(@BindingParam("eventos") Evento eve) {
		HashMap<String, Evento> evento = new HashMap<String, Evento>();
		evento.put("indicador", eve);
		Window window = (Window)Executions.createComponents(
				"content/modalConfigIndicador.zul", null, evento);
		window.doModal();
	}


	
	@GlobalCommand
	@NotifyChange({"eventos", "footer"})
	public void RefrescarTablaEventos() throws Exception {
		eventos=this.dao.obtenerEventoPorEstado();
	}
	
	@GlobalCommand
	@NotifyChange({"eventosPlanificar", "footer2"})
	public void RefrescarTablaPlanificar() throws Exception {
		eventosPlanificar=dao.obtenerEventosPorEstado("R");
	}
	
	@GlobalCommand
	@NotifyChange({"obtenerEventosPlanificados", "footer3"})
	public void RefrescarTablaResultados() throws Exception {
		eventosResultados=dao.obtenerEventosPorEstado("P");
	}
}


