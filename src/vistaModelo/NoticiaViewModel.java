package vistaModelo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.zkoss.bind.BindContext;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.image.Image;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import modelo.entidad.Categoria;
import modelo.entidad.Club;
import modelo.entidad.Noticia;
import modelo.entidad.Preferencia;
import modelo.entidad.TipoNoticia;
import modelo.hibernate.dao.DaoClub;
import modelo.hibernate.dao.DaoNoticia;
import modelo.hibernate.dao.DaoTipoNoticia;

public class NoticiaViewModel extends GenericForwardComposer<Window> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String footerMessage = "Total de %d Noticia(s) ";
	private NoticiaFilter noticiaFilter = new NoticiaFilter();

	private List<Noticia> noticias;
	private DaoNoticia daoNoticia = new DaoNoticia();
	private DaoTipoNoticia daoTipoNoticia = new DaoTipoNoticia();
	private DaoClub daoClub = new DaoClub();
	private boolean isEdit;
	private boolean isCreate;
	private boolean isVer;
	private boolean estado;

	private TipoNoticia tipoNoticiaSelected;
	@Wire
	private Intbox id;
	@Wire
	private Textbox tituloNoticia;
	@Wire
	private Datebox fechaInicioNoticia;
	@Wire
	private Datebox fechaExpNoticia;
	@Wire
	private Textbox descripcionNoticia;
	@Wire
	private Combobox comboTipoNoticia;
	@Wire
	private Combobox comboDifusionNoticia;
	@Wire
	Window modalDialog;
	@Wire
	public Label message;
	@Wire
	public Div box;
	
	private static AImage myImage;

	private Div contenido;

	private Div contenedor;

	private String pagina;

	@Init
	public void init() throws Exception {
		this.daoTipoNoticia = new DaoTipoNoticia();
		this.daoNoticia = new DaoNoticia();
		this.noticias = new ArrayList<Noticia>();
		this.noticias = daoNoticia.obtenerTodos();
		
		}
		//this.myImage = null;
	

	public NoticiaFilter getNoticiaFilter() {
		return noticiaFilter;
	}

	public ListModel<Noticia> getNoticiaModel() {
		return new ListModelList<Noticia>(noticias);
	}

	public String getFooter() {
		return String.format(footerMessage, noticias.size());
	}

	public List<Noticia> getFilterNoticia(NoticiaFilter noticiaFilter) {

		List<Noticia> someNoticias = new ArrayList<Noticia>();
		String tipo = noticiaFilter.getTipo().toLowerCase();
		String difusion = noticiaFilter.getDifusion().toLowerCase();
		String titulo = noticiaFilter.getTitulo().toLowerCase();
		List<Noticia> tiposAll = null;
		try {
			tiposAll = daoNoticia.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (Iterator<Noticia> i = tiposAll.iterator(); i.hasNext();) {
			Noticia tmp = i.next();
			if (tmp.getTipoNoticia().getNombreTipoNoticia().toLowerCase()
					.contains(tipo)
					&& tmp.getTituloNoticia().toLowerCase().contains(titulo)
					&& tmp.getDifusionNoticia().toLowerCase()
							.contains(difusion)) {
				someNoticias.add(tmp);
			}
		}
		return someNoticias;
	}

	@Command
	@NotifyChange({ "noticiaModel", "footer" })
	public void changeFilter() {
		this.noticias = this.getFilterNoticia(noticiaFilter);
	}
	@Command
	public void limpiarImagen(){
		this.myImage = null;
		Noticia noticia1 = new Noticia();
		HashMap noticiaMap = new HashMap();
		noticiaMap .put("objeto",noticia1);
		this.isEdit = false;
		this.isCreate = true;
		this.isVer = false;
		this.estado=false;
		noticiaMap.put("edit", this.isEdit);
		noticiaMap.put("create", this.isCreate);
		noticiaMap.put("ver", this.isVer);
		noticiaMap.put("estado", this.estado);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalRegistrarNoticia.zul", null,noticiaMap);
		window.doModal();
	}

	@Command
	public void changeEditable(@BindingParam("editarNoticia") Noticia noticia) {
		HashMap noticiaMap = new HashMap();
		try {
		    AImage nueva = new AImage("nombre",noticia.getImagenNoticia());
		    this.setMyImage(nueva);
		} catch (IOException e) {
			System.out.print("ERROR EN LA IMAGEN" + noticia.getIdNoticia());
		}
		noticiaMap.put("objeto", noticia);
		this.isEdit = true;
		this.isCreate = false;
		this.isVer = false;
		this.estado = false;
		noticiaMap.put("edit", this.isEdit);
		noticiaMap.put("create", this.isCreate);
		noticiaMap.put("estado", this.estado);
		noticiaMap.put("ver", this.isVer);
		System.out.print(isEdit);
		Window window = (Window) Executions.createComponents(
				"content/modalRegistrarNoticia.zul", null, noticiaMap);
		window.doModal();
	}

	@Command
	public void VerNoticia(@BindingParam("verNoticia") Noticia vernoticia) {
		System.out.print("id" + vernoticia.getIdNoticia());
		try {
		    AImage nueva = new AImage("nombre",vernoticia.getImagenNoticia());
		    this.setMyImage(nueva);
		} catch (IOException e) {
			System.out.print("ERROR EN LA IMAGEN" + vernoticia.getIdNoticia());
		}
		HashMap noticia1 = new HashMap();
		noticia1.put("objeto", vernoticia);
		this.isEdit = false;
		this.isCreate = false;
		this.isVer = true;
		this.estado = true;
		noticia1.put("edit", this.isEdit);
		noticia1.put("create", this.isCreate);
		noticia1.put("ver", this.isVer);
		noticia1.put("estado", this.estado);
		Window window = (Window) Executions.createComponents(
				"content/modalRegistrarNoticia.zul", null, noticia1);
		window.doModal();
	}

	@Command
	public void EliminarNoticia(
			@BindingParam("eliminarNoticia") Noticia eliminar) {
		JOptionPane.showMessageDialog(null, "La Noticia se Elimino con Exito");
	}

	public List<TipoNoticia> getAllTipoNoticia() {
		try {
			return daoTipoNoticia.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public void onClick$btnRegistrar() {

		if ((this.tituloNoticia.getValue().equals("")
				|| this.descripcionNoticia.equals("") || this.myImage == null
				|| this.fechaInicioNoticia.equals("") || this.fechaInicioNoticia
					.equals(""))) {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		} else {
			try {
				this.tipoNoticiaSelected = this.comboTipoNoticia.getSelectedItem().getValue();
				Club club= new Club();
				Noticia noticia = new Noticia();
				System.out.print("\nTipo Noticia: "+this.tipoNoticiaSelected+"\n Titulo:"+this.tituloNoticia.getValue()+"\n");
				System.out.print("\nDescripcion: "+this.descripcionNoticia.getValue()+"\n Fecha ini: "+this.fechaInicioNoticia.getValue()+"\n");
				System.out.print("\n Fecha fin"+ this.fechaExpNoticia.getValue() + "\n Difusion: "+this.comboDifusionNoticia.getValue()+"\n");
				noticia.setTipoNoticia(this.tipoNoticiaSelected);
				noticia.setTituloNoticia(this.tituloNoticia.getValue());
				noticia.setDescNoticia(this.descripcionNoticia.getValue());
				noticia.setEstadoNoticia("A");
				//imagen en byte[]
				noticia.setImagenNoticia(this.getMyImage().getByteData());
				//this.myImage = null;
				club.setIdClub(1);
				noticia.setClub(club);
				noticia.setFechaIniNoticia(this.fechaInicioNoticia.getValue());
				noticia.setFechaExpNoticia(this.fechaExpNoticia.getValue());
				noticia.setDifusionNoticia(this.comboDifusionNoticia.getSelectedItem().getValue());
				daoNoticia.agregarNoticia(noticia);
				Messagebox.show("Se ha Registrados Correctamente",
						"Information", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/registrarNoticias.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null,
						null);
				contenido.appendChild(contenedor);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@NotifyChange("myImage")
	public void onClick$btnLimpiar() {
		this.tituloNoticia.setText("");
		this.descripcionNoticia.setText("");
		this.comboDifusionNoticia.setValue("");
		this.fechaInicioNoticia.setValue(new Date());
		this.fechaExpNoticia.setValue(new Date());
		this.comboTipoNoticia.setValue("");
		this.myImage = null;
	}

	public void onClick$btnEditar() {

		if ((this.tituloNoticia.getValue().equals("") || this.comboTipoNoticia.getSelectedItem() == null
				|| this.descripcionNoticia.equals("") || this.myImage == null
				|| this.fechaInicioNoticia.equals("") || this.fechaInicioNoticia
					.equals(""))) {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		} else {
			try {
				Noticia noticia = daoNoticia.obtenerNoticia(id.getValue());
				
				noticia.setEditingStatus(false);
				noticia.setTipoNoticia(this.comboTipoNoticia.getSelectedItem().getValue());
				noticia.setTituloNoticia(tituloNoticia.getValue());
				noticia.setDescNoticia(descripcionNoticia.getValue());
				noticia.setEstadoNoticia("A");
				noticia.setFechaIniNoticia(fechaInicioNoticia.getValue());
				noticia.setFechaExpNoticia(fechaExpNoticia.getValue());
				noticia.setDifusionNoticia(comboDifusionNoticia.getSelectedItem().getValue());
				noticia.setImagenNoticia(this.getMyImage().getByteData());
				daoNoticia.modificarNoticia(noticia);
				Messagebox.show("Se ha Modificado Correctamente",
						"Information", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/registrarNoticias.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null,
						null);
				contenido.appendChild(contenedor);
			} catch (Exception x) {
				// TODO Auto-generated catch block
				x.printStackTrace();
			}
		}
	}

	@Command
	@NotifyChange({ "noticiaModel", "footer" })
	public void changeEditableStatus(@BindingParam("noticia") Noticia noticia) {
		Messagebox.show("¿Esta seguro de eliminar el registro?",
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
					public void onEvent(Event e) {
						if (Messagebox.ON_OK.equals(e.getName())) {
							// OK is clicked
							try {
								noticias.remove(noticia);
								daoNoticia.eliminarNoticia(noticia);
							} catch (Exception x) {
								// TODO Auto-generated catch block
								x.printStackTrace();
							}
							refreshRowTemplate(noticia);
						} else if (Messagebox.ON_CANCEL.equals(e.getName())) {
							// Cancel is clicked
						}
					}

					private void refreshRowTemplate(Noticia noticia) {
						// Metodo Para Refrescar la Tabla cuando elimina
						BindUtils.postNotifyChange(null, null,
								NoticiaViewModel.this, "noticiaModel");
						BindUtils.postNotifyChange(null, null,
								NoticiaViewModel.this, "footer");
					}
				});

	}

	@Command
	@NotifyChange({ "noticiaModel", "footer" })
	public void confirm(@BindingParam("noticia") Noticia noticia) {
		changeEditableStatus(noticia);
		try {
			noticias.remove(noticia);
			daoNoticia.eliminarNoticia(noticia);
			Messagebox.show("Se ha Eliminado Correctamente", "Information",
					Messagebox.OK, Messagebox.INFORMATION);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(noticia);
	}
	
	@Command("upload")
	@NotifyChange("myImage")
	public void onImageUpload(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx){
		UploadEvent upEvent = null;
		   Object objUploadEvent = ctx.getTriggerEvent();
		   if (objUploadEvent != null && (objUploadEvent instanceof UploadEvent)) {
		     upEvent = (UploadEvent) objUploadEvent;
		    }
		    if (upEvent != null) {
		     Media media = upEvent.getMedia();
		     int lengthofImage = media.getByteData().length;
		     if (media instanceof Image) {
		      if (lengthofImage > 500 * 1024) {
		    	  Messagebox.show("Seleccione una imagen de tamaño menor que 500Kb", "Error", Messagebox.OK, Messagebox.ERROR);
		        return;
		      }
		      else{
		        setMyImage((AImage) media);//Initialize the bind object to show image in zul page and Notify it also
		        System.out.println(media.getContentType());
		        System.out.println(media.getName());
		        System.out.println(getMyImage().getName());
		      }
		     }
		      else {
		    	  Messagebox.show("Seleccione un archivo de imagen", "Error", Messagebox.OK, Messagebox.ERROR);
		     }
		 } else {
			 System.out.println("Upload Event Is not Coming");
		     }
	}
	
	public AImage getMyImage() {
		return myImage;
	}

	public void setMyImage(AImage myImage) {
		this.myImage = myImage;
	}

	public void refreshRowTemplate(Noticia noticia) {
		/*
		 * This code is special and notifies ZK that the bean's value has
		 * changed as it is used in the template mechanism. This stops the
		 * entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, noticia, "editingStatus");
	}

}
