package vistaModelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

 
 
import modelo.entidad.Referencia; 
import modelo.hibernate.dao.DaoReferencia;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;



public class ReferenciaViewModel extends GenericForwardComposer<Window> { 

	private static final String footerMessage = "Total de %d Persona ";
	 
	private DaoReferencia dao = new DaoReferencia();
	private List<Referencia> todosTipos;
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;
	
 

	@Wire
	Window modalDialog;
	@Wire
	public Label message;
	@Wire
	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public ReferenciaViewModel() {
		super();
		try {
			todosTipos = dao.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public List<Referencia> getObtenerTipos() {
		return todosTipos;
	}

	 

	public String getFooter() {
		return String.format(footerMessage, todosTipos.size());
	}

 

 
	
//PARA REGISTRARA	
//	public void onClick$btnRegistrar() {
//
//		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
//			box.setVisible(true);
//			message.setValue("Debe llenar todos los campos");
//		}
//		else {
//			try {
//				Parentesco parentesco= new Parentesco(nombre.getText(),
//						descripcion.getText(),"A");
//				dao.agregarParentesco(parentesco);
//				modalDialog.detach();
//				pagina = "vista/parentesco.zul";
//				contenido.getChildren().clear();
//				contenedor = (Div) Executions.createComponents(pagina, null, null);
//				contenido.appendChild(contenedor);
//			}
//			catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}

	
//PARA LIMPIAR
//	public void onClick$btnLimpiar() {
//		nombre.setText("");
//		descripcion.setText("");
//		nombre.setFocus(true);
//	}
	
	//PARA EDITAR
//	public void onClick$btnEditar() {
//
//		System.out.println(id.getValue());
//		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
//			box.setVisible(true);
//			message.setValue("Debe llenar todos los campos");
//		}
//		else {
//			try {
//				Parentesco objeto = dao.obtenerParentesco(id.getValue());
//				objeto.setNombreParentesco(nombre.getValue());
//				objeto.setDescParentesco(descripcion.getValue());
//				dao.modificarParentesco(objeto);
//				modalDialog.detach();
//				pagina = "vista/parentesco.zul";
//				contenido.getChildren().clear();
//				contenedor = (Div) Executions.createComponents(pagina, null, null);
//				contenido.appendChild(contenedor);
//			}
//			catch (Exception x) {
//				// TODO Auto-generated catch block
//				x.printStackTrace();
//			}
//		}
//	}

//	@Command
//	public void changeEditable(@BindingParam("editarParentesco") Parentesco tip) {
//		HashMap parentesco = new HashMap();
//		parentesco.put("objParentesco", tip);
//		this.isEdit = true;
//		this.isCreate = false;
//		parentesco.put("edit", this.isEdit);
//		parentesco.put("create", this.isCreate);
//		Window window = (Window) Executions.createComponents(
//				"content/modalParentesco.zul", null, parentesco);
//		window.doModal();
//	}

	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}
//
//	@Command
//	@NotifyChange({"obtenerTipos", "footer"})
//	public void changeEditableStatus(@BindingParam("persona") Referencia tipo) {
//		Messagebox.show("¿Esta seguro de eliminar el registro?", 
//				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
//				Messagebox.QUESTION,
//				new org.zkoss.zk.ui.event.EventListener(){
//			public void onEvent(Event e){
//				if(Messagebox.ON_OK.equals(e.getName())){
//					//OK is clicked
//					try {
//						todosTipos.remove(tipo);
//						dao.eliminarReferencia(tipo);
//
//
//					} catch (Exception x) {
//						// TODO Auto-generated catch block
//						x.printStackTrace();
//					}
//					refreshRowTemplate(tipo);
//				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
//					//Cancel is clicked
//				}
//			}
//		}
//				);
//
//	}

//	@Command
//	@NotifyChange({"obtenerTipos", "footer"})
//	public void confirm(@BindingParam("persona") Parentesco tipo) {
//		changeEditableStatus(tipo);
//		try {
//			todosTipos.remove(tipo);
//			dao .eliminarReferencia(tipo);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		refreshRowTemplate(tipo);
//	}

	public void refreshRowTemplate(Referencia tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}       
	
	
	
// PARA ABRIR MODAL ASIGNAR ACCION
//	  @Command
//		public void changeEditableAsignarAccion(@BindingParam("asignarAccion") Referencia person) {
//		  System.out.println("WEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
//			HashMap asignarAccion = new HashMap();
//			asignarAccion.put("accion", person);
//			Window window = (Window)Executions.createComponents(
//					"content/modalAsignarAccion.zul", null, asignarAccion);
//			window.doModal();
//		} 
//	
//	
//	
//	
//    		
// 
//	@Command
//	public void changeEditable(@BindingParam("ceseAcciones") Referencia person) {
//		HashMap personas = new HashMap();
//		personas.put("objeto", person);
//		Window window = (Window)Executions.createComponents(
//				"content/modalCeseAccion.zul", null, personas);
//		window.doModal();
//	}
//	
//	@Command
//	public void reactivar(@BindingParam("reactivarMiembro") Referencia person) {
//		HashMap personas = new HashMap();
//		personas.put("objetoAccion", person);
//		Window window = (Window)Executions.createComponents(
//				"content/modalReactivarMiembro.zul", null, personas);
//		window.doModal();
//	}
//
//	 @Command({"visitaModal"}) @GlobalCommand("visitaModal")
//	    public void abrirModalRegistrarAcompanante(Event e) {
//		Window window = (Window)Executions.createComponents(
//				"content/modalVisita.zul", null, null);
//		window.doModal();
//	       
//	    }
	    
}
