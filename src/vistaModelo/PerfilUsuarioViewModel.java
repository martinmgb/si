package vistaModelo;	

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.BindContext;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.image.Image;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;













import modelo.entidad.Categoria;
import modelo.entidad.Club;
import modelo.entidad.Persona;
import modelo.entidad.Preferencia;
import modelo.entidad.Preferencia_persona;
import modelo.entidad.Usuario;
import modelo.hibernate.dao.DaoCategoria;
import modelo.hibernate.dao.DaoClub;
import modelo.hibernate.dao.DaoPersona;
import modelo.hibernate.dao.DaoPreferencia;
import modelo.hibernate.dao.DaoPreferenciaPersona;
import modelo.hibernate.dao.DaoUsuario;



public class PerfilUsuarioViewModel extends GenericForwardComposer<Window> { 

	private DaoUsuario daoUsuario = new DaoUsuario();
	private DaoPersona daoPersona = new DaoPersona();
	private DaoClub dao = new DaoClub();
	private Usuario user;
	private Persona people;
	private static AImage myImage;
	List<Categoria> categorias;
	private Categoria CategoriaSelected;
	private Preferencia preferenciaSelected;
	private  List<Preferencia> listaPreferencia;
	private static Set<Preferencia_persona> listaPreferenciaPersona;
	private DaoCategoria daocategoria = new DaoCategoria();
	private DaoPreferencia daopreferencia = new DaoPreferencia() ;
	private DaoPreferenciaPersona daopp = new DaoPreferenciaPersona();
	private static final String footerMessage3 = "Total de %d Preferencia(s)";
	private String preferencialimpiar;

	@Wire
	private Textbox apellidop;
	@Wire
	private Textbox nombrep;
	@Wire
	private Textbox nombre;
	@Wire
	private Textbox direccionp;
	@Wire
	private Textbox tlfcel;
	@Wire
	private Textbox tlfcasa;
	@Wire
	private Textbox correop;
	@Wire
	private Textbox contrasenaold;
	@Wire
	private Textbox newcontrasena;
	@Wire
	private Textbox recontrasena;

	//	public PerfilUsuarioViewModel() {
	//		super();
	//		Session miSession = Sessions.getCurrent();
	//		try {
	////			user = daoUsuario.obtenerUsuario((Integer) miSession.getAttribute("idUsuario"));
	//			this.user = daoUsuario.obtenerUsuario(2);
	//			//this.people= daoPersona.obtenerPersona(this.user.getPersona().getIdPersona());
	//			System.out.print("Evewnt0iodfxokdgvskmÑ: " + direccionp);
	//			//direccionp.setText(this.user.getNombreUsuario());
	//			
	//		} catch (Exception e) {
	//			// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
	//	}

	@Init
	public void init() throws Exception{
		Session miSession = Sessions.getCurrent();
		this.user=daoUsuario.obtenerUsuario((Integer) miSession.getAttribute("idUsuario"));
		//this.nombre.setText(this.user.getNombreUsuario());
		this.people=daoPersona.obtenerPersona(this.user.getPersona().getIdPersona());
		

		///////////////////////////////////////////////////////////////////////
		this.categorias= daocategoria.obtenerTodos();
		this.CategoriaSelected = new Categoria();
		this.listaPreferenciaPersona = people.getPreferencia_persona();
		AImage imagen = new AImage("",people.getImagenPersona());
		setMyImage(imagen);

	}

	public Categoria getCategoriaSelected() {
		return CategoriaSelected;
	}


	public void setCategoriaSelected(Categoria categoriaSelected) {
		CategoriaSelected = categoriaSelected;
	}


	public Preferencia getPreferenciaSelected() {
		return preferenciaSelected;
	}


	public void setPreferenciaSelected(Preferencia preferenciaSelected) {
		this.preferenciaSelected = preferenciaSelected;
	}

	public String getFooter3() {
		return String.format(footerMessage3, listaPreferenciaPersona.size());
	}

	public  Set<Preferencia_persona> getListaPreferenciaPersona() {
		return listaPreferenciaPersona;
	}


	public void setListaPreferenciaPersona(
			Set<Preferencia_persona> listaPreferenciaPersona) {
		PerfilUsuarioViewModel.listaPreferenciaPersona = listaPreferenciaPersona;
	}

	public String getPreferencialimpiar() {
		return preferencialimpiar;
	}

	public void setPreferencialimpiar(String preferencialimpiar) {
		this.preferencialimpiar = preferencialimpiar;
	}

	@Command
	@NotifyChange({"listaPreferenciaPersona","footer3"})
	public void agregarPreferenciasPersona(){
		if(this.preferenciaSelected==null){
			Messagebox.show("Debe Seleccionar Una Preferencia!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else if(buscarPreferencia(this.preferenciaSelected)==null){
			Preferencia_persona obj= new Preferencia_persona();
			obj.setPersona(people);
			obj.setPreferencia(preferenciaSelected);
			this.listaPreferenciaPersona.add(obj);
		}else
		{

			Messagebox.show("La preferencia ya se encuentra registrada!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
	}

	public Preferencia buscarPreferencia(Preferencia preferenciaSelected){
		for (Preferencia_persona preferenciaPersona: listaPreferenciaPersona) {
			if(preferenciaPersona.getPreferencia().getIdPreferencia()==preferenciaSelected.getIdPreferencia())
				return preferenciaSelected;
		}
		return null;
	}

	@Command
	@NotifyChange({"listaPreferenciaPersona","footer3"})
	public void eliminarPreferenciaPersona(@BindingParam("preferencia_persona") Preferencia_persona preferencia) throws Exception{
		this.listaPreferenciaPersona.remove(preferencia);
		this.daopp.eliminarPreferencia_persona(preferencia);
	}

	public List<Categoria> getCategorias() {
		try {
			categorias= daocategoria.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return categorias;
	}

	@Command
	@NotifyChange({"listaPreferencia","preferencialimpiar"})
	public void activarBotonDeBusqueda() throws Exception {
		if(getCategoriaSelected().getNombreCategoria() !=null){
			this.preferencialimpiar ="";
			this.listaPreferencia=daopreferencia.obtenerPreferenciaPorCategoria(this.getCategoriaSelected());
		}
	}



	public List<Preferencia> getListaPreferencia() {
		return listaPreferencia;
	}


	public void setListaPreferencia(List<Preferencia> listaPreferencia) {
		this.listaPreferencia = listaPreferencia;
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

	public Persona getPeople() {
		return people;
	}

	public void setPeople(Persona people) {
		this.people = people;
	}



	@Command
	public void Actualizar() throws Exception{
		if (!CamposVacio()){
			//System.out.print("Aqui"+user.getNombreUsuario());
			daoUsuario.modificarUsuario(user);
			people.setImagenPersona(myImage.getByteData());
			people.setPreferencia_persona(listaPreferenciaPersona);
			daoPersona.modificarPersona(people);
			Messagebox.show("Los Datos se actualizaron Correctamente!", 
					"Advertencia", Messagebox.OK, Messagebox.NONE);
		}

		else{
			Messagebox.show("Debe LLenar Todos Los Campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}

	} 


	public boolean CamposVacio(){
		if(user.getNombreUsuario() != null && !user.getNombreUsuario().equalsIgnoreCase("") && 
				this.people.getDirecPersona() != null && !this.people.getDirecPersona().equalsIgnoreCase("") &&
				this.people.getCorreoPersona()!= null && !this.people.getCorreoPersona().equalsIgnoreCase("") &&
				this.people.getTlfCasaPersona()!= null && !this.people.getTlfCasaPersona().equalsIgnoreCase("") &&
				this.people.getTlfCelPersona()!= null && !this.people.getTlfCelPersona().equalsIgnoreCase(""))
			return false;
		return true;
	}
	
	@Command("upload")
	@NotifyChange("myImage")
	public void onImageUpload(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx){
		UploadEvent upEvent = null;
		   Object objUploadEvent = ctx.getTriggerEvent();
		   if (objUploadEvent != null && (objUploadEvent instanceof UploadEvent)) {
		     upEvent = (UploadEvent) objUploadEvent;
		    }
		    if (upEvent != null) {
		     Media media = upEvent.getMedia();
		     int lengthofImage = media.getByteData().length;
		     if (media instanceof Image) {
		      if (lengthofImage > 500 * 1024) {
		    	  Messagebox.show("Seleccione una imagen de tamaño menor que 500Kb", "Error", Messagebox.OK, Messagebox.ERROR);
		        return;
		      }
		      else{
		        setMyImage((AImage) media);//Initialize the bind object to show image in zul page and Notify it also
		        System.out.println(media.getContentType());
		        System.out.println(media.getName());
		        System.out.println(getMyImage().getName());
		      }
		     }
		      else {
		    	  Messagebox.show("Seleccione un archivo de imagen", "Error", Messagebox.OK, Messagebox.ERROR);
		     }
		 } else {
			 System.out.println("Upload Event Is not Coming");
		     }
	}
	
	public AImage getMyImage() {
		return myImage;
	}
	
	public void setMyImage(AImage myImage) {
		this.myImage = myImage;
	}
}
