package vistaModelo;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.mapping.Value;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Map;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zkmax.zul.Chosenbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import modelo.entidad.Act_comision;
import modelo.entidad.Actividad;
import modelo.entidad.Area;
import modelo.entidad.Area_evento;
import modelo.entidad.Comision;
import modelo.entidad.Evento;
import modelo.entidad.Preferencia_evento;
import modelo.hibernate.dao.DaoAct_comision;
import modelo.hibernate.dao.DaoActividad;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoCategoria;
import modelo.hibernate.dao.DaoComision;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoPreferencia;
import vistaModelo.ComisionFilter;


public class RegistrarComisionViewModel extends GenericForwardComposer<Window>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String footerMessage = "Total de %d Comision(s) ";
	private ComisionFilter comisionFilter = new ComisionFilter();
	private DaoComision daoCom ;
	private List<Actividad> actividad;
	private DaoActividad  daoactividad = new DaoActividad();
	private Actividad actividadSelected;
	private Comision comision;
	List<Comision> todosCom;
	private DaoAct_comision daoactcomision;
	private static Set<Act_comision> listaActcomision;
	private ArrayList<Actividad> listaActividades;



	private boolean displayEdit = true;
	private boolean isEdit,isCreate,isView = false;

	private static final String footerMessage6 = "Total de %d Actividad(s)";
	@Wire
	private Intbox id;
	@Wire
	private Textbox nombre;
	@Wire
	private Textbox descripcion;
	@Wire
	Chosenbox actividades;
	@Wire
	private Window modalDialog;
	@Wire
	public Label message;
	@Wire
	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;

	public RegistrarComisionViewModel() {
		super();
		try {
			//todosCom = daoCom.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public List<Comision> getObtenerTipos() {
		return todosCom;
	}



	public ComisionFilter getComisionsFilter() {
		return comisionFilter;
	}


	public String getFooter() {
		return String.format(footerMessage, todosCom.size());
	}
	public ArrayList<Actividad> getListaArctividades() {
		return listaActividades;
	}


	public void setListaActividades(ArrayList<Actividad> listaActividades) {
		this.listaActividades = listaActividades;
	}


	@Command
	public void Cerrar(@BindingParam("win") Window win){
		win.detach();
	}
	@Init
	public void init(@ExecutionArgParam("objetoCom") Comision comision) {

		if (comision == null) {
			this.comision = new Comision();
			this.listaActcomision = new HashSet<Act_comision>();


		} else {
			this.comision = comision;
			this.listaActcomision = comision.getAct_comision();

		}
		this.daoCom = new DaoComision();
		this.daoactividad = new DaoActividad();
		this.daoactcomision = new DaoAct_comision();
		this.listaActividades= new ArrayList<Actividad>();

	}

	public List<Actividad> getActividad() {
		try {
			this.actividad=this.daoactividad.obtenerTodos();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return actividad;
	}
	public Set<Act_comision> getListaActcomision() {
		return listaActcomision;
	}

	public void setListaActcomision(Set<Act_comision> listaActcomision) {
		RegistrarComisionViewModel.listaActcomision = listaActcomision;
	}



	@Command
	@NotifyChange({"listaActcomision","footer6"})
	public void agregarActividadesComision(){


		if(actividadSelected==null){
			Messagebox.show("Debe Seleccionar una Actividad!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);

		}
		else if (buscarActividad(this.actividadSelected)==null){
			Act_comision obj1 = new Act_comision();
			obj1.setComision(comision);
			obj1.setActividad(actividadSelected);
			String estado="A";
			this.listaActcomision.add(obj1);
		}else
		{

			Messagebox.show("La actividad ya se encuentra registrada", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
	}


	public Actividad buscarActividad(Actividad actividadSelected ){
		for (Act_comision actcomision:listaActcomision) {
			if(actcomision.getActividad().getIdActividad()==actividadSelected.getIdActividad())
				return actividadSelected ;
		}
		return null;
	}

	@Command
	@NotifyChange({"listaActcomision","footer6"})
	public void eliminarActividadComision(@BindingParam("act_comision") Act_comision actividad)throws Exception{
		this.listaActcomision.remove(actividad);
		this.daoactcomision.eliminarAct_comision(actividad);


	}

	public String getFooter6() {
		return String.format(footerMessage6, this.listaActcomision.size());
	}






	public boolean CamposVacio(){
		if(comision.getNombreComision() != null && !comision.getNombreComision().equalsIgnoreCase("") && 
				comision.getDescComision() != null && !comision.getDescComision().equalsIgnoreCase("") &&
				this.listaActcomision.size()>0)
			return false;
		return true;
	}
	
	
	@NotifyChange({"listaActcomision","footer6"})
	public void onClick$btnLimpiar() {
		nombre.setText("");
		descripcion.setText("");
		listaActcomision.clear();
		nombre.setFocus(true);
	}

	//regitrar con la lista
	@Command
	@NotifyChange("*")
	public void RegistrarComision(@BindingParam("win") Window w) throws Exception{	
		if (!CamposVacio())
		{
			comision.setEditingStatus(false);
			comision.setEstadoComision("A");
			comision.setAct_comision(listaActcomision);
			this.daoCom.agregarComision(comision);
			pagina = "vista/comisiones.zul";
			Messagebox.show("Se ah registrado correctamente", 
					"Información", Messagebox.OK, Messagebox.INFORMATION);
			w.detach();
			BindUtils.postGlobalCommand(null, null, "RefrescarTablaComisiones", null);

		}else
		{
			Messagebox.show("Debe llenar todos Los campos", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}
	}


	@Command
	@NotifyChange("*")
	public void EditarComision(@BindingParam("win")Window win) throws Exception{
		if (!CamposVacio()){
			comision.setEditingStatus(false);
			comision.setEstadoComision("A");
			comision.setAct_comision(listaActcomision);
			this.daoCom.modificarComision(comision);
			Messagebox.show("Se ha actualizado correctamente", 
					"Información", Messagebox.OK, Messagebox.INFORMATION);
			win.detach();
			BindUtils.postGlobalCommand(null, null, "RefrescarTablaComisiones", null);

		}else
		{
			Messagebox.show("Debe llenar todos los campos", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}

	}

	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	public Comision getComision() {
		return comision;
	}

	public void setComision(Comision comision) {
		this.comision = comision;
	}

	public Actividad getActividadSelected() {
		return actividadSelected;
	}

	public void setActividadSelected(Actividad actividadSelected) {
		this.actividadSelected = actividadSelected;
	}

	public boolean isDesabilitar(){
		return this.listaActcomision.size()>0;
	}
}