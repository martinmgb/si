package vistaModelo;

public class IndicadorEventoFilter {
	private String nombre="";
	private String valorEsperado="";

	public String getNombre() {
		return nombre;
	}
	
	public String getValorEsperado() {
		return valorEsperado;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}
	
	public void setValorEsperado(String valorEsperado) {
		this.valorEsperado = valorEsperado==null?"":valorEsperado.trim();
	}
	
}
