package vistaModelo;

public class ArrendamientoFilter {

	private String codigo="", cedCliente="", area="", nombCliente="", apellido="";

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo==null?"":codigo.trim();
	}

	public String getCedCliente() {
		return cedCliente;
	}

	public void setCedCliente(String cedCliente) {
		this.cedCliente = cedCliente==null?"":cedCliente.trim();
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area==null?"":area.trim();

	}

	public String getNombCliente() {
		return nombCliente;
	}

	public void setNombCliente(String nombCliente) {
		this.nombCliente = nombCliente==null?"":nombCliente.trim();
	}
	//aqui falta la fecha

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido==null?"":apellido.trim();
	}


}


