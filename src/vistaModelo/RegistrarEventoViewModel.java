package vistaModelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Window;

import modelo.entidad.Area;
import modelo.entidad.Area_arrend;
import modelo.entidad.Area_evento;
import modelo.entidad.Categoria;
import modelo.entidad.Evento;
import modelo.entidad.Preferencia;
import modelo.entidad.Preferencia_evento;
import modelo.entidad.TipoEvento;
import modelo.entidad.TipoPublico;
import modelo.hibernate.dao.DaoAreaArrend;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoAreaEvento;
import modelo.hibernate.dao.DaoCategoria;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoPreferencia;
import modelo.hibernate.dao.DaoPreferencia_evento;
import modelo.hibernate.dao.DaoTipoEvento;
import modelo.hibernate.dao.DaoTipoPublico;

public class RegistrarEventoViewModel  extends GenericForwardComposer<Window> {

	private List<TipoEvento> listaTipoEvento;
	private List<TipoPublico> listaTipoPublico;
	private List<Categoria> categorias;
	private Categoria CategoriaSelected;
	private ArrayList<Area> listaAreas;
	private List<Area> listaTemporalAreas;
	private DaoArea  daoarea = new DaoArea();
	private Area areaSelected;
	private DaoTipoEvento daoTipoevento = new DaoTipoEvento();
	private DaoTipoPublico daotipopublico = new DaoTipoPublico();
	private DaoCategoria daocategoria = new DaoCategoria();
	private DaoPreferencia_evento daopreferencia_evento = new DaoPreferencia_evento();
	private Evento evento;
	private Preferencia preferenciaSelected;
	private  List<Preferencia> listaPreferencia;
	private static Set<Preferencia_evento> listaPreferenciaEvento;
	private static Set<Area_evento> listaAreaEvento;
	private DaoPreferencia preferencia ;
	private DaoEvento daoevento;
	private DaoAreaEvento daoareaevento;
	private DaoAreaArrend daoareaarrend;
	private String categorialimpiar;
	private String preferencialimpiar;
	private static final String footerMessage5 = "Total de %d Preferencia(s)";
	private static final String footerMessage6 = "Total de %d Areas(s)";
	@Init
	public void init(@ExecutionArgParam("objetoEvento") Evento evento) {

		if (evento == null) {
			this.evento = new Evento();
			this.listaPreferenciaEvento=new HashSet<Preferencia_evento>();
			this.listaAreaEvento = new HashSet<Area_evento>();


		} else {
			this.evento = evento;
			listaPreferenciaEvento = evento.getPreferencia_evento();
			this.listaAreaEvento = evento.getArea_evento();

		}
		this.preferencia= new DaoPreferencia();
		this.daoevento = new DaoEvento();
		this.daoarea = new DaoArea();
		this.daoareaevento=new DaoAreaEvento();
		this.daoareaarrend =new DaoAreaArrend();
		this.listaAreas=new ArrayList<Area>();

	}

	@Command
	public void Cerrar(@BindingParam("win") Window win){
		win.detach();
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}


	public Categoria getCategoriaSelected() {
		return CategoriaSelected;
	}

	public void setCategoriaSelected(Categoria categoriaSelected) {
		CategoriaSelected = categoriaSelected;
	}

	public Area getAreaSelected() {
		return areaSelected;
	}


	public void setAreaSelected(Area areaSelected) {
		this.areaSelected = areaSelected;
	}


	public Preferencia getPreferenciaSelected() {
		return preferenciaSelected;
	}

	public void setPreferenciaSelected(Preferencia preferenciaSelected) {
		this.preferenciaSelected = preferenciaSelected;
	}



	public List<Preferencia> getListaPreferencia() {
		return listaPreferencia;
	}


	public void setListaPreferencia(List<Preferencia> listaPreferencia) {
		this.listaPreferencia = listaPreferencia;
	}


	public Set<Preferencia_evento> getListaPreferenciaEvento() {
		return listaPreferenciaEvento;
	}

	public void setListaPreferenciaEvento(
			Set<Preferencia_evento> listaPreferenciaEvento) {
		this.listaPreferenciaEvento = listaPreferenciaEvento;
	}


	public Set<Area_evento> getListaAreaEvento() {
		return listaAreaEvento;
	}


	public void setListaAreaEvento(Set<Area_evento> listaAreaEvento) {
		this.listaAreaEvento = listaAreaEvento;
	}

	public String getCategorialimpiar() {
		return categorialimpiar;
	}


	public void setCategorialimpiar(String categorialimpiar) {
		this.categorialimpiar = categorialimpiar;
	}

	public String getPreferencialimpiar() {
		return preferencialimpiar;
	}

	public void setPreferencialimpiar(String preferencialimpiar) {
		this.preferencialimpiar = preferencialimpiar;
	}


	public ArrayList<Area> getListaAreas() {
		return listaAreas;
	}


	public void setListaAreas(ArrayList<Area> listaAreas) {
		this.listaAreas = listaAreas;
	}

	@Command
	@NotifyChange({"listaPreferencia","preferencialimpiar"})
	public void activarBotonDeBusqueda() throws Exception {
		if(CategoriaSelected !=null){
			this.preferencialimpiar ="";
			this.listaPreferencia=this.preferencia.obtenerPreferenciaPorCategoria(CategoriaSelected);
		}else
		{


		}
	}

	@Command

	//Filtro Por Fechas Para buscar las Areas Disponibles
	@NotifyChange({"listaAreas"})
	public void activarBotonDeBusquedaPorFecha() throws Exception {
		Date fechaActual=new Date();
		if((evento.getFechaEvento().after(fechaActual)|| evento.getFechaEvento().getDate()==fechaActual.getDate())){
			if(evento.getFechaEvento() !=null){
				this.listaAreas.clear();
				for (Area area: this.daoarea.obtenerTodos()){
					if (isDisponible(area))
						this.listaAreas.add(area);
				}
			}
			if(listaAreas.size()==0)
			{

				Messagebox.show("Todas las areas se encuentran ocupadas en esta fecha, seleccione otra", 
						"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			}
		}
		else{
			Messagebox.show("¡Eliga una Fecha Mayor o Igual a La de Hoy!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);

		}
	}

	public boolean isDisponible(Area area) throws Exception {
		String estado="C";

		// verificamos que no se vaya usar esa area en un evento
		for (Area_evento areaEvento : daoareaevento.obtenerPorArea(area) ) {
			if((areaEvento.getEvento().getFechaEvento().compareTo(evento.getFechaEvento())==0) && areaEvento.getEvento().getEstadoEvento().compareTo(estado)==0){
				return true;	

			}else if((areaEvento.getEvento().getFechaEvento().compareTo(evento.getFechaEvento())==0)){
				return false;
			}

		}

		// verificamos que no se vaya usar esa area en un Arrendamiento
		for (Area_arrend areaArrend : daoareaarrend.obtenerPorArea(area)) {
			if((areaArrend.getArrendamiento().getFechaArrend().getTime())- (evento.getFechaEvento().getTime())==0  )
				return false;
		}
		return true;



	}

	@Command
	@NotifyChange({"listaPreferenciaEvento","footer5"})
	public void agregarPreferenciasEvento(){
		if(this.preferenciaSelected==null){
			Messagebox.show("Debe seleccionar una sub-categoria", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else if(buscarPreferencia(this.preferenciaSelected)==null){
			Preferencia_evento obj= new Preferencia_evento();
			obj.setEvento(evento);
			obj.setPreferencia(preferenciaSelected);
			this.listaPreferenciaEvento.add(obj);
		}else
		{

			Messagebox.show("La sub-categoria ya se encuentra registrada", 
					"Información", Messagebox.OK, Messagebox.EXCLAMATION);
		}
	}

	public Preferencia buscarPreferencia(Preferencia preferenciaSelected){
		for (Preferencia_evento preferenciaEvento: listaPreferenciaEvento) {
			if(preferenciaEvento.getPreferencia().getIdPreferencia()==preferenciaSelected.getIdPreferencia())
				return preferenciaSelected;
		}
		return null;
	}

	//Lista de Los Tipos de eventos
	public List<TipoEvento> getListaTipoEvento() {

		try {
			listaTipoEvento = daoTipoevento.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaTipoEvento;
	}

	//Lista de los Tipos de Publico
	public List<TipoPublico> getListaTipoPublico() {
		try {
			listaTipoPublico = daotipopublico.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaTipoPublico;
	}

	//Lista de todas las Categorias
	public List<Categoria> getCategorias() {
		try {
			categorias= daocategoria.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return categorias;
	}

	@Command
	@NotifyChange({"listaAreaEvento","footer6","desibitar"})
	public void agregarAreasEvento(){

		if(areaSelected==null){
			Messagebox.show("Debe seleccionar un area", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else if(buscarArea(this.areaSelected)==null){
			Area_evento obj1= new Area_evento();
			obj1.setEvento(evento);
			obj1.setArea(areaSelected);
			String estado="A";
			this.listaAreaEvento.add(obj1);
		}else
		{

			Messagebox.show("El Area ya se encuentra registrada", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
	}





	public Area buscarArea(Area areaSelected ){
		for (Area_evento areaEvento:listaAreaEvento) {
			if(areaEvento.getArea().getIdArea()==areaSelected.getIdArea())
				return areaSelected ;
		}
		return null;
	}


	@Command
	@NotifyChange({"listaPreferenciaEvento","footer5"})
	public void eliminarPreferenciaEvento(@BindingParam("preferencia_evento") Preferencia_evento preferencia) throws Exception{
		this.listaPreferenciaEvento.remove(preferencia);
		this.daopreferencia_evento.eliminarPreferencia_evento(preferencia);
	}

	@Command
	@NotifyChange({"listaAreaEvento","desibitar"})
	public void eliminarAreaEvento(@BindingParam("area_evento") Area_evento area) throws Exception{
		this.listaAreaEvento.remove(area);
		this.daoareaevento.eliminarArea_evento(area);
	}



	@Command
	@NotifyChange("*")
	public void RegistrarEvento(@BindingParam("win") Window win) throws Exception{	
		if (!CamposVacio())
		{
			String estado="R";
			evento.setEditingStatus(false);
			evento.setEstadoEvento(estado);
			evento.setPreferencia_evento(listaPreferenciaEvento);
			evento.setArea_evento(listaAreaEvento);
			this.daoevento.agregarEvento(evento);
			Messagebox.show("Se ha registrado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
			win.detach();
			BindUtils.postGlobalCommand(null, null, "RefrescarTablaEventos", null);


		}else
		{
			Messagebox.show("Debe LLenar Todos Los Campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}
	}

	@Command
	public void EditarEvento(@BindingParam("win") Window win) throws Exception{
		if (!CamposVacio()){
			String estado="R";
			evento.setEditingStatus(false);
			evento.setEstadoEvento(estado);
			evento.setPreferencia_evento(listaPreferenciaEvento);
			evento.setArea_evento(listaAreaEvento);
			this.daoevento.modificarEvento(evento);
			Messagebox.show("El evento se actualizo correctamente", 
					"Información", Messagebox.OK, Messagebox.INFORMATION);
			win.detach();


		}else
		{
			Messagebox.show("Debe LLenar Todos Los Campos", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}

	} 
	@Command
	@NotifyChange({"evento","listaPreferenciaEvento","listaAreaEvento","categorialimpiar","preferencialimpiar"})
	public void LimpiarEvento(){
		this.evento = new Evento();
		listaPreferenciaEvento.clear();
		listaAreaEvento.clear();
		this.categorialimpiar = "";
		this.preferencialimpiar = "";

	}


	public boolean CamposVacio(){
		if(evento.getNombreEvento() != null && !evento.getNombreEvento().equalsIgnoreCase("") && 
				evento.getDescEvento() != null && !evento.getDescEvento().equalsIgnoreCase("") &&
				this.listaPreferenciaEvento.size()>0 && this.listaAreaEvento.size()>0)
			return false;
		return true;
	}


	public String getFooter5() {
		return String.format(footerMessage5, this.listaPreferenciaEvento.size());
	}

	public String getFooter6() {
		return String.format(footerMessage6, this.listaAreaEvento.size());
	}

	public boolean isDesibitar(){
		return this.listaAreaEvento.size()>0;
	}

}
