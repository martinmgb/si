package vistaModelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Window;

import modelo.entidad.Evento;
import modelo.hibernate.dao.DaoEvento;

public class EventoCanceladoViewModel {
	private static final String footerMessage = "Total de %d Eventos(s) ";
	private boolean isEdit,isCreate,isView,isVer  = false;
	private  EventoFilter filterEvento;
	private static  List<Evento>  eventos;
	private DaoEvento dao ;


	@Init
	public void init() throws Exception {

		this.eventos= new ArrayList<Evento>();
		filterEvento = new  EventoFilter();
		this.dao= new DaoEvento();
		eventos=this.dao.obtenerEventoParaCancelar();


	}


	public List<Evento> getEventos() {
		return eventos;
	}





	public void setEventos(List<Evento> eventos1) {
		this.eventos = eventos;
	}


	public EventoFilter getFilterEvento() {
		return filterEvento;
	}

	public void setFilterEvento(EventoFilter filterEvento) {
		this.filterEvento = filterEvento;
	}

	public List<Evento> getFilterTipos(EventoFilter tipoFilter) {

		List<Evento> someTipos = new ArrayList<Evento>();
		String tipo = tipoFilter.getTipo().toLowerCase();
		String nombre = tipoFilter.getNombre().toLowerCase();
		List<Evento> tiposAll = null;
		try {
			tiposAll = 	eventos=this.dao.obtenerEventoParaCancelar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Evento> i = tiposAll.iterator(); i.hasNext();) {
			Evento tmp = i.next();
			if (tmp.getTipoEvento().getNombreTipoEvento().toLowerCase().contains(tipo) && tmp.getNombreEvento().toLowerCase().contains(nombre)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}

	@Command
	@NotifyChange({"eventos", "footer"})
	public void changeFilter() {
		eventos = getFilterTipos(filterEvento);
	}

	public EventoFilter gettipoFilter() {
		return filterEvento;
	}


	public String getFooter() {
		return String.format(footerMessage, eventos.size());
	}

	@Command
	public void verEvento(@BindingParam("verEvento") Evento evento) {
		HashMap<String, Serializable> MapEvento = new HashMap<String, Serializable>();
		MapEvento.put("objetoEvento", evento);
		this.isEdit = false;
		this.isCreate = false;
		this.isView =true;
		MapEvento.put("edit", this.isEdit);
		MapEvento.put("create", this.isCreate);
		MapEvento.put("ver", this.isView);


		Window window = (Window) Executions.createComponents(
				"content/modalVerDetalleDeEvento.zul", null, MapEvento);
		window.doModal();
	}


	@GlobalCommand
	@NotifyChange({"eventos", "footer"})
	public void RefrescarTablaEventos() throws Exception {
		eventos=this.dao.obtenerEventoParaCancelar();
	}
}
