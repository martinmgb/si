package vistaModelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

 
//PERSONA
import modelo.entidad.Visita;
import modelo.entidad.Visita_acomp;
import modelo.hibernate.dao.DaoVisita;
import modelo.hibernate.dao.DaoVisita_Acomp;
import modelo.entidad.Evento;
import modelo.entidad.Persona;
import modelo.entidad.Sancion;
import modelo.entidad.TipoPersona;
 
 
public class VisitaViewModel extends GenericForwardComposer<Window> {
   	private static final String footerMessage = "Total de %d Miembros ";
    
    	private DaoVisita dao = new DaoVisita();
    	private DaoVisita_Acomp daoVisiAcom = new DaoVisita_Acomp();
//    	private List<Accion> todosTipos;
    	private List<Visita> todosTipos; // Para obtner las personas q se le asignaran acciones
    	private boolean displayEdit = true;
    	private static final long serialVersionUID = 1L;
    	private boolean isEdit,isCreate,isView = false;
    	private VisitaFilter visitaFilter = new VisitaFilter();
    	
    	
    	private Visita visita;
    	private Visita_acomp visita_acomp;
    	
    
    	@Wire
    	Window nroAccion;
    	
    	@Wire
    	private Datebox fechaVisita;
    	@Wire
    	Window modalDialog;
    	@Wire
    	public Label message;
    	@Wire
    	public Div box;
    	private Div contenido;

    	private Div contenedor;

    	private String pagina;

    	public boolean isDisplayEdit() {
    		return displayEdit;
    	}

    	public VisitaViewModel() {
    		super();
    		this.visita = new Visita();
    		this.visita_acomp = new Visita_acomp();
    		try {
    			todosTipos = dao.obtenerTodos();
    			
    			
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}

    	 

    	public Visita getVisita() {
			return visita;
		}

		public void setVisita(Visita visita) {
			this.visita = visita;
		}

		public List<Visita> getObtenerTipos() {
    		return todosTipos;
    	}

    	public VisitaFilter getVisitaFilter() {
    		return visitaFilter;
    	}

    	public String getFooter() {
    		return String.format(footerMessage, todosTipos.size());
    	}

    	@Command
    	@NotifyChange({"obtenerTipos", "footer"})
    	public void changeFilter() {
    		todosTipos = getFilterTiposVisita(visitaFilter);
    	}


    	public List<Visita> getFilterTiposVisita(VisitaFilter tipoFilter) {

    		List<Visita> someTipos = new ArrayList<Visita>();
    		String ced = tipoFilter.getCedula().toLowerCase();
    		 List<Visita> tiposAll = null;
    		try {
    			tiposAll = dao.obtenerTodos();
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} 

    		for (Iterator<Visita> i = tiposAll.iterator(); i.hasNext();) {
    			Visita tmp = i.next();
    			if (tmp.getPersona().getCedPersona().toLowerCase().contains(ced)  ) {
    				someTipos.add(tmp);
    			}
    		}
    		return someTipos;
    	}

    	
    	
 //Registra la Visita
 @Command
 public void  RegistrarVisita(@BindingParam("Persona") Persona persona,@BindingParam("win") Window win) throws Exception {
	 if (!CamposVacio()){
    		//	Persona persona = (Persona) arg.get("objetoIncidencia");
    		    String estado="A";
        		   System.out.println("id de la persona :"+persona.getIdPersona()+
        			     	"fecha de la visita: "+	visita.getFechaVisita() +
        			     	"hora de la visita: "+	visita.getHoraVisita() );
        	    visita.setEditingStatus(false);
    			visita.setEstadoVisita(estado);
    			visita.setPersona(persona);
    			visita.setFechaVisita(new Date());
    		 	// seteo en la tabla
    			this.dao.agregarVisita(visita);
    			// this.daoVisiAcom.agregarVisita_acomp(visita_acomp);
    			
    			 Messagebox.show("¿Desea agregar una Acompañante?", 
 						"Intorragativo", Messagebox.YES | Messagebox.NO,
 						Messagebox.QUESTION,
 						new org.zkoss.zk.ui.event.EventListener(){
 				
 					public void onEvent(Event e){
 						if(Messagebox.ON_YES.equals(e.getName())){
 							HashMap<String, Visita> visit = new HashMap<String, Visita>();
 							visit.put("objVisita", visita);
 
 							Window window = (Window)Executions.createComponents(
 									"content/modalVisitaNo.zul", null, visit);
 							window.doModal();
 							win.detach();
 						}
 						
 						else if(Messagebox.ON_NO.equals(e.getName())){
 							System.out.print("hola");
 							System.out.print("hola");
 						}

 					}
 
 				}
 						);
    			 pagina = "vista/registrarVisita.zul";
    			 Messagebox.show("Se ha registrado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
    					 
    			 win.detach();
	 
	 
	 }else
		{
			Messagebox.show("Debe llenar todos los campos", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}
    			 

  
 } 
    
	// VALIDA LOS CAMPOS VACIOS
	public boolean CamposVacio(){
		if(visita.getHoraVisita()!=null   ) 
			return false;
		return true;
	}

	//Metodo creado por Michael para Limpiar los Campos Lyn del Boton Limpiar
	@Command
	@NotifyChange({"visita" })
	public void LimpiarSancion(){

		this.visita= new Visita();
	 
	}
 
 
 
 
 
 
 
 
 
    			// MODIFICO EL ESTADO DE PERSONA
    			
    			
//    			@Command   
//    			public void  ModalCancelarEvento(@BindingParam("Evento") Evento evento,@BindingParam("win") Window win) throws Exception {
//    				
//    			}
    			
    			
    			

//    	public void onClick$btnLimpiar() {
//    		nroAccion.setText("");
//    		fechaIngreso.setText("");
//    		nombre.setFocus(true);
//    	}
//    	public void onClick$btnEditar() {
//
//    		System.out.println(id.getValue());
//    		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
//    			box.setVisible(true);
//    			message.setValue("Debe llenar todos los campos");
//    		}
//    		else {
//    			try {
//    				Accion objeto = dao.obtenerAccion(id.getValue());
//    				objeto.setNombreAccion(nombre.getValue());
//    				objeto.setDescAccion(descripcion.getValue());
//    				dao.modificarAccion(objeto);
//    				modalDialog.detach();
//    				pagina = "vista/parentesco.zul";
//    				contenido.getChildren().clear();
//    				contenedor = (Div) Executions.createComponents(pagina, null, null);
//    				contenido.appendChild(contenedor);
//    			}
//    			catch (Exception x) {
//    				// TODO Auto-generated catch block
//    				x.printStackTrace();
//    			}
//    		}
//    	}

//    	@Command
//    	public void changeEditable(@BindingParam("editarAccion") Accion tip) {
//    		HashMap accion = new HashMap();
//    		accion.put("objAccion", tip);
//    		this.isEdit = true;
//    		this.isCreate = false;
//    		accion.put("edit", this.isEdit);
//    		accion.put("create", this.isCreate);
//    		Window window = (Window) Executions.createComponents(
//    				"content/modalParentesco.zul", null, accion);
//    		window.doModal();
//    	}

    	@NotifyChange({"obtenerTipos", "displayEdit"})
    	public void setDisplayEdit(boolean displayEdit) {
    		this.displayEdit = displayEdit;
    	}

    	@Command
    	@NotifyChange({"obtenerTipos", "footer"})
    	public void changeEditableStatus(@BindingParam("visita") Visita tipo) {
    		Messagebox.show("¿Esta seguro de eliminar el registro?", 
    				"Interrogativo", Messagebox.OK | Messagebox.CANCEL,
    				Messagebox.QUESTION,
    				new org.zkoss.zk.ui.event.EventListener(){
    			public void onEvent(Event e){
    				if(Messagebox.ON_OK.equals(e.getName())){
    					//OK is clicked
    					try {
    						todosTipos.remove(tipo);
    						dao.eliminarVisita(tipo);


    					} catch (Exception x) {
    						// TODO Auto-generated catch block
    						x.printStackTrace();
    					}
    					refreshRowTemplate(tipo);
    				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
    					//Cancel is clicked
    				}
    			}
    		}
    				);

    	}
    	

    	@Command
    	@NotifyChange({"obtenerTipos", "footer"})
    	public void confirm(@BindingParam("visita") Visita tipo) {
    		changeEditableStatus(tipo);
    		try {
    			todosTipos.remove(tipo);
    			dao.eliminarVisita(tipo);
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		refreshRowTemplate(tipo);
    	}

    	public void refreshRowTemplate(Visita tipo) {
    		/*
    		 * This code is special and notifies ZK that the bean's value
    		 * has changed as it is used in the template mechanism.
    		 * This stops the entire Grid's data from being refreshed
    		 */
    		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
    	}
   // PARA ABRIR MODAL ASIGNAR ACCION
//			  	  @Command
//			  		public void changeEditableAsignarAccion(@BindingParam("AbrirModalAsignarAccion") Accion acciones) {
//			  			HashMap registrarAccion = new HashMap();
//			  			System.out.println("wwwwwwwwwwwwwwwwwwe:" + acciones.getPersonai().getCedPersona()  )  ;
//			  			registrarAccion.put("objAccion", acciones);
//			  			
//			  			Window window = (Window)Executions.createComponents(
//			  					"content/modalAsignarAccion.zul", null, registrarAccion);
//			  			window.doModal();
//			  		} 
//   	  }


   // ABRE MODAL DE REGISTRAR ACOMPANANTE	
  	 
  
  	  
  	  
  	  @Command
	 	public void abrirSi(@BindingParam("abrirsSi") Persona visitas) {
	 			HashMap visit = new HashMap();
	    		 visit.put("objVisita", visitas);
	    		 Window window = (Window)Executions.createComponents(
	    		 "content/modalVisitaSi.zul", null, visit);
	    		 window.doModal();
	    		}  
 

 
}
