package vistaModelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import modelo.entidad.Area;
import modelo.entidad.Evento;
import modelo.entidad.Funcion;
import modelo.entidad.Funcion_grupo;
import modelo.entidad.Grupo;
import modelo.entidad.Preferencia;
import modelo.entidad.Preferencia_evento;
import modelo.hibernate.dao.DaoFuncion;
import modelo.hibernate.dao.DaoFuncion_grupo;
import modelo.hibernate.dao.DaoGrupo;


public class RegistrarGrupoViewModel {
	private Grupo grupo;
	private List<Funcion> listaFunciones;
	private DaoFuncion daoFuncion;
	private Funcion FuncionSelect; 
	private static Set<Funcion_grupo> listaFuncionesGrupo;
	private static final String footerMessage = "Total de %d Funciones(s)";
	private DaoGrupo daoGrupo;
	private DaoFuncion_grupo daoFunciones_grupo;
	private String nombreGrupoLimpiar;
	private String descripcionGrupoLimpiar;

	public Set<Funcion_grupo> getListaFuncionesGrupo() {
		return listaFuncionesGrupo;
	}

	public void setListaFuncionesGrupo(Set<Funcion_grupo> listaFuncionesGrupo) {
		this.listaFuncionesGrupo = listaFuncionesGrupo;
	}

	public String getNombrelimpiar() {
		return nombreGrupoLimpiar;
	}


	public void setNombreLimpiar(String nombreGrupoLimpiar) {
		this.nombreGrupoLimpiar = nombreGrupoLimpiar;
	}

	public String getDescripcionGrupoLimpiar() {
		return descripcionGrupoLimpiar;
	}

	public void setDescripcionLimpiar(String descripcionGrupoLimpiar) {
		this.descripcionGrupoLimpiar = descripcionGrupoLimpiar;
	}

	@Init
	public void init(@ExecutionArgParam("objGrupo") Grupo grupoAp) throws Exception {



		if (grupoAp == null) {
			this.grupo = new Grupo();
			this.listaFuncionesGrupo =new HashSet<Funcion_grupo>();


		} else {
			this.grupo = grupoAp;
			listaFuncionesGrupo= grupo.getFuncion_grupo();


		}
		this.listaFunciones = new ArrayList<Funcion>();
		this.daoFuncion = new DaoFuncion();
		this.FiltrarFunciones();
		this.FuncionSelect = new Funcion();
		this.daoGrupo = new DaoGrupo();
		this.daoFunciones_grupo = new DaoFuncion_grupo();

	}

	public void  FiltrarFunciones() throws Exception {
		for (Funcion funcion : daoFuncion.obtenerTodos()) {
			if(funcion.getExtraFuncion().equals("Funcion"))
				this.listaFunciones.add(funcion);
		}	

	}

	@Command
	@NotifyChange({"listaFuncionesGrupo","footer"})
	public void agregarFuncionesGrupo(){
		if(this.FuncionSelect==null){
			Messagebox.show("Debe Seleccionar Una Funcion!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else if(buscarFuncion(this.FuncionSelect)==null){
			Funcion_grupo obj= new Funcion_grupo();
			obj.setGrupo(grupo);
			obj.setFuncion(FuncionSelect);
			this.listaFuncionesGrupo.add(obj);
		}else
		{

			Messagebox.show("La Funcion Ya Se Encuentra Registrada!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
	}

	public Funcion buscarFuncion(Funcion FuncionSelect){
		for (Funcion_grupo funcionGrupo: listaFuncionesGrupo) {
			if(funcionGrupo.getFuncion().getIdFuncion()== FuncionSelect.getIdFuncion())
				return FuncionSelect;
		}
		return null;
	}

	@Command
	public void Cerrar(@BindingParam("win") Window win){
		win.detach();
	}


	@Command
	@NotifyChange({"listaFuncionesGrupo","footer"})
	public void eliminarFuncionesGrupo(@BindingParam("FuncionesGrupo") Funcion_grupo funcion ) throws Exception{
		this.listaFuncionesGrupo.remove(funcion);
		this.daoFunciones_grupo.eliminarFuncion_grupo(funcion);
	}

	@Command
	@NotifyChange("*")
	public void RegistrarGrupo(@BindingParam("win") Window win) throws Exception{	
		if (!CamposVacio())
		{
			String estado="R";
			grupo.setEditingStatus(false);
			grupo.setEstadoGrupo(estado);
			grupo.setFuncion_grupo(listaFuncionesGrupo);
			this.daoGrupo.agregarGrupo(grupo);
			Messagebox.show("El Grupo se Registro Con Exito!", 
					"Información", Messagebox.OK, Messagebox.INFORMATION);
			win.detach();
			BindUtils.postGlobalCommand(null, null, "RefrescarTablaGrupos", null);


		}else
		{
			Messagebox.show("Debe LLenar Todos Los Campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}
	}


	@Command
	public void EditarGrupo(@BindingParam("win") Window win) throws Exception{
		if (!CamposVacio()){
			String estado="R";
			grupo.setEditingStatus(false);
			grupo.setEstadoGrupo(estado);
			grupo.setFuncion_grupo(listaFuncionesGrupo);
			this.daoGrupo.modificarGrupo(grupo);
			Messagebox.show("El Grupo Se Actualizo Correctamente!", 
					"Información", Messagebox.OK, Messagebox.INFORMATION);
			win.detach();


		}else
		{
			Messagebox.show("Debe LLenar Todos Los Campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}

	}

	@Command
	@NotifyChange({"grupo","listaFuncionesGrupo","nombreGrupoLimpiar","descripcionGrupoLimpiar"})
	public void LimpiarGrupo(){
		this.grupo = new Grupo();
		listaFuncionesGrupo.clear();
		this.nombreGrupoLimpiar = "";
		this.descripcionGrupoLimpiar = "";
	}

	public boolean CamposVacio(){
		if(grupo.getNombreGrupo() != null && !grupo.getNombreGrupo().equalsIgnoreCase("") && 
				grupo.getDescGrupo() != null && !grupo.getDescGrupo().equalsIgnoreCase("") &&
				this.listaFuncionesGrupo.size()>0)
			return false;
		return true;
	}


	public Funcion getFuncionSelect() {
		return FuncionSelect;
	}

	public void setFuncionSelect(Funcion funcionSelect) {
		FuncionSelect = funcionSelect;
	}

	public List<Funcion> getListaFunciones() {
		return listaFunciones;
	}

	public void setListaFunciones(List<Funcion> listaFunciones) {
		this.listaFunciones = listaFunciones;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public String getFooter() {
		return String.format(footerMessage, this.listaFuncionesGrupo.size());
	}
}