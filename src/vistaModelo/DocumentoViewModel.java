package vistaModelo;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;

public class DocumentoViewModel extends GenericForwardComposer<Window>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Command
	public void abrirModalRechazo(){
		Window window = (Window)Executions.createComponents(
				"content/modalRechazo.zul", null,null);
		window.doModal();
	}
	
}
