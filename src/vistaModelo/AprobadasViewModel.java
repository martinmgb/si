package vistaModelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import modelo.entidad.Area;
import modelo.entidad.Area_arrend;
import modelo.entidad.Area_evento;
import modelo.entidad.Arrendamiento;
import modelo.entidad.Evento;
import modelo.entidad.Preferencia;
import modelo.entidad.Rechazo;
import modelo.entidad.TipoRechazo;
import modelo.hibernate.dao.DaoArrendamiento;
import modelo.hibernate.dao.DaoRechazo;
import modelo.hibernate.dao.DaoTipoRechazo;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class AprobadasViewModel extends GenericForwardComposer<Window> {

	private static final long serialVersionUID = 1L;
	private static  String footerMessage = "En Total %d Solicitudes de Arrendamiento";
	List<Arrendamiento> solicitudesArrendamiento;
	private DaoArrendamiento daoArr = new DaoArrendamiento();
	private ArrendamientoFilter FilterSolicitudes;



	@Init
	public void init()throws Exception{
	
		this.FilterSolicitudes = new ArrendamientoFilter();
		String estadoPendiente = "P";
		solicitudesArrendamiento = daoArr.obtenerArrendamientosPorEstado1(estadoPendiente);
	}



	public ArrendamientoFilter getFilterSolicitudes() {
		return FilterSolicitudes;
	}

	public void setFilterSolicitudes(ArrendamientoFilter filterSolicitudes) {
		FilterSolicitudes = filterSolicitudes;
	}
	public List<Arrendamiento> getSolicitudesArrendamiento() {
		return solicitudesArrendamiento;
	}



	public void setSolicitudesArrendamiento(
			List<Arrendamiento> solicitudesArrendamiento) {
		this.solicitudesArrendamiento = solicitudesArrendamiento;
	}


	public String getFooter() {
		return String.format(footerMessage, solicitudesArrendamiento.size());
	}

	@Command
	public void verCampos(@BindingParam("objetoArrendamiento")Arrendamiento objeto){
		HashMap objetoMandarModal = new HashMap();
		objetoMandarModal.put("objeto", objeto);
		Window window = (Window)Executions.createComponents(
				"content/modalEvaluarArrendamiento.zul", null, objetoMandarModal);
		window.doModal();
	}

	
	//Este es el Metodo de la Grid de Solicitudes de Arrendamiento
	@Command
	@NotifyChange({"solicitudesArrendamiento", "footer"})
	public void changeFilter() throws Exception {
		solicitudesArrendamiento = getFilterArrendamiento(FilterSolicitudes);

	}


	public List<Arrendamiento> getFilterArrendamiento(ArrendamientoFilter tipoFilter) {

		List<Arrendamiento> someTipos = new ArrayList<Arrendamiento>();
		String cedula = tipoFilter.getCedCliente().toLowerCase();
		String nombre = tipoFilter.getNombCliente().toLowerCase();
		String apellido = tipoFilter.getApellido().toLowerCase();
		List<Arrendamiento> tiposAll = null;
		try {
			tiposAll =  daoArr.obtenerArrendamientosPorEstado1("P");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Arrendamiento> i = tiposAll.iterator(); i.hasNext();) {
			Arrendamiento tmp = i.next();
			if (tmp.getPersona().getNombrePersona().toLowerCase().contains(nombre) && tmp.getPersona().getCedPersona().toLowerCase().contains(cedula) && tmp.getPersona().getApellidoPersona().toLowerCase().contains(apellido)){
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
	@GlobalCommand
	@NotifyChange({"solicitudesArrendamiento", "footer"})
	public void RefrescarTablaSolicitudesArrendamientos() throws Exception {
		solicitudesArrendamiento=daoArr.obtenerArrendamientosPorEstado1("P");
	}


}
