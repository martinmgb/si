package vistaModelo;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Window;

public class ReporteArrendamientoMensualEstaViewModel {

	private Date fechaDesde;
	private Date fechaHasta;

	private AMedia archivoJasper;
	private Connection conexion;
	private String rutabase;
	private String rutaPdf;
	String pathPdfProyecto;
	private String pathProyecto;
	private String pathSubreport;
	Map<String,Object> parameterMap = new HashMap<String,Object>();
	String patch = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");

	@Init
	public void init(){

		setFechaDesde(new Date());
		setFechaHasta(new Date());
rutabase = new String();
//////////////////////////////////////////////////////////IMPRIMIR//////////////////////////////////////////////////////////////////
File file= new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathProyecto= file.getParentFile().getParentFile().getPath();
///////////////////////////////////////////////////////////////////////////////
int i,f;
f=this.pathProyecto.length();
i=f-7;
String z=this.pathProyecto.substring(i, f);
this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
if(z.compareTo("classes")==0){
this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().getPath();
this.rutabase=file.getParentFile().getParentFile().getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
}
System.out.print("Z: "+z+" F: "+"classes");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
String ruta = this.pathProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"estadisticos"+ System.getProperty("file.separator") + "arrendamientosMensuales.jrxml";
System.out.print("RUTA: "+ruta);
File filePdf = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathPdfProyecto = this.pathProyecto;
rutaPdf = this.pathPdfProyecto + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"estadisticos"+ System.getProperty("file.separator") + "arrendamientosMensuales.pdf";

this.pathProyecto= ruta.replaceAll("%20", " ");

	}
	

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public Map<String, Object> getParameterMap() {
		return parameterMap;
	}

	public void setParameterMap(Map<String, Object> parameterMap) {
		this.parameterMap = parameterMap;
	}

	public void generarEstadistico() throws SQLException, JRException, ParseException  {
		generarReporte();
	}

	public void generarReporte() throws SQLException, JRException, ParseException {
		try {
			Class.forName("org.postgresql.Driver");
			conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		archivoJasper = null;
		parameterMap.put("FechaIni",this.getFechaDesde());
		parameterMap.put("FechaFin",this.getFechaHasta());
		parameterMap.put("dirbase",rutabase);
		JasperReport jasperReport = JasperCompileManager.compileReport(this.pathProyecto);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);
		if (jasperPrint.getPages().isEmpty()) {
			Messagebox.show("No se han encontrado datos para generar el reporte", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}else {
		
			archivoJasper = new AMedia(rutaPdf, "pdf" ,"application/pdf", JasperExportManager.exportReportToPdf(jasperPrint));
		}
	}

	@Command
	public void abrirModal(@ContextParam(ContextType.VIEW) Component view) throws SQLException, ParseException, JRException {
		parameterMap.put("ruta", patch);		
		Map<String, Object> args = new HashMap<String, Object>();
		generarEstadistico();
		if (archivoJasper != null) {
			args.put("reporte", archivoJasper);
			final Window component = (Window) Executions.getCurrent().createComponents("content/modalReporteArrendamientoMensualEsta.zul", view, args);
			component.setAction("show: slideDown;hide: slideUp");
			component.doModal();
		}

	}


}
