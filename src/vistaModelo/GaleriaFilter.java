package vistaModelo;

public class GaleriaFilter{

	private String id="",nombreG="", imagenG="";

	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id==null?"":id.trim();
	}

	public String getNombreG() {
		return nombreG;
	}

	public void setNombreG(String nombreG) {
		this.nombreG = nombreG==null?"":nombreG.trim();
	}

	public String getImagenG(){
		return imagenG;
	}

	public void setImagenG(String imagenG) {
		this.imagenG = imagenG==null?"":imagenG.trim();
	}
}
