package vistaModelo;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modelo.entidad.Categoria;
import modelo.hibernate.dao.DaoCategoria;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;

public class ReporteEstaditicoPreEvenViewModel  extends GenericForwardComposer {


	private List<Categoria> categorias;
	private Categoria categoria;
	private DaoCategoria daoCategoria;

	private AMedia archivoJasper;
	private Connection conexion;
	private String rutabase;
	private String rutaPdf;
	private String pathSubreport;
	String pathPdfProyecto;
	private String pathProyecto = new String();
	File root ;
	Map<String,Object> parameterMap = new HashMap<String,Object>();
	String patch = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");

	@Init
	public void init(){
		categoria= new Categoria();
		daoCategoria = new DaoCategoria();
		categorias = new ArrayList<Categoria>();

		try {
			categorias.addAll(daoCategoria.obtenerTodos());
		} catch (Exception e) {
			// TODO: handle exception
		}
//////////////////////////////////////////////////////////IMPRIMIR//////////////////////////////////////////////////////////////////
File file= new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathProyecto= file.getParentFile().getParentFile().getPath();
///////////////////////////////////////////////////////////////////////////////
int i,f;
f=this.pathProyecto.length();
i=f-7;
String z=this.pathProyecto.substring(i, f);
this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
if(z.compareTo("classes")==0){
this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().getPath();
this.rutabase=file.getParentFile().getParentFile().getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
}
System.out.print("Z: "+z+" F: "+"classes");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
String ruta = this.pathProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"estadisticos"+ System.getProperty("file.separator") + "reportprefe.jrxml";
System.out.print("RUTA: "+ruta);
File filePdf = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathPdfProyecto = this.pathProyecto;
rutaPdf = this.pathPdfProyecto + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"estadisticos"+ System.getProperty("file.separator") + "reportCategoriaEventoEstadistico.pdf";

//Sustitucion de caracteres codificados (Para servidores con Windows)
this.pathProyecto= ruta.replaceAll("%20", " ");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		try{
			//		FileInputStream input = new FileInputStream(System.getProperty("user.dir") );
			//		System.out.print(input+"reportes/evento09.jasper");
//			root = new File("/workspace/canario0903/WebContent/appWeb/"); // Creamos un objeto file
//			System.out.println(root.getAbsolutePath());
		}catch(Exception e){

			e.printStackTrace();

		}

	}

	public List<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Map<String, Object> getParameterMap() {
		return parameterMap;
	}

	public void setParameterMap(Map<String, Object> parameterMap) {
		this.parameterMap = parameterMap;
	}

	public void generarEstadistico() throws SQLException, JRException, ParseException  {
		generarReporte();
	}

	public void generarReporte() throws SQLException, JRException, ParseException {

		try {
			Class.forName("org.postgresql.Driver");
			conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		archivoJasper = null;
		parameterMap.put("categoria",this.getCategoria().getNombreCategoria());
		parameterMap.put("dirbase",rutabase);

		//JasperReport reporte = (JasperReport) JRLoader.loadObjectFromFile(this.pathProyecto);
		JasperReport jasperReport = JasperCompileManager.compileReport(this.pathProyecto);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);
		if (jasperPrint.getPages().isEmpty()) {
			Messagebox.show("No se han encontrado datos para generar el reporte", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		} else {
			archivoJasper = new AMedia(rutaPdf, "pdf" ,"application/pdf", JasperExportManager.exportReportToPdf(jasperPrint));

		}
	}


	@Command
	public void abrirModal(@ContextParam(ContextType.VIEW) Component view) throws SQLException, ParseException, JRException {
		parameterMap.put("ruta", patch);		
		Map<String, Object> args = new HashMap<String, Object>();
		generarEstadistico();
		if (archivoJasper != null) {
			args.put("reporte", archivoJasper);
			final Window component = (Window) Executions.getCurrent().createComponents("content/modalReporteCategoriaEventoEstadistico.zul", view, args);
			component.setAction("show: slideDown;hide: slideUp");
			component.doModal();
		}

	}


}
