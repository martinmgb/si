package vistaModelo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modelo.entidad.Categoria;
import modelo.entidad.Preferencia;
import modelo.entidad.TipoIncidencia;
import modelo.entidad.TipoPersona;
import modelo.entidad.TipoSancion;
import modelo.hibernate.dao.DaoCategoria;
import modelo.hibernate.dao.DaoPreferencia;
import modelo.hibernate.dao.DaoTipoIncidencia;
import modelo.hibernate.dao.DaoTipoPersona;
import modelo.hibernate.dao.DaoTipoSancion;






import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
//import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleTextExporterConfiguration;
import net.sf.jasperreports.export.WriterExporterOutput;
import net.sf.jasperreports.view.JasperViewer;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Window;

import com.itextpdf.text.pdf.codec.Base64.OutputStream;

//import com.sun.xml.internal.ws.api.Component;



public class ReporteMiembroViewModel  extends GenericForwardComposer{
	public class Genero{
		private String idGenero;
		private String nombreGenero;
		public Genero() {
			super();
			// TODO Auto-generated constructor stub
		}

		public String getIdGenero() {
			return idGenero;
		}
		public void setIdGenero(String idGenero) {
			this.idGenero = idGenero;
		}
		public String getNombreGenero() {
			return nombreGenero;
		}
		public void setNombreGenero(String nombreGenero) {
			this.nombreGenero = nombreGenero;
		}


	}
	public class Estado{
		private String idEstado;
		private String nombreEstado;
		public Estado() {
			super();
			// TODO Auto-generated constructor stub
		}
		public String getIdEstado() {
			return idEstado;
		}
		public void setIdEstado(String idEstado) {
			this.idEstado = idEstado;
		}
		public String getNombreEstado() {
			return nombreEstado;
		}
		public void setNombreEstado(String nombreEstado) {
			this.nombreEstado = nombreEstado;
		}




	}

	private Connection conexion;

	private List<TipoPersona> tipoPersonas;
	private List<TipoSancion> tipoSancions;
	private List<TipoIncidencia> tipoIncidencias;
	private List<Categoria> categorias;
	private List<Preferencia> preferencias;
	private List<Genero> generos; 
	private List<Estado> estados;

	private TipoPersona tipoPersona;
	private TipoSancion tipoSancion;
	private Categoria categoria;
	private Preferencia preferencia;
	private Genero genero = new Genero();
	private Estado estado = new Estado();

	private TipoIncidencia tipoIncidencia;
	private DaoCategoria daoCategoria;
	private DaoPreferencia daoPreferencia;
	private DaoTipoPersona daoTipoPersona;
	private DaoTipoSancion daoTipoSancion;
	private DaoTipoIncidencia daoTipoIncidencia;
	private Date fechaDesde;
	private String pathProyecto;
	private String ruta2;
	private String pathTxtProyecto;
	private AMedia archivoJasper;
private List<TipoPersona> auxTipoPersona;
	
	private String rutatxt;
	private String rutaPdf;

	private String rutabase;

	String pathPdfProyecto;
	String patchZulProyecto;
	String patchNoEstructurado;

	String rutaZul;
	String patch = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");

	Map<String,Object> parameterMap = new HashMap<String,Object>();


	private Date fechaHasta;
	private String sexoCombo ;

	@Wire
	private Combobox  comboSexo;



	public String getSexoCombo() {
		return sexoCombo;
	}

	public void setSexoCombo(String sexoCombo) {
		this.sexoCombo = sexoCombo;
	}

	@Init
	public void init() throws SQLException, JRException{
		tipoIncidencia = new TipoIncidencia();
		tipoPersona =new TipoPersona();
		tipoSancion = new TipoSancion();
		categoria= new Categoria();
		preferencia = new Preferencia();
		daoTipoPersona = new DaoTipoPersona();
		daoTipoSancion = new DaoTipoSancion();
		daoPreferencia = new DaoPreferencia();
		daoCategoria = new DaoCategoria();
		daoTipoIncidencia = new DaoTipoIncidencia();
		tipoPersonas = new ArrayList<TipoPersona>();
		categorias = new ArrayList<Categoria>();
		preferencias = new ArrayList<Preferencia>();
		tipoSancions = new ArrayList<TipoSancion>();
		tipoIncidencias= new ArrayList<TipoIncidencia>();
		fechaDesde= new Date();
		fechaHasta= new Date();
		auxTipoPersona = new ArrayList<TipoPersona>();
		try {
			//TipoPersona tp = new TipoPersona();
			tipoPersonas.addAll(daoTipoPersona.obtenerTodos());
			tipoPersona.setNombreTipoPersona("Todos");
			tipoPersona.setIdTipoPersona(0);
			auxTipoPersona.add(tipoPersona);
			for (TipoPersona tipoPersona : this.getTipoPersonas()) {
				if (tipoPersona.getNombreTipoPersona().equals("Miembro Propietario")|| tipoPersona.getNombreTipoPersona().equals("Miembro Honorario") || tipoPersona.getNombreTipoPersona().equals("Miembro Beneficiario")) {
					auxTipoPersona.add(tipoPersona);
				}
				
			}

			tipoSancion.setNombreTipoSancion("Todos");
			tipoSancion.setIdTipoSancion(0);
			tipoSancions.add(tipoSancion);
			tipoSancions.addAll(daoTipoSancion.obtenerTodos());

			tipoIncidencia.setNombreTipoIncid("Todos");
			tipoIncidencia.setIdTipoIncid(0);
			tipoIncidencias.add(tipoIncidencia);
			tipoIncidencias.addAll(daoTipoIncidencia.obtenerTodos());

			categoria.setNombreCategoria("Todos");
			categoria.setIdCategoria(0);
			categorias.add(categoria);
			categorias.addAll(daoCategoria.obtenerTodos());

			preferencia.setNombrePreferencia("Todos");
			preferencia.setIdPreferencia(0);
			preferencias.add(preferencia);
			preferencias.addAll(daoPreferencia.obtenerTodos());

			generos = new ArrayList<Genero>();


			genero.setIdGenero("Todos");
			genero.setNombreGenero("Todos");
			generos.add(genero);
			Genero m = new Genero();
			m.setIdGenero("M");
			m.setNombreGenero("Masculino");
			generos.add(m);
			Genero f = new Genero();
			f.setIdGenero("F");
			f.setNombreGenero("Femenino");
			generos.add(f);


			estados = new ArrayList<Estado>();
			estado.setIdEstado("Todos");
			estado.setNombreEstado("Todos");
			estados.add(estado);
			Estado a = new Estado();
			a.setIdEstado("A");
			a.setNombreEstado("Activos");
			estados.add(a);
			Estado i = new Estado();
			i.setIdEstado("I");
			i.setNombreEstado("Inactivos");
			estados.add(i);
			Estado s = new Estado();
			s.setIdEstado("S");
			s.setNombreEstado("Sancionados");
			estados.add(s);


		} catch (Exception e) {
			e.printStackTrace();
		}


		/*********Para obtener el path del proyecto******/

//////////////////////////////////////////////////////////IMPRIMIR//////////////////////////////////////////////////////////////////
File file= new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathProyecto= file.getParentFile().getParentFile().getPath();
///////////////////////////////////////////////////////////////////////////////
int i,f;
f=this.pathProyecto.length();
i=f-7;
String z=this.pathProyecto.substring(i, f);
this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
if(z.compareTo("classes")==0){
this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().getPath();
this.rutabase=file.getParentFile().getParentFile().getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
}
System.out.print("Z: "+z+" F: "+"classes");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
String ruta = this.pathProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator") + "reportemiembro.jrxml";
System.out.print("RUTA: "+ruta);
File filePdf = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathPdfProyecto = this.pathProyecto;
rutaPdf = this.pathPdfProyecto + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator") + "reporteMiembros.pdf";

File fileZul = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.patchZulProyecto = this.pathProyecto;
rutaZul = this.patchZulProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"content"+ System.getProperty("file.separator") + "modalreporteEstructurado.zul";

//Sustitucion de caracteres codificados (Para servidores con Windows)
this.pathProyecto= ruta.replaceAll("%20", " ");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		File fileText = new File(ReporteMiembroViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		this.pathTxtProyecto = fileText.getParentFile().getParentFile().getPath();
		rutatxt = this.pathTxtProyecto + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator") + "reportemiembro.txt";
		//Sustitucion de caracteres codificados (Para servidores con Windows)
		//this.pathProyecto= ruta.replaceAll("%20", " ");

		//System.out.print("Path: "+this.pathProyecto);
		//this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");

		this.patchNoEstructurado = file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator") +"noEstructurados"+  System.getProperty("file.separator") + "reportemiembrone.jrxml";

		System.out.print(this.rutabase + "holllllljjj");
		/***********************************/
	}

	public Map<String, Object> getParameterMap() {
		return parameterMap;
	}

	public void setParameterMap(Map<String, Object> parameterMap) {
		this.parameterMap = parameterMap;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}


	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}


	public List<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

	public List<Preferencia> getPreferencias() {
		return preferencias;
	}

	public void setPreferencias(List<Preferencia> preferencias) {
		this.preferencias = preferencias;
	}

	public Categoria getCategoria() {
		return categoria;
	}
	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Preferencia getPreferencia() {
		return preferencia;
	}

	public void setPreferencia(Preferencia preferencia) {
		this.preferencia = preferencia;
	}

	public TipoPersona getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(TipoPersona tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public TipoSancion getTipoSancion() {
		return tipoSancion;
	}

	public void setTipoSancion(TipoSancion tipoSancion) {
		this.tipoSancion = tipoSancion;
	}

	public TipoIncidencia getTipoIncidencia() {
		return tipoIncidencia;
	}

	public void setTipoIncidencia(TipoIncidencia tipoIncidencia) {
		this.tipoIncidencia = tipoIncidencia;
	}

	public List<TipoPersona> getTipoPersonas() {
		return tipoPersonas;
	}

	public void setTipoPersonas(List<TipoPersona> tipoPersonas) {
		this.tipoPersonas = tipoPersonas;
	}

	public List<TipoSancion> getTipoSancions() {
		return tipoSancions;
	}

	public void setTipoSancions(List<TipoSancion> tipoSancions) {
		this.tipoSancions = tipoSancions;
	}

	public List<TipoIncidencia> getTipoIncidencias() {
		return tipoIncidencias;
	}

	public void setTipoIncidencias(List<TipoIncidencia> tipoIncidencias) {
		this.tipoIncidencias = tipoIncidencias;
	}

	public List<Genero> getGeneros() {
		return generos;
	}

	public List<TipoPersona> getAuxTipoPersona() {
		return auxTipoPersona;
	}

	public void setAuxTipoPersona(List<TipoPersona> auxTipoPersona) {
		this.auxTipoPersona = auxTipoPersona;
	}

	public void generarPdf() throws SQLException, JRException, ParseException  {
		generarReporte();
	}


	@Command({"imprimir"})
	public void generarReporte() throws SQLException, JRException, ParseException {
		//		SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
		//String desde = sdf.format(this.getFechaDesde());
		//String hasta = sdf.format(this.getFechaHasta());

		//		DateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		//		String desde = new String();
		//        desde = fmt.format(this.getFechaDesde()); //jdatechooser
		//String hasta = fmt.format(this.getFechaDesde()); //jdatechooser//
		String desde = new SimpleDateFormat("YYYY-MM-dd").format(this.getFechaDesde());
		String hasta = new SimpleDateFormat("YYYY-MM-dd").format(this.getFechaHasta());

		SimpleDateFormat formatoFecha = new SimpleDateFormat("YYYY-MM-dd") ;
		SimpleDateFormat formatoFechaH=  new SimpleDateFormat("YYYY-MM-dd") ;
		Date fechaD = formatoFecha.parse(desde);
		Date fechaH = formatoFechaH.parse(hasta);
		System.out.print(this.getGenero().getIdGenero()+ "HOLA");
		try {
			//System.out.print("entre");
			Class.forName("org.postgresql.Driver");
			// para la conexion con la bd
			conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
		} catch (ClassNotFoundException ex) {

			ex.printStackTrace();
		}
		//ruta donde esta el reporte, el japer
		String source = this.pathProyecto;
		//		Map<String,Object> parametermap = new HashMap<String,Object>();
		System.out.print(fechaD);
		System.out.print(fechaH);
		parameterMap.put("tiposancion", this.getTipoSancion().getNombreTipoSancion());
		parameterMap.put("tipoincidencia", this.getTipoIncidencia().getNombreTipoIncid());
		parameterMap.put("tipopersona", this.getTipoPersona().getNombreTipoPersona());
		parameterMap.put("preferencia", this.getPreferencia().getNombrePreferencia());
		parameterMap.put("genero", this.getGenero().getIdGenero());

		parameterMap.put("estado", this.getEstado().getIdEstado());
		//		parameterMap.put("tipopersona", "tp");
		//		parameterMap.put("preferencia", "p");
		//		parameterMap.put("genero", this.getGenero().getIdGenero());
		parameterMap.put("fechaDesde", this.getFechaDesde());
		parameterMap.put("fechaHasta", this.getFechaHasta());
		parameterMap.put("dirbase", this.rutabase);
		archivoJasper = null;
		JasperReport jasperReport = JasperCompileManager.compileReport(this.pathProyecto);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);
		if (jasperPrint.getPages().isEmpty()) {
			Messagebox.show("No se han encontrado datos para generar el reporte", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}else{
			archivoJasper = new AMedia(rutaPdf, "pdf" ,"application/pdf", JasperExportManager.exportReportToPdf(jasperPrint));

		}
	}




	@Command
	public void abrirModal(@ContextParam(ContextType.VIEW) Component view) throws SQLException, ParseException, JRException {
		parameterMap.put("ruta", patch);		
		Map<String, Object> args = new HashMap<String, Object>();
		generarPdf();
		if (archivoJasper != null) {
			args.put("reporte", archivoJasper);
			final Window component = (Window) Executions.getCurrent().createComponents("content/modalreporteEstructurado.zul", view, args);
			component.setAction("show: slideDown;hide: slideUp");
			component.doModal();
		}


	}

	@Command
	public void generarFormato() throws SQLException, FileNotFoundException, JRException{

		generarFormatoReporte();
	}

	public void generarFormatoReporte() throws SQLException, FileNotFoundException, JRException {
		try {
			//System.out.print("entre");
			Class.forName("org.postgresql.Driver");
			// para la conexion con la bd
			conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		parameterMap.put("tiposancion", this.getTipoSancion().getNombreTipoSancion());
		parameterMap.put("tipoincidencia", this.getTipoIncidencia().getNombreTipoIncid());
		parameterMap.put("tipopersona", this.getTipoPersona().getNombreTipoPersona());
		parameterMap.put("preferencia", this.getPreferencia().getNombrePreferencia());
		parameterMap.put("genero", this.getGenero().getIdGenero());
		parameterMap.put("estado", this.getEstado().getIdEstado());
		parameterMap.put("fechaDesde", this.getFechaDesde());
		parameterMap.put("fechaHasta", this.getFechaHasta());
		System.out.print(this.getGenero().getIdGenero()+ "HOLA");

		JasperReport jasperReport = JasperCompileManager.compileReport(this.patchNoEstructurado);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);

		if (jasperPrint.getPages().isEmpty()) {
			Messagebox.show("No se han encontrado datos para generar el reporte", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}else{
			JRExporter exporter = new JRTextExporter();
			exporter.setParameter(JRTextExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, System.getProperty("user.home") + "/reportes/miembros.txt");
			exporter.setParameter(JRTextExporterParameter.PAGE_WIDTH,130);
			exporter.setParameter(JRTextExporterParameter.PAGE_HEIGHT,130);

			exporter.exportReport();

			FileInputStream input = new FileInputStream(System.getProperty("user.home") + "/reportes/miembros.txt");
			Filedownload.save(input, "txt", "reporte.txt");	    

		}




	}

}
