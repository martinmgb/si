package vistaModelo;	
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import modelo.entidad.Desvinculacion;
import modelo.hibernate.dao.DaoDesvinculacion;
import modelo.entidad.MotivoDesvinculacion;
import modelo.hibernate.dao.DaoMotivoDesvinculacion;
import modelo.entidad.Persona;
import modelo.entidad.TipoPersona;
import modelo.entidad.Accion;
import modelo.hibernate.dao.DaoPersona;
import modelo.hibernate.dao.DaoTipoPersona;
import modelo.hibernate.dao.DaoAccion;



public class DesvincularAccionViewModel extends GenericForwardComposer<Window> { 

	private static final String footerMessage = "Total de %d Desvinculacion ";
	private DaoDesvinculacion dao = new DaoDesvinculacion();
	private DaoPersona daope = new DaoPersona();
	private DaoAccion daoacc = new DaoAccion();
	private DaoAccion accionDao = new DaoAccion();
	private DaoTipoPersona daot = new DaoTipoPersona();
	private DaoMotivoDesvinculacion daoMotivoDes = new DaoMotivoDesvinculacion();
	private List<Desvinculacion> todosTipos;
	private boolean displayEdit = true;


	private static final long serialVersionUID = 1L;
	//private boolean isEdit,isCreate,isView = false;
	private MotivoDesvinculacion motivoDesvSelec;
	private Persona persona; 
	private Accion accion; 
	private Desvinculacion desvinculacion; 
	// private MotivoDesvinculacion motDesvinculacion;



	@Wire
	private Intbox id;
	@Wire
	private Textbox nombre;
	@Wire
	private Textbox descripcion;
	@Wire
	private Datebox fechaDesvic;
	@Wire
	Window modalDialog, modalDialog2;
	@Wire
	public Label message;
	@Wire
	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;
	@Wire
	Window	win = modalDialog2; 

	public boolean isDisplayEdit() {
		return displayEdit;
	}


	


	public DesvincularAccionViewModel() {
		super();
		//motivoDesvSelec = new MotivoDesvinculacion();
		try {
			//	todosTipos = dao.obtenerTodos();
			//this.motDesvinculacion = new MotivoDesvinculacion();
			this.desvinculacion = new Desvinculacion();
			this.persona = new Persona();
			this.accion = new Accion();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public List<Desvinculacion> getObtenerTipos() {
		return todosTipos;
	}



// PARA CARGAR LOS COMBOS
	public List<MotivoDesvinculacion> getAllMotivoDesvinculacion() {
		try {
			return daoMotivoDes.obtenerTodos(); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	



	//	public MotivoDesvinculacion getMotDesvinculacion() {
	//		return motDesvinculacion;
	//	}
	//
	//	public void setMotDesvinculacion(MotivoDesvinculacion motDesvinculacion) {
	//		this.motDesvinculacion = motDesvinculacion;
	//	}


	public Desvinculacion getDesvinculacion() {
		return desvinculacion;
	}

	public void setDesvinculacion(Desvinculacion desvinculacion) {
		this.desvinculacion = desvinculacion;
	}
	public MotivoDesvinculacion getMotivoDesvSelec() {
		return motivoDesvSelec;
	}

	public void setMotivoDesvSelec(MotivoDesvinculacion motivoDesvSelec) {
		this.motivoDesvSelec = motivoDesvSelec;
	}

	public String getFooter() {
		return String.format(footerMessage, todosTipos.size());
	}

	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}
	public void refreshRowTemplate(Desvinculacion tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}


	@Command
	public void RegistrarDesvinculacion(@BindingParam("Persona") Persona persona,@BindingParam("Accion") Accion accion,@BindingParam("win") Window win) throws Exception { 
		String estado="A";
		String estadoA="D";

		desvinculacion.setEditingStatus(false);
		desvinculacion.setEstadoDesvinc(estado);
		desvinculacion.setPersona(persona);
		
		 Persona personas = desvinculacion.getPersona();
	//	 if(getListaAcciones(personas).size()==1) // chequea que no tenga mas acciones
		if(daope.getListaAcciones(personas).size()==1)
		 {   
			 persona.setEstadoPersona("F"); 
			 TipoPersona tipo = daot.obtenerTipoPersona(4);
			 personas.setTipoPersona(tipo);
		}
	 

		desvinculacion.setAccion(accion);
		accion.setEstadoAccion(estadoA);
		accion.setPersonai(null);
		accion.setFechaAccion(null);

		desvinculacion.setFechaDesvinc(new Date());


		this.dao.agregarDesvinculacion(desvinculacion);
		this.daope.modificarPersona(persona); // MODIFICO EL ESTADO DE PERSONA
		this.daoacc.modificarAccion(accion);

		Messagebox.show("Se ha desvinculado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
		BindUtils.postGlobalCommand(null, null, "RefrescarTablaTraeAccion", null);
		BindUtils.postGlobalCommand(null, null, "RefrescarTablaCeseAccion", null);
		win.detach();
		//modalDialog.detach();
		
	} @Command
	public void SalirDesvinculacion(@BindingParam("win") Window win) throws Exception { 
		win.detach();
		
	} 

	 
	  
	 // OBTENGO TODAS LAS ACCIONES DE UNA PERSONA
//			public List<Accion> getListaAcciones(Persona person) throws Exception {
//				
//				 Set<Accion> acciones = new HashSet<Accion>();
//				
//				 ArrayList<Accion> as = new ArrayList<Accion>(accionDao.obtenerTodos());
//			    
//				 for(Accion a : as){
//					 if(a.getPersonai()==null){
//						 System.out.println("ES NULLLL");
//					 }
//					 else if(a.getPersonai().getIdPersona() == person.getIdPersona()){
//					  
//						 acciones.add(a);
//					 }
//				 }
//
//				return new ArrayList<Accion>(acciones);
//			}








}