package vistaModelo;

public class TipoIndicadorFilter {

	private String id="", nombre="", descripcion="";

	public String getCodigo() {
		return id;
	}

	public void setCodigo(String codigo) {
		this.id = codigo==null?"":codigo.trim();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion==null?"":descripcion.trim();
	}
}
