package vistaModelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.postgresql.jdbc2.EscapedFunctions;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import controlador.ManejadorMail;
import modelo.hibernate.dao.DaoAccion;
import modelo.hibernate.dao.DaoPostulacion;
import modelo.hibernate.dao.DaoTipoPersona;
import modelo.entidad.Accion;
import modelo.entidad.Incidencia;
//PERSONA
import modelo.entidad.Persona;
import modelo.entidad.Postulacion;
import modelo.entidad.TipoPersona;
import modelo.hibernate.dao.DaoPersona;
import modelo.entidad.Parentesco;
 
public class AccionViewModel extends GenericForwardComposer<Window> {
   	private static final String footerMessage = "Total de %d Acciones ";
    	private AccionFilter accionFilter = new AccionFilter();
    	private DaoAccion dao = new DaoAccion();
    	private DaoPostulacion daop = new DaoPostulacion();
    	private DaoTipoPersona daot = new DaoTipoPersona();
    	private DaoPersona daope = new DaoPersona();
    	private List<Accion> todosTipos;
    	private List<Persona> todosTiposPersona; // Para obtner las personas q se le asignaran acciones
    	private boolean displayEdit = true;
    	private static final long serialVersionUID = 1L;
    	private boolean isEdit,isCreate,isView = false;
    	private Accion accion = new Accion();
    	private Accion acciones;
    
    	
    	@Wire
    	private Intbox idpost; // id de postulado
    	@Wire
    	private Textbox nroAccion;
    	
    	@Wire
    	private Date fechaIngreso;
    	@Wire
    	Window modalDialog;
    	@Wire
    	public Label message;
    	@Wire
    	public Div box;
    	
    	
    	
    	private Accion disponible; // VARIABLE PARA SABER MI ACCION DISPONIBLE
    	 
    	
    	private Div contenido;

    	private Div contenedor;

    	private String pagina;

    	public boolean isDisplayEdit() {
    		return displayEdit;
    	}

    	public AccionViewModel() {
    		super();
    		try {
    			todosTipos = dao.obtenerTodos();
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}
 // AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA 	
// METODO QUE ME PERMITE TRAERME LA PRIMERA COSA Q ENCUENTRA 
@Init
public void init() throws Exception{
	 
	List<Accion> disp = dao.obtenerTodos(); // OBTENGO TODAS LAS ACCIONES
	boolean d=false; 						// SABRE SI ENCONTRO ME SALE DEL WHILE
	int i=0;
	while (d==false && i<disp.size()){
		 
	if(disp.get(i).getEstadoAccion().equalsIgnoreCase("D")){//  ME TRAIGO LA PRIMERA DISPONIBLE
		disponible= disp.get(i); 
		d=true;
	 }
	 i++;
	 }  
}
//SETEO LA VARIABLE DISPONIBLE , PARA LLEVAR AL .ZUL
public Accion getDisponible() {
return disponible;
}

public void setDisponible(Accion disponible) {
this.disponible = disponible;
}
//FIN AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
  

		public List<Accion> getObtenerTipos() {
    		return todosTipos;
    	}

    	public Accion getAcciones() {
			return acciones;
		}

		public void setAcciones(Accion acciones) {
			this.acciones = acciones;
		}

		public AccionFilter getAccionFilter() {
    		return accionFilter;
    	}

    	public String getFooter() {
    		return String.format(footerMessage, todosTipos.size());
    	}

    	@Command
    	@NotifyChange({"obtenerTipos", "footer"})
    	public void changeFilter() {
    		todosTipos = getFilterTiposAccion(accionFilter);
    	}


    	public List<Accion> getFilterTiposAccion(AccionFilter tipoFilter) {

    		List<Accion> someTipos = new ArrayList<Accion>();
    		String ced = tipoFilter.getCedula().toLowerCase();
    		String nom = tipoFilter.getNombre().toLowerCase();
    		List<Accion> tiposAll = null;
    		try {
    			tiposAll = dao.obtenerTodos();
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} 

    		for (Iterator<Accion> i = tiposAll.iterator(); i.hasNext();) {
    			Accion tmp = i.next();
    			if (tmp.getPersonai().getCedPersona().toLowerCase().contains(ced) &&
    				tmp.getPersonai().getNombrePersona().toLowerCase().contains(nom)) {
    				someTipos.add(tmp);
    			}
    		}
    		return someTipos;
    	}
    	
    	
// REGISTRO UN MIEMBRO EN MODAL ASIGNAR ACCION
    public void onClick$agregarBtn() {
 System.out.println("entro 1");
    		try {
    			Postulacion postulacion = (Postulacion) arg.get("objPostulacion");
    			postulacion.setEstadoPostulacion("A");
    			TipoPersona tipo = daot.obtenerTipoPersona(1);
    			Persona persona = postulacion.getPersona();
    			persona.setTipoPersona(tipo);
    			persona.setEstadoPersona("A");
    			
    			Accion a=dao.obtenerPorNroAccion(nroAccion.getText());
    			a.setEstadoAccion("A");
    			a.setFechaAccion(new Date());
    			a.setPersonai(postulacion.getPersona());
    			
    			dao.modificarAccion(a);
    			daop.modificarPostulacion(postulacion);
    			daope.modificarPersona(persona);
    			Messagebox.show("Se ha registrado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
				
				modalDialog.detach();
				pagina = "vista/asignarAccion.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
				// ENVIO DE CORREO
				String mensaje, destinatario, asunto;
				mensaje = "Sr(a) " + a.getPersonai().getNombrePersona()+ " " + a.getPersonai().getApellidoPersona()
						+ " El presente correo es para informarle que su Solicitud de Membresía ha sido Aceptada."
						+ "Proximamente le estaremos notificando su Usuario y Clave para que pueda hacer uso de nuestro sistema."
						+ "Recuerda que Teide es tu Socio Ideal.";
				destinatario = a.getPersonai().getCorreoPersona();
				asunto = "Su Solicitud de Membresía a sido Aprobada";
				ManejadorMail.enviarEmail(mensaje, destinatario, asunto);
    			}
    			catch (Exception e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    		}
 

    	@NotifyChange({"obtenerTipos", "displayEdit"})
    	public void setDisplayEdit(boolean displayEdit) {
    		this.displayEdit = displayEdit;
    	}

    	@Command
    	@NotifyChange({"obtenerTipos", "footer"})
    	public void changeEditableStatus(@BindingParam("accion") Accion tipo) {
    		Messagebox.show("¿Esta seguro de eliminar el registro?", 
    				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
    				Messagebox.QUESTION,
    				new org.zkoss.zk.ui.event.EventListener(){
    			public void onEvent(Event e){
    				if(Messagebox.ON_OK.equals(e.getName())){
    					//OK is clicked
    					try {
    						todosTipos.remove(tipo);
    						dao.eliminarAccion(tipo);


    					} catch (Exception x) {
    						// TODO Auto-generated catch block
    						x.printStackTrace();
    					}
    					refreshRowTemplate(tipo);
    				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
    					//Cancel is clicked
    				}
    			}
    		}
    				);

    	}

    	@Command
    	@NotifyChange({"obtenerTipos", "footer"})
    	public void confirm(@BindingParam("accion") Accion tipo) {
    		changeEditableStatus(tipo);
    		try {
    			todosTipos.remove(tipo);
    			dao.eliminarAccion(tipo);
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		refreshRowTemplate(tipo);
    	}

    	public void refreshRowTemplate(Accion tipo) {
    		/*
    		 * This code is special and notifies ZK that the bean's value
    		 * has changed as it is used in the template mechanism.
    		 * This stops the entire Grid's data from being refreshed
    		 */
    		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
    	}
//ABRE MODAL ASIGNAR ACCION
  	  @Command
  		public void changeEditableAsignarAccion(@BindingParam("AbrirModalAsignarAccion") Accion acciones) {
  			HashMap registrarAccion = new HashMap();
  			 
  			registrarAccion.put("objAccion", acciones);
  			
  			Window window = (Window)Executions.createComponents(
  					"content/modalAsignarAccion.zul", null, registrarAccion);
  			window.doModal();
  		} 

  	  
//DESVINCULAR ACCION
    @Command
	public void msjDesvincular(){
		 JOptionPane.showMessageDialog(null, "Se Desvinculo Con Exito");

		}
	
	
 
	
	
	
	
	
	
	
	
	
}
