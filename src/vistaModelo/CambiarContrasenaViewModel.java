package vistaModelo;

import java.util.List;

import modelo.entidad.Usuario;
import modelo.hibernate.dao.DaoUsuario;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;




public class CambiarContrasenaViewModel extends GenericForwardComposer<Window> { 
	
	private static final long serialVersionUID = 1L;
	private DaoUsuario daoUsuario = new DaoUsuario();
	private Usuario user;
	
	@Wire
	public Textbox id;
	@Wire
	public Textbox contrasena;
	@Wire
	public Textbox usuario;
	@Wire
	public Label msg;
	@Wire
	public Div box;
	@Wire
	public Label message;
	@Wire
	private Textbox claveactual;
	@Wire
	private Textbox clavenueva;
	@Wire
	private Textbox repetirclave;
	
	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}
	
	@Init
	public void init() throws Exception{
		Session miSession = Sessions.getCurrent();
		this.user=daoUsuario.obtenerUsuario((Integer) miSession.getAttribute("idUsuario"));
		//this.nombre.setText(this.user.getNombreUsuario());
	}
	
	
public void onClick$btnCambiarContrasenna() {
		try {
			Session miSession = Sessions.getCurrent();
			Integer id = (Integer) miSession.getAttribute("idUsuario");
			Usuario usuario = daoUsuario.obtenerUsuario(id);
			if (usuario.getClaveUsuario().equals(claveactual.getText())){
				if( clavenueva.getValue().equals(repetirclave.getValue())){
					usuario.setClaveUsuario(clavenueva.getValue());
					daoUsuario.modificarUsuario(usuario);
					Messagebox.show("Cambio Exitoso!", 
							"Advertencia", Messagebox.OK, Messagebox.INFORMATION);
					claveactual.setText("");
					clavenueva.setText("");
					repetirclave.setText("");
					
				}
				else {
					Messagebox.show("Clave incorrecta!", 
							"Advertencia", Messagebox.OK, Messagebox.ERROR);
					claveactual.setText("");
					clavenueva.setText("");
					repetirclave.setText("");
				}
			}
			else {
				Messagebox.show("Clave Actual incorrecta!", 
						"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public boolean CamposVacio(){
		if(user.getNombreUsuario() != null && !user.getNombreUsuario().equalsIgnoreCase(""))
				
			return false;
		return true;
	}
}
	
	