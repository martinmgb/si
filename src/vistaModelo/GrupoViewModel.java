package vistaModelo;	
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import modelo.entidad.Evento;
import modelo.entidad.Funcion;
import modelo.entidad.Grupo;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoFuncion;
import modelo.hibernate.dao.DaoGrupo;


public class GrupoViewModel{ 

	private static final String footerMessage = "Total de %d Grupos ";
	private GrupoFilter grupoFilter = new GrupoFilter();
	private DaoGrupo dao = new DaoGrupo();
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;

	/*  Llamadas a la clase Funcion */

	private List<Grupo>  grupos;

	public List<Grupo> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<Grupo> grupos) {
		this.grupos = grupos;
	}

	/**************************/

	@Wire
	Intbox idGrupo;
	@Wire
	private Textbox nombre;
	@Wire
	private Textbox descripcion;
	@Wire
	Window modalDialog;
	@Wire
	public Label message;
	@Wire
	public Div box;

	private Div contenido;

	private Div contenedor;

	private String pagina;

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	@Init
	public void init() throws Exception {

		this.grupos= new ArrayList<Grupo>();
		this.dao= new DaoGrupo();
		grupos = dao.obtenerTodos();
	}


	public GrupoFilter getGrupoFilter() {
		return grupoFilter;
	}

	public String getFooter() {
		return String.format(footerMessage, grupos.size());
	}

	@Command
	@NotifyChange({"grupos", "footer"})
	public void changeFilter() {
		grupos = getFilterTipos(grupoFilter);
	}


	public List<Grupo> getFilterTipos(GrupoFilter tipoFilter) {

		List<Grupo> someTipos = new ArrayList<Grupo>();
		String nombre = tipoFilter.getNombre().toLowerCase();
		String des = tipoFilter.getDescripcion().toLowerCase();
		List<Grupo> tiposAll = null;
		try {
			tiposAll = dao.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Grupo> i = tiposAll.iterator(); i.hasNext();) {
			Grupo tmp = i.next();
			if (tmp.getNombreGrupo().toLowerCase().contains(nombre) && tmp.getDescGrupo().toLowerCase().contains(des)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}



	@Command
	@NotifyChange({"grupos", "footer"})
	public void editarGrupo(@BindingParam("editarGrupo") Grupo grupo) {
		HashMap<String, Object> MapGrupo = new HashMap<String, Object>();
		MapGrupo.put("objGrupo", grupo);
		this.isEdit = true;
		this.isCreate = false;
		this.isView =false;
		MapGrupo.put("edit", this.isEdit);
		MapGrupo.put("create", this.isCreate);
		MapGrupo.put("view", this.isView);
		Window window = (Window) Executions.createComponents(
				"content/modalGrupo.zul", null, MapGrupo);
		window.doModal();
	}

	@Command
	@NotifyChange({"grupos", "footer"})
	public void verGrupo(@BindingParam("verGrupo") Grupo grupo) {
		HashMap<String, Object> MapGrupo = new HashMap<String, Object>();
		MapGrupo.put("objGrupo", grupo);
		this.isEdit = false;
		this.isCreate = false;
		this.isView =true;
		MapGrupo.put("edit", this.isEdit);
		MapGrupo.put("create", this.isCreate);
		MapGrupo.put("view", this.isView);
		Window window = (Window) Executions.createComponents(
				"content/modalGrupo.zul", null, MapGrupo);
		window.doModal();
	}





	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeEditableStatus(@BindingParam("grupo") Grupo tipo) {
		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						grupos.remove(tipo);
						dao.eliminarGrupo(tipo);


					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(tipo);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}
		}
				);

	}
	@GlobalCommand
	@NotifyChange({"grupos", "footer"})
	public void RefrescarTablaGrupos() throws Exception {
		this.grupos = dao.obtenerTodos();
	}
	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void confirm(@BindingParam("grupo") Grupo tipo) {
		changeEditableStatus(tipo);
		try {
			grupos.remove(tipo);
			dao.eliminarGrupo(tipo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(tipo);
	}

	public void refreshRowTemplate(Grupo tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}
}