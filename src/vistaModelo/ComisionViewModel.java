package vistaModelo;	
import java.io.File;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import modelo.entidad.Act_comision;
import modelo.entidad.Area;
import modelo.entidad.Cargo;
import modelo.entidad.Comision;
import modelo.entidad.Evento;
import modelo.hibernate.dao.DaoCargo;
import modelo.hibernate.dao.DaoComision;
import modelo.hibernate.dao.DaoEvento_comision;
import modelo.hibernate.dao.DaoAct_comision;


public class ComisionViewModel  { 

	private static final String footerMessage = "Total de %d Comisione(s) ";
	private ComisionFilter filterComision;
	
	///////////////////////////////////////////////Para Boton Imprimir///////////////////////////////////////////////////////////
	private AMedia archivoJasper;
	private String rutaPdf;
	String pathPdfProyecto;
	private String pathProyecto;
	String patchZulProyecto;
	String rutaZul;
	Map<String,Object> parameterMap = new HashMap<String,Object>();
	private String rutabase;
	private Connection conexion;
	String patch = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");
	
	///////////////////////////////////////////////////////////////////////////////////////////////


	public ComisionFilter getFilterComision() {
		return filterComision;
	}

	private DaoComision dao;
	private DaoEvento_comision ec;
	private DaoAct_comision ac;
	private List<Comision> comisiones;

	public List<Comision> getComisiones() {
		return comisiones;
	}

	@Command
	@NotifyChange({"comisiones", "footer"})
	public void changeFilter() {
		comisiones = getFilterComision(filterComision);
	}
	public void setComisiones(List<Comision> comisiones) {
		this.comisiones = comisiones;
	}
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView, estado = false;

	@Init
	public void init() throws Exception {

		this.comisiones= new ArrayList<Comision>();
		filterComision = new  ComisionFilter();
		this.dao= new DaoComision();
		this.ec= new DaoEvento_comision();
		this.ac= new DaoAct_comision();
		comisiones = dao.obtenerTodos();
		
		//////////////////////////////////////////////////////////IMPRIMIR//////////////////////////////////////////////////////////////////
		File file= new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		this.pathProyecto= file.getParentFile().getParentFile().getPath();
		///////////////////////////////////////////////////////////////////////////////
		int i,f;
		f=this.pathProyecto.length();
		i=f-7;
		String z=this.pathProyecto.substring(i, f);
		this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
		if(z.compareTo("classes")==0){
			this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().getPath();
			this.rutabase=file.getParentFile().getParentFile().getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
		}
		System.out.print("Z: "+z+" F: "+"classes");
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////
		String ruta = this.pathProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "comisiones.jrxml";
		System.out.print("RUTA: "+ruta);
		File filePdf = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		this.pathPdfProyecto = this.pathProyecto;
		rutaPdf = this.pathPdfProyecto + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "comisiones.pdf";

		File fileZul = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		this.patchZulProyecto = this.pathProyecto;
		rutaZul = this.patchZulProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"content"+ System.getProperty("file.separator") + "modalreporteEstructurado.zul";

		//Sustitucion de caracteres codificados (Para servidores con Windows)
		this.pathProyecto= ruta.replaceAll("%20", " ");
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	}


	public void setFilterComision(ComisionFilter filterComision) {
		this.filterComision = filterComision;
	}



	@Command
	@NotifyChange({"comisiones", "footer"})
	public void EditarComisionActividad(@BindingParam("editarComision") Comision comision) {
		HashMap<String, Object> MapComision = new HashMap<String, Object>();
		MapComision .put("objetoCom", comision);
		this.isEdit = true;
		this.isCreate = false;
		this.isView =false;
		this.estado=true;
		MapComision.put("edit", this.isEdit);
		MapComision.put("create", this.isCreate);
		MapComision.put("ver", this.isCreate);
		Window window = (Window)Executions.createComponents(
				"content/modalComision.zul", null, MapComision);
		window.doModal();
	}

	@NotifyChange({"listaActcomision","footer6"})





	@Command
	public void verComision(@BindingParam("verComision") Comision comision) {
		HashMap<String, Serializable> MapComision  = new HashMap<String, Serializable>();
		MapComision .put("objetoCom", comision);
		this.isEdit = false;
		this.isCreate = false;
		this.isView =true;
		MapComision .put("edit", this.isEdit);
		MapComision .put("create", this.isCreate);
		MapComision .put("ver", this.isView);
		Window window = (Window) Executions.createComponents(
				"content/modalVerComision.zul", null, MapComision );
		window.doModal();
	}

	@SuppressWarnings("unchecked")
	@Command
	@NotifyChange({"comisiones", "footer"})
	public void eliminarComision(@BindingParam("comisionEliminar") Comision comision) {
		try {
			int nro=ec.obtenerPorComision(comision).size();
			if(nro>0){
				Messagebox.show("No se puede eliminar el registro", "Información", Messagebox.OK, Messagebox.INFORMATION);
			}else{
				Messagebox.show("¿Esta seguro de eliminar el registro? Se eliminaran las actividades asignadas", 
						"Interrogativo", Messagebox.OK | Messagebox.CANCEL,
						Messagebox.QUESTION,
						new org.zkoss.zk.ui.event.EventListener(){
					public void onEvent(Event e){
						if(Messagebox.ON_OK.equals(e.getName())){
							//OK is clicked
							try {
								try {
									List<Act_comision> arr = ac.obtenerPorComision(comision);
									for (int i = 0; i < arr.size(); i++) {
										ac.eliminarAct_comision(arr.get(i));
									}
									dao.eliminarComision(comision);
									comisiones.remove(comision);
									Messagebox.show("Se ha eliminado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
									
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}

							} catch (Exception x) {
								// TODO Auto-generated catch block
								x.printStackTrace();
							}
							refreshRowTemplate(comision);
						}else if(Messagebox.ON_CANCEL.equals(e.getName())){
							//Cancel is clicked
						}
					}

					private void refreshRowTemplate(Comision comision) {

						//Metodo Para Refrescar la Tabla cuando elimina
						BindUtils.postNotifyChange(null, null, ComisionViewModel.this, "comisiones");
						BindUtils.postNotifyChange(null, null,  ComisionViewModel.this, "footer");
					}
				}
						);
			}
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}


	}




	public List<Comision> getFilterComision(ComisionFilter tipoFilter) {

		List<Comision> someTipos = new ArrayList<Comision>();
		String nombre = tipoFilter.getNombre().toLowerCase();
		//String descripcion= tipoFilter.getDescripcion().toLowerCase();
		List<Comision> tiposAll = null;
		try {
			tiposAll = dao.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Comision> i = tiposAll.iterator(); i.hasNext();) {
			Comision tmp = i.next();
			if (tmp.getNombreComision().toLowerCase().contains(nombre)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}

	public String getFooter() {
		return String.format(footerMessage, this.comisiones.size());
	}
	@GlobalCommand
	@NotifyChange({"comisiones", "footer"})
	public void RefrescarTablaComisiones() throws Exception {
		comisiones =dao.obtenerTodos();
	}
	
	
///////////////////////////////////////////////METODOS PARA BOTON IMPRIMIR///////////////////////////////////////////////////////
	
public void generarPdf() throws SQLException, ParseException, JRException  {
generarReporte();
}

public void generarReporte() throws SQLException, ParseException, JRException{

try {
Class.forName("org.postgresql.Driver");
// para la conexion con la BD
conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
} catch (ClassNotFoundException ex) {

ex.printStackTrace();
}

parameterMap.put("dirbase", this.rutabase);

JasperReport jasperReport = JasperCompileManager.compileReport(this.pathProyecto);
JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);


archivoJasper = new AMedia(rutaPdf, "pdf" ,"application/pdf", JasperExportManager.exportReportToPdf(jasperPrint));

}


@Command
public void abrirModal(@ContextParam(ContextType.VIEW) Component view) throws SQLException, ParseException, JRException {
parameterMap.put("ruta", patch);		
Map<String, Object> args = new HashMap<String, Object>();
generarPdf();

args.put("reporte", archivoJasper);
final Window component = (Window) Executions.getCurrent().createComponents("content/modalImprimir.zul", view, args);
component.setAction("show: slideDown;hide: slideUp");
component.doModal();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


}
