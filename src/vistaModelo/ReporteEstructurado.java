package vistaModelo;

import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;

import net.sf.jasperreports.engine.JasperReport;

public class ReporteEstructurado{
	
	private AMedia reportePDF;
	
	
	@Init
	public void init(@ExecutionArgParam("reporte") AMedia reporte) {
		
		this.reportePDF = reporte;		
	}


	public AMedia getReportePDF() {
		return reportePDF;
	}


	public void setReportePDF(AMedia reportePDF) {
		this.reportePDF = reportePDF;
	}



}
