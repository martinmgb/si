package vistaModelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import modelo.entidad.Accion;
import modelo.entidad.Area;
import modelo.entidad.Area_evento;
import modelo.entidad.Evento;
import modelo.entidad.Persona;
import modelo.entidad.Postulacion;
import modelo.entidad.Preferencia;
import modelo.entidad.Preferencia_evento;
import modelo.entidad.Preferencia_persona;
import modelo.entidad.Rechazo;
import modelo.entidad.Referencia;
import modelo.entidad.Visita;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoAreaArrend;
import modelo.hibernate.dao.DaoAreaEvento;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoParentesco;
import modelo.hibernate.dao.DaoPersona;
import modelo.hibernate.dao.DaoPostulacion;
import modelo.hibernate.dao.DaoPreferencia;
import modelo.hibernate.dao.DaoPreferenciaPersona;
import modelo.hibernate.dao.DaoReferencia;

import org.zkoss.bind.BindContext;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.image.Image;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import controlador.ManejadorMail;

public class EvaluarPostulacionesMiembroViewModel extends GenericForwardComposer<Window>{



	// PARA APROBAR LA SOLICITUD 

	private static final String footerMessage = "Total de %d Referencia(s) ";
	private static final String footerMessage1 = "Total de %d Preferencia(s) ";
	private static final String footerMessageSP = "Total de %d Postulado(s) Pendiente(s) ";
	private static final String footerMessageSS = "Total de %d Postulacion(s) Solicitada(s) ";

	private static final String footerMessageReferencia = "Total de %d  Referencia()";
	private PostulacionFilter postulacionFilter = new PostulacionFilter();
	private DaoPostulacion dao = new DaoPostulacion();
	private DaoPersona daop = new DaoPersona();
	private DaoParentesco daopa = new DaoParentesco();
	private DaoReferencia  daoreferencia = new DaoReferencia ();
	private List<Postulacion> todosTipos;
	private List<Persona> referenciaPost;
	private List<Referencia> referencia;

	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;
	//Accion
	private Postulacion postulacion;
	private Accion accion = new Accion();
	//RECHAZO
	private Rechazo rechazo = new Rechazo();
	private Persona persona = new Persona(); 
	private List<Preferencia_persona> preferencia;
	private DaoPreferenciaPersona daopreferenciapersona = new DaoPreferenciaPersona();
	
	private static AImage myImage;

	//PARA LOS ESTADOS
	List<Postulacion> todosPostulados, solicitudesPendiente, solicitudesSolicitadas, todosRechazados ;
	//	List<Arrendamiento> todosArr, solArr, toincidenciaArr, tocancelArr;

	// todosARR= todosPostulados,  todas las postulaciones solicitadas ( solicitadas)
	// solArr = solicitudesPendiente , todas las postulaciones pendientes ( proceso)
	// toincidenciaArr = solicitudesSolicitadas, para incidencias, osea los aprovados
	//tocancelArr = todosRechazados , son los q son para cancelar ( rechazar)



	@Wire
	private Intbox idams;
	@Wire
	private Textbox nombre;
	@Wire
	private Textbox descripcion;
	@Wire
	Window modalDialog2;
	@Wire
	public Label message;
	@Wire
	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	@Init
	public void init(@ExecutionArgParam("objeto") Postulacion atenderSo) throws Exception {



		if (atenderSo == null) {



		}  else {
			this.referenciaPost = new ArrayList<Persona>();
			this.referencia =  new ArrayList<Referencia>();
			referencia = this.daoreferencia.obtenerReferenciaPorPostulado(atenderSo);
			for(int i=0; i<referencia.size(); i++){
				this.referenciaPost.add(referencia.get(i).getPersona());
			}
			this.persona= atenderSo.getPersona();
			this.preferencia=this.daopreferenciapersona.obtenerPorPersona(persona);
			if(this.persona.getImagenPersona()!=null)
				myImage= new AImage("",this.persona.getImagenPersona());
			else
				myImage= null;
		}




	}
	public List<Preferencia_persona> getPreferencia() {
		return preferencia;
	}
	

	public void setPreferencia(List<Preferencia_persona> preferencia) {
		this.preferencia = preferencia;
	}
	
	
	
	
	

	public Postulacion getPostulacion() {
		return postulacion;
	}

	public void setPostulacion(Postulacion postulacion) {
		this.postulacion = postulacion;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public List<Persona> getReferenciaPost() {
		return referenciaPost;
	}

	public void setReferenciaPost(List<Persona> referenciaPost) {
		this.referenciaPost = referenciaPost;
	}

	public String getFooter() {
		return String.format(footerMessage, this.referenciaPost.size());
	} 
	public String getFooter1() {
		return String.format(footerMessage1, this.preferencia.size()); 
	} 

	// MODAL RECHAZAR SOLICITUD
	@Command
	public void rec(@BindingParam("rechazarSol") Postulacion rechazarSo) {
		
	
	
	Messagebox.show("¿Desea Rechazar un Postulante?", 
				"Confirmación", Messagebox.YES | Messagebox.NO,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
		
			public void onEvent(Event e){
				if(Messagebox.ON_YES.equals(e.getName())){
					HashMap rechazarSolicitud = new HashMap();
					rechazarSolicitud.put("objeto", rechazarSo);
					Window window = (Window)Executions.createComponents(
							"content/modalRechazoPostulacion.zul", null, rechazarSolicitud);
					window.doModal();
				}
				
				else if(Messagebox.ON_NO.equals(e.getName())){
					System.out.print("hola");
					System.out.print("hola");
				}

			}}
			);

		}
				
	
	
	
	
	
	
	
	

	// PARA APROBAR LA SOLICITUD 
	public void onClick$aprobarSol() {
		System.out.println("entro 1");
		try {
 Postulacion post = dao.obtenerPostulacion(idams.getValue());

			post.setEstadoPostulacion("P"); 

			dao.modificarPostulacion(post);
			Messagebox.show("La solicitud ha sido aprobada", "Información", Messagebox.OK, Messagebox.INFORMATION);

			modalDialog2.detach();
System.out.println("preparandose");
			pagina = "vista/atenderPostulacionMembresia.zul";
			contenido.getChildren().clear();
			contenedor = (Div) Executions.createComponents(pagina, null, null);
			contenido.appendChild(contenedor);
			String mensaje, destinatario, asunto;
			mensaje = "Sr(a) " + post.getPersona().getNombrePersona()+ " " + post.getPersona().getApellidoPersona()
					+ " El presente correo es para informarle que su Solicitud de Membresía esta en Proceso de Asignacion."
			
			+ "Recuerda que Teide es tu Socio Ideal.";
			destinatario = post.getPersona().getCorreoPersona();
			asunto = "Su Solicitud esta en Proceso";
			ManejadorMail.enviarEmail(mensaje, destinatario, asunto);

System.out.println("Envio el correo");			
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	} 
	
	
	public AImage getMyImage() {
		return myImage;
	}

	public void setMyImage(AImage myImage) {
		this.myImage = myImage;
	}
	
	
	
	
	
	
	
	
	
}
