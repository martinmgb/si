package vistaModelo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import modelo.entidad.Area;
import modelo.entidad.Area_arrend;
import modelo.entidad.Area_evento;
import modelo.entidad.Arrendamiento;
import modelo.entidad.Cancelacion;
import modelo.entidad.Incidencia;
import modelo.entidad.Motivo;
import modelo.entidad.Persona;
import modelo.entidad.TipoArrendamiento;
import modelo.entidad.TipoIncidencia;
import modelo.entidad.TipoRechazo;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoAreaArrend;
import modelo.hibernate.dao.DaoAreaEvento;
import modelo.hibernate.dao.DaoArrendamiento;
import modelo.hibernate.dao.DaoCancelacion;
import modelo.hibernate.dao.DaoIncidencia;
import modelo.hibernate.dao.DaoMotivo;
import modelo.hibernate.dao.DaoPersona;
import modelo.hibernate.dao.DaoTipoArrendamiento;
import modelo.hibernate.dao.DaoTipoIncidencia;
import modelo.hibernate.dao.DaoTipoRechazo;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timebox;
import org.zkoss.zul.Window;

public class ArrendamientosFinalizadosViewModel extends GenericForwardComposer<Window>{

	@Wire
	Textbox cedula, areaM, cedulaM, ci, telefonoM, nombreM, emailM, apellidoM, descripcionM, descIncidencia, descripcionRechazoCancelacion;
	@Wire
	Datebox fechaM, fechaArr;
	@Wire
	Combobox comboMotivo, comboTipoIncidencia, comboTipoRechazoCancelacion;
	@Wire
	Timebox horaArr, horaIncidencia;
	@Wire
	Intbox idArrendamiento, idArr;
	@Wire
	Datebox fechaIncidencia;
	@Wire
	Window modalDialog, modalDialogRm;
	@Wire
	public Label message;
	@Wire
	public Div box;

	private Div contenido;

	private Div contenedor;

	private String pagina;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String footerMessage = "Total de %d Arrendamientos ";
	private static final String footerMessageIncidencia = "Total de %d Arrendamientos ";
	private static final String footerMessage6 = "Total de %d Areas(s)";
	private boolean displayEdit = true;
	private boolean isEdit,isCreate,isView = false;
	private DaoArrendamiento daoArr = new DaoArrendamiento();
	private DaoPersona daoPersona = new DaoPersona();
	private Persona persona;
	private Arrendamiento arrendamiento;
	private TipoArrendamiento motivoArrendamiento;
	private String cedulabuscar;
	private DaoAreaEvento daoareaevento;
	private DaoAreaArrend daoareaarrend;
	private String arealimpiar;
	private boolean loco;
	private DaoArea  daoarea ; 
	private DaoTipoRechazo daoTipoRechazo = new DaoTipoRechazo();
	private DaoAreaArrend daoAreaArrend = new DaoAreaArrend();
	private Area areaSelected;
	private DaoTipoArrendamiento daoTipoArrendamiento = new DaoTipoArrendamiento();
	private DaoTipoIncidencia daoTipoIncidencia = new DaoTipoIncidencia();
	private DaoIncidencia daoIncidencia = new DaoIncidencia();
	private DaoMotivo daoMotivo = new DaoMotivo();
	private Persona personaUsuario = new Persona();
	private static Set<Area_arrend> listaAreaArrendamiento;
	private List<Area> areas;
	List<TipoRechazo> listaTipoRechazo;
	TipoRechazo tipoRechazoSeleccionado;
	List<TipoArrendamiento> todosTipoArrendamiento;
	List<TipoIncidencia> listaTipoIncidencia;
	List<Arrendamiento> solicitudesArrendamientoAprobadas;
	private static List<Arrendamiento> arrendamientosFinalizados;
	List<Motivo> listaMotivos;
	List<Persona> personas;
	private Incidencia incidencia;
	private ArrayList<Area> listaAreas;
	private TipoArrendamiento motivoSeleccionado;
	private TipoIncidencia IncidenciaSeleccionada;
	private  ArrendamientoFilter filterArrendamiento = new ArrendamientoFilter();
	private ArrendamientoFilter arrendamientosFinalizadosFilter;
	private String incidencialimpiar;





	public ArrendamientosFinalizadosViewModel(){
		super();
		Area_arrend listaAreaArrendamiento = new Area_arrend();
		TipoArrendamiento motivoSeleccionado = new TipoArrendamiento();
		TipoIncidencia IncidenciaSeleccionada = new TipoIncidencia();
		List<Arrendamiento> arrendamientosParaFinalizar = new ArrayList<Arrendamiento>();
		this.arrendamientosFinalizados = new ArrayList<Arrendamiento>();
		this.arrendamientosFinalizadosFilter = new ArrendamientoFilter();

		persona = new Persona();
		arrendamiento= new Arrendamiento();
		motivoArrendamiento= new TipoArrendamiento ();
		this.listaAreaArrendamiento= new HashSet<Area_arrend>(); 
		this.areaSelected = new Area();
		this.daoareaevento=new DaoAreaEvento();
		this.daoareaarrend =new DaoAreaArrend();
		this.listaAreas=new ArrayList<Area>();
		daoarea = new DaoArea();
		this.arealimpiar ="";
		this.cedulabuscar="";
		this.incidencia = new Incidencia();
		tipoRechazoSeleccionado = new TipoRechazo();
		listaTipoRechazo = new ArrayList<TipoRechazo>();
		Date fecha =new Date();
		this.incidencialimpiar = "";


		String estadoAprobado = "A";

		solicitudesArrendamientoAprobadas = new ArrayList<Arrendamiento>();
		try {
			solicitudesArrendamientoAprobadas = daoArr.obtenerArrendamientosPorEstado1(estadoAprobado);
			arrendamientosParaFinalizar = daoArr.obtenerArrendamientoParaIncidencia();
			int j= 0;

			for(int i=0; i<arrendamientosParaFinalizar.size(); i++){
				System.out.println("Fecha arrendamiento:"+arrendamientosParaFinalizar.get(i).getFechaArrend());

				String estadoFinalizado = "F";

				if((arrendamientosParaFinalizar.get(i).getFechaArrend().getDate()==fecha.getDate()) 
						&& (arrendamientosParaFinalizar.get(i).getFechaArrend().getMonth()==fecha.getMonth())){

					arrendamientosParaFinalizar.get(i).setEstadoArrend(estadoFinalizado);
					daoArr.modificarArrendamiento(arrendamientosParaFinalizar.get(i));
					arrendamientosFinalizados.add(j, arrendamientosParaFinalizar.get(i));
					j++;

				}
			}

			String estadoInactivo = "I";
			Calendar calendarioArrendamiento = Calendar.getInstance();
			Calendar calendarioActual = Calendar.getInstance();

			for(int k=0; k<arrendamientosFinalizados.size();k++){

				calendarioArrendamiento.setTime(arrendamientosFinalizados.get(k).getFechaArrend());
				calendarioArrendamiento.add(Calendar.DAY_OF_YEAR, 30);
//				calendarioActual.setTime(fecha);
//				calendarioActual.add(Calendar.DAY_OF_YEAR, 30);
				Date fechaArrendamiento = calendarioArrendamiento.getTime();
//				Date fecha2 = calendarioActual.getTime();
//				System.out.println("Fecha Arrendamiento + 30 días "+fecha1);
//				System.out.println("Fecha actual + 30 días "+fecha2);
//				System.out.println("Fecha actual + 30 días "+fecha1.compareTo(fecha2));

				if(fechaArrendamiento.compareTo(fecha) <= 0){

					arrendamientosFinalizados.get(k).setEstadoArrend(estadoInactivo);
					daoArr.modificarArrendamiento(arrendamientosFinalizados.get(k));
				}
				
			}


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public List<Arrendamiento> getSolicitudesArrendamientoAprobadas() {
		return solicitudesArrendamientoAprobadas;
	}

	public void setSolicitudesArrendamientoAprobadas(
			List<Arrendamiento> solicitudesArrendamientoAprobadas) {
		this.solicitudesArrendamientoAprobadas = solicitudesArrendamientoAprobadas;
	}

	public List<Arrendamiento> getArrendamientosFinalizados() {
		return arrendamientosFinalizados;
	}

	public void setArrendamientosFinalizados(
			List<Arrendamiento> arrendamientosFinalizados) {
		this.arrendamientosFinalizados = arrendamientosFinalizados;
	}

	public ArrendamientoFilter getTipoFilter() {
		return arrendamientosFinalizadosFilter;
	}


	public void setTipoFilter(ArrendamientoFilter tipoFilter) {
		this.arrendamientosFinalizadosFilter = tipoFilter;
	}


	public String getArealimpiar() {
		return arealimpiar;
	}


	public void setArealimpiar(String arealimpiar) {
		this.arealimpiar = arealimpiar;
	}


	public ArrayList<Area> getListaAreas() {
		return listaAreas;
	}


	public void setListaAreas(ArrayList<Area> listaAreas) {
		this.listaAreas = listaAreas;
	}

	public String getCedulabuscar() {
		return cedulabuscar;
	}

	public void setCedulabuscar(String cedulabuscar) {
		this.cedulabuscar = cedulabuscar;
	}

	public TipoArrendamiento getMotivoArrendamiento() {
		return motivoArrendamiento;
	}

	public void setMotivoArrendamiento(TipoArrendamiento motivoArrendamiento) {
		this.motivoArrendamiento = motivoArrendamiento;
	}

	public Arrendamiento getArrendamiento() {
		return arrendamiento;
	}

	public void setArrendamiento(Arrendamiento arrendamiento) {
		this.arrendamiento = arrendamiento;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Incidencia getIncidencia() {
		return incidencia;
	}


	public void setIncidencia(Incidencia incidencia) {
		this.incidencia = incidencia;
	}

	public String getIncidencialimpiar() {
		return incidencialimpiar;
	}


	public void setIncidencialimpiar(String incidencialimpiar) {
		this.incidencialimpiar = incidencialimpiar;
	}


	public List<Motivo> getTiposRechazo() {
		try {
			this.listaMotivos=this.daoMotivo.obtenerTodos();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return listaMotivos;
	}

	public List<TipoIncidencia> getTiposIncidencia() {
		try {
			this.listaTipoIncidencia=this.daoTipoIncidencia.obtenerTodos();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return listaTipoIncidencia;
	}



	public  Set<Area_arrend> getListaAreaArrendamiento() {
		return listaAreaArrendamiento;
	}

	public void setListaAreaArrendamiento(Set<Area_arrend> listaAreaArrendamiento) {
		this.listaAreaArrendamiento = listaAreaArrendamiento;
	}

	public String getFooter6() {
		return String.format(footerMessage6, this.listaAreaArrendamiento.size());
	}

	public Area getAreaSelected() {
		return areaSelected;
	}

	public void setAreaSelected(Area areaSelected) {
		this.areaSelected = areaSelected;
	}



	@Command
	//Filtro Por Fechas Para buscar las Areas Disponibles
	@NotifyChange({"listaAreas"})
	public void activarBotonDeBusquedaPorFecha() throws Exception {
		Date fechaActual=new Date();
		if((arrendamiento.getFechaArrend().after(fechaActual) || arrendamiento.getFechaArrend().getDate()==fechaActual.getDate())){
			if(this.arrendamiento.getFechaArrend() !=null){
				this.listaAreas.clear();
				for (Area area: this.daoarea.obtenerTodos()){
					if (isDisponible(area))
						this.listaAreas.add(area);

				}
			}
			if(this.listaAreas.size()==0)
			{

				Messagebox.show("Todas las áreas se encuentran ocupadas en esta fecha seleccione otra!", 
						"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			}
		}	else{
			Messagebox.show("¡Eliga una fecha mayor o igual a la de hoy!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
	}

	@Command
	@NotifyChange({"listaAreaArrendamiento","footer6", "deshabilitar"})
	public void agregarAreasArrendamiento(){


		if(areaSelected==null){
			Messagebox.show("Debe Seleccionar un Area!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else if(buscarArea(this.areaSelected)==null){
			Area_arrend obj1= new Area_arrend();
			obj1.setArea(areaSelected);
			obj1.setArrendamiento(arrendamiento);
			this.listaAreaArrendamiento.add(obj1);//arrendamientosFinalizado
		}else
		{

			Messagebox.show("El Area Ya Se Encuentra Registrada!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);

		}
	}

	public boolean isDeshabilitar(){
		return this.listaAreaArrendamiento.size()>0;
	}


	public Area buscarArea(Area areaSelected ){
		for (Area_arrend areaArrendamiento:listaAreaArrendamiento) {
			if(areaArrendamiento.getArea().getIdArea()==areaSelected.getIdArea())
				return areaSelected ;
		}
		return null;
	}

	@Command
	@NotifyChange({"listaAreaArrendamiento","footer6", "deshabilitar"})
	public void eliminarAreaArrendamiento(@BindingParam("area_arrendamiento") Area_arrend area) throws Exception{
		this.listaAreaArrendamiento.remove(area);
		this.daoareaarrend.eliminarArea_arrend(area);
	}	  




	public List<TipoArrendamiento> getAllTipoArrendamiento() {
		try {
			return daoTipoArrendamiento.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}


	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public String getFooter() {
		return String.format(footerMessage, solicitudesArrendamientoAprobadas.size());
	}

	public String getFooter1() {
		return String.format(footerMessageIncidencia, arrendamientosFinalizados.size());
	}


	@Command
	public void verDetalleArrendamiento() {
		Window window = (Window) Executions.createComponents(
				"content/modalDetalleArrendamiento.zul", null, null);
		window.doModal();
	}

	@Command
	public void AbrirModalIncidenciaArrendamiento(@BindingParam("objetoArrendamiento") Arrendamiento objetoArr) {
		HashMap arrendamiento = new HashMap();
		arrendamiento.put("objetoArr", objetoArr);
		Window window = (Window)Executions.createComponents(
				"content/modalRegistrarIncidenciaArrendamiento.zul", null, arrendamiento);
		window.doModal();
	}



	@Command
	public void changeEditable(@BindingParam("editarArre") Arrendamiento objetoArr) {
		HashMap arrendamiento = new HashMap();
		arrendamiento.put("objetoArr", objetoArr);
		Window window = (Window) Executions.createComponents(
				"content/modalCancelarArrendamiento.zul", null, arrendamiento);
		window.doModal();
	}

	public void onClick$cancelarArrendamiento() {

		Arrendamiento arre  = (Arrendamiento) arg.get("objetoArr");
		HashMap objetoArr = new HashMap();
		objetoArr.put("objetoArr", arre);
		Window window = (Window)Executions.createComponents(
				"content/modalRechazoArrendamientoCancelacion.zul", null, objetoArr);
		window.doModal();
		modalDialog.detach();
	}

	public void onClick$btnRegistrarCancelacionArrendamiento() {

		if((descripcionRechazoCancelacion.getValue().equals(""))) {
			Messagebox.show("Debe llenar todos los campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else {
			try {

				DaoMotivo daoMotivo = new DaoMotivo();
				DaoCancelacion daoCancelacion = new DaoCancelacion();
				Motivo motivo = new Motivo();
				Cancelacion cancelacion = new Cancelacion();
				Arrendamiento arre  = (Arrendamiento) arg.get("objetoArr");
				cancelacion.setMotivo(comboTipoRechazoCancelacion.getSelectedItem().getValue());
				arre.setEstadoArrend("C");
				cancelacion.setArrendamiento(arre);
				cancelacion.setEstadoCancelacion("A");
				daoArr.modificarArrendamiento(arre);
				daoCancelacion.agregarCancelacion(cancelacion);
				JOptionPane.showMessageDialog(null, "Arrendamiento cancelado");
				modalDialogRm.detach();
				pagina = "vista/arrendamientos.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void onClick$btnLimpiarRechazoCancelacion() {
		comboTipoRechazoCancelacion.setText("");
		descripcionRechazoCancelacion.setText("");
		comboTipoRechazoCancelacion.setFocus(true);
	}



	public boolean isDisponible(Area area) throws Exception {
		String estado="C";

		// verificamos que no se vaya usar esa area en un arrendamiento
		for (Area_arrend areaArrend : daoareaarrend.obtenerPorArea(area)) {
			if((areaArrend.getArrendamiento().getFechaArrend().compareTo(arrendamiento.getFechaArrend())==0)&& areaArrend.getArrendamiento().getEstadoArrend().compareTo(estado)==0){
				return true;

			}else if((areaArrend.getArrendamiento().getFechaArrend().compareTo(arrendamiento.getFechaArrend())==0)){
				return false;
			}
		}

		// verificamos que no se vaya usar esa area en un Arrendamiento
		for (Area_evento areaEvento : daoareaevento.obtenerPorArea(area)) {
			if((areaEvento.getEvento().getFechaEvento().getTime())- (this.arrendamiento.getFechaArrend().getTime())==0)
				return false;
		}
		return true;
	}



	@Command
	@NotifyChange({"persona",""})
	public void BuscarCedula() throws Exception{
		boolean busqueda=false;
		personas = daoPersona.obtenerTodos();
		//System.err.println(personas.size());
		for (int i = 0; i < personas.size(); i++) {
			if(this.cedulabuscar.equals(personas.get(i).getCedPersona())){
				this.persona=personas.get(i);
				busqueda = true;
			}
		} 

		if(busqueda==false){
			Messagebox.show("La cedula no Existe!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			this.persona = new Persona();

		}
	}

	@Command
	@NotifyChange({"persona","arealimpiar","arrendamiento","listaAreaArrendamiento","areaSelected","listaAreas","cedulabuscar"})
	public void EnviarSolicitud() throws Exception{

		if (!CamposVacio()){
			String estado="P";
			arrendamiento.setEditingStatus(false);
			arrendamiento.setEstadoArrend(estado);
			this.arrendamiento.setFechaEmisionArrend(arrendamiento.getFechaArrend());
			this.arrendamiento.setPersona(persona);
			this.arrendamiento.setArea_arrend(listaAreaArrendamiento);
			this.daoArr.agregarArrendamiento(arrendamiento);
			Messagebox.show("La Solicitud se Registro Con Exito!", 
					"Advertencia", Messagebox.OK, Messagebox.NONE);

			arrendamiento= new Arrendamiento();
			persona = new Persona();
			this.listaAreaArrendamiento.clear();
			this.listaAreaArrendamiento= new HashSet<Area_arrend>(); 
			this.listaAreas.clear();
			this.listaAreas=new ArrayList<Area>();
			this.areaSelected = new Area();
			this.arealimpiar = "";
			this.cedulabuscar="";

		}else
		{
			Messagebox.show("Debe LLenar Todos Los Campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
	}

	@Command
	@NotifyChange({"persona","arrendamiento","listaAreaArrendamiento","cedulabuscar","arealimpiar"})
	public void Limpiar(){
		this.persona = new Persona();
		this.cedulabuscar="";	
		this.arrendamiento = new Arrendamiento();
		this.listaAreaArrendamiento.clear();
		this.arealimpiar= "";
	}

	public boolean CamposVacio(){
		if(arrendamiento.getDescArrend() != null && !arrendamiento.getDescArrend().equalsIgnoreCase("") && 
				this.listaAreaArrendamiento.size()>0)
			return false;
		return true;
	}

	@Command
	@NotifyChange({"incidencia","incidencialimpiar"})
	public void LimpiarIncidencia(){
		this.incidencia = new Incidencia();
		this.incidencialimpiar ="";
	}


	// Registrar incidencia a un arrendamiento
	@Command
	public void  RegistrarIncidencia(@BindingParam("Arrendamiento") Arrendamiento objetoArr,@BindingParam("win") Window win) throws Exception {

		if(!CamposVaciosIncidencia()){

			String estado="A";
			incidencia.setEditingStatus(false);
			incidencia.setEstadoIncidencia(estado);
			incidencia.setFechaIncidencia(objetoArr.getFechaArrend());
			incidencia.setArrendamiento(objetoArr);
			this.daoIncidencia.agregarIncidencia(incidencia);
			//pagina = "vista/registrarEvento.zul"; 
			Messagebox.show("Se ha registrado con éxito", "Información", Messagebox.OK, Messagebox.INFORMATION);
			win.detach();
		}

		else
		{
			Messagebox.show("Debe LLenar Todos Los Campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}

	}

	public boolean CamposVaciosIncidencia(){

		if(incidencia.getHoraIncidencia() != null && incidencia.getFechaIncidencia() !=null)
			return false;
		return true;

	}

	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}


	public List<Arrendamiento> getFilterArrendamientosFinalizados(ArrendamientoFilter tipoFilter) {

		List<Arrendamiento> someTipos = new ArrayList<Arrendamiento>();
		String cedula = tipoFilter.getCedCliente().toLowerCase();
		String nombre = tipoFilter.getNombCliente().toLowerCase();
		List<Arrendamiento> tiposAll = null;
		try {
			List<Arrendamiento> arrendamientosParaFinalizar = new ArrayList<Arrendamiento>();
			this.arrendamientosFinalizados = new ArrayList<Arrendamiento>();
			Date fecha =new Date();;
			motivoArrendamiento= new TipoArrendamiento ();
			this.listaAreaArrendamiento= new HashSet<Area_arrend>();
			arrendamientosParaFinalizar = daoArr.obtenerArrendamientoParaIncidencia();
			int j= 0;

			for(int i=0; i<arrendamientosParaFinalizar.size(); i++){
				System.out.println("Fecha arrendamiento:"+arrendamientosParaFinalizar.get(i).getFechaArrend());

				String estadoFinalizado = "F";

				if((arrendamientosParaFinalizar.get(i).getFechaArrend().getDate()==fecha.getDate()) 
						&& (arrendamientosParaFinalizar.get(i).getFechaArrend().getMonth()==fecha.getMonth())){

					arrendamientosParaFinalizar.get(i).setEstadoArrend(estadoFinalizado);
					daoArr.modificarArrendamiento(arrendamientosParaFinalizar.get(i));
					arrendamientosFinalizados.add(j, arrendamientosParaFinalizar.get(i));
					j++;

				}
			}

			String estadoInactivo = "I";
			Calendar calendarioArrendamiento = Calendar.getInstance();
			Calendar calendarioActual = Calendar.getInstance();

			for(int k=0; k<arrendamientosFinalizados.size();k++){

				calendarioArrendamiento.setTime(arrendamientosFinalizados.get(k).getFechaArrend());
				calendarioArrendamiento.add(Calendar.DAY_OF_YEAR, 30);
//				calendarioActual.setTime(fecha);
//				calendarioActual.add(Calendar.DAY_OF_YEAR, 30);
				Date fechaArrendamiento = calendarioArrendamiento.getTime();
//				Date fecha2 = calendarioActual.getTime();
//				System.out.println("Fecha Arrendamiento + 30 días "+fecha1);
//				System.out.println("Fecha actual + 30 días "+fecha2);
//				System.out.println("Fecha actual + 30 días "+fecha1.compareTo(fecha2));

				if(fechaArrendamiento.compareTo(fecha) <= 0){

					arrendamientosFinalizados.get(k).setEstadoArrend(estadoInactivo);
					daoArr.modificarArrendamiento(arrendamientosFinalizados.get(k));
				}
				
			}
			
			tiposAll = this.arrendamientosFinalizados;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Arrendamiento> i = tiposAll.iterator(); i.hasNext();) {
			Arrendamiento tmp = i.next();
			if (tmp.getPersona().getNombrePersona().toLowerCase().contains(nombre) 
					&& tmp.getPersona().getCedPersona().toLowerCase().contains(cedula)){
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}

	public ArrendamientoFilter getArrendamientosFinalizadosFilter() {
		return arrendamientosFinalizadosFilter;
	}


	public void setArrendamientosFinalizadosFilter(ArrendamientoFilter arrendamientosFinalizadosFilter) {
		this.arrendamientosFinalizadosFilter = arrendamientosFinalizadosFilter;
	}


	@Command
	@NotifyChange({"arrendamientosFinalizados","footer1"})
	public void changeFilterArrendamientosFinalizados(){
		arrendamientosFinalizados = getFilterArrendamientosFinalizados(arrendamientosFinalizadosFilter);
	}

	@GlobalCommand
	@NotifyChange({"arrendamientosFinalizados", "footer1"})
	public void RefrescarTablaArrendamientosFinalizados() throws Exception {
		
		arrendamientosFinalizados = new ArrayList<Arrendamiento>();
		System.out.println("asasasas: "+arrendamientosFinalizados.size());

	}

}
