package vistaModelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import modelo.entidad.Area;
import modelo.entidad.Area_evento;
import modelo.entidad.Area_recurso;
import modelo.entidad.Categoria;
import modelo.entidad.Comision;
import modelo.entidad.Evento;
import modelo.entidad.Evento_comision;
import modelo.entidad.MotivoDesvinculacion;
import modelo.entidad.Parentesco;
import modelo.entidad.Persona;
import modelo.entidad.Preferencia;
import modelo.entidad.Preferencia_evento;
import modelo.entidad.Preferencia_persona;
import modelo.entidad.Recurso;
import modelo.entidad.TipoPersona;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoArea_recurso;
import modelo.hibernate.dao.DaoCategoria;
import modelo.hibernate.dao.DaoComision;
import modelo.hibernate.dao.DaoEvento;
import modelo.hibernate.dao.DaoEvento_comision;
import modelo.hibernate.dao.DaoParentesco;
import modelo.hibernate.dao.DaoPersona;
import modelo.hibernate.dao.DaoPreferencia;
import modelo.hibernate.dao.DaoPreferenciaPersona;
import modelo.hibernate.dao.DaoPreferencia_evento;
import modelo.hibernate.dao.DaoRecurso;
import modelo.hibernate.dao.DaoTipoPersona;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.itextpdf.text.pdf.security.CertificateUtil;

public class BeneficiarioViewModel extends GenericForwardComposer<Window> {

	private static  String footerMessage = "En Total %d Miembros Propietarios Activos";
	private static  String footerMessage2 = "En Total %d Beneficiario(s)";
	private BeneficiarioFilter filtroBeneficiarios = new BeneficiarioFilter();
	List<Persona> beneficiarios;
	List<Persona> propietarios;
	List<Categoria> categorias;
	private Categoria CategoriaSelected;
	private Preferencia preferenciaSelected;
	private  List<Preferencia> listaPreferencia;
	private String categorialimpiar;
	private String preferencialimpiar;
	private static Set<Preferencia_persona> listaPreferenciaPersona;
	private Persona propietario;
	private Persona beneficiario;
	private DaoCategoria daocategoria = new DaoCategoria();
	private DaoPreferencia daopreferencia = new DaoPreferencia() ;
	private DaoParentesco daoParentesco = new DaoParentesco() ;
	private DaoPreferenciaPersona daopp = new DaoPreferenciaPersona();
	private static final String footerMessage3 = "Total de %d Preferencia(s)";
	List<Persona> personas;
	private DaoPersona daope = new DaoPersona();
	private Persona persona;
	private String cedulabuscar;

	DaoPersona dao= new DaoPersona();
	DaoTipoPersona daot= new DaoTipoPersona();
	private boolean encontrado;
	private boolean isEdit,isCreate = false;

	@Wire
	private Textbox cedula;
	@Wire
	private Textbox nombre;
	@Wire
	private Textbox apellido;
	@Wire
	private Datebox fechaNac;
	@Wire
	private Textbox correo;
	@Wire
	private Combobox comboPreferencia;
	@Wire
	private Combobox comboCategoria,comboParentesco;
 
	  
 
	
	
	
	@Wire
	Window modalDialog2;

	@Init
	public void init(@ExecutionArgParam("propietario") Persona propietario,@ExecutionArgParam("beneficiario") Persona beneficiario) throws Exception{
		this.propietario=propietario;
		this.categorias= new ArrayList<Categoria>();
		this.CategoriaSelected = new Categoria();
		beneficiarios=dao.obtenerBeneficiariosPorPropietario(this.propietario.getIdPersona());
		if (beneficiario == null) {
			this.beneficiario = new Persona();
			this.listaPreferenciaPersona=new HashSet<Preferencia_persona>();

		} else {
			this.beneficiario = beneficiario;
			this.listaPreferenciaPersona = beneficiario.getPreferencia_persona();
		}
	}

	
	// PARA CARGAR LOS COMBOS
		public List<Parentesco> getAllParentesco() {
			try {
				return daoParentesco.obtenerTodos(); 
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	
	
	

	public String getCedulabuscar() {
			return cedulabuscar;
		}


		public void setCedulabuscar(String cedulabuscar) {
			this.cedulabuscar = cedulabuscar;
		}


	public Categoria getCategoriaSelected() {
		return CategoriaSelected;
	}


	public void setCategoriaSelected(Categoria categoriaSelected) {
		CategoriaSelected = categoriaSelected;
	}


	public Preferencia getPreferenciaSelected() {
		return preferenciaSelected;
	}


	public void setPreferenciaSelected(Preferencia preferenciaSelected) {
		this.preferenciaSelected = preferenciaSelected;
	}


	public  BeneficiarioFilter  getfiltroBeneficiario() {
		return filtroBeneficiarios;
	}

	public List<Persona> getBeneficiarioModel() throws Exception {
		return beneficiarios;
	}

	public List<Persona> getPropietariosModel() throws Exception {
		propietarios=dao.obtenerPersonaMenosSancion("S",1,2,5);
		return propietarios;
	}

	public String getFooter() {
		return String.format(footerMessage, propietarios.size());
	}

	public String getFooter2() {
		return String.format(footerMessage2, beneficiarios.size());
	}

	public String getFooter3() {
		return String.format(footerMessage3, listaPreferenciaPersona.size());
	}


	public  Set<Preferencia_persona> getListaPreferenciaPersona() {
		return listaPreferenciaPersona;
	}


	public void setListaPreferenciaPersona(
			Set<Preferencia_persona> listaPreferenciaPersona) {
		BeneficiarioViewModel.listaPreferenciaPersona = listaPreferenciaPersona;
	}
	public Persona getBeneficiario() {
		return beneficiario;
	}


	public void setBeneficiario(Persona beneficiario) {
		this.beneficiario = beneficiario;
	}
	public String getCategorialimpiar() {
		return categorialimpiar;
	}


	public void setCategorialimpiar(String categorialimpiar) {
		this.categorialimpiar = categorialimpiar;
	}


	public String getPreferencialimpiar() {
		return preferencialimpiar;
	}


	public void setPreferencialimpiar(String preferencialimpiar) {
		this.preferencialimpiar = preferencialimpiar;
	}

	@Command
	@NotifyChange({"listaPreferenciaPersona","footer3"})
	public void agregarPreferenciasPersona(){
		if(this.preferenciaSelected==null){
			Messagebox.show("Debe Seleccionar Una Preferencia!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else if(buscarPreferencia(this.preferenciaSelected)==null){
			Preferencia_persona obj= new Preferencia_persona();
			obj.setPersona(this.beneficiario);
			obj.setPreferencia(preferenciaSelected);
			this.listaPreferenciaPersona.add(obj);
		}else
		{

			Messagebox.show("La preferencia ya se encuentra registrada!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
	}

	public Preferencia buscarPreferencia(Preferencia preferenciaSelected){
		for (Preferencia_persona preferenciaPersona: listaPreferenciaPersona) {
			if(preferenciaPersona.getPreferencia().getIdPreferencia()==preferenciaSelected.getIdPreferencia())
				return preferenciaSelected;
		}
		return null;
	}

	@Command
	@NotifyChange({"listaPreferenciaPersona","footer3"})
	public void eliminarPreferenciaPersona(@BindingParam("preferencia_persona") Preferencia_persona preferencia) throws Exception{
		this.listaPreferenciaPersona.remove(preferencia);
		this.daopp.eliminarPreferencia_persona(preferencia);
	}

	public List<Categoria> getCategorias() {
		try {
			categorias= daocategoria.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return categorias;
	}

	@Command
	@NotifyChange({"listaPreferencia","preferencialimpiar"})
	public void activarBotonDeBusqueda() throws Exception {
		if(getCategoriaSelected().getNombreCategoria() !=null){
			this.preferencialimpiar ="";
			this.listaPreferencia=daopreferencia.obtenerPreferenciaPorCategoria(this.getCategoriaSelected());
		}
	}



	public List<Preferencia> getListaPreferencia() {
		return listaPreferencia;
	}


	public void setListaPreferencia(List<Preferencia> listaPreferencia) {
		this.listaPreferencia = listaPreferencia;
	}


	@Command
	@NotifyChange({"beneficiarioModel", "footer2"})
	public void changeFilter() {
		beneficiarios = getFilterBeneficiario(filtroBeneficiarios);
	}

	@Command   
	public void  RegistrarBeneficiario(@BindingParam("Propietario") Persona  propietario,@BindingParam("win") Window win) throws Exception {

		//if (!CamposVacio()){
			//beneficiario.setCedPersona(cedula.getText());
			this.beneficiario.setEditingStatus(false);
			TipoPersona t = daot.obtenerTipoPersona(5);
			this.beneficiario.setTipoPersona(t);
			this.beneficiario.setEstadoPersona("A");
			this.beneficiario.setIdPadrePersona(propietario.getIdPersona());
			this.beneficiario.setPreferencia_persona(listaPreferenciaPersona);
			dao.agregarPersona(beneficiario);
			BindUtils.postGlobalCommand(null, null, "RefrescarTablaBeneficiarios", null);
			
			System.out.print(beneficiario);
			win.detach();

//		}
//		else
//		{
//			Messagebox.show("Debe LLenar Todos Los Campos!", 
//					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
//		}
	}


	public boolean CamposVacio(){
		if(this.beneficiario.getCedPersona() != null && !beneficiario.getCedPersona().equalsIgnoreCase("") && 
				this.beneficiario.getNombrePersona() != null && !beneficiario.getNombrePersona().equalsIgnoreCase("") &&
				this.beneficiario.getApellidoPersona()!= null && !beneficiario.getApellidoPersona().equalsIgnoreCase("") &&
				this.beneficiario.getCorreoPersona()!= null && !beneficiario.getCorreoPersona().equalsIgnoreCase("") &&
				this.beneficiario.getFechaNacPersona()!= null &&
				this.listaPreferenciaPersona.size()>0 )
			return false;
		return true;
	}

	//	@NotifyChange({"*"})
	//	public void onClick$btnRegistrar() {
	//
	//		//Messagebox.show("En Construcción", "Información", Messagebox.OK, Messagebox.INFORMATION);
	//
	//		if(cedula.getText()=="" || nombre.getText()=="" || apellido.getText()=="" || fecha.getText()==""
	//				|| correo.getText()=="") {
	//			Messagebox.show("¡Debe llenar todos los campos!", 
	//					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
	//		}
	//		else {
	//			try {
	//				System.out.print("listaaaaaaaa"+getListaPreferenciaPersona().size());
	//				Persona a = (Persona) arg.get("propietario");
	//				Persona beneficiario = new Persona();
	//				beneficiario.setCedPersona(cedula.getText());
	//				beneficiario.setNombrePersona(nombre.getText());
	//				beneficiario.setApellidoPersona(apellido.getText());
	//				beneficiario.setFechaNacPersona(fecha.getValue());
	//				beneficiario.setCorreoPersona(correo.getText());
	//				beneficiario.setEditingStatus(false);
	//				TipoPersona t = daot.obtenerTipoPersona(5);
	//				beneficiario.setTipoPersona(t);
	//				beneficiario.setEstadoPersona("A");
	//				beneficiario.setIdPadrePersona(a.getIdPersona());
	//				beneficiario.setPreferencia_persona(listaPreferenciaPersona);
	//				dao.agregarPersona(beneficiario);
	//				BindUtils.postGlobalCommand(null, null, "RefrescarTablaBeneficiarios", null);
	//				modalDialog2.detach();
	//			}
	//			catch (Exception e) {
	//				// TODO Auto-generated catch block.cedPersona
	//				e.printStackTrace();
	//			}
	//		}
	//	}


	private void refreshRowTemplate(Persona beneficiario) {

		//Metodo Para Refrescar la Tabla cuando elimina
		BindUtils.postNotifyChange(null, null,  BeneficiarioViewModel.this, "beneficiarioModel");
		BindUtils.postNotifyChange(null, null,  BeneficiarioViewModel.this, "footer2");
	}

	@Command   
	public void EditarBeneficiario(@BindingParam("win") Window win) throws Exception {

		if (!CamposVacio()){

			this.beneficiario.setEditingStatus(false);
			this.beneficiario.setEstadoPersona("A");
			this.beneficiario.setPreferencia_persona(listaPreferenciaPersona);
			dao.modificarPersona(beneficiario);
			BindUtils.postGlobalCommand(null, null, "RefrescarTablaBeneficiarios", null);
			win.detach();
			Messagebox.show("El Benficiario  Se Actualizo Correctamente!", 
					"Advertencia", Messagebox.OK, Messagebox.NONE);
			win.detach();

		}else
		{
			Messagebox.show("Debe LLenar Todos Los Campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}
	}

	@Command   
	@NotifyChange({"beneficiario","listaPreferenciaPersona","preferencialimpiar","categorialimpiar"})
	public void Limpiar() throws Exception {
		this.beneficiario = new Persona();
		this.listaPreferenciaPersona.clear();
		this.preferencialimpiar = "";
		this.categorialimpiar="";


	}
	@Command
	@NotifyChange({"beneficiarios", "footer"})
	public void eliminarBeneficiario(@BindingParam("beneficiarioEliminar") Persona beneficiario) {

		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						beneficiarios.remove(beneficiario);
						dao.eliminarPersona(beneficiario);
					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(beneficiario);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}

			/*private void refreshRowTemplate(Evento_comision evento) {

				//Metodo Para Refrescar la Tabla cuando elimina
				BindUtils.postNotifyChange(null, null,  ComisionEventoViewModel.this, "comisionEventoModel");
				BindUtils.postNotifyChange(null, null,  ComisionEventoViewModel.this, "footer");
			}*/
		}
				);

	}


	@Command
	public void changeEditable(@BindingParam("beneficiario")  Persona beneficiario) {
		HashMap a = new HashMap();
		Persona p = this.propietario;
		a.put("beneficiario", beneficiario);
		a.put("propietario", p);
		this.isEdit = true;
		this.isCreate = false;
		a.put("edit", this.isEdit);
		a.put("create", this.isCreate);
		Window window = (Window) Executions.createComponents(
				"content/modalBeneficiario.zul", null, a);
		window.doModal();
	}

	@Command
	public void showModalBeneficiario() {
		Persona propietario = new Persona();
		propietario=this.propietario;
		HashMap a = new HashMap();
		a.put("propietario", propietario);
		this.isEdit = false;
		this.isCreate = true;
		a.put("edit", this.isEdit);
		a.put("create", this.isCreate);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"content/modalBeneficiario.zul", null, a);
		window.doModal();
	}


	public List<Persona> getFilterBeneficiario(BeneficiarioFilter filtroBeneficiarios2) {

		List<Persona> someTipos = new ArrayList<Persona>();
		String cedula = filtroBeneficiarios2.getCedula().toLowerCase();
		String nombre = filtroBeneficiarios2.getNombre().toLowerCase();
		String apellido = filtroBeneficiarios2.getApellido().toLowerCase();
		//String des = tipoFilter.getTipo().toLowerCase();
		List<Persona> tiposAll = null;
		try {
			tiposAll = dao.obtenerBeneficiariosPorPropietario(this.propietario.getIdPersona());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Persona> i = tiposAll.iterator(); i.hasNext();) {
			Persona tmp = i.next();
			if (tmp.getCedPersona().toLowerCase().contains(cedula) 
					&& tmp.getNombrePersona().toLowerCase().contains(nombre) 
					&& tmp.getApellidoPersona().toLowerCase().contains(apellido)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}

	@GlobalCommand
	@NotifyChange({"beneficiarioModel", "footer2"})
	public void RefrescarTablaBeneficiarios() throws Exception {
		beneficiarios=dao.obtenerBeneficiariosPorPropietario(this.propietario.getIdPersona());
	}

	// BUSCA LA CEDULA DE UN MIEMBRO
//		@Command
//		@NotifyChange({"persona","cedulabuscar"})
//		public void BuscarCedula() throws Exception{
//			boolean mensaje=false;
//			//String estado="S";
//			
//			personas = daope.obtenerTodos();
//			if(this.cedulabuscar==null){
//				Messagebox.show("Ingrese una Cedula!", 
//						"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
//				mensaje = true;	
//			}
//			else{
//				for (int i = 0; i < personas.size(); i++) {
//					if(this.cedulabuscar.equals(personas.get(i).getCedPersona())){
//						Messagebox.show("El beneficiario ya esta registrado!", 
//								"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
//						mensaje=true;
//
//					} 	else if((this.cedulabuscar.equals(personas.get(i).getCedPersona())  &&
//							personas.get(i).getTipoPersona().getIdTipoPersona()==5)  )
//					{ 
//						this.persona=personas.get(i);
//						mensaje = true;	
//						
//					}
//
//				}
//			}
//
//
//			if(mensaje==false){
//				Messagebox.show("La cédula no existe!", 
//						"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);  
//			
//				this.cedula.setDisabled(false);
//				//this.nombre.setDisabled(true);
//			}
//		}
	
	// BUSCA LA CEDULA DE UN BENEFICIARIO
	public void onClick$btnBuscar() throws Exception {
		encontrado=false;
		//if (!"".equalsIgnoreCase(cedula.getValue())) {
		List<Persona> pers = dao.obtenerTodos();
		for (int i = 0; i < pers.size(); i++) {
			if (this.cedula.getText().equals(pers.get(i).getCedPersona())) {
				Messagebox.show("Ya se encuentra Registrado !", "Advertencia",
						Messagebox.OK, Messagebox.EXCLAMATION);
				this.cedula.setFocus(true);
				this.nombre.setDisabled(true);
				this.apellido.setDisabled(true); 
				this.comboParentesco.setDisabled(true);
				this.fechaNac.setDisabled(true);
			 	this.correo.setDisabled(true);
			 	this.comboCategoria.setDisabled(true);
				 
				
				encontrado = true;
			}
		}
		if (encontrado==false){
			
			
			Messagebox.show("La cedula no Existe!", "Advertencia",
					Messagebox.OK, Messagebox.EXCLAMATION);
			this.nombre.setDisabled(false);
			this.apellido.setDisabled(false); 
			this.comboParentesco.setDisabled(false);
			this.fechaNac.setDisabled(false);
		 	this.correo.setDisabled(false); 
			this.comboCategoria.setDisabled(false);
			
		}

	}	
	
	
	
	
	

}
