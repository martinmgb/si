package vistaModelo;	
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

//import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;




import modelo.entidad.Area;
import modelo.entidad.Indicador;
import modelo.entidad.TipoIndicador;
import modelo.entidad.UnidadMedida;
import modelo.hibernate.dao.DaoIndicador;
import modelo.hibernate.dao.DaoTipoIndicador;
import modelo.hibernate.dao.DaoUnidadMedida;


public class IndicadorViewModel extends GenericForwardComposer<Window> { 

	private static final String footerMessage = "Total de %d Indicadore(s) ";
	private IndicadorFilter indicadorFilter = new IndicadorFilter();
	private DaoIndicador dao = new DaoIndicador();
	private DaoTipoIndicador daoTipoIndicador = new DaoTipoIndicador();
	private DaoUnidadMedida daoUnidadMedida = new DaoUnidadMedida();
	private List<Indicador> todosTipos;
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;
	
///////////////////////////////////////////////Para Boton Imprimir///////////////////////////////////////////////////////////
private AMedia archivoJasper;
private String rutaPdf;
String pathPdfProyecto;
private String pathProyecto;
String patchZulProyecto;
String rutaZul;
Map<String,Object> parameterMap = new HashMap<String,Object>();
private String rutabase;
private Connection conexion;
String patch = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	

	@Wire
	private Intbox id;
	@Wire
	private Textbox nombre;
	@Wire
	private Textbox descripcion;
	@Wire
	private Combobox  comboTipoIndicador;
	@Wire
	private Combobox comboUnidadMedida;
	@Wire
	private Listbox  unidadMedida;
	@Wire
	Window modalDialog;
	@Wire
	public Label message;
	@Wire
	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;
	
	private TipoIndicador tipoIndicadorselect;
	private UnidadMedida  unidadselect;
	
	

	private String selecti;

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public IndicadorViewModel() {
		super();
		tipoIndicadorselect = new TipoIndicador();
		unidadselect = new UnidadMedida();
		try {
			todosTipos = dao.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//////////////////////////////////////////////////////////IMPRIMIR//////////////////////////////////////////////////////////////////
File file= new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathProyecto= file.getParentFile().getParentFile().getPath();
String ruta = this.pathProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "indicador.jrxml";

File filePdf = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathPdfProyecto = filePdf.getParentFile().getParentFile().getPath();
rutaPdf = this.pathPdfProyecto + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "indicador.pdf";

File fileZul = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.patchZulProyecto = fileZul.getParentFile().getParentFile().getPath();
rutaZul = this.patchZulProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"content"+ System.getProperty("file.separator") + "modalreporteEstructurado.zul";

//Sustitucion de caracteres codificados (Para servidores con Windows)
this.pathProyecto= ruta.replaceAll("%20", " ");
this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}

	public TipoIndicador getTipoIndicadorselect() {
		return tipoIndicadorselect;
	}

	public void setTipoIndicadorselect(TipoIndicador tipoIndicadorselect) {
		this.tipoIndicadorselect = tipoIndicadorselect;
	}
	public UnidadMedida getUnidadselect() {
		return unidadselect;
	}

	public void setUnidadselect(UnidadMedida unidadselect) {
		this.unidadselect = unidadselect;
	}

	public String getSelecti() {
		return selecti;
	}

	public void setSelecti(String selecti) {
		this.selecti = selecti;
	}

	
	public List<TipoIndicador> getAllTipoIndicador() {
	     try {
			return daoTipoIndicador.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	  }

	public List<UnidadMedida> getAllUnidadMedida() {
	     try {
			return daoUnidadMedida.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	  }
	
	public List<Indicador> getObtenerTipos() {
		return todosTipos;
	}

	public IndicadorFilter getIndicadorFilter() {
		return indicadorFilter;
	}

	public String getFooter() {
		return String.format(footerMessage, todosTipos.size());
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeFilter() {
		todosTipos = getFilterTipos(indicadorFilter);
	}



	public List<Indicador> getFilterTipos(IndicadorFilter tipoFilter) {

		List<Indicador> someTipos = new ArrayList<Indicador>();
		String nombre = tipoFilter.getNombre().toLowerCase();
		String des = tipoFilter.getDescripcion().toLowerCase();
		String tipoI= tipoFilter.getTipoIndicador().toLowerCase();
		List<Indicador> tiposAll = null;
		try {
			tiposAll = dao.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Indicador> i = tiposAll.iterator(); i.hasNext();) {
			Indicador tmp = i.next();
			if (tmp.getNombreIndicador().toLowerCase().contains(nombre) && tmp.getDescIndicador().toLowerCase().contains(des)&& tmp.getTipoIndicador().getNombreTipoInd().toLowerCase().contains(tipoI)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
 	public void onClick$btnRegistrar() {
		System.out.println("entro 1");
		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))|| (comboTipoIndicador.getValue().equals(""))|| (comboUnidadMedida.getValue().equals(""))) {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		}
		else {
			try {
				
				tipoIndicadorselect = comboTipoIndicador.getSelectedItem().getValue();
				unidadselect = comboUnidadMedida.getSelectedItem().getValue();
				Indicador indicador= new Indicador();
				indicador.setDescIndicador(descripcion.getValue());
				indicador.setEstadoIndicador("A");
				indicador.setNombreIndicador(nombre.getValue());
				indicador.setTipoIndicador(tipoIndicadorselect);
			    indicador.setUnidadMedida(unidadselect);
				dao.agregarIndicador(indicador);
				Messagebox.show("Se ha registrado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/indicador.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	} 
	
	public void onClick$btnLimpiar() {
		nombre.setText("");
		descripcion.setText("");
		nombre.setFocus(true);
	}
	public void onClick$btnEditar() {

		System.out.println(id.getValue());
		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))|| (comboTipoIndicador.getValue().equals(""))|| (comboUnidadMedida.getValue().equals(""))) {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		}
		else {
			try {
				Indicador objeto = dao.obtenerIndicador(id.getValue());
				objeto.setNombreIndicador(nombre.getValue());
				objeto.setDescIndicador(descripcion.getValue());
				objeto.setTipoIndicador(comboTipoIndicador.getSelectedItem().getValue());
				objeto.setUnidadMedida(comboUnidadMedida.getSelectedItem().getValue());
				dao.modificarIndicador(objeto);
				Messagebox.show("Se ha modificado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/indicador.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
			catch (Exception x) {
				// TODO Auto-generated catch block
				x.printStackTrace();
			}
		}
	}

	@Command
	public void changeEditable(@BindingParam("editarIndicador") Indicador tip) {
		HashMap indicador = new HashMap();
		indicador.put("objIndicador", tip);
		this.isEdit = true;
		this.isCreate = false;
		indicador.put("edit", this.isEdit);
		indicador.put("create", this.isCreate);
		Window window = (Window) Executions.createComponents(
				"content/modalIndicador.zul", null, indicador);
		window.doModal();
	}

	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeEditableStatus(@BindingParam("indicador") Indicador tipo) {
		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Interrogativo", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						if(dao.eliminarIndicador(tipo)){
							todosTipos.remove(tipo);
							Messagebox.show("Se ha eliminado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);

						}else{
							Messagebox.show("No se puede eliminar el registro", "Información", Messagebox.OK, Messagebox.INFORMATION);

						}
					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(tipo);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}
			private void refreshRowTemplate(Indicador indicador) {
				//Metodo Para Refrescar la Tabla cuando elimina
				BindUtils.postNotifyChange(null, null, IndicadorViewModel.this, "obtenerTipos");
				BindUtils.postNotifyChange(null, null, IndicadorViewModel.this, "footer");
			}
		}
				);

	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void confirm(@BindingParam("indicador") Indicador tipo) {
		changeEditableStatus(tipo);
		try {
			todosTipos.remove(tipo);
			dao.eliminarIndicador(tipo);
			Messagebox.show("Se ha eliminado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(tipo);
	}

	public void refreshRowTemplate(Indicador tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}
	
///////////////////////////////////////////////METODOS PARA BOTON IMPRIMIR///////////////////////////////////////////////////////
	
public void generarPdf() throws SQLException, ParseException, JRException  {
generarReporte();
}

public void generarReporte() throws SQLException, ParseException, JRException{

try {
Class.forName("org.postgresql.Driver");
// para la conexion con la BD
conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
} catch (ClassNotFoundException ex) {

ex.printStackTrace();
}

parameterMap.put("dirbase", this.rutabase);

JasperReport jasperReport = JasperCompileManager.compileReport(this.pathProyecto);
JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);


archivoJasper = new AMedia(rutaPdf, "pdf" ,"application/pdf", JasperExportManager.exportReportToPdf(jasperPrint));

}


@Command
public void abrirModal(@ContextParam(ContextType.VIEW) Component view) throws SQLException, ParseException, JRException {
parameterMap.put("ruta", patch);		
Map<String, Object> args = new HashMap<String, Object>();
generarPdf();

args.put("reporte", archivoJasper);
final Window component = (Window) Executions.getCurrent().createComponents("content/modalImprimir.zul", view, args);
component.setAction("show: slideDown;hide: slideUp");
component.doModal();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}