package vistaModelo;

public class InsolventeFilter {
	private String cedula="";
	private String nombre="";
	private String apellido="";
	private String fechavencimiento="";
	
	
	public String getCedula(){
		return cedula;
	}
	public String getNombre(){
		return nombre;
	}
	public String getApellido(){
		return apellido;	
	}
	public String getFechaVencimiento(){
		return fechavencimiento;
	}


public void setCedula(String cedula) {
	this.cedula = cedula==null?"":cedula.trim();
}

public void setNombre(String nombre) {
	this.nombre = nombre==null?"":nombre.trim();
}
public void setApellido(String apellido) {
	this.apellido = apellido==null?"":apellido.trim();

}
public void setFechaVencimiento(String fecha) {
	this.fechavencimiento = fechavencimiento==null?"":fechavencimiento.trim();

}
}
