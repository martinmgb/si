package vistaModelo;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkmax.zul.Nav;
import org.zkoss.zkmax.zul.Navbar;
import org.zkoss.zkmax.zul.Navitem;
import org.zkoss.zul.Div;

import modelo.entidad.Accion;
import modelo.entidad.Club;
import modelo.entidad.Funcion;
import modelo.entidad.Funcion_grupo;
import modelo.hibernate.dao.DaoAccion;
import modelo.hibernate.dao.DaoClub;
import modelo.hibernate.dao.DaoFuncion;
import modelo.hibernate.dao.DaoFuncion_grupo;

public class NavegacionViewModel{

	private DaoFuncion_grupo daoFuncionesGrupo = new DaoFuncion_grupo();
	private DaoFuncion daoFuncion = new DaoFuncion();
	private DaoAccion daoAccion = new DaoAccion();
	private DaoClub daoClub = new DaoClub();
	private List<Funcion_grupo> funcionesGrupo = new ArrayList<Funcion_grupo>();
	private List<Funcion> funcionesArbol = new ArrayList<Funcion>();
	private List<Accion> acciones = new ArrayList<Accion>();

	public NavegacionViewModel() {
		super();
		try {
			Session miSession = Sessions.getCurrent();
			Integer id = (Integer) miSession.getAttribute("idGrupo");
			for (int i = 0; i < daoFuncionesGrupo.obtenerTodos().size(); i++) {
				if(daoFuncionesGrupo.obtenerTodos().get(i).getGrupo().getIdGrupo() == id ){
					funcionesGrupo.add(daoFuncionesGrupo.obtenerTodos().get(i));
				}
			}


			for (int i = 0; i < funcionesGrupo.size(); i++) {
				funciones.add(funcionesGrupo.get(i).getFuncion());
				agregarRaiz(daoFuncion.obtenerFuncion(funcionesGrupo.get(i).getFuncion().getIdFuncion()));
			}
			System.out.println(funciones.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private List<Funcion> funciones = new ArrayList<Funcion>();

	@Wire("#navbar")
	Navbar navbar;

	@Wire("#contenido")
	private Div contenido;

	private Div contenedor;

	@AfterCompose
	public void afterCompose(@ContextParam(ContextType.VIEW) Component view) throws Exception {
		Selectors.wireComponents(view, this, false);
		cargarMenu();
		cargarAcciones();
	}

	/*public void agregarRaiz(Funcion funcion){
		try {
			int funPadre = funcion.getIdPadreFuncion();
			System.out.println(funPadre+"FUNCION PADRE");
			Funcion objNuevo = daoFuncion.obtenerFuncion(funPadre);
			boolean aceptado = false;
			while (objNuevo.getIdPadreFuncion() != 0) {
				for (int i = 0; i < funciones.size(); i++) {
					if(funciones.get(i).getIdFuncion() != objNuevo.getIdFuncion())
					{
						aceptado = true;
					}
					else {
						aceptado = false;
						break;
					}
				}
				if(aceptado == true){
					funciones.add(objNuevo);
					funPadre = objNuevo.getIdPadreFuncion();
					objNuevo = daoFuncion.obtenerFuncion(funPadre);
					System.out.println(funPadre+"FUNCION PADRE");
					if(objNuevo.getIdPadreFuncion() == 0){
						funciones.add(objNuevo);
					}
				}
				else break;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}*/

	public void agregarRaiz(Funcion funcion){
		try {
			int funPadre = funcion.getIdPadreFuncion();
			Funcion objNuevo = daoFuncion.obtenerFuncion(funPadre);
			boolean aceptado = false;
			boolean fin=false;
			while (fin==false) {
				for (int i = 0; i < funciones.size(); i++) {
					if(funciones.get(i).getIdFuncion() != objNuevo.getIdFuncion())
					{
						aceptado = true;
					}
					else {
						aceptado = false;
						break;
					}
				}
				if(aceptado == true){
					funciones.add(objNuevo);
					if(objNuevo.getIdPadreFuncion()!=0){
						funPadre = objNuevo.getIdPadreFuncion();
						objNuevo = daoFuncion.obtenerFuncion(funPadre);	
					}
					else{
						fin=true;
					}
				}
				else break;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public void cargarMenu(){


		List<Funcion> raices = llenarRaices();
		List<Funcion> padres = llenarPadres(raices);
		List<Funcion> hijos = llenarHijos(raices, padres);
		List<Funcion> nietos = llenarNietos(padres);

		//Creo las raices en el navbar
		for(Funcion f : raices){
			Nav root = new Nav();
			root.setLabel(f.getNombreFuncion());
			root.setIconSclass(f.getIconUriFuncion());
			root.setId(String.valueOf(f.getIdFuncion()));
			navbar.appendChild(root);

			//Creo los hijos de las raices que a su vez son padres
			for(Funcion fc : padres){

				if(fc.getIdPadreFuncion() == f.getIdFuncion()){
					Nav child = new Nav();
					child.setLabel(fc.getNombreFuncion());
					child.setIconSclass(fc.getIconUriFuncion());
					root.appendChild(child);

					//Creo los hijos de los padres
					for(Funcion nieto : nietos){
						if(nieto.getIdPadreFuncion() == fc.getIdFuncion()){
							final Navitem itemNieto = new Navitem();
							itemNieto.setLabel(nieto.getNombreFuncion());
							itemNieto.setIconSclass(nieto.getIconUriFuncion());
							itemNieto.setId(String.valueOf(nieto.getIdFuncion()));
							itemNieto.addEventListener("onClick", new EventListener(){
								public void onEvent(Event arg0) throws Exception{
									navegar(itemNieto);
									itemNieto.setSelected(true);
								}
							});
							child.appendChild(itemNieto);
						}
					}

				}

			}
			//Coloco los items directos de la raiz
			for(Funcion hijo : hijos){
				if(hijo.getIdPadreFuncion() == f.getIdFuncion()){
					final Navitem item = new Navitem();
					item.setLabel(hijo.getNombreFuncion());
					item.setIconSclass(hijo.getIconUriFuncion());
					item.setId(String.valueOf(hijo.getIdFuncion()));
					item.addEventListener("onClick", new EventListener(){
						public void onEvent(Event arg0) throws Exception{
							navegar(item);
							item.setSelected(true);
						}
					});
					root.appendChild(item);
				}
			}

		}
	}

	private List<Funcion> llenarNietos(List<Funcion> padres) {
		List<Funcion> nietos = new ArrayList<Funcion>();
		//Recorro el arreglo de padres y busco sus respectivos hijos
		for(Funcion padre : padres){
			for(Funcion f : funciones){
				if(f.getIdPadreFuncion() == padre.getIdFuncion()){
					nietos.add(f);
				}
			}
		}
		return nietos;
	}


	private List<Funcion> llenarHijos(List<Funcion> raices, List<Funcion> padres) {

		List<Funcion> hijos = new ArrayList<Funcion>();
		//Recorro el arreglo de raices y pregunto quienes son sus hijos
		for(Funcion f : raices){
			for(Funcion fc : funciones){
				if(fc.getIdPadreFuncion() == f.getIdFuncion()){
					hijos.add(fc);
				}
			}
		}

		//Recorro el arreglo de hijos y pregunto si el hijo es padre entonces lo saco del arreglo hijos 
		for(Funcion padre : padres){
			for(int i = 0; i<hijos.size(); i++){
				Funcion hijo = hijos.get(i);
				if(hijo.getIdFuncion() == padre.getIdFuncion()){
					hijos.remove(i);
				}
			}
		}

		return hijos;
	}


	private List<Funcion> llenarPadres(List<Funcion> raices) {

		List<Funcion> padres = new ArrayList<Funcion>();
		//Recorro el arreglo de raices y pregunto cuales son sus hijos
		for(Funcion raiz : raices){
			for(Funcion f : funciones){
				if(raiz.getIdFuncion() == f.getIdPadreFuncion()){
					//recorro el arreglo de funciones y pregunto si el hijo de la raiz tiene hijos
					for(Funcion fc : funciones){
						if(fc.getIdPadreFuncion() == f.getIdFuncion()){
							padres.add(f);
							break;
						}
					}
				}
			}
		}
		return padres;
	}


	private List<Funcion> llenarRaices() {
		List<Funcion> raices = new ArrayList<Funcion>();
		//Recorro el arreglo y pregunto si no tiene padres cada funcion
		for (Funcion fc : funciones ) {
			if (fc.getIdPadreFuncion() == 0) {
				raices.add(fc);
			}
		}
		return raices;
	}


	//Metodo que verifica que opcion se ha marcado
	public void navegar(Component comp){
		for(Funcion fc : funciones) {
			if(comp.getId().equalsIgnoreCase(String.valueOf(fc.getIdFuncion()))){
				cambiarPantalla(fc);
				break;
			}
		}


	}

	//Metodo que asigna al contenedor que ha de cargar
	private void cambiarPantalla(Funcion fc) {
		if(!fc.getPaginaFuncion().equalsIgnoreCase("")){
			contenido.getChildren().clear();
			contenedor = (Div) Executions.createComponents(fc.getPaginaFuncion(), null, null);
			contenido.appendChild(contenedor);
		}
	}

	private void cargarAcciones() throws Exception {
		acciones = daoAccion.obtenerTodos();
		Club club = daoClub.obtenerClub(1);
		int limAcciones = club.getLimiteAcciones();
		int contador = 0;

		if(acciones.size() == 0){
			for (int i = 0; i < 1500; i++) {
				Accion accion = new Accion();
				accion.setNroAccion("acc-"+(i+1));
				accion.setEstadoAccion("D");
				daoAccion.agregarAccion(accion);
			}
		}

		else{
			for (int i = 0; i < acciones.size(); i++) {
				if(acciones.get(i).getEstadoAccion().equals("A")){
					contador ++;	
				}
			}
			if(contador == acciones.size() && acciones.size() <= limAcciones ){
				for (int i = 0; i < 100 ; i++) {
					Accion accion = new Accion();
					accion.setNroAccion("acc-"+(acciones.size()+(i+1)));
					accion.setEstadoAccion("D");
					daoAccion.agregarAccion(accion);
				}
			}
		}
	}
}