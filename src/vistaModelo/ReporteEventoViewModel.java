package vistaModelo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modelo.entidad.Area;
import modelo.entidad.Categoria;
import modelo.entidad.Preferencia;
import modelo.entidad.TipoEvento;
import modelo.entidad.TipoPublico;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoCategoria;
import modelo.hibernate.dao.DaoPreferencia;
import modelo.hibernate.dao.DaoTipoEvento;
import modelo.hibernate.dao.DaoTipoPublico;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;

import vistaModelo.ReporteArrendamientoViewModel.Estado;

public class ReporteEventoViewModel extends GenericForwardComposer {

	public class Estado{
		private String idEstado;
		private String nombreEstado;
		public Estado() {
			super();
			// TODO Auto-generated constructor stub
		}
		public String getIdEstado() {
			return idEstado;
		}
		public void setIdEstado(String idEstado) {
			this.idEstado = idEstado;
		}
		public String getNombreEstado() {
			return nombreEstado;
		}
		public void setNombreEstado(String nombreEstado) {
			this.nombreEstado = nombreEstado;
		}
	}

	private List<TipoEvento> tipoEventos;
	private List<TipoPublico> tipoPublicos;
	private List<Categoria> categorias;
	private List<Preferencia> preferencias;
	private List<Area> areas;
	private Area area;
	private Categoria categoria;
	private Preferencia preferencia;
	private TipoEvento tipoEvento; 
	private TipoPublico tipoPublico;
	private DaoTipoPublico daoTipoPublico;
	private DaoCategoria daoCategoria;
	private DaoPreferencia daoPreferencia;
	private DaoTipoEvento daoTipoEvento;
	private DaoArea daoArea;
	private List<Estado> estados;
	String patch = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");
	private Connection conexion;
	private Date fechaDesde;
	private Date fechaHasta;
	private AMedia archivoJasper;	
	private String rutaPdf;
	String pathPdfProyecto;
	private String pathProyecto;
	String patchZulProyecto;
	String rutaZul;
	String patchNoEstructurado;
	private Estado estado = new Estado();
	Map<String,Object> parameterMap = new HashMap<String,Object>();
	private String rutabase;

	@Init
	public void init() {
		setFechaDesde(new Date());
		setFechaHasta(new Date());
		tipoEventos =new ArrayList<TipoEvento>();
		tipoPublicos = new ArrayList<TipoPublico>();
		categorias = new ArrayList<Categoria>();
		preferencias = new ArrayList<Preferencia>();
		areas = new ArrayList<Area>();
		categoria = new Categoria();
		preferencia = new Preferencia();
		tipoEvento = new TipoEvento();
		area = new Area();
		estado = new Estado();
		tipoPublico = new TipoPublico();
		daoTipoPublico = new DaoTipoPublico();
		daoCategoria = new DaoCategoria();
		daoPreferencia =new DaoPreferencia() ;
		daoTipoEvento = new DaoTipoEvento();
		daoArea = new DaoArea();

		try {
			tipoEvento.setNombreTipoEvento("Todos");
			tipoEventos.add(tipoEvento);
			tipoEventos.addAll(daoTipoEvento.obtenerTodos());

			tipoPublico.setNombreTipoPublico("Todos");
			tipoPublicos.add(tipoPublico);
			tipoPublicos.addAll(daoTipoPublico.obtenerTodos());

			categorias.addAll(daoCategoria.obtenerTodos());

			preferencia.setNombrePreferencia("Todos");
			preferencias.add(preferencia);
			preferencias.addAll(daoPreferencia.obtenerTodos());

			area.setNombreArea("Todos");
			areas.add(area);
			areas.addAll(daoArea.obtenerTodos());

			estados = new ArrayList<Estado>();
			estado.setIdEstado("Todos");
			estado.setNombreEstado("Todos");
			estados.add(estado);
			Estado r = new Estado();
			r.setIdEstado("R");
			r.setNombreEstado("Registrado");
			estados.add(r);
			Estado p = new Estado();
			p.setIdEstado("P");
			p.setNombreEstado("Planificado");
			estados.add(p);
			
			Estado f = new Estado();
			f.setIdEstado("F");
			f.setNombreEstado("Finalizado");
			estados.add(f);
			Estado c = new Estado();
			c.setIdEstado("C");
			c.setNombreEstado("Cancelado");
			estados.add(c);
		} catch (Exception e) {
			// TODO: handle exception
		}

//////////////////////////////////////////////////////////IMPRIMIR//////////////////////////////////////////////////////////////////
File file= new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathProyecto= file.getParentFile().getParentFile().getPath();
///////////////////////////////////////////////////////////////////////////////
int i,f;
f=this.pathProyecto.length();
i=f-7;
String z=this.pathProyecto.substring(i, f);
this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
if(z.compareTo("classes")==0){
this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().getPath();
this.rutabase=file.getParentFile().getParentFile().getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
}
System.out.print("Z: "+z+" F: "+"classes");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
String ruta = this.pathProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator") + "evento09.jrxml";
System.out.print("RUTA: "+ruta);
File filePdf = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathPdfProyecto = this.pathProyecto;
rutaPdf = this.pathPdfProyecto + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator") + "eventos.pdf";

File fileZul = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.patchZulProyecto = this.pathProyecto;
rutaZul = this.patchZulProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"content"+ System.getProperty("file.separator") + "modalreporteEstructurado.zul";

//Sustitucion de caracteres codificados (Para servidores con Windows)
this.pathProyecto= ruta.replaceAll("%20", " ");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
		//Sustitucion de caracteres codificados (Para servidores con Windows)
		this.pathProyecto= ruta.replaceAll("%20", " ");
		//this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
		this.patchNoEstructurado = file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator") +"noEstructurados"+  System.getProperty("file.separator") + "noEstructuradosEventos.jrxml";
	System.out.print(patchNoEstructurado + "TXTTT");
	}


	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public List<Area> getAreas() {
		return areas;
	}

	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}

	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	public List<TipoEvento> getTipoEventos() {
		return tipoEventos;
	}

	public void setTipoEventos(List<TipoEvento> tipoEventos) {
		this.tipoEventos = tipoEventos;
	}

	public List<TipoPublico> getTipoPublicos() {
		return tipoPublicos;
	}

	public void setTipoPublicos(List<TipoPublico> tipoPublicos) {
		this.tipoPublicos = tipoPublicos;
	}

	public List<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

	public List<Preferencia> getPreferencias() {
		return preferencias;
	}

	public void setPreferencias(List<Preferencia> preferencias) {
		this.preferencias = preferencias;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Preferencia getPreferencia() {
		return preferencia;
	}

	public void setPreferencia(Preferencia preferencia) {
		this.preferencia = preferencia;
	}

	public TipoEvento getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(TipoEvento tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public TipoPublico getTipoPublico() {
		return tipoPublico;
	}

	public void setTipoPublico(TipoPublico tipoPublico) {
		this.tipoPublico = tipoPublico;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public void generarPdf() throws SQLException, ParseException, JRException  {
		generarReporte();
	}

	public void generarReporte() throws SQLException, ParseException, JRException{

		try {
			Class.forName("org.postgresql.Driver");
			// para la conexion con la BD
			conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
		} catch (ClassNotFoundException ex) {

			ex.printStackTrace();
		}

		parameterMap.put("tevento", this.getTipoEvento().getNombreTipoEvento());
		parameterMap.put("tpublico", this.getTipoPublico().getNombreTipoPublico());
		parameterMap.put("area", this.getArea().getNombreArea());
		parameterMap.put("subcategoria", this.getPreferencia());
		parameterMap.put("fd", this.getFechaDesde());
		parameterMap.put("fh", this.getFechaHasta());
		parameterMap.put("estado", this.getEstado().getIdEstado());
		parameterMap.put("dirbase", this.rutabase);
		archivoJasper = null;
		JasperReport jasperReport = JasperCompileManager.compileReport(this.pathProyecto);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);
		if (jasperPrint.getPages().isEmpty()) {
			Messagebox.show("No se han encontrado datos para generar el reporte", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}else{
			archivoJasper = new AMedia(rutaPdf, "pdf" ,"application/pdf", JasperExportManager.exportReportToPdf(jasperPrint));
		}

	}


	@Command
	public void abrirModal(@ContextParam(ContextType.VIEW) Component view) throws SQLException, ParseException, JRException {
		parameterMap.put("ruta", patch);		
		Map<String, Object> args = new HashMap<String, Object>();
		generarPdf();
if (archivoJasper != null) {
	args.put("reporte", archivoJasper);
	final Window component = (Window) Executions.getCurrent().createComponents("content/modalreporteEstructuradoE.zul", view, args);
	component.setAction("show: slideDown;hide: slideUp");
	component.doModal();
}
		
	}

	@Command
	public void generarFormato() throws SQLException, FileNotFoundException, JRException{

		generarFormatoReporte();
	}

	public void generarFormatoReporte() throws SQLException, FileNotFoundException, JRException {
		try {
			//System.out.print("entre");
			Class.forName("org.postgresql.Driver");
			// para la conexion con la bd
			conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		parameterMap.put("tevento", this.getTipoEvento().getNombreTipoEvento());
		parameterMap.put("tpublico", this.getTipoPublico().getNombreTipoPublico());
		parameterMap.put("area", this.getArea().getNombreArea());
		parameterMap.put("subcategoria", this.getPreferencia());
		parameterMap.put("fd", this.getFechaDesde());
		parameterMap.put("fh", this.getFechaHasta());
		parameterMap.put("estado", this.getEstado().getIdEstado());

		JasperReport jasperReport = JasperCompileManager.compileReport(	this.patchNoEstructurado);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);
		if (jasperPrint.getPages().isEmpty()) {
			Messagebox.show("No se han encontrado datos para generar el reporte", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}else{
			JRExporter exporter = new JRTextExporter();
			exporter.setParameter(JRTextExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, System.getProperty("user.home") + "/reportes/eventos.txt");
			exporter.setParameter(JRTextExporterParameter.PAGE_WIDTH,170);
			exporter.setParameter(JRTextExporterParameter.PAGE_HEIGHT,170);
			exporter.exportReport();

			FileInputStream input = new FileInputStream(System.getProperty("user.home") + "/reportes/eventos.txt");
			Filedownload.save(input, "txt", "eventos.txt");	 
		}
		   

	}



}
