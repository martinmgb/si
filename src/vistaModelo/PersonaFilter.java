package vistaModelo;

public class PersonaFilter {
	private String id="";
	private String cedula="";
	private String nombre="";
	private String apellido="";
	private String sexo="";
	private String fechaN="";
	private String lugarN="";
	private String estadoCiv="";
	private String tlfCas="";
	private String direccion="";
	private String correo="";
	private String profesion="";
	private String cargo="";
	private String estado="";
	private String tlfCel="";
	private String direcPersona="";
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id==null?"":id.trim();
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo==null?"":sexo.trim();
	}

	public String getFechaN() {
		return fechaN;
	}

	public void setFechaN(String fechaN) {
		this.fechaN = fechaN==null?"":fechaN.trim();
	}

	public String getLugarN() {
		return lugarN;
	}

	public void setLugarN(String lugarN) {
		this.lugarN = lugarN==null?"":lugarN.trim();
	}

	public String getEstadoCiv() {
		return estadoCiv;
	}

	public void setEstadoCiv(String estadoCiv) {
		this.estadoCiv = estadoCiv==null?"":estadoCiv.trim();
	}

	public String getTlfCas() {
		return tlfCas;
	}

	public void setTlfCas(String tlfCas) {
		this.tlfCas = tlfCas==null?"":tlfCas.trim();
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion==null?"":direccion.trim();
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo==null?"":correo.trim();
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion==null?"":profesion.trim();
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo==null?"":cargo.trim();
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado==null?"":estado.trim();
	}

	public void setCedula(String cedula) {
		this.cedula = cedula==null?"":cedula.trim();
	}


	


	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido==null?"":apellido.trim();
	}
	public void setTlfCel(String tlfCel) {
		this.tlfCel = apellido==null?"":tlfCel.trim();
	}
	 
	
	public void setDirecPersona(String direcPersona) {
		this.direcPersona = direcPersona;
	}
	
	public String getCedula() {
		return cedula;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public String getTlfCel() {
		return tlfCel;
	}
	public String getDirecPersona() {
		return direcPersona;
	}
	

	

}
