package vistaModelo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JOptionPane;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

 
//PERSONA
import modelo.entidad.Acompanante;
import modelo.hibernate.dao.DaoAcompanante;
import modelo.hibernate.dao.DaoPersona;
import modelo.hibernate.dao.DaoVisita;
import modelo.hibernate.dao.DaoVisita_Acomp;
import modelo.hibernate.dao.DaoAcompanante;
import modelo.entidad.Accion;
import modelo.entidad.Parentesco;
import modelo.entidad.Persona;
import modelo.entidad.TipoPersona;
import modelo.entidad.Visita;
import modelo.entidad.Visita_acomp;
 
public class AcompananteViewModel extends GenericForwardComposer<Window> {
   	private static final String footerMessage = "Total de %d Miembros ";
    
    	private DaoAcompanante dao = new DaoAcompanante();
    	private DaoVisita_Acomp daoVisiAcom = new DaoVisita_Acomp();
    	private DaoVisita daoVisita = new DaoVisita();
//    	private List<Accion> todosTipos;
    	private List<Acompanante> todosTipos; // Para obtner las personas q se le asignaran acciones
    	private List<Acompanante> acompanantes; // para llenar el campo cedula
    	private boolean displayEdit = true;
    	private static final long serialVersionUID = 1L;
    	private boolean isEdit,isCreate,isView = false;
    //	private AcompananteFilter visitaFilter = new AcompananteFilter();
    	private Acompanante acompanante; 
    	private DaoAcompanante daoacom = new DaoAcompanante();
    	
    	private Visita_acomp visita_acomp,visita_acomp1,visita_acomp2;
    	 private Visita visita;
    	 private String cedulabuscar;
        
    	@Wire
    	Window nroAccion;
    	
    	@Wire
    	Window fechaIngreso;
    	@Wire
    	Window modalDialog2;
    	@Wire
    	public Label message;
    	@Wire
    	public Div box;
    	private Div contenido;

    	private Div contenedor;

    	private String pagina;

    	public boolean isDisplayEdit() {
    		return displayEdit;
    	}

    	
    	
    	
    	public AcompananteViewModel() {
    		super();
    		this.visita_acomp = new Visita_acomp();
    		visita_acomp1 = new Visita_acomp();
    		visita_acomp2 = new Visita_acomp();
    	 
        	try {
    			todosTipos = dao.obtenerTodos();
    			this.acompanante = new Acompanante();
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}


    	public List<Acompanante> getObtenerTipos() {
    		return todosTipos;
    	}

//    	public AcompananteFilter getAcompananteFilter() {
//    		return acompananteFilter;
//    	}

    	public Acompanante getAcompanante() {
			return acompanante;
		}
 
		public void setAcompanante(Acompanante acompanante) {
			this.acompanante = acompanante;
		}




		public String getCedulabuscar() {
			return cedulabuscar;
		}




		public void setCedulabuscar(String cedulabuscar) {
			this.cedulabuscar = cedulabuscar;
		}




		public String getFooter() {
    		return String.format(footerMessage, todosTipos.size());
    	}

//    	@Command
//    	@NotifyChange({"obtenerTipos", "footer"})
//    	public void changeFilter() {
//    		todosTipos = getFilterTiposAcompanante(acompananteFilter);
//    	}
//
//
//    	public List<Acompanante> getFilterTiposAcompanante(AcompananteFilter tipoFilter) {
//
//    		List<Acompanante> someTipos = new ArrayList<Acompanante>();
//    		String ced = tipoFilter.getCedula().toLowerCase();
//    		 List<Acompanante> tiposAll = null;
//    		try {
//    			tiposAll = dao.obtenerTodos();
//    		} catch (Exception e) {
//    			// TODO Auto-generated catch block
//    			e.printStackTrace();
//    		} 
//
//    		for (Iterator<Acompanante> i = tiposAll.iterator(); i.hasNext();) {
//    			Acompanante tmp = i.next();
//    			if (tmp.getPersona().getCedPersona().toLowerCase().contains(ced)  ) {
//    				someTipos.add(tmp);
//    			}
//    		}
//    		return someTipos;
//    	}
    	
    	
    	
//    	public void onClick$btnRegistrar() {
//
//    		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
//    			box.setVisible(true);
//    			message.setValue("Debe llenar todos los campos");
//    		}
		
// REGISTRA VISITA_ACOMPAÑANTE, Y ACOMPAÑANTE		
	@Command
	public void RegistrarAcompanante(@BindingParam("Visita") Visita visita,@BindingParam("win") Window win) throws Exception { 
	 String estado="A";
	 //DATOS DEL ACOMPAÑANTE
	 acompanante.setEditingStatus(false);
	 acompanante.setEstadoAcompanante(estado);
	 //DATOS DE LA VISITA_ACOMPAÑANTE
	 visita_acomp.setAcompanante(acompanante);
	 visita_acomp.setVisita(visita);
	 // AGREGO EN LAS TABLAS
	 if(acompanante.getIdAcompanante()==0){
	 this.dao.agregarAcompanante(acompanante);}
	 this.daoVisiAcom.agregarVisita_acomp(visita_acomp);
	 BindUtils.postGlobalCommand(null, null, "RefrescarTablaAcompanantes", null);
	 pagina = "vista/registrarVisita.zul";
	 Messagebox.show("Se ha Registrado correctamente", "Information", Messagebox.OK, Messagebox.INFORMATION);
			 
	 win.detach(); 
	} 
	
	// BUSCA LA CEDULA DE UN MIEMBRO
		@Command
		@NotifyChange({"acompanante","cedulabuscar"})
		public void BuscarCedula(@BindingParam("ObjVisita") Visita visitas) throws Exception{
			boolean mensaje=false;
			 
			acompanantes = dao.obtenerTodos();
			if(this.acompanante.getCedAcompanante()==null){
				Messagebox.show("Ingrese una Cedula!", 
						"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
				mensaje = true;	
			}
			else{
				for (int i = 0; i < acompanantes.size(); i++) {
					if(this.acompanante.getCedAcompanante().equals(acompanantes.get(i).getCedAcompanante()) ){
						this.acompanante = acompanantes.get(i);
//						Messagebox.show("Ha excedido número de visita al club!", 
//								"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
						mensaje=true;
					}  					
//					if(this.visita_acomp.getAcompanante().getIdAcompanante() ==
//							visita_acomp.getVisita().getIdVisita()) {
//						Messagebox.show("Ha excedido número de visita al club!", 
//								"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
//						mensaje=true;
//
//					}
				}
				List<Visita> visi = daoVisita.obtenerVisitaxMiembroxMes(visitas.getPersona());
				List<Visita_acomp> viac = new ArrayList<Visita_acomp>() ;
				if (visi.size()!=0)
				{
					viac = daoVisiAcom.obtenerAcompananteXVisitaXMiembro(visi, acompanante);
					System.out.println(viac);
				}
				if(viac.size()>0)
				{
					acompanante = new Acompanante();
					Messagebox.show("Ha excedido número de visita al club!", 
							"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
				}
			}
				if(mensaje==false){
				Messagebox.show("La ced<ula no Existe!", 
						"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);  
				 
			}
		 
		}
	
	
	
	
	
	
	
	
	
		//
//		
//    	public void onClick$btnLimpiar() {
//    		nroAccion.setText("");
//    		fechaIngreso.setText("");
//    		nombre.setFocus(true);
//    	}
//    	public void onClick$btnEditar() {
//
//    		System.out.println(id.getValue());
//    		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
//    			box.setVisible(true);
//    			message.setValue("Debe llenar todos los campos");
//    		}
//    		else {
//    			try {
//    				Accion objeto = dao.obtenerAccion(id.getValue());
//    				objeto.setNombreAccion(nombre.getValue());
//    				objeto.setDescAccion(descripcion.getValue());
//    				dao.modificarAccion(objeto);
//    				modalDialog.detach();
//    				pagina = "vista/parentesco.zul";
//    				contenido.getChildren().clear();
//    				contenedor = (Div) Executions.createComponents(pagina, null, null);
//    				contenido.appendChild(contenedor);
//    			}
//    			catch (Exception x) {
//    				// TODO Auto-generated catch block
//    				x.printStackTrace();
//    			}
//    		}
//    	}

//    	@Command
//    	public void changeEditable(@BindingParam("editarAccion") Accion tip) {
//    		HashMap accion = new HashMap();
//    		accion.put("objAccion", tip);
//    		this.isEdit = true;
//    		this.isCreate = false;
//    		accion.put("edit", this.isEdit);
//    		accion.put("create", this.isCreate);
//    		Window window = (Window) Executions.createComponents(
//    				"content/modalParentesco.zul", null, accion);
//    		window.doModal();
//    	}

    	@NotifyChange({"obtenerTipos", "displayEdit"})
    	public void setDisplayEdit(boolean displayEdit) {
    		this.displayEdit = displayEdit;
    	}

    	@Command
    	@NotifyChange({"obtenerTipos", "footer"})
    	public void changeEditableStatus(@BindingParam("visita") Acompanante tipo) {
    		Messagebox.show("¿Esta seguro de eliminar el registro?", 
    				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
    				Messagebox.QUESTION,
    				new org.zkoss.zk.ui.event.EventListener(){
    			public void onEvent(Event e){
    				if(Messagebox.ON_OK.equals(e.getName())){
    					//OK is clicked
    					try {
    						todosTipos.remove(tipo);
    						dao.eliminarAcompanante(tipo);


    					} catch (Exception x) {
    						// TODO Auto-generated catch block
    						x.printStackTrace();
    					}
    					refreshRowTemplate(tipo);
    				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
    					//Cancel is clicked
    				}
    			}
    		}
    				);

    	}
    	

    	@Command
    	@NotifyChange({"obtenerTipos", "footer"})
    	public void confirm(@BindingParam("visita") Acompanante tipo) {
    		changeEditableStatus(tipo);
    		try {
    			todosTipos.remove(tipo);
    			dao.eliminarAcompanante(tipo);
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		refreshRowTemplate(tipo);
    	}

    	public void refreshRowTemplate(Acompanante tipo) {
    		/*
    		 * This code is special and notifies ZK that the bean's value
    		 * has changed as it is used in the template mechanism.
    		 * This stops the entire Grid's data from being refreshed
    		 */
    		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
    	}
  	
  	  @Command({"visitaModal"}) @GlobalCommand("visitaModal")
  		    public void abrirModalRegistrarAcompanante(Event e) {
  			Window window = (Window)Executions.createComponents(
  					"content/modalAcompanante.zul", null, null);
  			window.doModal();
  		       
  		    }
 

 
}
