package vistaModelo;	
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import modelo.entidad.TipoIncidencia;
import modelo.entidad.TipoIndicador;
import modelo.hibernate.dao.DaoTipoIndicador;


public class TipoIndicadorViewModel extends GenericForwardComposer<Window> { 

	private static final String footerMessage = "Total de %d Tipo(s) de Indicador ";
	private TipoIndicadorFilter IndicadorFilter = new TipoIndicadorFilter();
	private DaoTipoIndicador dao = new DaoTipoIndicador();
	private List<TipoIndicador> todosTipos;
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;
	
	///////////////////////////////////////////////Para Boton Imprimir///////////////////////////////////////////////////////////
	private AMedia archivoJasper;
	private String rutaPdf;
	String pathPdfProyecto;
	private String pathProyecto;
	String patchZulProyecto;
	String rutaZul;
	Map<String,Object> parameterMap = new HashMap<String,Object>();
	private String rutabase;
	private Connection conexion;
	String patch = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	@Wire
	private Intbox id;
	@Wire
	private Textbox nombre;
	@Wire
	private Textbox descripcion;
	@Wire
	Window modalDialog;
	@Wire
	public Label message;
	@Wire
	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public TipoIndicadorViewModel() {
		super();
		try {
			todosTipos = dao.obtenerTodos();
			//////////////////////////////////////////////////////////IMPRIMIR//////////////////////////////////////////////////////////////////
			File file= new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			this.pathProyecto= file.getParentFile().getParentFile().getPath();
			///////////////////////////////////////////////////////////////////////////////
			int i,f;
			f=this.pathProyecto.length();
			i=f-7;
			String z=this.pathProyecto.substring(i, f);
			this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
			if(z.compareTo("classes")==0){
				this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().getPath();
				this.rutabase=file.getParentFile().getParentFile().getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
			}
			System.out.print("Z: "+z+" F: "+"classes");
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////
			String ruta = this.pathProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "tipoindicador.jrxml";
			System.out.print("RUTA: "+ruta);
			File filePdf = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			this.pathPdfProyecto = this.pathProyecto;
			rutaPdf = this.pathPdfProyecto + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "tipoindicador.pdf";

			File fileZul = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			this.patchZulProyecto = this.pathProyecto;
			rutaZul = this.patchZulProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"content"+ System.getProperty("file.separator") + "modalreporteEstructurado.zul";

			//Sustitucion de caracteres codificados (Para servidores con Windows)
			this.pathProyecto= ruta.replaceAll("%20", " ");
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	public List<TipoIndicador> getObtenerTipos() {
		return todosTipos;
	}

	public TipoIndicadorFilter getIndicadorFilter() {
		return IndicadorFilter;
	}

	public String getFooter() {
		return String.format(footerMessage, todosTipos.size());
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeFilter() {
		todosTipos = getFilterTipos(IndicadorFilter);
	}



	public List<TipoIndicador> getFilterTipos(TipoIndicadorFilter tipoFilter) {

		List<TipoIndicador> someTipos = new ArrayList<TipoIndicador>();
		String nombre = tipoFilter.getNombre().toLowerCase();
		String des = tipoFilter.getDescripcion().toLowerCase();
		List<TipoIndicador> tiposAll = null;
		try {
			tiposAll = dao.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<TipoIndicador> i = tiposAll.iterator(); i.hasNext();) {
			TipoIndicador tmp = i.next();
			if (tmp.getNombreTipoInd().toLowerCase().contains(nombre) && tmp.getDescTipoInd().toLowerCase().contains(des)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
	public void onClick$btnRegistrar() {

		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		}
		else {
			try {
				TipoIndicador tipoIndicador = new TipoIndicador();
				tipoIndicador.setNombreTipoInd(nombre.getText());
				tipoIndicador.setDescTipoInd(descripcion.getText());
				tipoIndicador.setEstadoTipoInd("A");
				dao.agregarTipoIndicador(tipoIndicador);
				Messagebox.show("Se ha registrado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/tipoIndicador.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void onClick$btnLimpiar() {
		nombre.setText("");
		descripcion.setText("");
		nombre.setFocus(true);
	}
	public void onClick$btnEditar() {

		System.out.println(id.getValue());
		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		}
		else {
			try {
				TipoIndicador objeto = dao.obtenerTipoIndicador(id.getValue());
				objeto.setNombreTipoInd(nombre.getValue());
				objeto.setDescTipoInd(descripcion.getValue());
				dao.modificarTipoIndicador(objeto);
				Messagebox.show("Se ha modificado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/tipoIndicador.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
			catch (Exception x) {
				// TODO Auto-generated catch block
				x.printStackTrace();
			}
		}
	}

	@Command
	public void changeEditable(@BindingParam("editarTipoIndicador") TipoIndicador tip) {
		HashMap tipoIndicador = new HashMap();
		tipoIndicador.put("objTipoIndicador", tip);
		this.isEdit = true;
		this.isCreate = false;
		tipoIndicador.put("edit", this.isEdit);
		tipoIndicador.put("create", this.isCreate);
		Window window = (Window) Executions.createComponents(
				"content/modalTipoIndicador.zul", null, tipoIndicador);
		window.doModal();
	}

	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeEditableStatus(@BindingParam("tipoIndicador") TipoIndicador tipo) {
		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Interrogativo", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {

						if(dao.eliminarTipoIndicador(tipo)){
							todosTipos.remove(tipo);
							Messagebox.show("Se ha eliminado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);

						}else{
							Messagebox.show("No se puede eliminar el registro", "Información", Messagebox.OK, Messagebox.INFORMATION);

						}
					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(tipo);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}
			private void refreshRowTemplate(TipoIndicador tipoIndicador) {
				//Metodo Para Refrescar la Tabla cuando elimina
				BindUtils.postNotifyChange(null, null, TipoIndicadorViewModel.this, "obtenerTipos");
				BindUtils.postNotifyChange(null, null, TipoIndicadorViewModel.this, "footer");
			}
		}
				);

	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void confirm(@BindingParam("tipoIndicador") TipoIndicador tipo) {
		changeEditableStatus(tipo);
		try {
			todosTipos.remove(tipo);
			dao.eliminarTipoIndicador(tipo);
			Messagebox.show("Se ha eliminado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(tipo);
	}

	public void refreshRowTemplate(TipoIndicador tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}
	///////////////////////////////////////////////METODOS PARA BOTON IMPRIMIR///////////////////////////////////////////////////////

	public void generarPdf() throws SQLException, ParseException, JRException  {
		generarReporte();
	}

	public void generarReporte() throws SQLException, ParseException, JRException{

		try {
			Class.forName("org.postgresql.Driver");
			// para la conexion con la BD
			conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
		} catch (ClassNotFoundException ex) {

			ex.printStackTrace();
		}

		parameterMap.put("dirbase", this.rutabase);

		JasperReport jasperReport = JasperCompileManager.compileReport(this.pathProyecto);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);


		archivoJasper = new AMedia(rutaPdf, "pdf" ,"application/pdf", JasperExportManager.exportReportToPdf(jasperPrint));

	}


	@Command
	public void abrirModal(@ContextParam(ContextType.VIEW) Component view) throws SQLException, ParseException, JRException {
		parameterMap.put("ruta", patch);		
		Map<String, Object> args = new HashMap<String, Object>();
		generarPdf();

		args.put("reporte", archivoJasper);
		final Window component = (Window) Executions.getCurrent().createComponents("content/modalImprimir.zul", view, args);
		component.setAction("show: slideDown;hide: slideUp");
		component.doModal();
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}