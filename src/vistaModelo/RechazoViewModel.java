package vistaModelo;	
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import controlador.ManejadorMail;
import modelo.entidad.Area;
import modelo.entidad.Persona;
import modelo.entidad.Rechazo;
import modelo.entidad.TipoArea;
import modelo.entidad.TipoPersona;
import modelo.entidad.TipoRechazo;
import modelo.hibernate.dao.DaoRechazo;
import modelo.hibernate.dao.DaoTipoPersona;
import modelo.hibernate.dao.DaoTipoRechazo;

import modelo.entidad.Postulacion;
import modelo.hibernate.dao.DaoPersona;
import modelo.hibernate.dao.DaoPostulacion; 

public class RechazoViewModel extends GenericForwardComposer<Window> { 


	private DaoRechazo dao = new DaoRechazo();
	private DaoPostulacion postu = new DaoPostulacion();
	private DaoTipoPersona daot = new DaoTipoPersona();
	private DaoPersona daope = new DaoPersona();
	private List<Rechazo> todosTipos;
	private List<Postulacion> todosTiposPos;
	
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;
	 List<TipoRechazo> todosTiposRechazo;
	 private DaoTipoRechazo daoTipoRechazo = new DaoTipoRechazo();
	//RECHAZO
			private Rechazo rechazo = new Rechazo();
			private Postulacion postulacion = new Postulacion();
	//TIPO RECHAZO
			private TipoRechazo tipoRechazoselect;

	@Wire
	private Intbox idar;
	 
	@Wire
	private Combobox comboTipoRechazo;
	@Wire
	private Textbox descripcionRechazo;
	@Wire
	Window modalDialogRm;
	@Wire
	Window modalDialog2;
	@Wire
	public Label message;
	@Wire
	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;

	public boolean isDisplayEdit() {
		return displayEdit;
	}
	
	
	// para cargar los combos
		public List<TipoRechazo> getAllTiporechazo() {
		     try {
				return daoTipoRechazo.obtenerTodos(); 
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		  }
	
	
	

	public RechazoViewModel() {
		super();
		tipoRechazoselect = new TipoRechazo();
		try {
			todosTipos = dao.obtenerTodos();
			todosTiposPos = postu.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//METODO PARA GUARDAR EL COMBO
	public TipoRechazo getTipoRechazoselect() {
		return tipoRechazoselect;
	}

	public void setTipoRechazoselect(TipoRechazo tipoRechazoselect) {
		this.tipoRechazoselect = tipoRechazoselect;
	}
//


	public List<Rechazo> getObtenerTipos() {
		return todosTipos;
	}


	 
	// REGISTRA EL MODAL DEL RECHAZO DE LA SOLICITUD DE MENBRESIA
	public void onClick$btnRegistrar() {
	System.out.println("entro 1");
		if((comboTipoRechazo.getValue().equals("")) || (descripcionRechazo.getValue().equals(""))) {
			message.setValue("Debe llenar todos los campos");
		}
	 
		else {
			try {
				//tipoRechazoselect = comboTipoRechazo.getSelectedItem().getValue();
			 
				Rechazo rech= new Rechazo();		
				rech.setTipoRechazo(comboTipoRechazo.getSelectedItem().getValue());  // idTipoRechazo	
				rech.setDescRechazo(descripcionRechazo.getText()); // descripcion del rechazo
				rech.setEstadoRechazo("A");	
				rech.setEditingStatus(false);
				Postulacion objPostulacion =  (Postulacion) arg.get("objeto");
				rech.setPostulacion(objPostulacion);
				objPostulacion.setEstadoPostulacion("R");
				 
//				TipoPersona tipo = daot.obtenerTipoPersona(1) 
//				Persona persona = postulacion.getPersona();
//				persona.setTipoPersona(tipo);
//    			persona.setEstadoPersona("A");
    			
			 
				
				dao.agregarRechazo(rech);
			//	daope.modificarPersona(persona);
				Messagebox.show("La solicitud ha sido rechazada", "Información", Messagebox.OK, Messagebox.INFORMATION);
					
				modalDialogRm.detach();
				modalDialog2.detach();
				pagina = "vista/atenderPostulacionMembresia.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
				String mensaje, destinatario, asunto;
				mensaje = "Sr(a) " + rech.getPostulacion().getPersona().getNombrePersona()+ " " + rech.getPostulacion().getPersona().getApellidoPersona()
						+ " El presente correo es para informarle que su Solicitud de Membresía no fue aceptada."
						 
						+ "Recuerda que Teide es tu Socio Ideal.";
				destinatario = rech.getPostulacion().getPersona().getCorreoPersona();
				asunto = "Solicitud Rechazada";
				ManejadorMail.enviarEmail(mensaje, destinatario, asunto);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	} 

/*
	public List<TipRechazo> getFilterTipos(TipoRechazoFilter tipoFilter) {

		List<TipoRechazo> someTipos = new ArrayList<TipoRechazo>();
		String nombre = tipoFilter.getNombre().toLowerCase();
		String des = tipoFilter.getDescripcion().toLowerCase();
		List<TipoRechazo> tiposAll = null;
		try {
			tiposAll = dao.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<TipoRechazo> i = tiposAll.iterator(); i.hasNext();) {
			TipoRechazo tmp = i.next();
			if (tmp.getNombreTipoRechazo().toLowerCase().contains(nombre) && tmp.getDescTipoRechazo().toLowerCase().contains(des)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}*/
	 
 
	
	

	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeEditableStatus(@BindingParam("Rechazo")Rechazo tipo) {
		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						todosTipos.remove(tipo);
						dao.eliminarRechazo(tipo);
					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(tipo);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}
			private void refreshRowTemplate(Rechazo rechazo) {
				//Metodo Para Refrescar la Tabla cuando elimina
				BindUtils.postNotifyChange(null, null, RechazoViewModel.this, "obtenerTipos");
				BindUtils.postNotifyChange(null, null, RechazoViewModel.this, "footer");
			}
		}
				);

	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void confirm(@BindingParam("Rechazo") Rechazo tipo) {
		changeEditableStatus(tipo);
		
		try {
			todosTipos.remove(tipo);
			dao.eliminarRechazo(tipo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(tipo);
	}

	public void refreshRowTemplate(Rechazo tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}
	
	

	
	
	
	
	
	
	
	
}