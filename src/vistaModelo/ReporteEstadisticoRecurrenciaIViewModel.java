package vistaModelo;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modelo.entidad.Area;
import modelo.entidad.TipoArrendamiento;
import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoTipoArrendamiento;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

public class ReporteEstadisticoRecurrenciaIViewModel extends GenericForwardComposer {


	
	String patch = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");

	
	private Connection conexion;
	@Wire
	private Combobox  comboTipoArrenda;

	private Date fechaDesde;

	private Date fechaHasta;

	private AMedia archivoJasper;
		
	private String rutaPdf;
	
	String pathPdfProyecto;
	
	private String pathProyecto;
	
	String patchZulProyecto;
	
	String rutaZul;
	
	Map<String,Object> parameterMap = new HashMap<String,Object>();

	private String rutabase;
	private String pathSubreport;

	@Init
	public void init() {
	
		
		setFechaDesde(new Date());
		setFechaHasta(new Date());
//////////////////////////////////////////////////////////IMPRIMIR//////////////////////////////////////////////////////////////////
File file= new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathProyecto= file.getParentFile().getParentFile().getPath();
///////////////////////////////////////////////////////////////////////////////
int i,f;
f=this.pathProyecto.length();
i=f-7;
String z=this.pathProyecto.substring(i, f);
this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
if(z.compareTo("classes")==0){
this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().getPath();
this.rutabase=file.getParentFile().getParentFile().getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
}
System.out.print("Z: "+z+" F: "+"classes");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
String ruta = this.pathProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"estadisticos"+ System.getProperty("file.separator") + "reporteincidenciasestadistico.jrxml";
System.out.print("RUTA: "+ruta);
File filePdf = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.pathPdfProyecto = this.pathProyecto;
rutaPdf = this.pathPdfProyecto + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"estadisticos"+ System.getProperty("file.separator") + "reporteIncidenciasEstadistico.pdf";

File fileZul = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
this.patchZulProyecto = this.pathProyecto;
rutaZul = this.patchZulProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"content"+ System.getProperty("file.separator") + "modalreporteEstructurado.zul";

//Sustitucion de caracteres codificados (Para servidores con Windows)
this.pathProyecto= ruta.replaceAll("%20", " ");
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	}

	public Date getFechaDesde() {
		return fechaDesde;
	}


	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}


	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public String getPathProyecto() {
		return pathProyecto;
	}

	public Map<String, Object> getParameterMap() {
		return parameterMap;
	}


	public void setParameterMap(Map<String, Object> parameterMap) {
		this.parameterMap = parameterMap;
	}

	public void setPathProyecto(String pathProyecto) {
		this.pathProyecto = pathProyecto;
	}

	public void generarPdf() throws SQLException, ParseException, JRException  {
		generarReporte();
	}

	public void generarReporte() throws SQLException, ParseException, JRException{

		String desde = new SimpleDateFormat("YYYY-MM-dd").format(this.getFechaDesde());
		String hasta = new SimpleDateFormat("YYYY-MM-dd").format(this.getFechaHasta());

		SimpleDateFormat formatoFecha = new SimpleDateFormat("YYYY-MM-dd") ;
		SimpleDateFormat formatoFechaH=  new SimpleDateFormat("YYYY-MM-dd") ;
		Date fechaD = formatoFecha.parse(desde);
		Date fechaH = formatoFechaH.parse(hasta);

		try {
			System.out.print("entre");
			Class.forName("org.postgresql.Driver");
			// para la conexion con la BD
			conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
		} catch (ClassNotFoundException ex) {

			ex.printStackTrace();
		}

		archivoJasper = null;
		Map<String,Object> parameterMap = new HashMap<String,Object>();
		parameterMap.put("fechaDesde", this.getFechaDesde());
		parameterMap.put("fechaHasta", this.getFechaHasta());
		parameterMap.put("dirbase",this.rutabase);
		

		JasperReport jasperReport = JasperCompileManager.compileReport(this.pathProyecto);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);
		if (jasperPrint.getPages().isEmpty()) {
			Messagebox.show("No se han encontrado datos para generar el reporte", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}else{
			archivoJasper = new AMedia(rutaPdf, "pdf" ,"application/pdf", JasperExportManager.exportReportToPdf(jasperPrint));
		}

	}


	@Command
	public void abrirModal(@ContextParam(ContextType.VIEW) Component view) throws SQLException, ParseException, JRException {
		parameterMap.put("ruta", patch);		
		Map<String, Object> args = new HashMap<String, Object>();
		generarPdf();
	if (archivoJasper != null) {
		args.put("reporte", archivoJasper);
		final Window component = (Window) Executions.getCurrent().createComponents("content/modalreporteEstadisticoRecurrenciaI.zul", view, args);
		component.setAction("show: slideDown;hide: slideUp");
		component.doModal();
	}
		
	}






 
}
