package vistaModelo;	
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import modelo.entidad.MotivoDesvinculacion;
import modelo.entidad.MotivoDesvinculacion;
import modelo.hibernate.dao.DaoMotivo;
import modelo.hibernate.dao.DaoMotivoDesvinculacion;


public class MotivoDesvinculacionViewModel extends GenericForwardComposer<Window> { 

	private static final String footerMessage = "Total de %d Motivo(s) ";
	private MotivoFilter motivodesFilter = new MotivoFilter();
	private DaoMotivoDesvinculacion dao = new DaoMotivoDesvinculacion();
	private List<MotivoDesvinculacion> todosTipos;
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;
	///////////////////////////////////////////////Para Boton Imprimir///////////////////////////////////////////////////////////
	private AMedia archivoJasper;
	private String rutaPdf;
	String pathPdfProyecto;
	private String pathProyecto;
	String patchZulProyecto;
	String rutaZul;
	Map<String,Object> parameterMap = new HashMap<String,Object>();
	private String rutabase;
	private Connection conexion;
	String patch = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	@Wire
	private Intbox id;
	@Wire
	private Textbox nombre;
	@Wire
	private Textbox descripcion;
	@Wire
	Window modalDialog;
	@Wire
	public Label message;
	@Wire
	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public MotivoDesvinculacionViewModel() {
		super();
		try {
			todosTipos = dao.obtenerTodos();
			/////////////////////////////////////////////////////////IMPRIMIR//////////////////////////////////////////////////////////////////
			File file= new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			this.pathProyecto= file.getParentFile().getParentFile().getPath();
			String ruta = this.pathProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "tipopersona.jrxml";

			File filePdf = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			this.pathPdfProyecto = filePdf.getParentFile().getParentFile().getPath();
			rutaPdf = this.pathPdfProyecto + System.getProperty("file.separator") +"WebContent"+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"reportes"+ System.getProperty("file.separator")+"imprimir"+ System.getProperty("file.separator") + "tipopersona.pdf";

			File fileZul = new File(ReporteArrendamientoViewModel.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			this.patchZulProyecto = fileZul.getParentFile().getParentFile().getPath();
			rutaZul = this.patchZulProyecto + System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator")+"content"+ System.getProperty("file.separator") + "modalreporteEstructurado.zul";

			//Sustitucion de caracteres codificados (Para servidores con Windows)
			this.pathProyecto= ruta.replaceAll("%20", " ");
			this.rutabase=file.getParentFile().getParentFile().getPath()+ System.getProperty("file.separator") +"appWeb" + System.getProperty("file.separator");
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public List<MotivoDesvinculacion> getObtenerTipos() {
		return todosTipos;
	}

	public MotivoFilter getMotivoFilter() {
		return motivodesFilter;
	}

	public String getFooter() {
		return String.format(footerMessage, todosTipos.size());
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeFilter() {
		todosTipos = getFilterTipos(motivodesFilter);
	}


	public List<MotivoDesvinculacion> getFilterTipos(MotivoFilter tipoFilter) {

		List<MotivoDesvinculacion> someTipos = new ArrayList<MotivoDesvinculacion>();
		String nombre = tipoFilter.getNombre().toLowerCase();
		String des = tipoFilter.getDescripcion().toLowerCase();
		List<MotivoDesvinculacion> tiposAll = null;
		try {
			tiposAll = dao.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<MotivoDesvinculacion> i = tiposAll.iterator(); i.hasNext();) {
			MotivoDesvinculacion tmp = i.next();
			if (tmp.getNombreMotivoDesvinc().toLowerCase().contains(nombre) && tmp.getDescMotivoDesvinc().toLowerCase().contains(des)) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}
	public void onClick$btnRegistrar() {

		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		}
		else {
			try {
				MotivoDesvinculacion MotivoDesvinculacion= new MotivoDesvinculacion();
				MotivoDesvinculacion.setNombreMotivoDesvinc(nombre.getText());
				MotivoDesvinculacion.setDescMotivoDesvinc(descripcion.getText());
				MotivoDesvinculacion.setEstadoMotivoDesvinc("A");
				dao.agregarMotivoDesvinculacion(MotivoDesvinculacion);
				Messagebox.show("Se ha Registrado Correctamente", "Information", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/motivoDesvinculacion.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void onClick$btnLimpiar() {
		nombre.setText("");
		descripcion.setText("");
		nombre.setFocus(true);
	}
	public void onClick$btnEditar() {

		System.out.println(id.getValue());
		if((nombre.getValue().equals("")) || (descripcion.getValue().equals(""))) {
			box.setVisible(true);
			message.setValue("Debe llenar todos los campos");
		}
		else {
			try {
				MotivoDesvinculacion objeto = dao.obtenerMotivoDesvinculacion(id.getValue());
				objeto.setNombreMotivoDesvinc(nombre.getValue());
				objeto.setDescMotivoDesvinc(descripcion.getValue());
				dao.modificarMotivoDesvinculacion(objeto);
				Messagebox.show("Se ha Modificado Correctamente", "Information", Messagebox.OK, Messagebox.INFORMATION);
				modalDialog.detach();
				pagina = "vista/motivoDesvinculacion.zul";
				contenido.getChildren().clear();
				contenedor = (Div) Executions.createComponents(pagina, null, null);
				contenido.appendChild(contenedor);
			}
			catch (Exception x) {
				// TODO Auto-generated catch block
				x.printStackTrace();
			}
		}
	}

	@Command
	public void changeEditable(@BindingParam("editarMotivo") MotivoDesvinculacion tip) {
		HashMap MotivoDesvinculacion = new HashMap();
		MotivoDesvinculacion.put("objMotivodes", tip);
		this.isEdit = true;
		this.isCreate = false;
		MotivoDesvinculacion.put("edit", this.isEdit);
		MotivoDesvinculacion.put("create", this.isCreate);
		Window window = (Window) Executions.createComponents(
				"content/modalMotivoDesvinculacion.zul", null, MotivoDesvinculacion);
		window.doModal();
	}

	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeEditableStatus(@BindingParam("MotivoDesvinculacion") MotivoDesvinculacion tipo) {
		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						todosTipos.remove(tipo);
						dao.eliminarMotivoDesvinculacion(tipo);
						Messagebox.show("!Se ha Eliminado Correctamente", "Information", Messagebox.OK, Messagebox.INFORMATION);



					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(tipo);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}
			private void refreshRowTemplate(MotivoDesvinculacion MotivoDesvinculacion) {
				//Metodo Para Refrescar la Tabla cuando elimina
				BindUtils.postNotifyChange(null, null, MotivoDesvinculacionViewModel.this, "obtenerTipos");
				BindUtils.postNotifyChange(null, null, MotivoDesvinculacionViewModel.this, "footer");
			}
		}
				);

	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void confirm(@BindingParam("MotivoDesvinculacion") MotivoDesvinculacion tipo) {
		changeEditableStatus(tipo);
		try {
			todosTipos.remove(tipo);
			dao.eliminarMotivoDesvinculacion(tipo);
			Messagebox.show("Se ha Eliminado Correctamente", "Information", Messagebox.OK, Messagebox.INFORMATION);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(tipo);
	}

	public void refreshRowTemplate(MotivoDesvinculacion tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}
	///////////////////////////////////////////////METODOS PARA BOTON IMPRIMIR///////////////////////////////////////////////////////

	public void generarPdf() throws SQLException, ParseException, JRException  {
		generarReporte();
	}

	public void generarReporte() throws SQLException, ParseException, JRException{

		try {
			Class.forName("org.postgresql.Driver");
			// para la conexion con la BD
			conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teideBD","postgres","postgres");
		} catch (ClassNotFoundException ex) {

			ex.printStackTrace();
		}

		parameterMap.put("dirbase", this.rutabase);

		JasperReport jasperReport = JasperCompileManager.compileReport(this.pathProyecto);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, conexion);


		archivoJasper = new AMedia(rutaPdf, "pdf" ,"application/pdf", JasperExportManager.exportReportToPdf(jasperPrint));

	}


	@Command
	public void abrirModal(@ContextParam(ContextType.VIEW) Component view) throws SQLException, ParseException, JRException {
		parameterMap.put("ruta", patch);		
		Map<String, Object> args = new HashMap<String, Object>();
		generarPdf();

		args.put("reporte", archivoJasper);
		final Window component = (Window) Executions.getCurrent().createComponents("content/modalImprimir.zul", view, args);
		component.setAction("show: slideDown;hide: slideUp");
		component.doModal();
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}