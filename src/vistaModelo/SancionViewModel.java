package vistaModelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timebox;
import org.zkoss.zul.Window;

import modelo.hibernate.dao.DaoArea;
import modelo.hibernate.dao.DaoArrendamiento;
import modelo.hibernate.dao.DaoPersona;
import modelo.hibernate.dao.DaoSancion;
import modelo.hibernate.dao.DaoTipoSancion;
import modelo.entidad.Accion;	  //ACCION
import modelo.entidad.Area;
import modelo.entidad.Area_arrend;
import modelo.entidad.Arrendamiento;
import modelo.entidad.Persona;    //PERSONA 
import modelo.entidad.Postulacion;
import modelo.entidad.Sancion;    //SANCION
import modelo.entidad.TipoIncidencia;
import modelo.entidad.TipoPersona;
import modelo.entidad.TipoSancion;



public class SancionViewModel extends GenericForwardComposer<Window> {

	private static final String footerMessage = "Total de %d Sancion(es) ";
	private static final String footerMessageSancionados = "Total de %d Miembro(s) Sancionado(s)";
	private SancionFilter sancionFilter = new SancionFilter();

	private List<Sancion> todosTipos, todosTiposS;
	List<TipoIncidencia> todosTiposIncidencia;
	List<Persona> personas;
	private boolean displayEdit = true;
	private static final long serialVersionUID = 1L;
	private boolean isEdit,isCreate,isView = false;
	private String cedulabuscar;
	private DaoSancion dao = new DaoSancion();
	private DaoPersona daope = new DaoPersona();
	private DaoTipoSancion daotisan = new DaoTipoSancion();

	private Persona persona;
	private Accion accion;
	private Sancion sancion;
	private Sancion sancion1;


	//ACCION
	//	private Accion accion = new Accion();

	//PERSONA

	public Sancion getSancion1() {
		return sancion1;
	}

	public void setSancion1(Sancion sancion1) {
		this.sancion1 = sancion1;
	}

	@Wire
	private Textbox descripcion;
	@Wire
	private Datebox fechaReactivacion, fechaSancion;
	@Wire
	private Timebox horaReactivacion;
	@Wire
	private Combobox comboTipoSancion;
	@Wire
	Window modalDialog;



	//private Persona persona;
	private boolean mensaje, mensaje1;

	public Div box;
	private Div contenido;

	private Div contenedor;

	private String pagina;

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public SancionViewModel() {
		super();
		try {
			todosTipos = dao.obtenerTodos(); // TRAE TODAS LAS SANCIONES
			this.sancion1 = new Sancion();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Init
	public void init(@ExecutionArgParam("objetoSancion") Sancion sancion) throws Exception {

		if (sancion == null) {
			todosTiposS = dao.obtenerPorSancion("A"); // TRAE LAS SANCIONES ACTIVAS
			this.sancion = new Sancion();
			this.persona = new Persona(); 



			//	this.listaAreaArrendamiento = new HashSet<Area_arrend>(); 
		}else
		{ this.sancion = sancion;


		}
		this.dao = new DaoSancion();


	}





	// para cargar los combos
	public List<TipoSancion> getAllTipoSancion() {
		try {
			return daotisan.obtenerTodos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}



	public String getCedulabuscar() {
		return cedulabuscar;
	}

	public void setCedulabuscar(String cedulabuscar) {
		this.cedulabuscar = cedulabuscar;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Accion getAccion() {
		return accion;
	}

	public void setAccion(Accion accion) {
		this.accion = accion;
	}
	public Sancion getSancion() {
		return sancion;
	}

	public void setSancion(Sancion sancion) {
		this.sancion = sancion;
	}


	public List<Sancion> getObtenerTipos() {
		return todosTipos;
	}

	public List<Sancion> getTodosTiposS() {
		return todosTiposS;
	}

	public void setTodosTiposS(List<Sancion> todosTiposS) {
		this.todosTiposS = todosTiposS;
	}

	public SancionFilter getSancionFilter() {
		return sancionFilter;
	} 

	public String getFooter() {
		return String.format(footerMessage, todosTipos.size());
	}
	public String getFooterSancionados() {
		return String.format(footerMessageSancionados, todosTiposS.size());
	}

	@Command
	@NotifyChange({"todosTiposS", "footerMessageSancionados"})
	public void changeFilterReactivar() {
		todosTiposS = getFilterTiposReactivar(sancionFilter);
	}


	public List<Sancion> getFilterTiposReactivar(SancionFilter tipoFilter) {

		List<Sancion> someTipos = new ArrayList<Sancion>();
		 String ced = tipoFilter.getCedula().toLowerCase();
		List<Sancion> tiposAll = null;
		try {
			tiposAll = dao.obtenerPorSancion("A");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		for (Iterator<Sancion> i = tiposAll.iterator(); i.hasNext();) {
			Sancion tmp = i.next();
			if (tmp.getPersona().getCedPersona().toLowerCase().contains(ced)  ) {
				someTipos.add(tmp);
			}
		}
		return someTipos;
	}


	@NotifyChange({"obtenerTipos", "displayEdit"})
	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void changeEditableStatus(@BindingParam("sancion") Sancion tipo) {
		Messagebox.show("¿Esta seguro de eliminar el registro?", 
				"Confirmación", Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener(){
			public void onEvent(Event e){
				if(Messagebox.ON_OK.equals(e.getName())){
					//OK is clicked
					try {
						todosTipos.remove(tipo);
						dao.eliminarSancion(tipo);


					} catch (Exception x) {
						// TODO Auto-generated catch block
						x.printStackTrace();
					}
					refreshRowTemplate(tipo);
				}else if(Messagebox.ON_CANCEL.equals(e.getName())){
					//Cancel is clicked
				}
			}
		}
				);

	}

	@Command
	@NotifyChange({"obtenerTipos", "footer"})
	public void confirm(@BindingParam("sancion") Sancion tipo) {
		changeEditableStatus(tipo);
		try {
			todosTipos.remove(tipo);
			dao.eliminarSancion(tipo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refreshRowTemplate(tipo);
	}

	public void refreshRowTemplate(Sancion tipo) {
		/*
		 * This code is special and notifies ZK that the bean's value
		 * has changed as it is used in the template mechanism.
		 * This stops the entire Grid's data from being refreshed
		 */
		BindUtils.postNotifyChange(null, null, tipo, "editingStatus");
	}
	// REGISTRA UNA SANCION DE UN MIEMBRO
	@Command
	@NotifyChange({"sancion","persona", "cedulabuscar"})
	public void registrarSancion() throws Exception {
		if (!CamposVacio()){
			//Persona personas = (Persona) arg.get("objeto");
			Date fecha = new Date();
			String estado="A";
			String estadoP="S";
			sancion.setEditingStatus(false);
			sancion.setEstadoSancion(estado); 
			sancion.setFechaSancion(fecha);
			sancion.setPersona(persona);
			persona.setEstadoPersona(estadoP);
			this.dao.agregarSancion(sancion); 
			daope.modificarPersona(persona);      // MODIFICO EL ESTADO DE PERSONA
			Messagebox.show("Se ha registrado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);

			this.sancion= new Sancion();
			this.persona= new Persona();
			this.cedulabuscar = "";
		}else
		{
			Messagebox.show("Debe llenar todos los Campos!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);	
		}

	}
	// VALIDA LOS CAMPOS VACIOS
	public boolean CamposVacio(){
		if(sancion.getDescSancion()!=null  && !sancion.getDescSancion().equalsIgnoreCase("")
				&&sancion.getTipoSancion()!=null ) 
			return false;
		return true;
	}

	//Metodo Boton Limpiar
	@Command
	@NotifyChange({"sancion","persona", "cedulabuscar"})
	public void LimpiarSancion(){

		this.sancion= new Sancion();
		this.persona= new Persona();
		this.cedulabuscar = "";
	}

 
	// BUSCA LA CEDULA DE UN MIEMBRO
	@Command
	@NotifyChange({"persona","cedulabuscar"})
	public void BuscarCedula() throws Exception{
		boolean mensaje=false;
		String estado="S";
		personas = daope.obtenerTodos();
		if(this.cedulabuscar==null){
			Messagebox.show("Ingrese una Cedula!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			mensaje = true;	
		}
		else{
			for (int i = 0; i < personas.size(); i++) {
				if(this.cedulabuscar.equals(personas.get(i).getCedPersona())  &&
						personas.get(i).getEstadoPersona().compareTo(estado)==0){
					Messagebox.show("El miembro ya tiene una Sanción!", 
							"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
					mensaje=true;

				} 	else if((this.cedulabuscar.equals(personas.get(i).getCedPersona())  &&
						personas.get(i).getTipoPersona().getIdTipoPersona()==1) || (this.cedulabuscar.equals(personas.get(i).getCedPersona())  &&
								personas.get(i).getTipoPersona().getIdTipoPersona()==2) )
				{ 
					this.persona=personas.get(i);
					mensaje = true;	
				}

			}
		}


		if(mensaje==false){
			Messagebox.show("La cedula no Existe!", 
					"Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);  
			this.cedulabuscar=null;
		}
	}










	// ABRE MODAL QUE PERMITE REACTIVAR MIEMBRO 
	@Command
	public void reactivar(@BindingParam("reactivarMiembro") Sancion sancion) {
		HashMap person = new HashMap();
		person.put("objSancion", sancion);
		Window window = (Window)Executions.createComponents(
				"content/modalReactivarMiembro.zul", null, person);
		window.doModal();
	}

	// REACTIVA UN  MIEMBRO
	@Command
	public void reactivarMiembro(@BindingParam("objeto") Sancion sancion2) throws Exception {
		System.out.println("entro 1");
		//Persona persona = (Persona) arg.get("objeto");

		String estado="I";
		String estadoP="A";


		sancion2.setEstadoSancion(estado);
		sancion2.setFechaReactivacion(fechaReactivacion.getValue());
		//	  		 sancion2.setHoraReactivacion(horaReactivacion.getValue());


		//			   sancion.setPersona(persona);
		persona.setEstadoPersona(estadoP);
		System.out.println("Fecha"+this.sancion1.getFechaReactivacion());
		daope.modificarPersona(persona);      // MODIFICO EL ESTADO DE PERSONA
		Messagebox.show("Se ha reactivado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);

		modalDialog.detach();
		pagina = "vista/reactivarMiembro.zul";

		//this.dao.modificarSancion(sancion);
		//contenido.getChildren().clear();


	}
	// REACTIVA UN MIEMBRO
	public void onClick$agregarReactivacion() {
		System.out.println("entro 1");
		try {
			Sancion sancion = (Sancion) arg.get("objSancion");
			sancion.setEstadoSancion("I");
			//	TipoPersona tipo = daot.obtenerTipoPersona(1);
			System.out.println(" " );
			Persona persona = sancion.getPersona();
			persona.setEstadoPersona("A");

			sancion.setFechaReactivacion(new Date());
			//a.setFechaAccion(new Date());
			//a.setPersonai(postulacion.getPersona());
			dao.modificarSancion(sancion);

			daope.modificarPersona(persona);
			Messagebox.show("Se ha reactivado correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);

			modalDialog.detach();
			pagina = "vista/reactivarMiembro.zul";
			contenido.getChildren().clear();
			contenedor = (Div) Executions.createComponents(pagina, null, null);
			contenido.appendChild(contenedor);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
