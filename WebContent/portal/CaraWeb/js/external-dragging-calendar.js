var Script = function () {
    /* initialize the calendar
     -----------------------------------------------------------------*/

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#calendar').fullCalendar({
        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        },
        contentHeight: 300,
        events: {
        url: 'http://127.0.0.1:8000/WebServices/eventos',
        type: 'POST',
        error: function() {
            //alert('no Existen Eventos Activos para Mostrar');
        }
        },
        eventRender: function(event, element) {
            element.attr('href', 'javascript:void(0);');
            element.click(function() {
                var monthNames = ["Enero", "Febrero", "Marzo",
                                  "Abril", "Mayo", "Junio", "Julio",
                                  "Agosto", "Septiembre", "Octubre",
                                  "Noviembre", "Diciembre"];
                var date = event.start;
                var day = date.getDate();
                var monthIndex = date.getMonth();
                var year = date.getFullYear();
                $('#eventTitle').html(event.title);
                $('#eventBody').html(event.description);
                $('#eventHora').html("<b>Hora:</b> "+event.hora);
                $('#eventTipo').html("<b>Tipo de Evento:</b> "+event.tipo);
                $('#eventFecha').html("<img src='img/logo1.jpg' align='right' width='40%'> <b>Fecha:</b> " + day + ' de ' + monthNames[monthIndex] + ' del ' + year);
                //$(this).css('background-color', 'red');
                $('#fullCalModal').modal();
            });
        }
    });
    $.ajax({
        method: "POST",
        url: "http://127.0.0.1:8000/WebServices/noticias",
        success: function( dato ) {
            var datos = JSON.parse(JSON.stringify(dato));
            if (datos.length>0) {
                console.log(datos);
                var n;
                var modal="";
                var out="<div class='active item'><img src='data:image/png;base64,"+datos[0].fields.imagennoticia+"' alt=''>";
                out += "<div class='carousel-caption'>";
                out += "<p>"+datos[0].fields.titulonoticia+" <a data-toggle='modal' href='#myModal"+datos[0].pk+"' style='color:#F57C00 !important;''> Leer mas </a></p>";
                out += "</div></div>";
                modal="<div class='modal fade' id='myModal"+datos[0].pk+"' role='dialog'><div class='modal-dialog'><!--Modal content--><div class='modal-content'><div class='modal-header' style='padding:35px 50px;'><button type='button' class='close' data-dismiss='modal'>&times;</button><h4><li class='fa fa-newspaper-o'></li>"+datos[0].fields.titulonoticia+"</h4></div><div class='modal-body' style='padding:40px 50px;'><img src='data:image/png;base64,"+datos[0].fields.imagennoticia+"' width='500px' height='300px' alt=''>"+datos[0].fields.descnoticia+"<br/><br/><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button></div></div></div></div></div>";
                for(n = 1; n < datos.length; n++) {
                    out += "<div class='item'><img src='data:image/png;base64,"+datos[n].fields.imagennoticia+"' alt=''>";
                    out += "<div class='carousel-caption'>";
                    out += "<p>"+datos[n].fields.titulonoticia+" <a data-toggle='modal' href='#myModal"+datos[n].pk+"' style='color:#F57C00 !important;''> Leer mas </a></p>";
                    out += "</div></div>";
                    modal+="<div class='modal fade' id='myModal"+datos[n].pk+"' role='dialog'><div class='modal-dialog'><!--Modal content--><div class='modal-content'><div class='modal-header' style='padding:35px 50px;'><button type='button' class='close' data-dismiss='modal'>&times;</button><h4><li class='fa fa-newspaper-o'></li>"+datos[n].fields.titulonoticia+"</h4></div><div class='modal-body' style='padding:40px 50px;'><img src='data:image/png;base64,"+datos[n].fields.imagennoticia+"' width='500px' height='300px' alt=''>"+datos[n].fields.descnoticia+"<br/><br/><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button></div></div></div></div></div>";
                }; 
                document.getElementById("caruselnoticias").innerHTML  = out;
                document.getElementById("modales").innerHTML  = modal;
            }}
        });
}();